﻿using System;

using UnityEngine;

using Vanilla.Core.Inputs;

namespace Vanilla.UI.Examples
{

	[Serializable]
	public class DialogueRoutineExample : DialogueRoutine<DialogueRoutineExample, VanillaDialogueExample> { }

	public class VanillaDialogueExample : VanillaDialogue<DialogueRoutineExample, VanillaDialogueExample>
	{

		public BoolInputAction addTestString;
		public BoolInputAction startDialogue;
		
		[TextArea]
		public string testInput =
			"What a great day,/ I could hang out here forever./// Well,/ maybe not forever,/ I'd get hungry eventually./././";


		public override void OnEnable()
		{
			addTestString.slot.onSlotChange.AddListener(HandleAddTestString);
			startDialogue.slot.onSlotChange.AddListener(HandleStartDialogue);

			base.OnEnable();
		}


		public override void OnDisable()
		{
			base.OnDisable();

			addTestString.slot.onSlotChange.RemoveListener(HandleAddTestString);
			startDialogue.slot.onSlotChange.RemoveListener(HandleStartDialogue);
		}


		private void HandleAddTestString
		(
			bool old,
			bool @new)
		{
			if (@new) routine.textBank.Add(testInput);
		}


		private void HandleStartDialogue
		(
			bool old,
			bool @new)
		{
			if (@new) routine.Begin();
		}

	}

}