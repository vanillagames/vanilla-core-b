﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Vanilla.Sequencing.Examples
{
    [Serializable]
    public class ExampleChapter : VanillaChapter<ExampleChapter, ExampleEpoch, ExampleSequence>
    {
        public Color defaultColor;
        
        public Image image;

        void Start()
        {
            defaultColor = image.color;
        }
        
        public override void EventTranspired(ExampleEpoch @event)
        {
            Log($"My event transpired! Changing color...");

            image.color = @event.past ? @event.color : defaultColor;
        }

        public override void Analyze(ExampleSequence sequence)
        {
            Log($"I've been analyzed! I start at [{timeline.startTime}] and go for [{timeline.duration}] seconds.");
        }

        public override void Started(BasicEpoch epoch)
        {
            Log("I've just begun!");
        }

        public override void Ended(BasicEpoch epoch)
        {
            Log("I've just ended!");
        }

        public override void Skipped(bool timePositive)
        {
            Warning("This chapter was skipped entirely!");
        }

        public override void Run()
        {
            image.fillAmount = timeline.localNormal;
        }
    }
}