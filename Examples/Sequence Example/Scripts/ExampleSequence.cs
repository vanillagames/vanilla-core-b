﻿using System;

using UnityEngine;

namespace Vanilla.Sequencing.Examples
{
    [Serializable]
    public class ExampleEpoch : ToggleableEpoch<ExampleEpoch>
    {
        [SerializeField]
        public Color color;

        public override void Reset() {}
    }
    
    [Serializable]
    public class ExampleSequence : VanillaSequence<ExampleChapter, ExampleEpoch, ExampleSequence> {}
}