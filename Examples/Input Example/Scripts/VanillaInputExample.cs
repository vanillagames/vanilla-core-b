﻿//using UnityEngine;
//
//using Vanilla.Math;
//
//namespace Vanilla.Core.Inputs.Example
//{
//
//	public class VanillaInputExample : VanillaBehaviour
//	{
//		[ReadOnly]
//		public ConeHead     coneHead;
//		
//		public Transform[] choiceEntries;
//
//		public readonly ExampleSelectable[,] choiceGrid = new ExampleSelectable[3, 3];
//
//		public ProtectedBoolInputAction selectInput;
//		
//		public ProtectedFloatInputAction xAxisInput;
//		public ProtectedFloatInputAction yAxisInput;
//
//		[ReadOnly]
//		public float x;
//		[ReadOnly]
//		public float y;
//		
//		private ProtectedIntSlot xCursorSlot = new ProtectedIntSlot();
//		private ProtectedIntSlot yCursorSlot = new ProtectedIntSlot();
//		
//		public const float xSensitivity = 0.01f;
//		
//		public const float ySensitivity = 0.01f;
//
//		public const float c_DefaultSize = 0.2f;
//		public const float c_GrownSize = 0.3f;
//
//		public Vector2Int cursorPosition;
//
//		public Material currentMaterial;
//		public Material defaultMaterial;
//		
//		public void Awake()
//		{
//			coneHead = GetComponentInChildren<ConeHead>();
//
//			if (xAxisInput  != null) xAxisInput.onValueChange.AddListener(HandleXInput);
//			if (yAxisInput  != null) yAxisInput.onValueChange.AddListener(HandleYInput);
//			if (selectInput != null) selectInput.onValueChange.AddListener(HandleSelectInput);
//			
//			var i = 0;
//
//			for (var yLoop = 0; yLoop < choiceGrid.GetLength(1); yLoop++)
//			{
//				for (var xLoop = 0; xLoop < choiceGrid.GetLength(0); xLoop++, i++)
//				{
//					choiceGrid[xLoop,
//					           yLoop] = choiceEntries[i].GetComponent<ExampleSelectable>();
//				}
//			}
//
//			ExampleSelectable.hoverSlot.onSlotChange.AddListener(HandleTargetChange);
//
//			ExampleSelectable.hoverSlot.current = choiceGrid[1,
//			                                            1];
//		}
//
//
//		public void HandleXInput
//		(
//			float value)
//		{
//			x += value * xSensitivity;
//
//			x.Clamp(min: -1.0f,
//			        max: 1.99f);
//
//			UpdateSelection();
//		}
//
//
//		public void HandleYInput
//		(
//			float value)
//		{
//			y += value * ySensitivity;
//
//			y.Clamp(min: -1.0f,
//			        max: 1.99f);
//
//			UpdateSelection();
//		}
//
//
//		public void HandleSelectInput
//		(
//			bool value)
//		{
//			if (SoftNullCheck(targetSlot.current)) return;
//			
//			targetSlot.current.localScale = Vector3.one *
//			                                         ( value ?
//				                                           0.3f :
//				                                           0.2f );
//		}
//
//
//		private void UpdateSelection()
//		{
//			ExampleSelectable.hoverSlot.current = choiceGrid[x.GetCeilingInt()
//			                                             .GetClamp(min: 0,
//			                                                       max: 2),
//			                                            y.GetCeilingInt()
//			                                             .GetClamp(min: 0,
//			                                                       max: 2)];
//		}
//
//
//		public void HandleTargetChange
//		(
//			ExampleSelectable @old,
//			ExampleSelectable @new)
//		{
//			if (!SilentNullCheck(@old))
//			{
//				SelectionExited(@old);
//			}
//
//			if (!SilentNullCheck(@new))
//			{
//				SelectionEntered(@new);
//			}
//		}
//		
//		private void SelectionExited(ExampleSelectable t)
//		{
//			Shrink(t);
//			
//			Unhighlight(t.GetComponent<MeshRenderer>());
//		}
//		
//		private void SelectionEntered
//		(
//			ExampleSelectable e)
//		{
//			AssignTargetScale(e.transform);
//			
//			AssignSelectionMaterial(e.GetComponent<MeshRenderer>());
//			
//			coneHead.target = e.transform;
//		}
//
//		private void Unhighlight
//		(
//			Renderer m)
//		{
//			m.sharedMaterial = defaultMaterial;
//		}
//
//		private void Highlight
//		(
//			Renderer m)
//		{
//			m.sharedMaterial = currentMaterial;
//		}
//
//
//		private void AssignSelectionMaterial
//		(
//			Renderer m)
//		{
//			m.sharedMaterial = selectInput.state ?
//				                   currentMaterial :
//				                   defaultMaterial;
//		}
//
//
//		private void Shrink
//		(
//			Transform t)
//		{
//			t.localScale = DefaultScale();
//		}
//
//
//		private void Grow
//		(
//			Transform t)
//		{
//			t.localScale = SelectedScale();
//		}
//
//
//		private void AssignTargetScale
//		(
//			Transform t)
//		{
//			t.localScale = selectInput.state ?
//				               SelectedScale() :
//				               DefaultScale();
//		}
//
//
//		private Vector3 DefaultScale()
//		{
//			return Vector3.one * c_DefaultSize;
//		}
//		
//		private Vector3 SelectedScale()
//		{
//			return Vector3.one * c_GrownSize;
//		}
//	}
//
//}