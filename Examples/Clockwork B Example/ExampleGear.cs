﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Clockwork.B.Examples
{
    [Serializable]
    public class ExampleGear : BaseGear
    {
        protected override void Handle_Enter()
        {
            Log("Enter forwards");
        }

        protected override void Handle_Exit()
        {
            Log("Exit forwards");
        }

        protected override void Run()
        {
            Log($"Running!\nLocalTime this frame [{localTime}]\nLocalNormal this frame [{localNormal}]");
        }
    }
}