﻿using System;
using System.Collections;
using System.Collections.Generic;
using RotaryHeart.Lib;
using UnityEngine;
using UnityEngine.UI;
using Vanilla.Clockwork;

namespace Vanilla.Clockwork.Examples
{
    [Serializable]
    public class ExampleGearList : ProtectedList<ExampleGear> {}
    
    [Serializable]
    public class ExampleGear : VanillaGear<ExampleGear, ExampleMotor, ExampleGearList>
    {
        [Header("Example Gear")]
        [ReadOnly] public Image image;
        
        public void Awake()
        {
            image = GetComponentInChildren<Image>();
        }
        
        protected override void Run(ExampleMotor motor, bool timePositive)
        {
            image.fillAmount = localNormal;
        }

        protected override void EnteredForwards(ExampleMotor motor)
        {
            Log("EnteredForwards");
        }

        protected override void ExitedForwards(ExampleMotor motor)
        {
            Log("ExitedForwards");
        }
        
        protected override void EnteredBackwards(ExampleMotor motor)
        {
            Log("EnteredBackwards");
        }

        protected override void ExitedBackwards(ExampleMotor motor)
        {
            Log("ExitedBackwards");
        }
    }
}