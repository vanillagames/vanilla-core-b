﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vanilla.Clockwork.Examples
{
    [Serializable]
    public class ExampleMotor : VanillaMotor<ExampleGear, ExampleMotor, ExampleGearList>
    {

    }
}