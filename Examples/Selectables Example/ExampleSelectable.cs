﻿namespace Vanilla.Selectables
{

	public class ExampleSelectable : BaseSelectable<ExampleSelectable,
													ExampleSelector>
	{

		public override void Hovered() { }

		public override void Dehovered() { }

		public override void Selected() { }

		public override void Deselected() { }

	}

}