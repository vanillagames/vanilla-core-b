﻿namespace Vanilla.Selectables
{

	public class ExampleSelector : BaseSelector<ExampleSelectable,
												ExampleSelector>
	{

		protected override void HandleHoverChange
		(
			ExampleSelectable old,
			ExampleSelectable @new)
		{
			
		}


		protected override void HandleSelectionChange
		(
			ExampleSelectable old,
			ExampleSelectable @new)
		{
			
		}

	}

}