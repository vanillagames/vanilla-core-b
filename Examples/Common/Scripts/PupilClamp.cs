﻿using UnityEngine;

using Vanilla.Math;

using Vanilla.Transforms;

public class PupilClamp : ActiveTargetBehaviour
{

	public float     clampSize;
	public Transform target;


	private void Update()
	{
		t.localPosition = t.InverseTransformDirection(t.GetDirectionTo(target)
		                                               .GetRadialClamp(clampSize))
		                   .XYToXY();
	}


	public override Transform FindTarget()
	{
		return GetComponentInParent<ConeHead>()
		   .target;
	}

}