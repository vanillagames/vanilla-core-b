﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Vanilla.Transforms;

public class ConeHead : LookAtAdvanced
{
    
    public override Transform FindTarget()
    {
        return Camera.main?.transform;
    }

}