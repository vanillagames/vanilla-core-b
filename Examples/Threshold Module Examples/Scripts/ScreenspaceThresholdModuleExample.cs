﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla;
using Vanilla.Math;

public class ScreenspaceThresholdModuleExample : ActiveBehaviour
{
	public Position2DThresholdModule positionModule;

	public Camera cam;
	
	public Gradient gradient;
	
	public List<Vector2> recordedPositions = new List<Vector2>(32);


	protected override void Awake()
	{
		base.Awake();
		
		positionModule.onThresholdBreach += newValue =>
		                                    {
			                                    recordedPositions.Cycle(newValue);
		                                    };
	}


	void Update()
	{
		var ss = cam.ScreenToViewportPoint(Input.mousePosition);
		
		positionModule.Compare(ss);
	}


	protected override void InSceneValidation()
	{
		base.InSceneValidation();
		
		cam = Camera.main;

		positionModule.Validate(this);
	}


	private void OnDrawGizmos()
	{
		for (var i = 0; i < recordedPositions.Count; i++)
		{
			var v = recordedPositions[i];

			Gizmos.color = gradient.Evaluate(time: Mathf.Lerp(a: 0.0f,
			                                                  b: 1.0f,
			                                                  t: i.GetNormal(recordedPositions.Count)));

			Gizmos.DrawSphere(center: v,
			                  radius: 0.1f);
		}
	}

}