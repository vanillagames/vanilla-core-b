﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla;
using Vanilla.Math;

public class Position3DThresholdModuleExample : ActiveBehaviour
{
	public Position3DThresholdModule positionModule;

	public Gradient gradient;
	
	public List<Vector3> recordedPositions = new List<Vector3>(32);


	protected override void Awake()
	{
		base.Awake();
		
		positionModule.onThresholdBreach += newValue =>
		                                    {
			                                    recordedPositions.Cycle(newValue);
		                                    };
	}


	void Update()
	{
		positionModule.Compare(t.position);
	}


	protected override void InSceneValidation()
	{
		base.InSceneValidation();
		
		positionModule.Validate(this);
	}


	private void OnDrawGizmos()
	{
		for (var i = 0; i < recordedPositions.Count; i++)
		{
			var v = recordedPositions[i];

			Gizmos.color = gradient.Evaluate(time: Mathf.Lerp(a: 0.0f,
			                                                  b: 1.0f,
			                                                  t: i.GetNormal(recordedPositions.Count)));

			Gizmos.DrawSphere(center: v,
			                  radius: 0.1f);
		}
	}

}