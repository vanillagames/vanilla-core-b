﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla;
using Vanilla.Math;

public class Direction2DThresholdModuleExample : ActiveBehaviour
{

	public DirectionVector2DThresholdModule rotationModule;

	public Gradient gradient;

//	public float degreesToRotate = 1.0f;

//	public Transform otherThing;
	
	public List<Vector2> recordedDirections = new List<Vector2>(32);


	protected override void Awake()
	{
		base.Awake();

		rotationModule.onThresholdBreach += newValue => { recordedDirections.Cycle(newValue); };
	}


	void Update()
	{

		var p     = t.position;
		var delta = Time.deltaTime;

//		var dir = t.position.GetNormalizedDirectionTo(Vector3.zero);
//
//		var angle = Mathf.Atan2(y: dir.y,
//		                        x: dir.x) * Mathf.Rad2Deg;
//
//		otherThing.rotation = Quaternion.Euler(x: 0.0f,
//		                                       y: angle,
//		                                       z: 0.0f);
		
//		t.forward = t.forward.XZToXY().GetRotated(degreesToRotate).XYToXZ(0.0f);

//		t.forward = t.forward.SquashZ();

//		Log(t.forward.ToString());
		
		t.forward = t.forward.SquashZ();

//		return;
		
		rotationModule.Compare(t.forward);

		for (var i = 0; i < recordedDirections.Count; i++)
		{
			var d = recordedDirections[i];

			Debug.DrawRay(start: p,
			              dir: d,
			              color: gradient.Evaluate(time: i.GetNormal(recordedDirections.Count)),
			              duration: delta);
		}
	}


	protected override void InSceneValidation()
	{
		base.InSceneValidation();

		rotationModule.Validate(this);
	}

}