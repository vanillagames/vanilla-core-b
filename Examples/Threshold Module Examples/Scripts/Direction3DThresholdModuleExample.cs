﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla;
using Vanilla.Math;

public class Direction3DThresholdModuleExample : ActiveBehaviour
{

	public DirectionVector3DThresholdModule rotationModule;

	public Gradient gradient;

	public List<Vector3> recordedDirections = new List<Vector3>(32);


	protected override void Awake()
	{
		base.Awake();

		rotationModule.onThresholdBreach += newValue => { recordedDirections.Cycle(newValue); };
	}


	void Update()
	{
		var p     = t.position;
		var delta = Time.deltaTime;

		rotationModule.Compare(t.forward);

		for (var i = 0; i < recordedDirections.Count; i++)
		{
			var d = recordedDirections[i];

			Debug.DrawRay(start: p,
			              dir: d,
			              color: gradient.Evaluate(time: i.GetNormal(recordedDirections.Count)),
			              duration: delta);
		}
	}


	protected override void InSceneValidation()
	{
		base.InSceneValidation();

		rotationModule.Validate(this);
	}

}