﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Vanilla;
using Vanilla.Basic;
using Vanilla.Math;

namespace Vanilla.Examples
{
    public class ExampleRayDetection : AutomaticRayDetection<ExampleRayDetectionArea, ExampleRayDetection>
    {
        public enum HouseAreaType
        {
            Unknown = 0,
            EntryHall = 1,
            DiningRoom = 2,
            Library = 3,
            MasterBedroom = 4,
            Bathroom = 5,
            Basement = 6,
            Attic = 7,
            Stairwell = 8
        }
        
        public enum CityAreaType
        {
            Unknown = 0,
            Highway = 1,
            CBD = 2,
            Hospital = 3,
            FireStation = 4,
            Stadium = 5,
            Airport = 6,
            University = 7,
            Mall = 8
        }
        
        [ReadOnly]
        public HouseAreaType currentHouseAreaCategory = HouseAreaType.Unknown;
        [ReadOnly]
        public CityAreaType currentCityAreaCategory = CityAreaType.Unknown;

        public Material selectedMaterial;

        public override void Update()
        {
            base.Update();
        }

        protected override void HandleDetectedObjectChange(GameObject old, GameObject @new)
        {
            
        }

        protected override void HandleDetectionChange(ExampleRayDetectionArea old, ExampleRayDetectionArea @new)
        {
            if (componentDetected)
            {
                currentHouseAreaCategory = (HouseAreaType) @new.areaID;
                currentCityAreaCategory = (CityAreaType) @new.areaID;
            }
            else
            {
                currentHouseAreaCategory = HouseAreaType.Unknown;
                currentCityAreaCategory = CityAreaType.Unknown;
            }
        }
    }
}