﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using Vanilla;
using Vanilla.Basic;

namespace Vanilla.Examples
{

    public class ExampleRayDetectionArea : VanillaBehaviour, IDetectable<ExampleRayDetectionArea, ExampleRayDetection>
    {

        public byte areaID;

        public Material defaultMaterial;

        public UnityEvent onAreaEnter = new UnityEvent();
        public UnityEvent onAreaExit  = new UnityEvent();

        [SerializeField, ReadOnly]
        private MeshRenderer _renderer;

        public MeshRenderer renderer
        {
            get
            {
                if (_renderer != null) return _renderer;

                return _renderer = GetComponent<MeshRenderer>();
            }
        }


        void Awake()
        {
            defaultMaterial = renderer.material;

            gameObject.layer = LayerMask.NameToLayer("Water");
        }


        [SerializeField, ReadOnly]
        private ExampleRayDetection _detectionSystem;

        public ExampleRayDetection detectionSystem
        {
            get => _detectionSystem;
            set => _detectionSystem = value;
        }


        public void DetectionBegun(ExampleRayDetectionArea replaced)
        {
            Log($"I've been detected by [{detectionSystem.gameObject.name}]");

            renderer.material = detectionSystem.selectedMaterial;

            onAreaEnter.Invoke();
        }


        public void DetectionEnded(ExampleRayDetectionArea replace)
        {
            Log($"[{detectionSystem.gameObject.name}] can't see me anymore.");

            renderer.material = defaultMaterial;

            onAreaExit.Invoke();

            detectionSystem = null;
        }

    }

}