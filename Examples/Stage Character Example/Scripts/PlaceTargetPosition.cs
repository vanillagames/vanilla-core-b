﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceTargetPosition : MonoBehaviour
{
    public Transform targetPosition;

    private Camera cam;

    void Awake()
    {
        cam = Camera.main;
    }
    
    void Update()
    {
        if (!targetPosition || !Input.GetMouseButtonDown(0)) return;

        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out var hit, Mathf.Infinity))
        {
            targetPosition.position = hit.point;
        }
    }
}
