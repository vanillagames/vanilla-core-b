﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Vanilla.UI;

namespace Vanilla.UI.Examples
{
    public class ExampleDraggableUI : DraggableUI
    {
        public Text pointerIDText;

        private void OnEnable()
        {
            pointerIDText.text = touchIndex.ToString();
        }

        private void OnDisable()
        {
            pointerIDText.text = touchIndex.ToString();
        }
    }
}