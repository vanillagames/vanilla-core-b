﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Math.Examples
{
    [Serializable]
    public class NormalTransformTrack : VanillaClass
    {
        public Transform target;

        public void NormalUpdate(float value)
        {
            var p = target.localPosition;

            p.x = value;

            target.localPosition = p;
        }
        
        public override void Reset() {}
    }
    
    public class VanillaNormalExamples : VanillaBehaviour
    {
        [Range(0, 1)] public float exampleInput;

        [Space(10)] public MasterNormal masterNormal;
        [Space(10)] public BasicNormal basicNormal = new BasicNormal();
        [Space(10)] public CustomNormal customNormal = new CustomNormal();
        [Space(10)] public EasedNormal easedNormal = new EasedNormal();
        [Space(10)] public CustomEasedNormal customEasedNormal = new CustomEasedNormal();
        [Space(10)] public CurveNormal curveNormal = new CurveNormal();
        [Space(10)] public CustomCurveNormal customCurveNormal = new CustomCurveNormal();

        public NormalTransformTrack[] transformTracks = new NormalTransformTrack[7];
        
        public void Awake()
        {
            masterNormal.Initialize();
            
            masterNormal.slaveNormals.Add(basicNormal);
            masterNormal.slaveNormals.Add(customNormal);
            masterNormal.slaveNormals.Add(easedNormal);
            masterNormal.slaveNormals.Add(customEasedNormal);
            masterNormal.slaveNormals.Add(curveNormal);
            masterNormal.slaveNormals.Add(customCurveNormal);
            
            masterNormal.onNewValue.AddListener(transformTracks[0].NormalUpdate);
            basicNormal.onNewValue.AddListener(transformTracks[1].NormalUpdate);
            customNormal.onNewValue.AddListener(transformTracks[2].NormalUpdate);
            easedNormal.onNewValue.AddListener(transformTracks[3].NormalUpdate);
            customEasedNormal.onNewValue.AddListener(transformTracks[4].NormalUpdate);
            curveNormal.onNewValue.AddListener(transformTracks[5].NormalUpdate);
            customCurveNormal.onNewValue.AddListener(transformTracks[6].NormalUpdate);
        }

        protected override void InSceneValidation()
        {
            base.InSceneValidation();

            masterNormal.normal = exampleInput;
        }
    }
}