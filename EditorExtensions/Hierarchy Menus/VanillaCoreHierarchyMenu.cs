﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using UnityEditor;

using Vanilla.Core;

namespace Vanilla.EditorExtensions
{
    public class VanillaCoreHierarchyMenu : VanillaHierarchyMenuBase
    {
        // ----------------------------------------------------------------------------------------------- Constants //

        #region Constants

        private const string ToolsMenuPath = Constants.CoreHierarchyMenuPath + ToolsSubMenu;

        private const string PrimitivesMenuPath =
            Constants.CoreHierarchyMenuPath + PrimitivesSubMenu;

        private const string LettersMenuPath =
            Constants.CoreHierarchyMenuPath + LettersSubMenu + Letter;

        private const string ToolsPrefabPath = Constants.PrefabsPath + ToolsSubMenu;
        private const string PrimitivesPrefabPath = Constants.PrefabsPath + PrimitivesSubMenu;
        private const string LettersPrefabPath = Constants.PrefabsPath + LettersSubMenu;

        private const string ToolsSubMenu = "Tools/";
        private const string PrimitivesSubMenu = "Primitives/";
        private const string LettersSubMenu = "Letters/";

        private const string Boxlet = "Boxlet";
        private const string Capsule = "Capsule";
        private const string Circle = "Circle";
        private const string Cone = "Cone";
        private const string Dome = "Dome";
        private const string Donut = "Donut";
        private const string Knot = "Knot";
        private const string Pyramid = "Pyramid";
        private const string Ramp = "Ramp";
        private const string Sphere = "Sphere";
        private const string Tube = "Tube";

        private const string Letter = "Letter ";

        private const string LowPath = "/Low";
        private const string MedPath = "/Med";
        private const string HighPath = "/High";

        private const string LowQuality = " [Low]";
        private const string MedQuality = " [Med]";
        private const string HighQuality = " [High]";

        private const string Inverted = " [Inverted]";

        private const int Priority = 10;

        #endregion

        // --------------------------------------------------------------------------------------------------- Tools //

        #region Tools

        [MenuItem(ToolsMenuPath + "Editor Cam", false, Priority)]
        public static void CreateEditorCamera(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: ToolsPrefabPath,
                prefabName: "Editor Cam");
        }
        
        [MenuItem(ToolsMenuPath + "Transform Compass", false, Priority)]
        public static void CreateTransformCompass(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: ToolsPrefabPath,
                prefabName: "Transform Compass");
        }

        #endregion

        // ---------------------------------------------------------------------------------------------- Primitives //

        #region Primitives

        // Boxlet //

        [MenuItem(PrimitivesMenuPath + Boxlet + LowPath, false, Priority)]
        public static void CreateBoxletLow(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Boxlet + LowQuality);
        }

        [MenuItem(PrimitivesMenuPath + Boxlet + MedPath, false, Priority)]
        public static void CreateBoxletMed(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Boxlet + MedQuality);
        }

        [MenuItem(PrimitivesMenuPath + Boxlet + HighPath, false, Priority)]
        public static void CreateBoxletHigh(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Boxlet + HighQuality);
        }

        // Capsule //

        [MenuItem(PrimitivesMenuPath + Capsule + LowPath, false, Priority)]
        public static void CreateCapsuleLow(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Capsule + LowQuality);
        }

        [MenuItem(PrimitivesMenuPath + Capsule + MedPath, false, Priority)]
        public static void CreateCapsuleMed(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Capsule + MedQuality);
        }

        [MenuItem(PrimitivesMenuPath + Capsule + HighPath, false, Priority)]
        public static void CreateCapsuleHigh(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Capsule + HighQuality);
        }

        // Circle //

        [MenuItem(PrimitivesMenuPath + Circle + LowPath, false, Priority)]
        public static void CreateCircleLow(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Circle + LowQuality);
        }

        [MenuItem(PrimitivesMenuPath + Circle + MedPath, false, Priority)]
        public static void CreateCircleMed(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Circle + MedQuality);
        }

        [MenuItem(PrimitivesMenuPath + Circle + HighPath, false, Priority)]
        public static void CreateCircleHigh(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Circle + HighQuality);
        }

        // Cone //

        [MenuItem(PrimitivesMenuPath + Cone + LowPath, false, Priority)]
        public static void CreateConeLow(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Cone + LowQuality);
        }

        [MenuItem(PrimitivesMenuPath + Cone + MedPath, false, Priority)]
        public static void CreateConeMed(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Cone + MedQuality);
        }

        [MenuItem(PrimitivesMenuPath + Cone + HighPath, false, Priority)]
        public static void CreateConeHigh(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Cone + HighQuality);
        }

        // Dome //

        [MenuItem(PrimitivesMenuPath + Dome + LowPath, false, Priority)]
        public static void CreateDomeLow(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Donut + LowQuality);
        }

        [MenuItem(PrimitivesMenuPath + Dome + MedPath, false, Priority)]
        public static void CreateDomeMed(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Donut + MedQuality);
        }

        [MenuItem(PrimitivesMenuPath + Dome + HighPath, false, Priority)]
        public static void CreateDomeHigh(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Donut + HighQuality);
        }

        // Dome - Inverted //

        [MenuItem(PrimitivesMenuPath + Dome + LowPath + Inverted, false, Priority)]
        public static void CreateDomeLowInverted(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Donut + LowQuality + Inverted);
        }

        [MenuItem(PrimitivesMenuPath + Dome + MedPath + Inverted, false, Priority)]
        public static void CreateDomeMedInverted(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Donut + MedQuality + Inverted);
        }

        [MenuItem(PrimitivesMenuPath + Dome + HighPath + Inverted, false, Priority)]
        public static void CreateDomeHighInverted(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Donut + HighQuality + Inverted);
        }

        // Donut //

        [MenuItem(PrimitivesMenuPath + Donut + LowPath, false, Priority)]
        public static void CreateDonutLow(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Donut + LowQuality);
        }

        [MenuItem(PrimitivesMenuPath + Donut + MedPath, false, Priority)]
        public static void CreateDonutMed(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Donut + MedQuality);
        }

        [MenuItem(PrimitivesMenuPath + Donut + HighPath, false, Priority)]
        public static void CreateDonutHigh(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Donut + HighQuality);
        }

        // Knot //

        [MenuItem(PrimitivesMenuPath + Knot + HighPath, false, Priority)]
        public static void CreateKnotHigh(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Knot + HighQuality);
        }

        // Pyramid //

        [MenuItem(PrimitivesMenuPath + Pyramid + LowPath, false, Priority)]
        public static void CreatePyramidLow(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Pyramid + LowQuality);
        }

        // Ramp - Inverted //

        [MenuItem(PrimitivesMenuPath + Ramp + LowPath + Inverted, false, Priority)]
        public static void CreateRampLowInverted(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Ramp + LowQuality + Inverted);
        }

        [MenuItem(PrimitivesMenuPath + Ramp + MedPath + Inverted, false, Priority)]
        public static void CreateRampMedInverted(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Ramp + MedQuality + Inverted);
        }

        [MenuItem(PrimitivesMenuPath + Ramp + HighPath + Inverted, false, Priority)]
        public static void CreateRampHighInverted(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Ramp + HighQuality + Inverted);
        }

        // Sphere //

        [MenuItem(PrimitivesMenuPath + Sphere + LowPath, false, Priority)]
        public static void CreateSphereLow(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Sphere + LowQuality);
        }

        [MenuItem(PrimitivesMenuPath + Sphere + MedPath, false, Priority)]
        public static void CreateSphereMed(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Sphere + MedQuality);
        }

        [MenuItem(PrimitivesMenuPath + Sphere + HighPath, false, Priority)]
        public static void CreateSphereHigh(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Sphere + HighQuality);
        }

        // Sphere - Inverted //

        [MenuItem(PrimitivesMenuPath + Sphere + LowPath + Inverted, false, Priority)]
        public static void CreateSphereLowInverted(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Sphere + LowQuality + Inverted);
        }

        [MenuItem(PrimitivesMenuPath + Sphere + MedPath + Inverted, false, Priority)]
        public static void CreateSphereMedInverted(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Sphere + MedQuality + Inverted);
        }

        [MenuItem(PrimitivesMenuPath + Sphere + HighPath + Inverted, false, Priority)]
        public static void CreateSphereHighInverted(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Sphere + HighQuality + Inverted);
        }

        // Tube //

        [MenuItem(PrimitivesMenuPath + Tube + LowPath, false, Priority)]
        public static void CreateTubeLow(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Tube + LowQuality);
        }

        [MenuItem(PrimitivesMenuPath + Tube + MedPath, false, Priority)]
        public static void CreateTubeMed(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Tube + MedQuality);
        }

        [MenuItem(PrimitivesMenuPath + Tube + HighPath, false, Priority)]
        public static void CreateTubeHigh(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: PrimitivesPrefabPath,
                prefabName: Tube + HighQuality);
        }

        #endregion

        // ------------------------------------------------------------------------------------------------- Letters //

        #region Letters

        [MenuItem(LettersMenuPath + "V", false, Priority)]
        public static void CreateLetterV(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: LettersPrefabPath,
                prefabName: Letter + "V");
        }

        [MenuItem(LettersMenuPath + "A", false, Priority)]
        public static void CreateLetterA(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: LettersPrefabPath,
                prefabName: Letter + "A");
        }

        [MenuItem(LettersMenuPath + "N", false, Priority)]
        public static void CreateLetterN(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: LettersPrefabPath,
                prefabName: Letter + "N");
        }

        [MenuItem(LettersMenuPath + "I", false, Priority)]
        public static void CreateLetterI(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: LettersPrefabPath,
                prefabName: Letter + "I");
        }

        [MenuItem(LettersMenuPath + "L", false, Priority)]
        public static void CreateLetterL(MenuCommand menuCommand)
        {
            CreateInstance(
                menuCommand: menuCommand,
                pathToPrefab: LettersPrefabPath,
                prefabName: Letter + "L");
        }

        #endregion
    }
}

#endif