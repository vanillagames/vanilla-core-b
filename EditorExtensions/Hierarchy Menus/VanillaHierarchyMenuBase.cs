// Creation Date                                                                              [ 12/02/2020 5:42:41 PM ]
// Project                                                                                             [ Vanilla Core ]
// Developer                                                                                                [ Vanilla ]

#if UNITY_EDITOR

using UnityEditor;

using UnityEngine;

namespace Vanilla.EditorExtensions
{
	public abstract class VanillaHierarchyMenuBase
	{
		// --------------------------------------------------------------------------------------------- Instantiate //

		/// <summary>
		///     This function instantiates a new instance of a prefab based on the given input paths.
		/// </summary>
		/// 
		/// <param name="menuCommand">
		/// 	The context of the menu option chosen.
		/// </param>
		/// 
		/// <param name="pathToPrefab">
		/// 	Which directory path, including Assets/, leads to the desired prefab?
		/// </param>
		/// 
		/// <param name="prefabName">
		/// 	What name is the prefab file? No file extension is required.
		/// </param>
		public static void CreateInstance
		(
			MenuCommand menuCommand,
			string      pathToPrefab,
			string      prefabName)
		{
			var asset = AssetDatabase.LoadAssetAtPath(assetPath: $"{pathToPrefab}{prefabName}.prefab",
			                                          type: typeof(GameObject));

			if (asset == null)
			{
				Vanilla.Warning($"We were unable to find this prefab. It was expected to be found at the following path:\n\n{pathToPrefab}{prefabName}.prefab");
				
				return;
			}
			
			var instance = PrefabUtility.InstantiatePrefab(assetComponentOrGameObject: asset) as GameObject;

			if (instance == null)
			{
				Vanilla.Warning($"The prefab [{prefabName}] could not be instantiated. Is the prefab file corrupted?");

				return;
			}

			GameObjectUtility.SetParentAndAlign(child: instance,
			                                    parent: menuCommand.context as GameObject);

			Undo.RegisterCreatedObjectUndo(objectToUndo: instance,
			                               name: "Create " + instance.name);

			Selection.activeGameObject = instance;
		}
	}
}

#endif