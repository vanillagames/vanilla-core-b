﻿using System;
using System.Globalization;
using System.IO;

using UnityEngine;

using Vanilla.Core;
#if UNITY_EDITOR
using System.Collections.Generic;
using System.Xml.Schema;

using UnityEditor;

namespace Vanilla.EditorExtensions
{

	public class ScriptTemplates : UnityEditor.AssetModificationProcessor
	{

		private const string c_VanillaBehaviourTemplate = "VanillaBehaviour.cs.txt";

		private const string c_DeveloperNamePlaceholder = "#DEVELOPERNAME#";

		private const string c_ProjectNamePlaceholder = "#PROJECTNAME#";

		private const string c_CreationDatePlaceholder = "#CREATIONDATE#";

		private static char[] _splitters = {'/', '\\', '.'};

		private static List<string> _wordsToDelete = new List<string> {"Extensions", "Scripts", "Editor"};


		[MenuItem(itemName: Constants.AssetsSubPath          +
		                    Constants.CreateSubPath          +
		                    Constants.VanillaSubPath         +
		                    Constants.CoreSubPath            +
		                    Constants.ScriptTemplatesSubPath +
		                    "New VanillaBehaviour",
			isValidateFunction: false,
			priority: 0)]
		public static void CreateScriptFromTemplate()
		{
			ProjectWindowUtil.CreateScriptAssetFromTemplateFile(templatePath:
			                                                    Constants.ScriptTemplatesPath +
			                                                    c_VanillaBehaviourTemplate,
			                                                    defaultNewFileName: "NewVanillaBehaviour.cs");
		}


		public static void OnWillCreateAsset
		(
			string path)
		{
			// --------------------------------------------------------------------------- Reading from the new file //

			path = path.Replace(oldValue: ".meta",
			                    newValue: string.Empty);

			var index = path.LastIndexOf(value: ".",
			                             comparisonType: StringComparison.Ordinal);

			if (index == -1)

				//                Debug.Log("You created an asset but no file extension was found on it. I guess it was a folder!");
				return;

			var file = path.Substring(index);

			if (file != ".cs") return;

			index = Application.dataPath.LastIndexOf(value: "Assets",
			                                         comparisonType: StringComparison.Ordinal);

			path = Application.dataPath.Substring(startIndex: 0,
			                                      length: index)
			     + path;

			file = File.ReadAllText(path);

			// ----------------------------------------------------------------------------- Replacing template tags //

			// Replacing #DEVELOPERNAME#

			var replacementIndex = file.IndexOf(value: c_DeveloperNamePlaceholder,
			                                    comparisonType: StringComparison.Ordinal);

			file = file.Replace(oldValue: c_DeveloperNamePlaceholder,
			                    newValue: PlayerSettings.companyName);


			file = file.Remove(startIndex: replacementIndex - 24,
			                   count: PlayerSettings.companyName.Length);

			// Replacing #PROJECTNAME#

			replacementIndex = file.IndexOf(value: c_ProjectNamePlaceholder,
			                                comparisonType: StringComparison.Ordinal);

			file = file.Replace(oldValue: c_ProjectNamePlaceholder,
			                    newValue: PlayerSettings.productName);

			file = file.Remove(startIndex: replacementIndex - 24,
			                   count: PlayerSettings.productName.Length);

			// Replacing #CREATIONDATE#

			var dateString = DateTime.Now.ToString(CultureInfo.CurrentCulture);

			replacementIndex = file.IndexOf(value: c_CreationDatePlaceholder,
			                                comparisonType: StringComparison.Ordinal);

			file = file.Replace(oldValue: c_CreationDatePlaceholder,
			                    newValue: dateString);

			file = file.Remove(startIndex: replacementIndex - 24,
			                   count: dateString.Length);

			
			
			// -------------------------------------------------------------------------------- Writing back to file //

			File.WriteAllText(path: path,
			                  contents: file);

			AssetDatabase.Refresh();
		}

	}

}
#endif