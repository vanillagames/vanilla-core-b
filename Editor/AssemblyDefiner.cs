﻿#if UNITY_EDITOR

#define DEFINE_MODULES
#define DEFINE_ASSEMBLIES
#define DEFINE_PLUGINS

#define LOG_DEFINE_REPORT

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

using UnityEditor;
using UnityEditor.Compilation;

using UnityEngine;

namespace Vanilla
{

	static class AssemblyDefiner
	{

		/// <summary>
		/// 	Automatically adds and removes defines based on certain conditions.
		/// </summary>
		[InitializeOnLoadMethod]
		public static void AddDefineSymbols()
		{
			var newDefines = new SortedSet<string>();

			// ------------------------------------------------------------ Get a sorted set of the outgoing defines // 

			var oldDefines = new SortedSet<string>(PlayerSettings
			                                      .GetScriptingDefineSymbolsForGroup(targetGroup:
			                                                                         EditorUserBuildSettings
				                                                                        .selectedBuildTargetGroup)
			                                      .Split(';'));

			// Without this, oldDefines seems to contain a mysterious whitespace entry by default.
			// Let's ensure it's removal.

			oldDefines.RemoveWhere(OldDefineIsNullOrWhitespace);

			bool OldDefineIsNullOrWhitespace
			(
				string s)
			{
				return string.IsNullOrWhiteSpace(s);
			}

			// -------------------------------------------------- Add defines based on Modules folder subdirectories // 

			#if DEFINE_MODULES

			// Please note that the anticipated format for this folder is Assets/+Modules/<author-name>/<module-name>

			foreach (var moduleAuthorPath in Directory.GetDirectories("Assets/+Modules/"))
			{
//				Debug.Log($"Found a module author directory [{moduleAuthorPath}]");

				foreach (var moduleSubdirectoryPath in Directory.GetDirectories(moduleAuthorPath))
				{
//					Debug.Log($"Found a module subdirectory [{moduleSubdirectoryPath}]");

					var lastSlashIndex = moduleAuthorPath.LastIndexOf(value: "/",
					                                                  comparisonType: StringComparison.Ordinal) + 1;

					var name = moduleSubdirectoryPath.Substring(startIndex: lastSlashIndex,
					                                            length: moduleSubdirectoryPath.Length - lastSlashIndex)
					                                 .Replace(oldChar: Path.DirectorySeparatorChar,
					                                          newChar: '_');

//					Debug.Log($"Cleaned up module define name [{name}]");

					newDefines.Add(name);
				}
			}


			#endif

			// ------------------------------------------------------------- Add defines based on current assemblies // 

			#if DEFINE_ASSEMBLIES

			var assemblies =
				CompilationPipeline.GetAssemblies(assembliesType: AssembliesType.PlayerWithoutTestAssemblies);

			foreach (var a in assemblies)
			{
				newDefines.Add(a.name.Replace(oldValue: ".",
				                              newValue: "_"));
			}

			#endif

			// ------------------------------------------------------------ Add defines based on current DLL plugins // 

			#if DEFINE_PLUGINS

			foreach (var plugin in PluginImporter.GetImporters(BuildTarget.Android))
			{
				var path = plugin.assetPath;

				var lastSlashIndex = path.LastIndexOf(value: "/",
				                                      comparisonType: StringComparison.Ordinal) + 1;

				var name = path.Substring(startIndex: lastSlashIndex,
				                          length: path.Length - lastSlashIndex - 4)
				               .Replace(oldChar: '.',
				                        newChar: '_');

				var textInfo = new CultureInfo("en-AU").TextInfo;

				name = textInfo.ToTitleCase(name);

				newDefines.Add(name);
			}

			#endif

			// ---------------------------------------------------------- Log added / removed defines in the console // 

			#if LOG_DEFINE_REPORT

			var removedDefines = 0;
			var addedDefines   = 0;

			var removedDefinePrint = string.Empty;
			var addedDefinePrint   = string.Empty;

			foreach (var o in oldDefines.Where(o => !newDefines.Contains(o)))
			{
//				Debug.Log($"This define has been removed? [{o}]");

				removedDefines++;

				removedDefinePrint += $"{o}\n";
			}

			foreach (var n in newDefines.Where(n => !oldDefines.Contains(n)))
			{
//				Debug.Log($"This define has been added? [{n}]");

				addedDefines++;

				addedDefinePrint += $"{n}\n";
			}

			if (addedDefines > 0 || removedDefines > 0)
			{
				var print =
					"AssemblyDefiner has detected the following automatic changes to the Player Settings define symbols:\n\n";

				if (removedDefines > 0)
				{
					print += $"{removedDefines} Removed:\n{removedDefinePrint}\n";
				}

				if (addedDefines > 0)
				{
					print += $"{addedDefines} Added:\n{addedDefinePrint}";
				}

				Debug.Log(print);
			}

			#endif

			// ---------------------------------------------------------- Set newDefines list back to PlayerSettings // 

			var output = string.Join(separator: ";",
			                         values: newDefines);

			PlayerSettings.SetScriptingDefineSymbolsForGroup(targetGroup:
			                                                 EditorUserBuildSettings.selectedBuildTargetGroup,
			                                                 defines: output);
		}

	}

}
#endif