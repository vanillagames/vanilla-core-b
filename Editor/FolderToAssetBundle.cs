﻿using System.Collections;
using System.Collections.Generic;

using System.IO;
using System.Linq;

using UnityEditor;

using UnityEngine;
using UnityEngine.Networking;

public class FolderToAssetBundle
{

    private const string LocalAssetBundleOutputPath = "Assets/+AssetBundles";

    private static readonly string ProjectPath = Application.dataPath;
    
    private static readonly char Slash = Path.DirectorySeparatorChar;

    /// <summary>
    ///     Flips any instances of the wrong slash in a path (i.e. \ instead of /) to the correct slash for this platform.
    /// </summary>
    private static string CorrectDirectorySeperators
        (string directory) =>
            directory.Replace(oldChar: Path.AltDirectorySeparatorChar,
                              newChar: Path.DirectorySeparatorChar);
    
    /// <summary>
    ///     Extracts the end folder name from a path
    /// </summary>
    private static string ExtractLastPartOfPath
        (string directory)
    {
        var lastSlashIndex = directory.LastIndexOf(Path.DirectorySeparatorChar) + 1;

        return directory.Substring(startIndex: lastSlashIndex,
                                   length: directory.Length - lastSlashIndex);
    }

    /// <summary>
    ///     Turns a full path (i.e. D:/Projects/etc) into a project-relative path (i.e. one that starts from the Assets folder)
    /// </summary>
    private static string LocalizeFullPath
        (string directory) =>
            $"Assets{Slash}{directory.Remove(startIndex: 0, count: ProjectPath.Length + 1)}";

    /// <summary>
    ///     Turns a project-relative path (i.e. one that starts from the Assets folder) into a full path (i.e. D:/Projects/etc)
    /// </summary>
    private static string GlobalizeLocalPath
        (string directory) =>
            $"{ProjectPath.Substring(startIndex: 0, length: ProjectPath.Length - 6)}{directory}";

    // ------------------------------------------------------------------------------------------------ Context Menu //

    [MenuItem(itemName: "Assets/Export Folder As AssetBundle",
        isValidateFunction: true)]
    private static bool NewMenuOptionValidation()
    {
        // This Context Menu option will be greyed out if this method returns false
        return Directory.Exists(AssetDatabase.GetAssetPath(Selection.activeObject));
    }


    [MenuItem(itemName: "Assets/Export Folder As AssetBundle")]
    private static void ExportFolderAsAssetBundle()
    {
        var path = GlobalizeLocalPath(AssetDatabase.GetAssetPath(Selection.activeObject));

        path = CorrectDirectorySeperators(path);

        Debug.Log($"Full Path [{path}]");

        Debug.Log($"Local Path [{LocalizeFullPath(path)}]");

        Debug.Log($"Bundle Name {ExtractLastPartOfPath(path)}");

        RecursiveAssetBundlerDrill(startingDirectory: path,
                                   bundleName: ExtractLastPartOfPath(path));

        AssetDatabase.RemoveUnusedAssetBundleNames();

        if (!Directory.Exists(LocalAssetBundleOutputPath))
        {
            Directory.CreateDirectory(LocalAssetBundleOutputPath);
        }

        var outputPath = GlobalizeLocalPath($"Assets{Slash}+AssetBundles");

        outputPath = CorrectDirectorySeperators(outputPath);

//        BuildPipeline.BuildAssetBundles(outputPath: outputPath,
//                                        assetBundleOptions: BuildAssetBundleOptions.ChunkBasedCompression,
//                                        targetPlatform: EditorUserBuildSettings.activeBuildTarget);

        var bundleName = ExtractLastPartOfPath(path);

//        BuildPipeline.P
        
        BuildPipeline.BuildAssetBundles(outputPath: outputPath,
                                        builds: new[]
                                                {
                                                    new AssetBundleBuild
                                                    {
                                                        assetBundleName = bundleName,
                                                        assetNames =
                                                            AssetDatabase.GetAssetPathsFromAssetBundle(
                                                                assetBundleName: bundleName)
                                                    }
                                                },
                                        assetBundleOptions: BuildAssetBundleOptions.ChunkBasedCompression,
                                        targetPlatform: EditorUserBuildSettings.activeBuildTarget);
        
        AssetDatabase.Refresh();
    }


//
//    [MenuItem(itemName: "Assets/Export Folder As AssetBundle")]
//    private static void ExportFolderAsAssetBundleNew()
//    {
//        var outputPath = CorrectDirectorySeperators($"{Application.dataPath}{Slash}+AssetBundles");
//
//        if (!Directory.Exists(outputPath))
//        {
//            Directory.CreateDirectory(outputPath);
//        }
//
//        // Get all selected *assets*
////        var assets = Selection.objects.Where(o => !string.IsNullOrEmpty(AssetDatabase.GetAssetPath(o))).ToArray();
//
////        var assets = GetAllAssetsFromDirectory(AssetDatabase.GetAssetPath(Selection.activeObject)).ToArray();
//
////        var foldersToSearch = Selection.objects.Select(AssetDatabase.GetAssetPath).ToArray();
//
////        foldersToSearch[0] = AssetDatabase.GetAssetPath(Selection.objects);
//
//        var bundleName = 
//            ExtractLastPartOfPath(directory: CorrectDirectorySeperators
//                                    (directory: AssetDatabase.GetAssetPath
//                                         (assetObject: Selection.activeObject)));
//
////        Debug.Log($"BundleName [{bundleName}]");
//        
//        Object[] selectedObjects = { Selection.activeObject };
//
//        var assets = AssetDatabase.FindAssets(filter: string.Empty,
//                                              searchInFolders: selectedObjects.Select(AssetDatabase.GetAssetPath)
//                                                                              .ToArray())
//                                  .Select(g => AssetDatabase.LoadAssetAtPath<Object>(AssetDatabase.GUIDToAssetPath(g)))
//                                  .ToList();
//
//        for (var i = assets.Count - 1; i >= 0; i--)
//        {
//            var a = assets[i];
//
//            if (Directory.Exists(AssetDatabase.GetAssetPath(a)))
//            {
////                Debug.Log($"Removed directory [{assets[i]}]");
//                
//                assets.RemoveAt(i);
//                
//                continue;
//            }
//
////            Debug.Log($"Keeping asset [{a.name}]");
//        }
//
//        if (assets.Count == 0)
//        {
//            Debug.LogWarning("No valid assets selected.");
//
//            return;
//        }
//
////        return;
//        
//        var assetBundleBuilds = new List<AssetBundleBuild>();
//        var processedBundles  = new HashSet<string>();
//
//        // Get asset bundle names from selection
//        foreach (var o in assets)
//        {
//            var assetPath = AssetDatabase.GetAssetPath(o);
//            var importer  = AssetImporter.GetAtPath(assetPath);
//
//            if (importer == null)
//            {
//                continue;
//            }
//
//            // Get asset bundle name & variant
////            var assetBundleName    = importer.assetBundleName;
////            var assetBundleVariant = importer.assetBundleVariant;
//
////            var assetBundleFullName = string.IsNullOrEmpty(assetBundleVariant) ?
////                                          assetBundleName :
////                                          assetBundleName + "." + assetBundleVariant;
//
//            importer.assetBundleName = bundleName;
//            
//            // Only process assetBundleFullName once. No need to add it again.
//            if (processedBundles.Contains(bundleName))
//            {
//                continue;
//            }
//
//            processedBundles.Add(bundleName);
//
//            var build = new AssetBundleBuild
//                        {
//                            assetBundleName    = bundleName,
////                            assetBundleVariant = assetBundleVariant,
//                            assetNames         = AssetDatabase.GetAssetPathsFromAssetBundle(bundleName)
//                        };
//
//
//            assetBundleBuilds.Add(build);
//        }
//
//        Debug.Log(assetBundleBuilds.Count);
//
//        BuildPipeline.BuildAssetBundles(outputPath: outputPath,
//                                        builds: assetBundleBuilds.ToArray(),
//                                        assetBundleOptions: BuildAssetBundleOptions.ChunkBasedCompression,
//                                        targetPlatform: EditorUserBuildSettings.activeBuildTarget);
//    }


    // ----------------------------------------------------------------------------------------------------- Helpers //
    
    /// <summary>
    ///     This function will iterate through all files in all subdirectories of the given path and,
    ///     if the file isn't a meta file (which aren't required in AssetBundles?), will assign it to a
    ///     bundle of the given bundleName.
    /// </summary>
    ///
    /// <param name="startingDirectory">
    ///     What directory should we iterate through?
    /// </param>
    ///
    /// <param name="bundleName">
    ///    What name should this new bundle go by?
    /// </param>
    private static void RecursiveAssetBundlerDrill
    (
        string startingDirectory,
        string bundleName)
    {
        foreach (var directory in Directory.GetDirectories(startingDirectory))
        {
            foreach (var filename in Directory.GetFiles(directory))
            {
                if (filename.EndsWith(".meta")) continue;
				
                Debug.Log($"Working on asset at path [{filename}]");

                var localizedAssetPath = LocalizeFullPath(filename);
				
                var assetImporter = AssetImporter.GetAtPath(localizedAssetPath);

                if (assetImporter == null)
                {
                    Debug.LogWarning($"Getting the AssetImporter for asset at path [{localizedAssetPath}] failed.");

                    continue;
                }

                assetImporter.assetBundleName = bundleName;

                assetImporter.SaveAndReimport();
            }

            RecursiveAssetBundlerDrill(startingDirectory: directory,
                                      bundleName: bundleName);
        }
    }

    public static List<string> GetAllAssetsFromDirectory
    (
        string       directory)
    {
        var output = new List<string>();

        RecursiveAssetGetter(startingDirectory: directory,
                             assets: output);

        return output;
    }


    private static void RecursiveAssetGetter
    (
        string startingDirectory,
        List<string> assets)
    {
        foreach (var directory in Directory.GetDirectories(startingDirectory))
        {
            foreach (var filename in Directory.GetFiles(directory))
            {
                if (filename.EndsWith(".meta")) continue;
				
                Debug.Log($"Collecting asset [{filename}]");

                assets.Add(ExtractLastPartOfPath(filename));
            }

            RecursiveAssetGetter(startingDirectory: directory, 
                                 assets: assets);
        }
    }
}