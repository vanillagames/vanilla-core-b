﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Branding
{
    [CreateAssetMenu(fileName = "New Swatch", menuName = "Vanilla/Branding/Swatch")]
    public class VanillaSwatch : ScriptableObject
    {
        public Color[] colors = new Color[0];
    }
}