﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Vanilla.Basic;

namespace Vanilla.Branding
{
    [CreateAssetMenu(fileName = "New Brand Pack", menuName = "Vanilla/Branding/Brand Pack")]
    public class VanillaBrandPack : VanillaScriptable
    {
        public VanillaSwatch swatch;

        public Texture2D[] logos = new Texture2D[0];
        
        public Texture2D[] textures = new Texture2D[0];

        public string[] taglines = new string[0];
        protected override void OnValidate() {}
    }
}