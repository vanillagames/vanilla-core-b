﻿using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Transforms
{

	public class StageCharacter : ActiveTargetBehaviour
	{
		
		public Transform boundsTarget;

		public float marginOfError  = 3.0f;
		public float rotationScalar = 3.0f;

		[ReadOnly]
		public Vector3 targetPosition;
		public Vector3 targetPositionBounds = new Vector3(x: 5.0f,
		                                                  y: 0.0f,
		                                                  z: 0.25f);
		
		[SerializeField] 
		[ReadOnly]
		public bool facingTarget;


		public void Awake()
		{
			if (boundsTarget)
				targetPositionBounds = boundsTarget.GetComponent<Collider>()
				                                   .bounds.size;
		}


		private void Update()
		{
			targetPosition = target.position.GetRectangleClamp(targetPositionBounds);

			facingTarget = t.IsLookingAt(target: targetPosition,
			                          marginOfErrorInDegrees: marginOfError);

			if (!facingTarget)
				t.Rotate(axis: Vector3.up,
				         angle: Vector3.SignedAngle(from: t.forward,
				                                    to: t.GetDirectionTo(targetPosition),
				                                    axis: Vector3.up) *
				                Time.deltaTime                        *
				                rotationScalar);
			else
				t.DirtyLerpLocalPosition(to: targetPosition,
				                         factor: Time.deltaTime);
		}


		private void OnDrawGizmos()
		{
			if (!CanDrawGizmos()) return;

			Gizmos.DrawSphere(center: targetPosition,
			                  radius: 0.25f);

			Gizmos.DrawCube(center: Vector3.zero,
			                size: targetPositionBounds * 2.0f);
		}


		public override Transform FindTarget()
		{
			return _target;
		}

	}

}