﻿namespace Vanilla
{

	/// <summary>
	/// 	This is an example of a 'self-selectable' base class.
	///
	/// 	By calling Hover or Select, this class will receive it's own controlled callbacks
	/// 	if they pass through 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class BaseSelfSelectable<T> : VanillaBehaviour,
	                                              ISelfSelectable<T>
		where T : class, ISelfSelectable<T>
	{

		public static readonly SelfHoverSlot<T> hoverSlot = new SelfHoverSlot<T>();
		public SelfHoverSlot<T> HoverSlot() => hoverSlot;

		public static readonly SelfSelectionSlot<T> selectionSlot = new SelfSelectionSlot<T>();
		public SelfSelectionSlot<T> SelectionSlot() => selectionSlot;

		public void TryHover()
		{
			hoverSlot.current = this as T;
		}
		
		public void TrySelect()
		{
			// You can only Select() the currently hovered object.
			if (hoverSlot.current != this as T) return;

			selectionSlot.current = this as T;
		}
		
		
		public abstract void Hovered();

		public abstract void Dehovered();


		public abstract void Selected();

		public abstract void Deselected();

	}

}