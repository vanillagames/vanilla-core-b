﻿namespace Vanilla
{

	public abstract class BaseSelectable<TSelectable, 
	                                     TSelector> : VanillaBehaviour,
	                                                  ISelectable<TSelectable, 
		                                                          TSelector>
		where TSelectable : class, 
							ISelectable<TSelectable, 
										TSelector>
		where TSelector   : class, 
						    ISelector<TSelectable, 
							          TSelector>
	{

		public static readonly ProtectedSlot<TSelectable> hoverSlot = new ProtectedSlot<TSelectable>();

		public static readonly ProtectedSlot<TSelectable> selectionSlot = new ProtectedSlot<TSelectable>();


		public void RequestHover()
		{
			hoverSlot.current = this as TSelectable;
		}
		
		public void RequestSelect()
		{
			// You can only Select() the currently hovered object.
			if (hoverSlot.current != this as TSelectable) return;

			selectionSlot.current = this as TSelectable;
		}


		public abstract void Hovered();

		public abstract void Dehovered();

		public abstract void Selected();

		public abstract void Deselected();

	}

}