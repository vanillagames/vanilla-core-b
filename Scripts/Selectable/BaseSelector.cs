﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Selectables
{

    public abstract class BaseSelector<TSelectable, 
                              TSelector> : ISelector<TSelectable, 
                                                     TSelector>
        where TSelectable : BaseSelectable<TSelectable, TSelector>
        where TSelector : BaseSelector<TSelectable, TSelector>
    {

        /// <summary>
        /// 	Which instance of T is hovered over?
        /// </summary>
        public static readonly ProtectedSlot<TSelectable> hoverSlot = new ProtectedSlot<TSelectable>();

        
        /// <summary>
        /// 	Which instance of T is selected?
        /// </summary>
        public static readonly ProtectedSlot<TSelectable> selectionSlot = new ProtectedSlot<TSelectable>();


        
        public BaseSelector()
        {
            hoverSlot.onSlotChange.AddListener(HandleHoverChange_Internal);

            selectionSlot.onSlotChange.AddListener(HandleSelectionChange_Internal);
        }
        
        
        private void HandleHoverChange_Internal
        (
            TSelectable @old,
            TSelectable @new)
        {
            if (old != null)
            {
                old.Dehovered();
            }

            if (@new != null)
            {
                @new.Hovered();
            }

            HandleHoverChange(old: @old,
                              @new: @new);
        }
        
        
        private void HandleSelectionChange_Internal
        (
            TSelectable @old,
            TSelectable @new)
        {
            if (old != null)
            {
                old.Deselected();
            }

            if (@new != null)
            {
                @new.Selected();
            }

            HandleSelectionChange(old:  @old,
                                  @new: @new);
        }


        protected abstract void HandleHoverChange
        (
            TSelectable @old,
            TSelectable @new);
        
        
        protected abstract void HandleSelectionChange
        (
            TSelectable @old,
            TSelectable @new);

        
        public void Hover
        (
            TSelectable newHover)
        {
            hoverSlot.current = newHover;
        }

        
        public void Select
        (
            TSelectable newSelection)
        {
            selectionSlot.current = newSelection;
        }

    }

}