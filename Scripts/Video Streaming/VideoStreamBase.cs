﻿using System;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

namespace Vanilla
{

	[RequireComponent(requiredComponent: typeof(VideoPlayer),
		requiredComponent2: typeof(MeshRenderer))]
	public class VideoStreamBase : VanillaBehaviour
	{

		[SerializeField, ReadOnly]
		private VideoPlayer _videoPlayer;
		public VideoPlayer videoPlayer => _videoPlayer;

		public Material targetMaterial;

		public RenderTexture targetRenderTexture;

		[SerializeField, ReadOnly]
		private bool _playing = false;

		public bool playing
		{
			get => _playing;
			protected set => _playing = value;
		}

		[SerializeField, ReadOnly]
		private double _videoDelta = 0.0f;
		public double videoDelta
		{
			get => _videoDelta;
			set
			{
				buffering = Mathf.Abs((float) ( _videoDelta - value )) < Mathf.Epsilon;

				_videoDelta = value;
			}
		}

		[SerializeField, ReadOnly]
		private bool _buffering;

		public bool buffering
		{
			get => _buffering;
			set
			{
				if (_buffering == value) return;

				_buffering = value;

				if (_buffering)
				{
					onVideoBufferingPause?.Invoke();
				}
				else
				{
					onVideoBufferingResume?.Invoke();
				}

//				Time.timeScale = _buffering ?
//					                 0 :
//					                 1;
			}
		}

		public UnityAction onVideoBegun;
		public UnityAction onVideoFinished;

		public UnityAction onVideoBufferingPause;
		public UnityAction onVideoBufferingResume;

		private static readonly int MainTex = Shader.PropertyToID("_MainTex");


		protected override void InSceneValidation()
		{
			base.InSceneValidation();

			_videoPlayer               = GetComponent<VideoPlayer>();
			_videoPlayer.targetTexture = targetRenderTexture;


		}


		void Update()
		{
			// If the video isn't playing or the frame we're on doesn't match the frame rate of the video, drop out.
			// Skipping this check will lead to checking frames that look like they're buffering on paper but aren't.
			if (!videoPlayer.isPlaying ||
			    Time.frameCount % (int) ( videoPlayer.frameRate + 1 ) != 0)
				return;

			videoDelta = videoPlayer.time;
		}

		public void Initialize()
		{
			if (targetRenderTexture && targetMaterial)
			{
				targetMaterial.SetTexture(nameID: MainTex,
				                          value: targetRenderTexture);
			}

			// This happens when the video is done 'preparing'.
			// This can take a while if the video is big and needs loading.
			videoPlayer.prepareCompleted += VideoPrepared;

			// This happens when the video first starts playing.
			// [Android] Also called when the app is minimized and re-opened
			videoPlayer.started += VideoStarted;

			// This happens when the video ends.
			videoPlayer.loopPointReached += VideoEnded;

			videoPlayer.errorReceived += VideoPlaybackError;
		}

		public void Deinitialize()
		{
			videoPlayer.prepareCompleted -= VideoPrepared;

			videoPlayer.started -= VideoStarted;

			videoPlayer.loopPointReached -= VideoEnded;

			videoPlayer.errorReceived -= VideoPlaybackError;
		}

		protected virtual string FetchPlaybackURL() => "https://www.vimeo.com/";

		/// <summary>
		/// 	This is used to start the process of video playback.
		/// </summary>
		public virtual void PrepareToPlayVideo()
		{
			videoPlayer.url = FetchPlaybackURL();
			
			videoPlayer.Prepare();
		}

		protected virtual void VideoPrepared
		(
			VideoPlayer source)
		{
			Log("The video is ready to play!");

			videoPlayer.Play();
		}


		protected virtual void VideoStarted
		(
			VideoPlayer source)
		{
			Log("The video started!");

			playing = true;

			onVideoBegun?.Invoke();
		}


		protected virtual void VideoEnded
		(
			VideoPlayer source)
		{
			Log("The video is over.");

			playing = false;

			_videoDelta = 0.0f;

			onVideoFinished?.Invoke();
		}


		protected virtual void VideoPlaybackError
		(
			VideoPlayer source,
			string      message)
		{
			Error($"Video playback error:\n{message}");
		}

	}

}