﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace Vanilla.Basic
{

    public class InternetConnectionIcon : VanillaBehaviour
    {
        public Image icon;

        public static readonly Color connected = new Color(0.5f, 1.0f, 0.5f, 1.0f);
        public static readonly Color disconnected = new Color(1.0f, 0.5f, 0.5f, 1.0f);

        void Awake()
        {
            icon = GetComponent<Image>();

            InternetConnection.OnConnectivityChange.AddListener(OnConnectivityChange);
        }

        public void OnConnectivityChange(bool newState)
        {
            if (icon != null)
            {
                icon.color = newState ? connected : disconnected;
            }
        }
    }
}