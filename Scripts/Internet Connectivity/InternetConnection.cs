﻿using System.Collections;

using UnityEngine;

namespace Vanilla
{
    public class InternetConnection : VanillaBehaviour
    {
        [SerializeField, ReadOnly]
        private static bool _internetAvailable;
        public static bool internetAvailable
        {
            get => _internetAvailable;
            set
            {
                if (_internetAvailable == value) return;

                _internetAvailable = value;
                
                OnConnectivityChange.Invoke(_internetAvailable);
                
                Debug.Log($"Internet connectivity has changed and is now{(internetAvailable ? string.Empty : " no longer")} available.");
            }
        }

        public float timerFrequency = 2.0f;

        public static readonly GenericEvent<bool> OnConnectivityChange = new GenericEvent<bool>();

        IEnumerator Start()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                Debug.LogWarning("No internet connection detected.");
            }
            
            if (Mathf.Approximately(timerFrequency, 0.0f))
                yield break;
            
            WaitForSeconds wait = new WaitForSeconds(timerFrequency);

            while (true)
            {
                Check();

                yield return wait;
            }
        }

        public void Check()
        {
            internetAvailable = Application.internetReachability != NetworkReachability.NotReachable;
        }
    }
}