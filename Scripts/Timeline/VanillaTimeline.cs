﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

using Malee;
using RotaryHeart.Lib.SerializableDictionary;
//using Vanilla.Clockwork;
using Vanilla.Math;

namespace Vanilla.Timelines
{
	[Serializable]
	public abstract class VanillaTimeline<T0, T1, T2> : VanillaTimelineBase
		where T0 : TimelineChapter<T0,T1, T2>
		where T1 : VanillaTimeline<T0,T1, T2>
		where T2 : ReorderableArray<T0>
	{
		[Serializable]
		public class ChapterDictionary : SerializableDictionaryBase<string, T0> {}
		
//		public override float length
//		{
//			get
//			{
//				if (_length > 0.0f) return _length;
//
//				UpdateChapterTimings();
//
//				_length = 0.0f;
//				
//				foreach (var c in chapters)
//				{
//					_length += c.length;
//				}
//
//				return _length;
//			}
//		}

		public override string currentChapterName
		{
			get
			{
				if (!string.IsNullOrEmpty(_currentChapterName)) return _currentChapterName;

				return _currentChapterName = chapters[chapterIndex].name;
			}
			set => _currentChapterName = value;
		}

		[Header("Chapters")]
		[SerializeField]
		private ChapterDictionary _chapterDictionary;
		public ChapterDictionary chapterDictionary
		{
			get
			{
				if (_chapterDictionary != null) return _chapterDictionary;
				
				_chapterDictionary = new ChapterDictionary();

				foreach (var c in chapters)
				{
					_chapterDictionary.Add(c.name, c);
				}

				return _chapterDictionary;
			}
		}

		public ProtectedSlot<T0> currentChapterSlot = new ProtectedSlot<T0>();

		[Reorderable(paginate = true, pageSize = 8, elementNameProperty = "name")]
		[SerializeField]
		public T2 chapters;

		public override void Initialize()
		{
			base.Initialize();

//			T0.timeline = this as T1;
		}
		
		public override float CalculateTimelineLength()
		{
			UpdateChapterTimings();
			
			return chapters.Sum(c => c.length);
		}

		protected void UpdateChapterTimings()
		{
			float l = 0;

			for (int i = 0; i < chapters.Length; i++)
			{
				chapters[i].index = i;
				
				chapters[i].startTime = l;

				l += chapters[i].length;

				chapters[i].endTime = l;
			}
		}

		// When changing the currentChapter value, we can re-use the TimelineState enum in a different context (since it has the same layout) to get around a switch limitation.
		// This is only used when calculating what to do with the currentChapter
		BaseTimelineState ChapterToTimelineState(int incomingChapter)
		{
			return incomingChapter == 0 ? BaseTimelineState.Start : incomingChapter == chapters.Length - 1 ? BaseTimelineState.End : BaseTimelineState.Middle;
		}

		public virtual void HandleChapterTurnover(T0 @old, T0 @new)
		{
			Log($"Handling chapter turnover from [{@old?.name}] to [{@new?.name}]");

			if (timeDirectionIsPositive)
			{
				@old?.onChapterEnd.Invoke();
			}
			else
			{
				@old?.onChapterStart.Invoke();
			}

			if (@new != null)
			{
				currentChapterName = @new.name;

				if (timeDirectionIsPositive)
				{
					@new.onChapterStart.Invoke();
				}
				else
				{
					@new.onChapterEnd.Invoke();
				}
			}
		}

		public override void OnEnable()
		{
			base.OnEnable();
			
			currentChapterSlot.onSlotChange.AddListener(HandleChapterTurnover);
			
			currentChapterSlot.current = chapterIndex.IsWithinExclusiveRange(chapters.Length) ? chapters[chapterIndex] : null;
		}

		public virtual void OnDisable()
		{
			currentChapterSlot.onSlotChange.RemoveListener(HandleChapterTurnover);
		}
	}
}