﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//
//using UnityEngine;
//using UnityEngine.Events;
//
//using Vanilla;
//using Vanilla.Math;
//
//namespace Vanilla.Timelines
//{
//    public class VanillaTimelineLoop<T> : VanillaTimelineBase<T>
//        where T : TimelineChapter
//    {
//        public override float time { get; set; }
//
//        protected override int chapterIndex
//        {
//            get => _chapterIndex;
//            set => _chapterIndex = value;
//        }
//
//        public override int GetPreviousChapterID()
//        {
//            return (chapterIndex - 1).GetWrap(chapters.Length - 1);
//        }
//
//        public override int GetNextChapterID()
//        {
//            return (chapterIndex+1).GetWrap(chapters.Length - 1);
//        }
//
//        public override void MilestoneReached(int milestone)
//        {
//            Log($"Milestone reached! [{milestone}]");
//        }
//
//        public override void TimelineUpdate()
//        {
//            
//        }
//
//        public override void HandleChapterTurnover(TimelineChapter old, TimelineChapter @new)
//        {
//            
//        }
//    }
//}