﻿using System;
using System.Collections;
using System.Collections.Generic;
using Malee;
using UnityEngine;
using UnityEngine.Events;

using Vanilla;
using Vanilla.Math;

namespace Vanilla.Timelines
{
	public abstract class VanillaTimelineLinear<T0, T1, T2> : VanillaTimeline<T0, T1, T2>
		where T0 : TimelineChapter<T0, T1, T2>
		where T1 : VanillaTimeline<T0,T1, T2>
		where T2 : ReorderableArray<T0>
	{
		[ReadOnly]
		public BaseTimelineState linearState;
		
		public override float time
		{
			get => _time;
			set
			{
				// Immediately clamp the new value.
				// We never want time to be able to extend before 0 or past the length of the timeline.
				value.Clamp(0.0f, length);
				
				// Get the difference between value and the current time
				float delta = value - _time;
				
				_time = value;

				timelineNormal = _time / length;

				// If the difference is tiny, do nothing.
				if (delta.IsZero()) return;

				// Let's track if we're going forward through time
				timeDirectionIsPositive = delta.IsPositive();

				if (timeDirectionIsPositive)
				{
					// If we're moving forward through time, keep incrementing the chapterIndex until the new time value is no longer greater than the current chapters end time.
					// Because of chapter management reasons, we also need to increment if chapterIndex is less/equal to -1 (aka 'pre-start' chapter, which is null!)
//					while (chapterIndex < chapters.Length && (chapterIndex <= -1 || _time >= chapters[chapterIndex].endTime)) 
//						chapterIndex++;

					while (chapterIndex < chapters.Length && (chapterIndex <= -1 || _time >= chapters[chapterIndex].endTime))
					{
						if (currentChapterSlot.current == null)
						{
							chapterIndex++;
						}
						else
						{
							switch (currentChapterSlot.current.GetTimelineAction())
							{
								case TimelineChapterOptions.Skip:
									_time += currentChapterSlot.current.length;
									_chapterIndex++;
									currentChapterSlot._current = chapters[_chapterIndex];
									break;
								
								case TimelineChapterOptions.Loop:
									
									break;
								
								case TimelineChapterOptions.Ignore:
									
									break;
							}
							
							if (currentChapterSlot.current.chapterOptions.HasFlag(TimelineChapterOptions.Loop))
							{
								
							}
						}
					}

					while (
						chapterIndex < chapters.Length && 
						(chapterIndex <= -1 || 
						(_time >= chapters[chapterIndex].endTime && chapters[chapterIndex].chapterOptions.HasFlag(TimelineChapterOptions.Loop)))) 
						chapterIndex++;
				}
				else
				{
					// If we're moving backward through time, keep decrementing the chapterIndex until the new time value is no longer lower than the current chapters start time.
					// Because of chapter management reasons, we also need to decrement if chapterIndex is greater/equal to the number of chapters (aka 'post-end' chapter, which is also null!)
//					while (chapterIndex > -1 && (chapterIndex >= chapters.Length || _time <= chapters[chapterIndex].startTime)) 
//						chapterIndex--;
				}
				
				if (currentChapterSlot.current != null)
				{
					chapterNormal = currentChapterSlot.current.GetChapterNormal(_time);
				}

				TimelineUpdate();
			}
		}

		protected override int chapterIndex
		{
			get => _chapterIndex;
			set
			{
				value = Mathf.Clamp(value, -1, chapters.Length);

				if (_chapterIndex == value) return;

				timeDirectionIsPositive = value > _chapterIndex;

				if (timeDirectionIsPositive)
				{
					while (value > _chapterIndex)
					{
						_chapterIndex++;

						if (_chapterIndex < chapters.Length)
						{
							currentChapterSlot.current = chapters[_chapterIndex];

							time = currentChapterSlot.current.startTime;
							
							chapterNormal = 0.0f;

							timelineNormal = _time / length;
						}
						else
						{
							currentChapterSlot.current = null;

							linearState = BaseTimelineState.End;

							currentChapterName = "End";

							_time = length; 
//							time = length; 
							// Changing this to run the setter as well means we can reliably change either the chapter
							// or time itself to achieve the same result.
							
							chapterNormal = 1.0f;

							timelineNormal = 1.0f;

							onReachedEndOfTimeline.Invoke();
						}
						
						MilestoneReached(_chapterIndex);

						onMileStoneReached.Invoke(_chapterIndex);
					}
				}
				else
				{
					while (value < _chapterIndex)
					{
						_chapterIndex--;
						
						if (_chapterIndex > -1)
						{
							currentChapterSlot.current = chapters[_chapterIndex];

							_time = currentChapterSlot.current.endTime;
							
							chapterNormal = 1.0f;

							timelineNormal = _time / length;
						}
						else
						{
							currentChapterSlot.current = null;

							linearState = BaseTimelineState.Start;

							currentChapterName = "Start";

							_time = 0.0f;
							
							chapterNormal = 0.0f;

							timelineNormal = 0.0f;

							onReachedBeginningOfTimeline.Invoke();
						}
						
						MilestoneReached(_chapterIndex);

						onMileStoneReached.Invoke(_chapterIndex);
					}
				}
			}
		}

		public override void HandleChapterTurnover(T0 @old, T0 @new)
		{
			if (@old == null && @new != null)
			{
				linearState = BaseTimelineState.Middle;
			}
			
			base.HandleChapterTurnover(@old, @new);
		}
	}
}