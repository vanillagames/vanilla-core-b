﻿using System;

using Malee;

using UnityEngine;
using UnityEngine.Events;
using Vanilla.Math;
using Random = System.Random;

namespace Vanilla.Timelines.Examples
{
    public enum ExampleState
    {
        FadeUp,
        ZoomIn,
        RotateAround
    }

    [Serializable]
    public class ExampleChapter : TimelineChapter<ExampleChapter, ExampleLinearTimeline, ExampleChapterList>
    {
        [SerializeField] public ExampleState state;
    }

    /// <summary>
    ///     This step is important because, natively, Unity can't serialize generic classes.
    ///     For something to be serialized and thus visible in the inspector, it needs to be
    ///     'sealed off' with an inherited class that uses 'final' types in the generic slots.
    /// </summary>
    [Serializable]
    public class ExampleChapterList : ReorderableArray<ExampleChapter> {}
    
    [Serializable]
    public class ExampleLinearTimeline : VanillaTimelineLinear<ExampleChapter, ExampleLinearTimeline, ExampleChapterList>
    {
        [Header("Example Timeline")] 
        [ReadOnly] public ExampleState exampleState;
        
        public Transform mainCharacter;

        public Light directionalLight;
        public float lightFrom = 0.0f;
        public float lightTo = 1.0f;

        public Camera mainCamera;
        public float zoomFrom = 60.0f;
        public float zoomTo = 45.0f;

        public float rotateAroundSpeed = 8.0f;
        
        public void OnEnable()
        {
            base.OnEnable();
            
            chapterDictionary["Fade Up"].onChapterStart.AddListener(FadeUpBegun);

            chapterDictionary["Zoom In"].onChapterStart.AddListener(ZoomInBegun);
            chapterDictionary["Zoom In"].onChapterEnd.AddListener(ZoomInEnd);
            
            chapterDictionary["Rotate Around"].onChapterEnd.AddListener(RotateAroundEnd);
            
            onMileStoneReached.AddListener(MileStoneReachedHandler);
            
            onReachedBeginningOfTimeline.AddListener(ReachedBeginning);
            onReachedEndOfTimeline.AddListener(ReachedEnd);
            
            onTimeDirectionBecamePositive.AddListener(TimePositiveHandler);
            onTimeDirectionBecameNegative.AddListener(TimeNegativeHandler);
        }
        
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                chapterIndex = -1;
            }
            
            if (Input.GetKeyDown(KeyCode.W))
            {
                chapterIndex = 0;
            }
            
            if (Input.GetKeyDown(KeyCode.E))
            {
                chapterIndex = 1;
            }
            
            if (Input.GetKeyDown(KeyCode.R))
            {
                chapterIndex = 2;
            }
            
            if (Input.GetKeyDown(KeyCode.T))
            {
                chapterIndex = 3;
            }
        }
        
        public override void TimelineUpdate()
        {
            switch (exampleState)
            {
                case ExampleState.FadeUp:
                    directionalLight.intensity = Mathf.Lerp(lightFrom, lightTo, chapterNormal.GetHermite());
                    break;
                
                case ExampleState.ZoomIn:
                    mainCamera.fieldOfView = Mathf.Lerp(zoomFrom, zoomTo, chapterNormal.GetHermite());
                    break;
                
                case ExampleState.RotateAround:
                    mainCamera.transform.RotateAround(mainCharacter.position, Vector3.up, Time.deltaTime * rotateAroundSpeed);
                    break;
            }

            switch (linearState)
            {
                case BaseTimelineState.End:
                    Log($"_time is now [{_time}]");
                    break;
            }
        }

        public override void HandleChapterTurnover(ExampleChapter @old, ExampleChapter @new)
        {
            base.HandleChapterTurnover(@old, @new);

            if (@new != null)
            {
                exampleState = @new.state;
            }
        }

        public void ReachedBeginning()
        {
            Log("Reached beginning!");
        }

        public void ReachedEnd()
        {
            Log("We reached the end!");
        }
        
        public override void MilestoneReached(int milestone)
        {
            Log($"Milestone Reached! (Override) [{milestone}]");
        }

        public void TimeNegativeHandler()
        {
            Log("Time became negative!");
        }

        public void TimePositiveHandler()
        {
            Log("Time became positive!");
        }
        
        public void MileStoneReachedHandler(int m)
        {
            Log($"Milestone Reached! (Subscribed) [{m}]");
        }

        public void FadeUpBegun()
        {
            Log("Okay, action! Bring the house lights up.");
        }

        public void ZoomInBegun()
        {
            Log("Now... zoom in for a close-up.");
        }

        public void ZoomInEnd()
        {
            Log("Thats perfect! Okay, now rotate around them indefinitely.");
        }
        
        public void RotateAroundEnd()
        {
            Log("Once more!");
            
            Log($"Setting _time [{_time}] to [{chapterDictionary["Rotate Around"].startTime}]");
            
            // Looping might be really easy and look like this.
            // Integrating it at a lower level might be complicated, particularly regarding jumping chapters
            // or big sections of time. How would that work? _time = value.GetWrap()?
            _time = chapterDictionary["Rotate Around"].startTime;
            
            Log($"_time is now [{_time}]");
        }
    }
}