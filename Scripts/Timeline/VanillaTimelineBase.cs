﻿using System;
using Malee;
using UnityEngine;
using UnityEngine.Events;
using Vanilla.Math;

namespace Vanilla.Timelines
{
    public enum BaseTimelineState
    {
        Start,
        Middle,
        End
    }

//    public enum TimelineChapterAction
//    {
//        Run,
//        Loop,
//        Skip,
//        Ignore
//    }

    [Flags]
    public enum TimelineChapterOptions
    {
        Skip = 1,
        Loop = 2,
        Ignore = 4
    }
    
    [Flags]
    public enum TimeLockFlags
    {
        StartForwards = 1,
        StartBackwards = 2,
        EndForwards = 4,
        EndBackwards = 8
    }
    
    public abstract class VanillaTimelineBase : VanillaBehaviour
    {
        [Header("[ Vanilla Timeline ]")] 
        [SerializeField, ReadOnly] protected float _time;
        [SerializeField, ReadOnly] public float length;
        [SerializeField, ReadOnly] private float _timelineNormal;
        [SerializeField, ReadOnly] protected string _currentChapterName = string.Empty;
        [SerializeField, ReadOnly] protected int _chapterIndex = -1;
        [SerializeField, ReadOnly] protected float _chapterNormal;
//        [Tooltip("Is the time passed to us positive? i.e. Is the timeline heading forwards or backwards?")]
        [SerializeField, ReadOnly] private bool _timeDirectionIsPositive = false;
        
        protected bool timeDirectionIsPositive
        {
            get => _timeDirectionIsPositive;
            set
            {
                if (_timeDirectionIsPositive == value) return;

                _timeDirectionIsPositive = value;

                if (value)
                {
                    onTimeDirectionBecamePositive.Invoke();
                }
                else
                {
                    onTimeDirectionBecameNegative.Invoke();
                }
            }
        }


        public abstract float time
        {
            get; 
            set;
        }

        
        public float timelineNormal
        {
            get => _timelineNormal;
            set => _timelineNormal = value.GetBetween01();
        }

        protected abstract int chapterIndex
        {
            get; 
            set; 
        }
        
        public float chapterNormal
        {
            get => _chapterNormal;
            set => _chapterNormal = value.GetBetween01();
        }

        public abstract string currentChapterName
        {
            get;
            set;
        }
        
        // Methods

        public virtual void OnEnable()
        {
            Initialize();
        }

        public virtual void Initialize()
        {
            length = CalculateTimelineLength();
        }

        public abstract void MilestoneReached(int milestone);

        public abstract void TimelineUpdate();

        public abstract float CalculateTimelineLength();
        
        // Events
        
        [HideInInspector] public UnityEvent onReachedBeginningOfTimeline = new UnityEvent();
        [HideInInspector] public UnityEvent onReachedEndOfTimeline = new UnityEvent();
        
        [HideInInspector] public UnityEvent onTimeDirectionBecamePositive = new UnityEvent();
        [HideInInspector] public UnityEvent onTimeDirectionBecameNegative = new UnityEvent();
		
        [HideInInspector] public GenericEvent<int> onMileStoneReached = new GenericEvent<int>();
    }
    
    [Serializable]
    public class TimelineChapter<T0, T1, T2> : VanillaClass
        where T0 : TimelineChapter<T0, T1, T2>
        where T1 : VanillaTimeline<T0,T1, T2>
        where T2 : ReorderableArray<T0>
    {
        public static T1 timeline;
        
        [HideInInspector, ReadOnly] public int index; // What index is this chapter?

        [SerializeField] public string name; // What happens during this chapter?

        [SerializeField] public float length; // How long should this chapter run for in seconds?

        [SerializeField] public bool loop; // If true, when the end of this chapter is reached, it will
                                                     // set _time to either startTime or endTime depending on
                                                     // the current state of timeDirection.

         /// <summary>
         ///     Common chapter and time management options.
         /// </summary>
         [SerializeField, EnumFlags(2)] protected internal TimelineChapterOptions chapterOptions = 0;
                                                     
        /// <summary>
        ///     A series of bitwise flags for quickly assessing if a chapter should block any further time manipulation under certain conditions.
        /// </summary>
        [SerializeField, EnumFlags(2)] protected internal TimeLockFlags timeLockFlags = 0;

        /// <summary>
        ///     How much time will have elapsed by the start of the chapter?
        /// </summary>
        [HideInInspector, ReadOnly] public float startTime;

        /// <summary>
        ///     How much time will have elapsed by the end of the chapter?
        /// </summary>
        [HideInInspector, ReadOnly] public float endTime;

        /// <summary>
        /// 	This allows us to listen for the start of a specific chapter, whenever it is reached.
        /// 	Please note that this is best suited for listening for a specific pre-determined chapter
        /// 	rather than checking chapterIndex in the listener. Doing that might be inaccurate!
        /// </summary>
        [HideInInspector] public UnityEvent onChapterStart = new UnityEvent();
			
        /// <summary>
        /// 	This allows us to listen for the end of a specific chapter, whenever it is reached.
        /// 	Please note that this is best suited for listening for a specific pre-determined chapter
        /// 	rather than checking chapterIndex in the listener. Doing that might be inaccurate!
        /// </summary>
        [HideInInspector] public UnityEvent onChapterEnd = new UnityEvent();

        public float GetChapterNormal(float currentTime)
        {
            return (currentTime - startTime) / length;
        }

        public void Enter(VanillaTimelineBase timeline)
        {
            
        }

        public void Exit(VanillaTimelineBase timeline)
        {
            
        }

        public TimelineChapterOptions GetTimelineAction()
        {
//            if (chapterFlags.HasFlag)

            return TimelineChapterOptions.Skip;
        }

//        public void SetChapterNormal(ref float normal, float currentTime)
//        {
//            normal = (currentTime - startTime) / length;
//        }
			
        public override void Reset()
        {
            index = -1;
            name = string.Empty;
            length = -1.0f;
            startTime = 0.0f;
            endTime = 0.0f;
				
            onChapterStart.RemoveAllListeners();
            onChapterEnd.RemoveAllListeners();
        }
    }
}