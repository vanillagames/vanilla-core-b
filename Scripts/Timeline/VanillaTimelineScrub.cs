﻿using System.Collections;
using System.Collections.Generic;
using Malee;
using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Timelines
{
    public class VanillaTimelineScrub : VanillaBehaviour
    {
        public VanillaTimelineBase timeline;

        public float time;
        
        [SerializeField, ReadOnly]
        public float timelineEndTime;

        void Awake()
        {
            timelineEndTime = timeline.length;
        }
            
        void Update()
        {
            time.Clamp(0, timelineEndTime);
            
            timeline.time = time;
        }
    }
}