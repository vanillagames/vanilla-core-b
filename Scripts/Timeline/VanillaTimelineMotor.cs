﻿using Malee;
using UnityEngine;

namespace Vanilla.Timelines
{
    public class VanillaTimelineMotor : VanillaBehaviour
    {
        [SerializeField]
        private VanillaTimelineBase _timeline;
        public VanillaTimelineBase timeline
        {
            get => _timeline != null ? _timeline : _timeline = GetComponent<VanillaTimelineBase>();
        }

        public float timeMultiplier = 1.0f;

        void Update()
        {
            if (timeline)
            {
                timeline.time += Time.deltaTime * timeMultiplier;
            }
        }
    }
}