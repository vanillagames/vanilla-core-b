﻿using UnityEngine;

namespace Vanilla
{
    [ExecuteInEditMode]
    public class LookAtInEditor : LookAt {}
}