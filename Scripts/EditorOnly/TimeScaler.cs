// Creation Date                                                                              [ 28/01/2020 3:33:21 PM ]
// Project                                                                                               [ Rewild2020 ]
// Developer                                                                                                [ Vanilla ]

using UnityEngine;

namespace Vanilla.SceneWorks
{
	public class TimeScaler : VanillaSingleton<TimeScaler>
	{
		// ----------------------------------------------------------------------------------------------- Variables //
		
		#region Variables

		public KeyCode normal = KeyCode.F9;
		public KeyCode positive2 = KeyCode.F10;
		public KeyCode positive4 = KeyCode.F11;
		public KeyCode positive16 = KeyCode.F12;

		#endregion

		// -------------------------------------------------------------------------------------------- Update Loops //

		#region Update Loops

		void Update()
		{
			#if UNITY_EDITOR
				if (Input.GetKeyDown(normal)) NormalSpeed();
				if (Input.GetKeyDown(positive2)) Time.timeScale = 2;
				if (Input.GetKeyDown(positive4)) FastForward();
				if (Input.GetKeyDown(positive16)) ReallyFastForward();
			#endif

			#if UNITY_ANDROID && DEVELOPMENT_BUILD
				if (Input.touchCount < 4) return;
	
				switch (Input.GetTouch(3).phase)
				{
					case TouchPhase.Began: 
						ReallyFastForward();
						break;
					
					case TouchPhase.Ended: 
						NormalSpeed();
						break;
				}
			#endif
		}

		#endregion

		// ------------------------------------------------------------------------------------------------- Helpers //

		#region Helpers

		public void NormalSpeed()
		{
			Time.timeScale = 1;
		}

		public void FastForward()
		{
			Time.timeScale = 4;
		}
		
		public void ReallyFastForward()
		{
			Time.timeScale = 16;
		}

		#endregion

	}
}