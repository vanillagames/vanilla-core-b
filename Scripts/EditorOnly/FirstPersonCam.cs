﻿using UnityEngine;

namespace Vanilla.Core 
{
	public class FirstPersonCam : ActiveBehaviour 
	{
		[Header("Moving")]
		public float defaultMoveSpeed = 0.01f;
		public float fastMoveSpeed = 0.1f;

		[Header("Looking")]
		public float ySensitivity = 8.0f;
		public float xSensitivity = 8.0f;

		public float zoomSensitivity = 20.0f;

		public float smoothing = 16.0f;

		Quaternion targetRotation;
		Vector3 targetPosition;

		[Header("References")]
		[SerializeField, ReadOnly]
		private Camera cam;

		[Header("Controls")]
		public KeyCode controlKey = KeyCode.Mouse2;
		
		public KeyCode forwardKey = KeyCode.W;
		public KeyCode backKey = KeyCode.S;
		public KeyCode leftKey = KeyCode.A;
		public KeyCode rightKey = KeyCode.D;

		public KeyCode turboKey = KeyCode.LeftShift;
		public KeyCode toggleCameraKey = KeyCode.Tab;

		[Tooltip("If part of the scene is trying to move this camera, you may get unexpected results due to the way positional smoothing is applied. Turn this option on to mitigate the problem!")]
		public bool directPositionApplication = false;
		
		private const string MouseScrollWheelInputString = "Mouse ScrollWheel";
		private const string MouseYAxisInputString = "Mouse Y";
		private const string MouseXAxisInputString = "Mouse X";

		protected override void InSceneValidation()
		{
#if UNITY_EDITOR
			base.InSceneValidation();

			cam = GetComponent<Camera>();
#endif
		}

		public void OnEnable() 
		{
#if UNITY_EDITOR
			targetPosition = t.position;
			targetRotation = t.rotation;
#endif
		}

		void Update() 
		{
#if UNITY_EDITOR
			UpdateCameraState();

			// If this key isn't down, stop here.
			if (!Input.GetKey(key: controlKey)) return;

			UpdateRotation();
			UpdatePosition();
			UpdateZoom();
#endif
		}

		void UpdateRotation()
		{
#if UNITY_EDITOR
			var newEulers = targetRotation.eulerAngles + new Vector3(
				                x: -Input.GetAxis(MouseYAxisInputString) * ySensitivity,
				                y: Input.GetAxis(MouseXAxisInputString) * xSensitivity,
				                z: 0);

			newEulers.z = 0;

			targetRotation = Quaternion.Euler(newEulers);

			t.localRotation = Quaternion.Lerp(
				a: t.localRotation, 
				b: targetRotation, 
				t: Time.deltaTime * smoothing);
#endif
		}

		private void UpdatePosition()
		{
#if UNITY_EDITOR
			var currentSpeed = Input.GetKey(turboKey) ? 
				fastMoveSpeed : 
				defaultMoveSpeed;

			if (Input.GetKey(forwardKey)) 
			{
				targetPosition += t.forward * currentSpeed;
			}

			if (Input.GetKey(backKey)) 
			{
				targetPosition -= t.forward * currentSpeed;
			}

			if (Input.GetKey(leftKey)) 
			{
				targetPosition -= t.right * currentSpeed;
			}

			if (Input.GetKey(rightKey)) 
			{
				targetPosition += t.right * currentSpeed;
			}

			if (directPositionApplication)
			{
				t.position = targetPosition;
			}
			else
			{
				t.position = Vector3.Lerp(
					a: t.position,
					b: targetPosition,
					t: Time.deltaTime * smoothing);
			}
#endif
		}

		void UpdateZoom()
		{
#if UNITY_EDITOR
			if (!cam) return;
			
			cam.fieldOfView -= Input.GetAxis(MouseScrollWheelInputString) * zoomSensitivity;
#endif
		}

		void UpdateCameraState()
		{
#if UNITY_EDITOR
			if (!cam || !Input.GetKeyDown(toggleCameraKey)) return;

			cam.enabled = !cam.enabled;
#endif
		}
	}
}