﻿using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Transforms
{

	public class RadialPossession : VanillaPossession
	{

		private const float c_RadiusMin = 0.0f;
		private const float c_RadiusMax = 5.0f;

		[Header("Radius")]
		[SerializeField]
		[Range(min: c_RadiusMin,
			   max: c_RadiusMax)]
		private float _radius = 0.5f;
		public float radius
		{
			get => _radius;
			set =>
				_radius = value.GetClamp(min: c_RadiusMin,
				                         max: c_RadiusMax);
		}


		public override Vector3 NewTargetPosition()
		{
			return Random.insideUnitSphere * _radius;
		}


		public override Vector3 ClampPosition
		(
			Vector3 p)
		{
			return p.GetRadialClamp(_radius);
		}

	}

}