﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Transforms
{
    public class SharedBillboard : ActiveTargetBehaviour
    {
        public static Transform staticTarget;

        void OnBecameVisible()
        {
            enabled = true;
        }

        void OnBecameInvisible()
        {
            enabled = false;
        }

        void Update()
        {
            t.LookAt(staticTarget);
        }

        public override Transform FindTarget()
        {
            return staticTarget = Camera.main.transform;
        }

    }
}