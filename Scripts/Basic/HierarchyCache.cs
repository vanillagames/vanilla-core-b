﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vanilla.Basic
{
    [CreateAssetMenu(fileName = "New Hierarchy Cache", menuName = "Vanilla/Basic/Hierarchy Cache")]
    public class HierarchyCache : ScriptableObject
    {
        [SerializeField] public bool writeProtected;

        // ------------------------------------------------------------------------------------------------ Combined //
        
        // Copy

        public void CopyAll<T>(T[] objects, bool localPosition, bool localRotation, bool localScale)
            where T : MonoBehaviour
        {
            if (WriteProtectionCheck()) return;
            
            CopyPositions(objects, localPosition);
            CopyRotations(objects, localRotation);
            CopyScales(objects, localScale);
        }

        public void CopyAll<T>(List<T> objects, bool localPosition, bool localRotation, bool localScale)
            where T : MonoBehaviour
        {
            if (WriteProtectionCheck()) return;

            CopyPositions(objects, localPosition);
            CopyRotations(objects, localRotation);
            CopyScales(objects, localScale);
        }

        public void CopyAll(Transform[] objects, bool localPosition, bool localRotation, bool localScale)
        {
            if (WriteProtectionCheck()) return;

            CopyPositions(objects, localPosition);
            CopyRotations(objects, localRotation);
            CopyScales(objects, localScale);
        }

        public void CopyAll(List<Transform> objects, bool localPosition, bool localRotation, bool localScale)
        {
            if (WriteProtectionCheck()) return;

            CopyPositions(objects, localPosition);
            CopyRotations(objects, localRotation);
            CopyScales(objects, localScale);
        }

        // Paste

        public void PasteAll<T>(T[] objects, bool localPosition, bool localRotation)
            where T : MonoBehaviour
        {
            PastePositions(objects, localPosition);
            PasteRotations(objects, localRotation);
            PasteScales(objects);
        }

        public void PasteAll<T>(List<T> objects, bool localPosition, bool localRotation)
            where T : MonoBehaviour
        {
            PastePositions(objects, localPosition);
            PasteRotations(objects, localRotation);
            PasteScales(objects);
        }

        public void PasteAll(Transform[] objects, bool localPosition, bool localRotation)
        {
            PastePositions(objects, localPosition);
            PasteRotations(objects, localRotation);
            PasteScales(objects);
        }

        public void PasteAll(List<Transform> objects, bool localPosition, bool localRotation)
        {
            PastePositions(objects, localPosition);
            PasteRotations(objects, localRotation);
            PasteScales(objects);
        }

        // ----------------------------------------------------------------------------------------------- Positions //

        [SerializeField] public Vector3[] positions;

        // Copy

        public void CopyPositions<T>(T[] objects, bool local) where T : MonoBehaviour
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            positions = new Vector3[objects.Length];

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    positions[i] = objects[i].transform.localPosition;
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    positions[i] = objects[i].transform.position;
                }
            }
        }

        public void CopyPositions<T>(List<T> objects, bool local) where T : MonoBehaviour
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            positions = new Vector3[objects.Count];

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    positions[i] = objects[i].transform.localPosition;
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    positions[i] = objects[i].transform.position;
                }
            }
        }

        public void CopyPositions(Transform[] objects, bool local)
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            positions = new Vector3[objects.Length];

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    positions[i] = objects[i].localPosition;
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    positions[i] = objects[i].position;
                }
            }
        }

        public void CopyPositions(List<Transform> objects, bool local)
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            positions = new Vector3[objects.Count];

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    positions[i] = objects[i].localPosition;
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    positions[i] = objects[i].position;
                }
            }
        }

        // Paste

        public void PastePositions<T>(T[] objects, bool local) where T : MonoBehaviour
        {
            if (objects == null) return;

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].transform.localPosition = positions[i];
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].transform.position = positions[i];
                }
            }
        }

        public void PastePositions<T>(List<T> objects, bool local) where T : MonoBehaviour
        {
            if (objects == null) return;

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].transform.localPosition = positions[i];
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].transform.position = positions[i];
                }
            }
        }

        public void PastePositions(Transform[] objects, bool local)
        {
            if (objects == null) return;

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].localPosition = positions[i];
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].position = positions[i];
                }
            }
        }

        public void PastePositions(List<Transform> objects, bool local)
        {
            if (objects == null) return;

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].localPosition = positions[i];
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].position = positions[i];
                }
            }
        }

        // ----------------------------------------------------------------------------------------------- Rotations //

        [SerializeField] public Quaternion[] rotations;

        // Copy

        public void CopyRotations<T>(T[] objects, bool local) where T : MonoBehaviour
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            rotations = new Quaternion[objects.Length];

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    rotations[i] = objects[i].transform.localRotation;
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    rotations[i] = objects[i].transform.rotation;
                }
            }
        }

        public void CopyRotations<T>(List<T> objects, bool local) where T : MonoBehaviour
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            rotations = new Quaternion[objects.Count];

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    rotations[i] = objects[i].transform.localRotation;
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    rotations[i] = objects[i].transform.rotation;
                }
            }
        }

        public void CopyRotations(Transform[] objects, bool local)
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            rotations = new Quaternion[objects.Length];

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    rotations[i] = objects[i].localRotation;
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    rotations[i] = objects[i].rotation;
                }
            }
        }

        public void CopyRotations(List<Transform> objects, bool local)
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            rotations = new Quaternion[objects.Count];

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    rotations[i] = objects[i].localRotation;
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    rotations[i] = objects[i].rotation;
                }
            }
        }

        // Paste

        public void PasteRotations<T>(T[] objects, bool local) where T : MonoBehaviour
        {
            if (objects == null) return;

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].transform.localRotation = rotations[i];
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].transform.rotation = rotations[i];
                }
            }
        }

        public void PasteRotations<T>(List<T> objects, bool local) where T : MonoBehaviour
        {
            if (objects == null) return;

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].transform.localRotation = rotations[i];
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].transform.rotation = rotations[i];
                }
            }
        }

        public void PasteRotations(Transform[] objects, bool local)
        {
            if (objects == null) return;

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].localRotation = rotations[i];
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].rotation = rotations[i];
                }
            }
        }

        public void PasteRotations(List<Transform> objects, bool local)
        {
            if (objects == null) return;

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].localRotation = rotations[i];
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    objects[i].rotation = rotations[i];
                }
            }
        }

        // -------------------------------------------------------------------------------------------------- Scales //

        [SerializeField] public Vector3[] scales;

        // Copy

        public void CopyScales<T>(T[] objects, bool local) where T : MonoBehaviour
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            scales = new Vector3[objects.Length];

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    scales[i] = objects[i].transform.localScale;
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    scales[i] = objects[i].transform.lossyScale;
                }
            }
        }

        public void CopyScales<T>(List<T> objects, bool local) where T : MonoBehaviour
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            scales = new Vector3[objects.Count];

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    scales[i] = objects[i].transform.localScale;
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    scales[i] = objects[i].transform.lossyScale;
                }
            }
        }

        public void CopyScales(Transform[] objects, bool local)
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            scales = new Vector3[objects.Length];

            if (local)
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    scales[i] = objects[i].localScale;
                }
            }
            else
            {
                for (int i = 0; i < objects.Length; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    scales[i] = objects[i].lossyScale;
                }
            }
        }

        public void CopyScales(List<Transform> objects, bool local)
        {
            if (WriteProtectionCheck()) return;

            if (objects == null) return;

            scales = new Vector3[objects.Count];

            if (local)
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    scales[i] = objects[i].localScale;
                }
            }
            else
            {
                for (int i = 0; i < objects.Count; i++)
                {
                    if (NullCheck(objects[i], i)) continue;

                    scales[i] = objects[i].lossyScale;
                }
            }
        }

        // Paste

        /// <summary>
        ///     Copies the values contained in this caches scales array into the given objects/transforms localScale.
        ///
        ///     lossyScale has no setter, so locality isn't an option here.
        /// </summary>
        public void PasteScales<T>(T[] objects) where T : MonoBehaviour
        {
            if (objects == null) return;

            // lossyScale has no setter.

            for (int i = 0; i < objects.Length; i++)
            {
                if (NullCheck(objects[i], i)) continue;

                objects[i].transform.localScale = scales[i];
            }
        }

        /// <summary>
        ///     Copies the values contained in this caches scales array into the given objects/transforms localScale.
        ///
        ///     lossyScale has no setter, so locality isn't an option here.
        /// </summary>
        public void PasteScales<T>(List<T> objects) where T : MonoBehaviour
        {
            if (objects == null) return;

            // lossyScale has no setter.

            for (int i = 0; i < objects.Count; i++)
            {
                if (NullCheck(objects[i], i)) continue;

                objects[i].transform.localScale = scales[i];
            }
        }

        /// <summary>
        ///     Copies the values contained in this caches scales array into the given objects/transforms localScale.
        ///
        ///     lossyScale has no setter, so locality isn't an option here.
        /// </summary>
        public void PasteScales(Transform[] objects)
        {
            if (objects == null) return;

            // lossyScale has no setter.

            for (int i = 0; i < objects.Length; i++)
            {
                if (NullCheck(objects[i], i)) continue;

                objects[i].localScale = scales[i];
            }
        }

        /// <summary>
        ///     Copies the values contained in this caches scales array into the given objects/transforms localScale.
        ///
        ///     lossyScale has no setter, so locality isn't an option here.
        /// </summary>
        public void PasteScales(List<Transform> objects)
        {
            if (objects == null) return;

            // lossyScale has no setter.

            for (int i = 0; i < objects.Count; i++)
            {
                if (NullCheck(objects[i], i)) continue;

                objects[i].localScale = scales[i];
            }
        }

        // -------------------------------------------------------------------------------------------------- Errors //

        public bool NullCheck(object o, int i)
        {
            if (o != null) return false;

            Debug.LogWarning($"Can't copy/paste object at index [{i}] because it is null.");

            return true;
        }

        public bool NullCheck(Transform t, int i)
        {
            if (t != null) return false;

            Debug.LogWarning($"Can't copy/paste transform at index [{i}] because it is null.");

            return true;
        }

        // ---------------------------------------------------------------------------------------- Write-protection //

        public bool WriteProtectionCheck()
        {
            if (!writeProtected) return false;
            
            Debug.LogWarning($"The HierarchyCache [{name}] is currently write-protected to prevent " +
                             $"accidental over-writing. Please un-tick the writeProtected flag on this asset " +
                             $"file to re-enable all Copy methods.");
            return true;

        }
    }
}