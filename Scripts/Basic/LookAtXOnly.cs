﻿using UnityEngine;

namespace Vanilla.Transforms
{

	public class LookAtXOnly : ActiveTargetBehaviour
	{

		private void Update()
		{
			t.LookAtXOnly(target);
		}


		public override Transform FindTarget()
		{
			return _target;
		}

	}

}