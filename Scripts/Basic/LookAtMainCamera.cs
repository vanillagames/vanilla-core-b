﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Transforms;

namespace Vanilla.Transforms
{

	public class LookAtMainCamera : ActiveTargetBehaviour
	{

		void Update()
		{
			t.LookAt(target);
		}


		public override Transform FindTarget()
		{
			return Camera.main.transform;
		}

	}

}