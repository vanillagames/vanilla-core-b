﻿using UnityEngine;

namespace Vanilla.Transforms
{

	public class DebugCam : ActiveBehaviour
	{

		public enum TranslationType
		{

			Direct,
			TargetDirect,
			TargetLerp

		}

		[Header("KeyCodes")]
		public KeyCode controlKey = KeyCode.Mouse2;

		public float defaultMoveSpeed = 0.1f;

		public float fastMoveSpeed = 1.0f;

		public KeyCode forwardKey = KeyCode.W;
		public KeyCode leftKey    = KeyCode.A;
		public KeyCode backKey = KeyCode.S;
		public KeyCode rightKey = KeyCode.D;

		public float lookSmoothing = 16.0f;

		public float moveSmoothing = 8.0f;

		[Header("Moving")]
		public TranslationType positionTranslationMode = TranslationType.TargetLerp;

		[Header("Looking")]
		public TranslationType rotationTranslationMode = TranslationType.TargetLerp;

		[SerializeField] [ReadOnly]
		private Vector3 targetEulers;
		
		[SerializeField] [ReadOnly]
		private Vector3 targetPosition;

		public KeyCode turboKey     = KeyCode.LeftShift;
		public float   xSensitivity = 8.0f;

		public float ySensitivity = 8.0f;


		private void OnEnable()
		{
			#if UNITY_EDITOR
			targetPosition = t.position;
			targetEulers   = t.eulerAngles;
			#endif
		}


		private void Update()
		{
			#if UNITY_EDITOR
			
			switch (positionTranslationMode)
			{
				case TranslationType.Direct:

					DirectPosition();

					break;

				case TranslationType.TargetDirect:

					TargetPositionDirect();

					break;

				case TranslationType.TargetLerp:

					TargetPositionLerp();

					break;
			}

			switch (rotationTranslationMode)
			{
				case TranslationType.Direct:

					DirectRotation();

					break;

				case TranslationType.TargetDirect:

					TargetRotationDirect();

					break;

				case TranslationType.TargetLerp:

					TargetRotationLerp();

					break;
			}

			#endif
		}


		private void DirectPosition()
		{
			if (!Input.GetKey(controlKey)) return;

			t.position = GetFramePosition(currentPosition: t.position,
			                              currentSpeed: Input.GetKey(turboKey) ?
				                                            fastMoveSpeed :
				                                            defaultMoveSpeed);
		}


		private void TargetPositionDirect()
		{
			if (Input.GetKey(controlKey))
				targetPosition = GetFramePosition(currentPosition: targetPosition,
				                                  currentSpeed: Input.GetKey(turboKey) ?
					                                                fastMoveSpeed :
					                                                defaultMoveSpeed);

			t.position = targetPosition;
		}


		private void TargetPositionLerp()
		{
			if (Input.GetKey(controlKey))
				targetPosition = GetFramePosition(currentPosition: targetPosition,
				                                  currentSpeed: Input.GetKey(turboKey) ?
					                                                fastMoveSpeed :
					                                                defaultMoveSpeed);

			t.position = Vector3.Lerp(a: t.position,
			                          b: targetPosition,
			                          t: Time.deltaTime * moveSmoothing);
		}


		private void DirectRotation()
		{
			if (!Input.GetKey(controlKey)) return;

			t.eulerAngles = GetTargetEulers(t.eulerAngles);
		}


		private void TargetRotationDirect()
		{
			if (Input.GetKey(controlKey)) targetEulers = GetTargetEulers(targetEulers);

			t.eulerAngles = targetEulers;
		}


		private void TargetRotationLerp()
		{
			if (Input.GetKey(controlKey)) targetEulers = GetTargetEulers(targetEulers);

			t.rotation = Quaternion.Lerp(a: t.rotation,
			                             b: Quaternion.Euler(targetEulers),
			                             t: Time.deltaTime * lookSmoothing);
		}


		private Vector3 GetFramePosition
		(
			Vector3 currentPosition,
			float   currentSpeed)
		{
			if (Input.GetKey(forwardKey)) currentPosition += t.forward * currentSpeed;

			if (Input.GetKey(backKey)) currentPosition -= t.forward * currentSpeed;

			if (Input.GetKey(leftKey)) currentPosition -= t.right * currentSpeed;

			if (Input.GetKey(rightKey)) currentPosition += t.right * currentSpeed;

			return currentPosition;
		}


		private Vector3 GetTargetEulers
		(
			Vector3 currentEulers)
		{
			return new Vector3(x: currentEulers.x - Input.GetAxis("Mouse Y") * ySensitivity,
			                   y: currentEulers.y + Input.GetAxis("Mouse X") * xSensitivity,
			                   z: 0);
		}

	}

}
