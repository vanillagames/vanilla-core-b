﻿using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Transforms
{

	public class LookAtYOnlySmooth : ActiveTargetBehaviour
	{

		public float smoothing = 4.0f;


		private void Update()
		{
			t.DirtyLerpRotation(to: Quaternion.LookRotation(forward: ( target.position - t.position ).SquashY()),
			                    factor: Time.deltaTime / smoothing);
		}


		public override Transform FindTarget()
		{
			return Camera.main.transform;
		}

	}

}