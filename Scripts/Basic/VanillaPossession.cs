﻿using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Transforms
{

	public abstract class VanillaPossession : ActiveBehaviour
	{

		private Vector3 _directionModifier;

		private Vector3 _eulerModifier;

		private Vector3 _eulers;
		
		private Vector3 _targetDirection;

		protected Vector3 _targetPosition;

		[ReadOnly]
		private float _timer;
		[ReadOnly]
		private float _timerLength;
		[SerializeField]
		[Range(min: 0,
			max: 0.25f)]
		public float directionScalar = 0.01f;

		[Header("Position")] [SerializeField]
		public bool hauntPosition;

		[Header("Rotation")] [SerializeField]
		public bool hauntRotation;
		[Header("Targets")] [SerializeField]
		private Range newTargetTimerRange = new Range(min: 0.1f,
		                                              max: 1.0f);
		[SerializeField]
		[Range(min: 0,
			max: 1)]
		public float rotationScalar = 0.01f;
		[SerializeField]
		[Range(min: 0.1f,
			max: 20.0f)]
		public float rotationSpeedLimit = 1.0f;


		private void Update()
		{
			if (_timer.HasElapsedRandomizedLength(timerLength: ref _timerLength,
			                                      timerLengthRange: ref newTargetTimerRange))
			{
				_targetPosition = NewTargetPosition();

				_targetDirection = _targetPosition - t.localPosition;
			}

			if (hauntPosition) HauntPosition();

			if (hauntRotation) HauntRotation();
		}


		public void HauntPosition()
		{
			var p = t.localPosition;

			_directionModifier += _targetDirection * directionScalar;

			p = ClampPosition(p + _directionModifier);

			t.localPosition = p;
		}


		public void HauntRotation()
		{
			_eulerModifier =
				( _eulerModifier + _targetDirection * rotationScalar ).GetBoxClamp(min: -rotationSpeedLimit,
				                                                                   max: rotationSpeedLimit);

			_eulers += _eulerModifier;

			t.localEulerAngles = _eulers;
		}


		public abstract Vector3 NewTargetPosition();


		public abstract Vector3 ClampPosition
		(
			Vector3 p);

	}

}