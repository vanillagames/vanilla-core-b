﻿#if UNITY_EDITOR || DEVELOPMENT_BUILD
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanilla;
using Vanilla.Basic;
using Vanilla.Math;

namespace Vanilla.Basic
{
    public class TransformCompassArm : VanillaBehaviour
    {
        [SerializeField] private TransformCompass _compass;
        public TransformCompass compass =>
            _compass != null ? _compass : _compass = GetComponentInParent<TransformCompass>();

        public Transform pipe;
        public Transform pointerPivot;

        [SerializeField] private Axis3D _axis = Axis3D.X;
        public Axis3D axis
        {
            get => _axis;
            set
            {
                _axis = value;

                NameSelf(axis.ToString());
            }
        }
        
        [SerializeField] private float _length = 0.5f;
        public float Length
        {
            get => _length;
            set
            {
                _length = value;

                ValidateAll();
            }
        }

//        public void UpdatePivot(Vector3 pivotPosition, Vector3 pivotEulers, Vector3 pivotScale)
//        {
//            pointerPivot.localPosition = pivotPosition;
//            pointerPivot.localEulerAngles = pivotEulers;
//            pointerPivot.localScale = pivotScale;
//        }

//        public void UpdatePipe(Vector3 pipePosition, Vector3 pipeScale)
//        {
//            pipe.localPosition = pipePosition;
//            pipe.localScale = pipeScale;
//        }
        
//        public void UpdateArm(bool invertPointer, bool usePipes, float length, float pipePosition, float pointerScale, float pipeWidth, float pipeGap)
//        {
////            length.Clamp(TransformCompass.minLength, TransformCompass.maxLength);
////            
////            pointerScale.Clamp(TransformCompass.minLength * 0.25f, length * 0.25f);
//            
//            pointerPivot.localPosition = new Vector3(0, 0, length);
//
//            pointerPivot.localEulerAngles = invertPointer ? Vector3.right * 180.0f : Vector3.zero;
//
//            pointerPivot.localScale = Vector3.one * pointerScale;
//            
//            if (!usePipes)
//            {
//                pipe.gameObject.SetActive(false);
//
//                return;
//            }
//
//            if (!invertPointer)
//            {
//                pipePosition -= pointerScale * 0.5f;
//            }
//            
//            pipe.localPosition = pipePosition * Vector3.forward;
//
//            var pipeLength = pipePosition - pipeGap;
//
//            if (pipeLength.IsZeroOrLower())
//            {
//                pipe.gameObject.SetActive(false);
//
//                return;
//            }
//            
//            pipe.gameObject.SetActive(true);
//
//            pipe.localScale = new Vector3(pipeWidth, pipeLength, pipeWidth);
//        }
        
        protected override void InSceneValidation()
        {
            ValidateAll();
        }

        private void ValidateAll()
        {
            if (!pipe || !pointerPivot)
            {
                Warning("We're missing some references over here!");
                return;
            }

            ValidateAxis();
            
            ValidateMeasurements();
        }

        void ValidateAxis()
        {
            NameSelf(axis.ToString());

            gameObject.SetActive(_axis != Axis3D.None);

            var renderers = GetComponentsInChildren<MeshRenderer>();

            var block = new MaterialPropertyBlock();

            switch (_axis)
            {
                case Axis3D.X:

                    transform.localEulerAngles = Vector3.up * 90.0f;

                    block.SetColor(name: TransformCompass.materialBlockColorName,
                                   value: new Color(r: 1, 
                                                    g: 0, 
                                                    b: 0, 
                                                    a: 0.25f));

                    break;

                case Axis3D.Y:

                    transform.localEulerAngles = Vector3.right * -90.0f;

                    block.SetColor(name: TransformCompass.materialBlockColorName,
                                   value: new Color(r: 0, 
                                                    g: 1, 
                                                    b: 0, 
                                                    a: 0.25f));

                    break;

                case Axis3D.Z:
                case Axis3D.None:

                    transform.localEulerAngles = Vector3.zero;

                    block.SetColor(name: TransformCompass.materialBlockColorName,
                                   value: new Color(
                                                    r: 0, 
                                                    g: 0, 
                                                    b: 1, 
                                                    a: 0.25f));

                    break;
            }

            foreach (var v in renderers)
            {
                v.SetPropertyBlock(block, 0);
            }
        }

        void ValidateMeasurements()
        {
            _length.Clamp(
                min: TransformCompass.minLength, 
                max: TransformCompass.maxLength);
            
            pointerPivot.localPosition = new Vector3(
                x: 0, 
                y: 0, 
                z: _length);
            
            pointerPivot.localEulerAngles = compass.InvertPointer ? 
                Vector3.right * 180.0f : 
                Vector3.zero;
            
            pointerPivot.localScale = Vector3.one * compass.PointerScale;

            if (!compass.Pipes)
            {
                pipe.gameObject.SetActive(false);

                return;
            }

            var pipeLength = _length * 0.5f;

            if (!compass.InvertPointer)
            {
                pipeLength -= compass.PointerScale * 0.5f;
            }
            
            pipe.localPosition = pipeLength * Vector3.forward;
            
            pipeLength -= compass.PipeGap;

            if (pipeLength.IsZeroOrLower())
            {
                pipe.gameObject.SetActive(false);
                
                return;
            }
            
            pipe.gameObject.SetActive(true);

            pipe.localScale = new Vector3(
                x: compass.PipeWidth, 
                y: pipeLength, 
                z: compass.PipeWidth);

        }
    }
}
#endif
