﻿#if UNITY_EDITOR || DEVELOPMENT_BUILD
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Vanilla.Math;

namespace Vanilla.Basic
{
    public interface ITransformCompass
    {
        bool InvertPointer { get; set; }
        bool Pipes { get; set; }
        float Length { get; set; }
        float PointerScale { get; set; }
        float PipeWidth { get; set; }
        float PipeGap { get; set; }
    }
    
    [ExecuteInEditMode]
    public class TransformCompass : VanillaBehaviour, ITransformCompass
    {
//        public Material xMaterial;
//        public Material yMaterial;
//        public Material zMaterial;
        
        [SerializeField, ReadOnly]
        private TransformCompassArm _x;
        public TransformCompassArm x
        {
            get
            {
                if (_x != null) return _x;
                
                foreach (var a in GetComponentsInChildren<TransformCompassArm>())
                {
                    Log($"This arms axis is {a.axis}");
                    
                    if (a.axis != Axis3D.X) continue;

                    _x = a;
                }

                return _x;
            }
        }
        
        [SerializeField, ReadOnly]
        private TransformCompassArm _y;
        public TransformCompassArm y
        {
            get
            {
                if (_y != null) return _y;
                
                foreach (var a in GetComponentsInChildren<TransformCompassArm>())
                {
                    if (a.axis != Axis3D.Y) continue;

                    _y = a;
                }

                return _y;
            }
        }
        
        [SerializeField, ReadOnly]
        private TransformCompassArm _z;
        public TransformCompassArm z
        {
            get
            {
                if (_z != null) return _z;
                
                foreach (var a in GetComponentsInChildren<TransformCompassArm>())
                {
                    if (a.axis != Axis3D.Z) continue;

                    _z = a;
                }

                return _z;
            }
        }

        public const float minLength = 0.1f;
        public const float maxLength = 10.0f;

        public const string materialBlockColorName = "_Color";
        public const string SubtitleIfNoParent = " ~ ";
        
        [SerializeField] private bool _invertPointer;
        public bool InvertPointer
        {
            get => _invertPointer;
            set
            {
                _invertPointer = value;

                ValidateAll();
            }
        }

        [SerializeField] private bool _usePipes;
        public bool Pipes
        {
            get => _usePipes;
            set
            {
                _usePipes = value;

                ValidateAll();
            }
        }

        [SerializeField] private float _length = 0.5f;
        public float Length
        {
            get => _length;
            set
            {
                _length = value;

                ValidateAll();
            }
        }

        [SerializeField] private float _pointerScale = 0.1f;
        public float PointerScale
        {
            get => _pointerScale;
            set
            {
                _pointerScale = value;


                ValidateAll();
            }
        }

        [SerializeField] private float _pipeWidth = 0.025f;
        public float PipeWidth
        {
            get => _pipeWidth;
            set
            {

                ValidateAll();
            }
        }

        [SerializeField] private float _pipeGap = 0.05f;
        public float PipeGap
        {
            get => _pipeGap;
            set
            {
                _pipeGap = value;

                ValidateAll();
            }
        }

        protected override void InSceneValidation()
        {
            ValidateAll();
        }

        private void ValidateAll()
        {
            ValidateSelf();
        }

        public void ValidateSelf()
        {
//            NameSelf(transform.parent ? transform.parent.name : SubtitleIfNoParent);
            
            _length.Clamp(
                min: minLength, 
                max: maxLength);

            _pointerScale.Clamp(
                min: minLength * 0.25f, 
                max: _length * 0.25f);
            
            _pipeWidth.Clamp(
                min: minLength * 0.125f, 
                max: _pointerScale);

            _pipeGap.Clamp(
                min: 0, 
                max: _length * 0.25f);

            x.Length = _length;
            y.Length = _length;
            z.Length = _length;
        }

        public void SetArmLengths(float xLength, float yLength, float zLength)
        {
            x.Length = xLength;
            y.Length = yLength;
            z.Length = zLength;
        }
        
//        private void ValidateDependencies()
//        {
//            var _pipePosition = _length * 0.5f;
//
//            if (x)
//                x.UpdateArm(
//                    invertPointer: _invertPointer,
//                    usePipes: _usePipes, 
//                    length: _length,
//                    pipePosition: _pipePosition,
//                    pointerScale: _pointerScale,
//                    pipeWidth: _pipeWidth,
//                    pipeGap: _pipeGap);
//            
//            if (y)
//                y.UpdateArm(
//                    invertPointer: _invertPointer,
//                    usePipes: _usePipes, 
//                    length: _length,
//                    pipePosition: _pipePosition,
//                    pointerScale: _pointerScale,
//                    pipeWidth: _pipeWidth,
//                    pipeGap: _pipeGap);
//            
//            if (z)
//                z.UpdateArm(
//                    invertPointer: _invertPointer,
//                    usePipes: _usePipes, 
//                    length: _length,
//                    pipePosition: _pipePosition,
//                    pointerScale: _pointerScale,
//                    pipeWidth: _pipeWidth,
//                    pipeGap: _pipeGap);
//        }

        private void ValidateDependencies()
        {
//            var pivotPosition = new Vector3(0, 0, _length);
//            var pivotEulers = _invertPointer ? Vector3.right * 180.0f : Vector3.zero;
//            var pivotScale = Vector3.one * _pointerScale;
//            
//            if (x)
//            {
//                x.UpdatePivot(
//                    pivotPosition: pivotPosition, 
//                    pivotEulers: pivotEulers, 
//                    pivotScale: pivotScale);
//            }
//
//            if (y)
//            {
//                y.UpdatePivot(
//                    pivotPosition: pivotPosition, 
//                    pivotEulers: pivotEulers, 
//                    pivotScale: pivotScale);
//            }
//            
//            if (z)
//            {
//                z.UpdatePivot(
//                    pivotPosition: pivotPosition, 
//                    pivotEulers: pivotEulers, 
//                    pivotScale: pivotScale);
//            }

//            if (!_usePipes)
//            {
//                x.pipe.gameObject.SetActive(false);
//                y.pipe.gameObject.SetActive(false);
//                z.pipe.gameObject.SetActive(false);
//                
//                return;
//            }
            
//            var pipeLength = _length * 0.5f;

//            if (!_invertPointer)
//            {
//                pipeLength -= _pointerScale * 0.5f;
//            }

//            var pipePosition = pipeLength * Vector3.forward;

//            pipeLength -= _pipeGap;
            
//            if (pipeLength.IsZeroOrLower())
//            {
//                x.pipe.gameObject.SetActive(false);
//                y.pipe.gameObject.SetActive(false);
//                z.pipe.gameObject.SetActive(false);
//                
//                return;
//            }
            
//            x.pipe.gameObject.SetActive(true);
//            y.pipe.gameObject.SetActive(true);
//            z.pipe.gameObject.SetActive(true);

//            var pipeScale = new Vector3(_pipeWidth, pipeLength, _pipeWidth);
            
//            if (x)
//            {
//                x.UpdatePipe(
//                    pipePosition: pipePosition, 
//                    pipeScale: pipeScale);
//            }
//
//            if (y)
//            {
//                y.UpdatePipe(
//                    pipePosition: pipePosition, 
//                    pipeScale: pipeScale);
//            }
//            
//            if (z)
//            {
//                z.UpdatePipe(
//                    pipePosition: pipePosition, 
//                    pipeScale: pipeScale);
//            }
        }
    }
}
#endif