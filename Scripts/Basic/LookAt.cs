﻿using UnityEngine;

using Vanilla.Transforms;

namespace Vanilla
{
	public class LookAt : ActiveTargetBehaviour
	{
		public void Update()
		{
			if (!target) return;

			t.LookAt(target);
		}
		
		public override Transform FindTarget()
		{
			return _target;
		}

	}
}