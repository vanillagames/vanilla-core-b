﻿using UnityEngine;

using Vanilla.Math;
using Vanilla.Transforms;

namespace Vanilla.Transforms
{

	public class LookAtAdvanced : ActiveTargetBehaviour
	{

		public enum LookAtLock
		{

			None,
			YOnly,
			XOnly

		}

		private Quaternion baseRotation;

		[ReadOnly]
		public bool canSeeTarget;

		public bool clamp;

		[Range(min: 0,
			max: 90.0f)]
		public float clampDegrees = 45.0f;

		public Transform clampReferenceTransform;

		[Header("Look Settings")]
		public bool flip;

		public InterpolationType interpolation = InterpolationType.EaseInAndOut;

		public LookAtLock lookAtLock = LookAtLock.None;

		public bool lookForwardIfTargetOutsideClampRange;

		[Header("Look Speed")]
		public float speed = 8.0f;


		protected override void Awake()
		{
			base.Awake();

			baseRotation = t.localRotation;
		}


		private void Update()
		{

			if (!_target) return; 

			canSeeTarget = t.IsLookingAt(target: _target,
			                          marginOfErrorInDegrees: clampDegrees);

			var dir = flip ?
				          t.position       - _target.position :
				          _target.position - t.position;

			switch (lookAtLock)
			{
				case LookAtLock.YOnly:

					dir.y = 0.0f;

					break;

				case LookAtLock.XOnly:

					dir.x = 0.0f;

					break;
			}

			var targetRotation = dir == Vector3.zero ?
				                     Quaternion.identity :
				                     Quaternion.LookRotation(dir);

			if (clamp)
			{
				if (clampReferenceTransform) baseRotation = clampReferenceTransform.rotation;

				if (lookForwardIfTargetOutsideClampRange)
				{
					if (canSeeTarget)
						targetRotation.Clamp(baseQuaternion: baseRotation,
						                     limitInDegrees: clampDegrees);
					else
						targetRotation = baseRotation;
				}
				else
				{
					targetRotation.Clamp(baseQuaternion: baseRotation,
					                     limitInDegrees: clampDegrees);
				}
			}

			t.DirtyLerpRotation(to: targetRotation,
			                    factor: ( Time.deltaTime * speed ).GetInterpolate(interpolation));
		}


		private void OnDrawGizmosSelected()
		{
			if (!CanDrawGizmos()) return;

			DrawDebugRay(rayOrigin: transform.position,
			             rayDir: baseRotation.ToDirection(),
			             rayLength: 1.0f,
			             color: Color.magenta,
			             duration: Time.deltaTime);
		}


		public override Transform FindTarget()
		{
			if (!_target)
				return _target;
			else
				return null;
		}  

	}

}