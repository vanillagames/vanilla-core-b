﻿using UnityEngine;

namespace Vanilla.Transforms
{

	public class Rotate : ActiveBehaviour
	{

		public Vector3 axis = Vector3.up;

		public Space locality = Space.Self;

		public float rate = 1.0f;


		private void Update()
		{
			t.Rotate(eulers: Time.deltaTime * rate * axis,
			         relativeTo: locality);
		}

	}

}