﻿using UnityEngine;

namespace Vanilla.Transforms
{
    public class LookAtYOnly : ActiveTargetBehaviour
    {
        void Update()
        {
            if (!target) return;
            
            t.LookAtYOnly(target);
        }


        public override Transform FindTarget()
        {
            return _target;
        }

    }
}