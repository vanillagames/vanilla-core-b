﻿using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Transforms
{

	public class ClampFromTransform : ActiveBehaviour
	{

		public Transform bounds;


		private void Update()
		{
			var bHalf = bounds.localScale * 0.5f;
			var bPos  = bounds.position;

			t.position = t.position.GetRectangleClamp(minBounds: bPos - bHalf,
			                                          maxBounds: bPos + bHalf);
		}

	}

}