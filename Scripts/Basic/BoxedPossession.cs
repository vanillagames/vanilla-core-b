﻿using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Transforms
{

	public class BoxedPossession : VanillaPossession
	{

		[Header("Cube Size")]
		public const float c_SizeMin = 0.0f;
		public const float c_SizeMax = 5.0f;

		[SerializeField]
		[Range(min: c_SizeMin,
			   max: c_SizeMax)]
		private float _cubeSize = 0.5f;

		public float cubeSize
		{
			get => _cubeSize;
			set =>
				_cubeSize = value.GetClamp(min: c_SizeMin,
				                           max: c_SizeMax);
		}


		public override Vector3 NewTargetPosition()
		{
			return VanillaMath.GetRandomCube(_cubeSize);
		}


		public override Vector3 ClampPosition
		(
			Vector3 p)
		{
			return p.GetBoxClamp(_cubeSize);
		}

	}

}