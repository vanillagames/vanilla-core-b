﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace Vanilla.Persistence.Examples
{
    
    // This needs to be a sealed off inherited class because, otherwise, it can't be serialized!
    // (...or seen in the inspector!)
    [Serializable]
    public class ExampleJsonFile : JsonFile<ExampleData> { }

    public class ExampleJsonFileScene : MonoBehaviour
    {
        [Header("Example Settings File Reader")]
        [SerializeField]
        public ExampleJsonFile exampleJsonFile;

        [ContextMenu("Read")]
        public void Read()
        {
            exampleJsonFile.ReadFromDisk();
            
            FileToUI();
        }
        
        [ContextMenu("Write")]
        public void Write()
        {
            UIToFile();
            
            exampleJsonFile.WriteToDisk();
        }
        
        // Binding InputFields to the persistent data
        
        [Header("Demonstration UI Elements")]
        public InputField ipText;
        public InputField areaText;

        public InputField playerOneName;
        public InputField playerOneHealth;
        public InputField playerOneLevel;

        public InputField playerTwoName;
        public InputField playerTwoHealth;
        public InputField playerTwoLevel;

        public InputField playerThreeName;
        public InputField playerThreeHealth;
        public InputField playerThreeLevel;

        private void OnEnable()
        {
            FileToUI();
        }

        public void ToggleAutoSave(bool newState)
        {
//            exampleFileReader.autoWrite = newState;
        }
        
        [ContextMenu("File To UI")]
        void FileToUI()
        {
            ipText.text = exampleJsonFile.data.serverIPAddress;
            areaText.text = exampleJsonFile.data.areaId.ToString();

            if (exampleJsonFile.data.players.Length > 0)
            {
                playerOneName.text = exampleJsonFile.data.players[0].name;
                playerOneHealth.text = exampleJsonFile.data.players[0].health.ToString();
                playerOneLevel.text = exampleJsonFile.data.players[0].level.ToString();
            }

            if (exampleJsonFile.data.players.Length > 1)
            {
                playerTwoName.text = exampleJsonFile.data.players[1].name;
                playerTwoHealth.text = exampleJsonFile.data.players[1].health.ToString();
                playerTwoLevel.text = exampleJsonFile.data.players[1].level.ToString();
            }

            if (exampleJsonFile.data.players.Length > 2)
            {
                playerThreeName.text = exampleJsonFile.data.players[2].name;
                playerThreeHealth.text = exampleJsonFile.data.players[2].health.ToString();
                playerThreeLevel.text = exampleJsonFile.data.players[2].level.ToString();
            }
        }

        /// <summary>
        ///     When running this, it demonstrates that changing any of the values in the example data class
        ///     will also run WriteToDisk(), effectively immediately saving the change to disk.
        ///
        ///     That's right, it's auto-save!
        /// </summary>
        [ContextMenu("UI To File")]
        public void UIToFile()
        {
//            if (!string.IsNullOrEmpty(ipText.text))
//                exampleFileReader.data.serverIPAddress = ipText.text;
//
//            if (!string.IsNullOrEmpty(areaText.text))
//                exampleFileReader.data.areaId = int.Parse(areaText.text);
//
//            if (!string.IsNullOrEmpty(playerOneName.text))
//                exampleFileReader.data.players[0].playerName = playerOneName.text;
//            if (!string.IsNullOrEmpty(playerOneHealth.text))
//                exampleFileReader.data.players[0].playerHealth = float.Parse(playerOneHealth.text);
//            if (!string.IsNullOrEmpty(playerOneLevel.text))
//                exampleFileReader.data.players[0].playerLevel = int.Parse(playerOneLevel.text);
//
//            if (!string.IsNullOrEmpty(playerTwoName.text))
//                exampleFileReader.data.players[1].playerName = playerTwoName.text;
//            if (!string.IsNullOrEmpty(playerTwoHealth.text))
//                exampleFileReader.data.players[1].playerHealth = float.Parse(playerTwoHealth.text);
//            if (!string.IsNullOrEmpty(playerTwoLevel.text))
//                exampleFileReader.data.players[1].playerLevel = int.Parse(playerTwoLevel.text);
//
//            if (!string.IsNullOrEmpty(playerThreeName.text))
//                exampleFileReader.data.players[2].playerName = playerThreeName.text;
//            if (!string.IsNullOrEmpty(playerThreeHealth.text))
//                exampleFileReader.data.players[2].playerHealth = float.Parse(playerThreeHealth.text);
//            if (!string.IsNullOrEmpty(playerThreeLevel.text))
//                exampleFileReader.data.players[2].playerLevel = int.Parse(playerThreeLevel.text);
            
            exampleJsonFile.data.serverIPAddress = ipText.text;

            exampleJsonFile.data.areaId = int.Parse(areaText.text);

            exampleJsonFile.data.players[0].name = playerOneName.text;
            exampleJsonFile.data.players[0].health = float.Parse(playerOneHealth.text);
            exampleJsonFile.data.players[0].level = int.Parse(playerOneLevel.text);

            exampleJsonFile.data.players[1].name = playerTwoName.text;
            exampleJsonFile.data.players[1].health = float.Parse(playerTwoHealth.text);
            exampleJsonFile.data.players[1].level = int.Parse(playerTwoLevel.text);

            exampleJsonFile.data.players[2].name = playerThreeName.text;
            exampleJsonFile.data.players[2].health = float.Parse(playerThreeHealth.text);
            exampleJsonFile.data.players[2].level = int.Parse(playerThreeLevel.text);
        }
    }
}