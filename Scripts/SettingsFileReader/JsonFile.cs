using System;
using System.ComponentModel;
using System.IO;
using System.Text;

using UnityEngine;
using UnityEngine.Events;

namespace Vanilla.Persistence
{
	[Serializable]
	public abstract class JsonFile<Data>
		where Data : IJsonData
	{

		[Header("Settings")]

		[SerializeField, ReadOnly]
		protected string fileName;

		[SerializeField, ReadOnly]
		private bool _loaded;
		public bool loaded => _loaded;
		
		[NonSerialized]
		private string _path;
		public string path
		{
			get
			{
				if (!string.IsNullOrEmpty(_path)) return _path;

				return _path = $"{Application.persistentDataPath}{Path.DirectorySeparatorChar}{fileName}";
			}
		}
		
		[Header("Data Class")] 
		[SerializeField]
		public Data data;

		public bool encrypt = true;

		public int encryptKey = 467247428;
		
		public void Validate<T>(T behaviour) => fileName = $"{GetType().Name}.vsf";

		public void ReadFromDisk()
		{
			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Debug.Log("Reading from disk...");
			#endif
			
			if (!File.Exists(path: path))
			{
				Debug.LogWarning(message: $"No file was found at the path\n{path}");
				
				return;
			}

			data.FromJson(json: encrypt ?
				                    File.ReadAllText(path: path).XOREncrypt(key: encryptKey) :
				                    File.ReadAllText(path: path));

//			var contents = File.ReadAllText(path: path);
//
//			if (encrypt) contents.XOREncrypt(key: encryptKey);
//
//			data.FromJson(json: contents.ToJSONObject());

			_loaded = true;
		}

		public void WriteToDisk()
		{
			#if UNITY_EDITOR
				Debug.Log("Writing to disk...");
			#endif

			// Is one of these more problematic performance/memory wise?
			
			File.WriteAllText(path: path,
			                  contents: encrypt ?
				                            data.ToJson().ToString(aIndent: 4).XOREncrypt(encryptKey) :
				                            data.ToJson().ToString(aIndent: 4));
			
//			var output = data.ToJson().ToString(aIndent: 4);
//			
//			if (encrypt) output = output.XOREncrypt(key: encryptKey);
//
//			File.WriteAllText(path: path, 
//			                  contents: output);

		}
	}
}