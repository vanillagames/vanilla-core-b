﻿using System;

using UnityEngine;

using SimpleJSON;

namespace Vanilla.Persistence.Examples
{

	// Example Data Classes

	/// <summary>
	///     Why this data structure?
	///
	///     This is a semi-complicated example on purpose to demonstrate how, theoretically, this class
	///     could even be used to store custom classes and subclasses. It goes over and above on purpose!
	///
	///     The purpose of having a static reference to the reader interfacing with this settings class is
	///     that, in each properties setter, you can decide on a per-item basis which values should require a
	///     a save to the disk or not. Some values might be session-independent!
	///
	///     Moreoever, it provides a connection to the context of the file reader and thus the greater context
	///     of the program.
	///
	///     To see it in action, right click this component in the inspector during playmode and select
	///     "Demonstrate write" from the context menu.
	/// </summary>
	[Serializable]
	public class ExampleData : IJsonData
	{

		private const string c_IPKey      = "ip";
		private const string c_AreaKey    = "area";
		private const string c_PlayersKey = "players";

		[SerializeField]
		public string serverIPAddress;

		[SerializeField]
		public int areaId;

		[SerializeField]
		public ExampleDataSubclass[] players;

		public JSONNode ToJson()
		{
			var o = new JSONObject();

			// 'Top level' fields are easily added using .Add()
			o.Add(aKey: c_IPKey,
			      aItem: serverIPAddress);

			o.Add(aKey: c_AreaKey,
			      aItem: areaId);

			// Adding array or list objects requires creating a JSONArray first and adding new JSONObjects to it.
			// Because we're 'in the land of javascript' here, you can add to the array.
			// More-over, you can use any kind of key you want when using .Add()!
			// i.e. .Add("playerThing", thing);
			// A value like this would be retrieved with the same key/value!
			var playerData = new JSONArray();

			foreach (var p in players)
			{
				var data = new JSONObject();

				data.Add(aKey: ExampleDataSubclass.c_NameKey,
				         aItem: p.name);

				data.Add(aKey: ExampleDataSubclass.c_HealthKey,
				         aItem: p.health);

				data.Add(aKey: ExampleDataSubclass.c_LevelKey,
				         aItem: p.level);

				playerData.Add(aItem: data);
			}

			o.Add(aKey: c_PlayersKey,
			      aItem: playerData);

			// The int passed in here determines how many spaces-per-tab to use when pretty-printing.
			// Not passing anything in will result in minified output.
			return o;
		}


		public void FromJson(JSONNode json)
		{
			// The extracted JSON will be in string form, so string values can be assigned directly:
			serverIPAddress = json[c_IPKey];

			// If the expected value is not a string, the basic types have built-in casts like this:
			areaId = json[c_AreaKey].AsInt;

			// For arrays, simply cast it as a JSONArray first.
			JSONArray playerArray = json[c_PlayersKey].AsArray;

			// Now, you can get the count...
			players = new ExampleDataSubclass[playerArray.Count];

			// And iterate through!
			for (var i = 0; i < playerArray.Count; i++) players[i].FromJson(playerArray[i]);
		}

	}

	[Serializable]
	public class ExampleDataSubclass : IJsonData
	{

		// Keys
		
		public const string c_NameKey   = "name";
		public const string c_HealthKey = "health";
		public const string c_LevelKey  = "level";

		// Data

		[SerializeField]
		public string name;

		[SerializeField]
		public float health;

		[SerializeField]
		public int level;

		// Conversions

		public JSONNode ToJson()
		{
			var o = new JSONObject();

			o.Add(aKey: c_NameKey,
			      aItem: name);

			o.Add(aKey: c_HealthKey,
			      aItem: health);

			o.Add(aKey: c_LevelKey,
			      aItem: level);

			return o;
		}


		public void FromJson(JSONNode o)
		{
			name   = o[c_NameKey];
			health = o[c_HealthKey].AsFloat;
			level  = o[c_LevelKey].AsInt;
		}

	}

}