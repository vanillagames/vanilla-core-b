﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameObjectPool : MonoBehaviour
{
    [Header("Object Prefab")]
    [Header("[ GameObject Pool ]")]
    public GameObject prefab;
    
    [Header("Get / Return Switches")]
    [Tooltip("If true, GameObjects obtained via Get() will automatically be turned on (local to the hierarchy).")]
    public bool getActivatesObject;
    [Tooltip("If true, GameObjects obtained via Get() will automatically be re-parented to the inUseParent transform.")]
    public bool getReparentsObject;
        
    [Tooltip("The parent that objects will be re-parented to when accessed via Get(), if the option is turned on.")]
    public Transform targetParent;
        
    [Space(10), Tooltip("If true, GameObjects given back via Return() will automatically be turned off (local to the hierarchy).")]
    public bool returnDeactivatesObject;
    [Tooltip("If true, GameObjects given back via Return() will automatically be re-parented to this GameObjects Transform.")]
    public bool returnResetsParent;
    [Tooltip("If true, GameObjects given back via Return() will automatically have their localPosition set to Vector3.zero.")]
    public bool returnResetsPosition;
    [Tooltip("If true, GameObjects given back via Return() will automatically have their localEulerAngles set to Vector3.zero.")]
    public bool returnResetsRotation;
    [Tooltip("If true, GameObjects given back via Return() will automatically have their localScale set to Vector3.one.")]
    public bool returnResetsScale;


    [Header("Count Settings")] public bool limitless;

    public bool fillOnAwake;
    
    public int size = 10;

    private int _totalCount;

    [SerializeField]
    private PoolLevel level;

    protected int totalCount
    {
        get => _totalCount;
        set
        {
            _totalCount = Mathf.Clamp(value, 0, int.MaxValue);

            UpdatePoolLevel();
        }
    }

    [Header("Unused Objects")]
    [SerializeField]
    private List<GameObject> _unused;

    public List<GameObject> unused => _unused ?? (_unused = new List<GameObject>());

    [Header("In-Use Objects")]
    [SerializeField]
    private List<GameObject> _used;

    public List<GameObject> used => _used ?? (_used = new List<GameObject>());
    
    public enum PoolLevel
    {
        Empty,
        Populated,
        Full,
        Overflow
    }

    // ----------------------------------------------------------------------------------------------- Initialization

    public virtual void Awake()
    {
        if (fillOnAwake)
        {
            Fill();
        }
    }
    
    // ----------------------------------------------------------------------------------------------- Level management

    [ContextMenu("Fill")]
    /// <summary>
    /// Spawns pool objects until the level is full.
    /// </summary>
    public void Fill()
    {
        while (totalCount < size)
        {
            CreateNew();
        }
    }

    [ContextMenu("Empty")]
    /// <summary>
    /// Destroys all tracked instances, used or unused.
    /// </summary>
    public void Empty()
    {
        while (used.Count > 0)
        {
            Destroy(used[0]);
        }

        while (unused.Count > 0)
        {
            Destroy(unused[0]);
        }
    }

    [ContextMenu("Trim")]
    /// <summary>
    /// Repeatedly destroys instances until the pool is no longer overflowing.
    /// Preferences unused instances over used instances.
    /// </summary>
    public void Trim()
    {
        while (totalCount > size && unused.Count > 0)
        {
            Destroy(unused[0]);
        }

        while (totalCount > size && used.Count > 0)
        {
            Destroy(used[0]);
        }
    }

    // ----------------------------------------------------------------------------------------------- Object borrowing

    [ContextMenu("Get")]
    /// <summary>
    /// Returns a pre-existing unused instance of this pools object, or spawns a new one if the pool is not full.
    /// </summary>
    public virtual GameObject Get()
    {
        GameObject t;

        if (unused.Count > 0)
        {
            t = unused[0];

            TurnOn(t);
        }
        else
        {
            t = Spawn();

            if (t)
            {
                TurnOn(t);
            }
        }

        return t;
    }

    /// <summary>
    /// Deactivates a given pool object and returns it to the unused list.
    /// </summary>
    public virtual void Return(GameObject target)
    {
        if (PoolObjectIsNull(target)) return;

        SwitchToUnusedList(target);

		if (returnDeactivatesObject) {
        	target.SetActive(false);
		}

        if (returnResetsParent)
        {
            target.transform.SetParent(transform);

            if (returnResetsPosition)
            {
                target.transform.localPosition = Vector3.zero;
            }

            if (returnResetsRotation)
            {
                target.transform.localEulerAngles = Vector3.zero;
            }
        }
        else
        {
            if (returnResetsPosition)
            {
                target.transform.position = Vector3.zero;
            }

            if (returnResetsPosition)
            {
                target.transform.eulerAngles = Vector3.zero;
            }
        }

        if (returnResetsScale)
        {
            target.transform.localScale = Vector3.one;
        }
    }

    [ContextMenu("Return All")]
    /// <summary>
    /// Deactivates all pool objects currently in use and returns them to the unused list.
    /// </summary>
    public virtual void ReturnAll()
    {
        while (used.Count > 0)
        {
            Return(used[0]);
        }
    }

    // ------------------------------------------------------------------------------------------------ Object creation

    [ContextMenu("Spawn")]
    /// <summary>
    /// Creates a new instance of the pool prefab as long as it doesn't exceed any of the set limitations.
    /// </summary>
    /// <returns></returns>
    public virtual GameObject Spawn()
    {
        if (limitless)
            return CreateNew();

        switch (level)
        {
            case PoolLevel.Full:
            case PoolLevel.Overflow:
                Debug.LogWarning($"No new instances of [{prefab.name}] may be created; the pool is [{level}].");
                return null;

            default:
                return CreateNew();
        }
    }

    [ContextMenu("Destroy")]
    /// <summary>
    /// Untracks and destroys a nominated pool object.
    /// </summary>
    public virtual void Destroy(GameObject target)
    {
        if (PoolObjectIsNull(target)) return;

        if (level == PoolLevel.Empty)
        {
            Debug.LogError(
                "This pool object cannot be destroyed because the pool is already marked as" +
                " Empty. How did this happen?");
            return;
        }

        if (unused.Contains(target))
        {
            unused.Remove(target);
        }
        else if (used.Contains(target))
        {
            used.Remove(target);
        }
        else
        {
            Debug.LogWarning(
                $"This pool object [{target.name}] is about to be destroyed but" +
                $" it is also not featured in either the used or unused lists. How did this happen?");
        }

        totalCount--;

        if (Application.isPlaying)
        {
            Destroy(target);
        }
        else
        {
            DestroyImmediate(target);
        }
    }

    // ------------------------------------------------------------------------------------------------------- Internal

    /// <summary>
    /// Spawns a new GameObject instance of this pools prefab, without regard to the pool size or limits.
    /// Use Get() instead if use of the pools limitation system is desired.
    /// </summary>
    protected virtual GameObject CreateNew()
    {
        if (prefab == null)
        {
            Debug.LogError("This pools prefab slot is empty and thus no new instances can be created.");
            return null;
        }
		
		GameObject g = null;
		
		if (Application.isPlaying) {
        	g = Instantiate(prefab, transform);
		} else {
            #if UNITY_EDITOR
			    g = PrefabUtility.InstantiatePrefab(prefab, transform) as GameObject;
            #endif
		}
        
        unused.Add(g);

        totalCount++;

        g.SetActive(false);

        return g;
    }

    /// <summary>
    /// Turns on a pool object.
    /// </summary>
    protected virtual void TurnOn(GameObject target)
    {
        if (PoolObjectIsNull(target)) return;

        SwitchToUsedList(target);

		if (getActivatesObject) {
        	target.SetActive(true);
		}
		
		if (getReparentsObject)
        {
            target.transform.SetParent(targetParent);
        }
    }

    /// <summary>
    /// Turns on all unused pool objects.
    /// </summary>
    protected virtual void TurnOnAll()
    {
        while (unused.Count > 0)
        {
            TurnOn(unused[0]);
        }
    }

    private void SwitchToUnusedList(GameObject target)
    {
        used.Remove(target);
        unused.Add(target);
    }

    private void SwitchToUsedList(GameObject target)
    {
        unused.Remove(target);

        used.Add(target);
    }

    public void UpdatePoolLevel()
    {
        if (totalCount <= 0)
        {
            level = PoolLevel.Empty;
        }
        else if (totalCount == size)
        {
            level = PoolLevel.Full;
        }
        else if (totalCount > size)
        {
            level = PoolLevel.Overflow;
        }
        else
        {
            level = PoolLevel.Populated;
        }
    }

    private bool PoolObjectIsNull(GameObject target)
    {
        if (target == null)
        {
            Debug.LogWarning("This pool object cannot be worked with because it is already null.");
            return true;
        }
        else
        {
            return false;
        }
    }
}