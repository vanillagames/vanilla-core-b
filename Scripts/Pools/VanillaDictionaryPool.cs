﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
    using UnityEditor;
#endif

using RotaryHeart.Lib.SerializableDictionary;

using Object = UnityEngine.Object;

namespace Vanilla
{
    /*
    VanillaDictionaryPool<T> is a generic object pool for prefabs with a VanillaBehaviour.
    
    Unlike a standard VanillaPool<T>, a VanillaDictionaryPool moves used pool objects of type T1
    into a dictionary with a key of value type T0. 
    
    If a more generic object pool is required for use with prefabs that don't need or have any
    kind of scripts attached, another kind of pool should be made based on this script.
    */
    
    [Serializable]
    public abstract class VanillaDictionaryPool<T0, T1, T2, T3> : VanillaClass 
//        where T0 : struct                                                 // The dictionary key type     (byte)
        where T1 : VanillaBehaviour, IDictionaryPoolable<T0, T1, T2, T3>  // The dictionary value type   (UNetConnection<T>)
        where T2 : SerializableDictionaryBase<T0, T1>, new()              // The dictionary type         (UNetConnectionDictionary<T>)
        where T3 : VanillaDictionaryPool<T0,T1,T2,T3>                     // The pool type (this!)       (UNetDictionaryPool<T>)
    {
        [Header("Object Prefab")]
        [Header("[ Vanilla Dictionary Pool ]")]
        public GameObject prefab;

        [Header("Get / Return Switches")]
        [Tooltip("If true, GameObjects obtained via Get() will automatically be turned on (local to the hierarchy).")]
        public bool getActivatesObject;
        [Tooltip("If true, GameObjects given back via Return() will automatically be turned off (local to the hierarchy).")]
        public bool returnDeactivatesObject;
        
        [Space(10), Tooltip("If true, GameObjects obtained via Get() will automatically be reparented to the inUseParent transform.")]
        public bool getReparentsObject;
        [Tooltip("If true, GameObjects given back via Return() will automatically be re-parented to this GameObjects Transform.")]
        public bool returnReparentsObject;
        
        [Space(10), Tooltip("The parent that objects will be reparented to when accessed via Get(), if the option is turned on.")]
        public Transform targetUsedParent;

        [Space(10), Tooltip("The parent that objects will be reparented to when no longer used via Return(), if the option is turned on.")]
        public Transform targetUnusedParent;
        
        [Space(10), Tooltip("If true, GameObjects given back via Return() will automatically have their localPosition set to Vector3.zero.")]
        public bool returnResetsPosition;
        [Tooltip("If true, GameObjects given back via Return() will automatically have their localEulerAngles set to Vector3.zero.")]
        public bool returnResetsRotation;
        [Tooltip("If true, GameObjects given back via Return() will automatically have their localScale set to Vector3.one.")]
        public bool returnResetsScale;

        [Header("Count Settings")]
        public bool limitless;

        [Tooltip("If true, calling Get() when all objects are currently in use will result in the object " +
                 "that has been active the longest being returned and handed out instead.")]
        public bool cycle;

        public int size = 10;

        [SerializeField, ReadOnly]
        private int _totalCount;
        
        [SerializeField, ReadOnly] protected PoolLevel level;
        protected int totalCount
        {
            get => _totalCount;
            set
            {
                _totalCount = Mathf.Clamp(used.Keys.Count + unused.Count, 0, limitless ? int.MaxValue : size);
                
                UpdatePoolLevel();
            }
        }
        
        [Header("Unused Objects")]
        [SerializeField]
        private List<T1> _unused;
        public List<T1> unused => _unused ?? (_unused = new List<T1>());

        [Header("In-Use Objects")]
        [SerializeField]
        private List<T0> _keys;
        public List<T0> keys => _keys ?? (_keys = new List<T0>());
        
        [SerializeField]
        private T2 _used;
        public T2 used => _used ?? (_used = new T2());

        // ------------------------------------------------------------------------------------------- Level management

        /// <summary>
        /// Spawns pool objects until the level is full.
        /// </summary>
        public virtual void Fill()
        {
            while (totalCount < size)
            {
                CreateNew();
            }
        }

        /// <summary>
        /// Destroys all tracked instances, used or unused.
        /// </summary>
        public virtual void Empty()
        {
            foreach (var kvp in used)
            {
                kvp.Value.gameObject.Destroy();
            }

            for (var i = unused.Count; i > 0; i--)
            {
                if (!SilentNullCheck(unused[0]))
                {
                    unused[0].gameObject.Destroy();
                }

                unused.RemoveAt(0);
            }

            used.Clear();
            unused.Clear();
            keys.Clear();
            
            UpdateTotalCount();
        }

        /// <summary>
        /// Repeatedly destroys instances until the pool is no longer overflowing.
        /// Preferences unused instances over used instances.
        /// </summary>
        public virtual void Trim()
        {
            while (totalCount > size && unused.Count > 0)
            {
                Destroy(unused[0]);
            }
            
            while (totalCount > size && keys.Count > 0)
            {
                Destroy(used[keys[0]]);
            }
        }

        /// <summary>
        /// Establishes if the pool has too few or too many objects and then runs Fill() or Trim() accordingly.
        /// </summary>
        public virtual void BalanceTotal()
        {
            if (totalCount == size) return;
            
            if (totalCount < size)
            {
                Fill();
            }
            else
            {
                Trim();
            }
        }

        public virtual void BalanceTotal(int count)
        {
            if (count > size)
            {
                count = size;
            }
            
            if (totalCount == count) return;
            
            if (totalCount < count)
            {
                Fill();
            }
            else
            {
                Trim();
            }
        }

        public virtual void Clean()
        {
            for (var i = 0; i < keys.Count; i++)
            {
                if (!used.ContainsKey(keys[i]))
                {
                    keys.Remove(keys[i]);
                    continue;
                }
                
                // We're only looking for nulls, so if this dictionary entry isn't null, skip it.
                if (!SilentNullCheck(used[keys[i]])) continue;

                used.Remove(keys[i]);
                keys.Remove(keys[i]);
            }
        }
        
        // ------------------------------------------------------------------------------------------- Object borrowing

        /// <summary>
        /// Returns a pre-existing unused instance of this pools object, or spawns a new one if the pool is not full.
        /// </summary>
        public virtual T1 Get(T0 key)
        {
            if (KeyInUse(key))
            {
                #if UNITY_EDITOR || DEVELOPMENT_BUILD
                    Warning($"This key [{key}] is already in use and can't be used again until it has been returned.");
                #endif
                
                return null;
            }

            var t = unused.Count > 0 ? unused[0] : SourceNewInstance();

            if (SoftNullCheck(t))
            {
                #if UNITY_EDITOR || DEVELOPMENT_BUILD
                    Warning($"Attempting to get a value to go with this key [{key}] returned null.");
                #endif
                
                return null;
            }

            t.key = key;
            
            TurnOn(key, t);
            
            return t;
        }

        /// <summary>
        ///     The role of this function is to handle exactly how this particular pool class sources a new
        ///     instance of T1 for usage. The technique differs from class to class and allows for easy
        ///     extension with new use-cases.
        /// </summary>
        private protected abstract T1 SourceNewInstance();

        /// <summary>
        /// If the key is in 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual bool Return(T0 key)
        {
            if (!KeyInUse(key)) return false;

            if (SoftNullCheck(used[key]))
            {
                Warning($"The given key [{key}] was found in the dictionary but its corresponding [{typeof(T1)}] was null.");

                used.Remove(key);
            }
            
            Return(used[key]);
            
            return true;
        }
        
        /// <summary>
        /// Deactivates a given pool object and returns it to the unused list.
        /// </summary>
        public virtual void Return(T1 target)
        {
            if (SoftNullCheck(target)) return;

            SwitchToUnusedList(target);

            if (returnDeactivatesObject)
            {
                target.Deactivate();
            }

            if (returnReparentsObject)
            {
                target.transform.SetParent(targetUnusedParent);

                if (returnResetsPosition)
                {
                    target.transform.localPosition = Vector3.zero;
                }

                if (returnResetsRotation)
                {
                    target.transform.localEulerAngles = Vector3.zero;
                }
            }
            else
            {
                if (returnResetsPosition)
                {
                    target.transform.position = Vector3.zero;
                }

                if (returnResetsRotation)
                {
                    target.transform.eulerAngles = Vector3.zero;
                }
            }

            if (returnResetsScale)
            {
                target.transform.localScale = Vector3.one;
            }
            
            target.Reset();
            
            target.OnReturn();
        }
        
        /// <summary>
        /// Deactivates all pool objects currently in use and returns them to the unused list.
        /// </summary>
        public virtual void ReturnAll()
        {
            while (keys.Count > 0)
            {
                Return(used[keys[0]]);
            }
        }

        public virtual bool KeyInUse(T0 key)
        {
            bool inUse = used.ContainsKey(key);

            if (inUse)
            {
                Warning($"This dictionary key is already in use [{key}]");
            }
            
            return inUse;
        }

        public void ReturnKey(T0 key)
        {
            if (keys.Contains(key))
            {
                keys.Remove(key);
            }
        }
        
        // -------------------------------------------------------------------------------------------- Object creation

        /// <summary>
        /// Creates a new instance of the pool prefab as long as it doesn't exceed any of the set limitations.
        /// </summary>
//        public abstract T1 CheckCreationConditions();
//        {
//            if (cycle)
//            {
//                
//            }
//            
//            if (limitless || level == PoolLevel.Populated || level == PoolLevel.Empty) return CreateNew();
//            
//            Warning(
//                $"No new instances of [{prefab.name}] may be created; " +
//                $"the pool is [{level}] with [{totalCount}] objects.");
//            return null;
//        }
        
        /// <summary>
        /// Untracks and destroys a nominated pool object.
        /// </summary>
        public virtual void Destroy(T1 target)
        {
            if (SoftNullCheck(target)) return;

            if (level == PoolLevel.Empty)
            {
                Error(
                    "This pool object cannot be destroyed because the pool is already marked as" +
                    " Empty. How did this happen?");
                return;
            }

            if (unused.Contains(target))
            {
                unused.Remove(target);
            }
            else if (used.ContainsValue(target))
            {
                used.Remove(target.key);
            }
            else
            {
                Warning(
                    $"This pool object [{target.gameObject.name}] is about to be destroyed but" +
                    $" it is also not featured in either the used or unused lists. How did this happen?");
            }

            UpdateTotalCount();

            ReturnKey(target.key);

            target.gameObject.Destroy();
        }
        
        // --------------------------------------------------------------------------------------------------- Internal

        /// <summary>
        /// Spawns a new GameObject instance of this pools prefab, without regard to the pool size or limits.
        /// Use Get() instead if use of the pools limitation system is desired.
        /// </summary>
        protected virtual T1 CreateNew()
        {
            // Is our prefab null?
            if (HardNullCheck(prefab)) return null;

            #if UNITY_EDITOR
            // Instantiate a copy of it.
            var g = Application.isPlaying
                ? Object.Instantiate(prefab, targetUnusedParent)
                : (GameObject) PrefabUtility.InstantiatePrefab(prefab, targetUnusedParent);
            #else
                GameObject g = Object.Instantiate(prefab, targetUnusedParent);
            #endif
            
            // Did that go wrong?
            if (SoftNullCheck(g))
            {
                Error($"Instantiation of prefab [{prefab.name}] failed");
                return null;
            }

            // Drill down through the new copys hierarchy looking for component of type T1.
            var t = g.GetComponentInChildren<T1>(true);
            
            // Did that go wrong?
            if (SoftNullCheck(t))
            {
                Error($"No component inheriting from this pools nominated generic type " +
                      $"[{typeof(T1).FullName}] could be found attached to the instantiated prefab. ");

                g.Destroy();
                
                return null;
            }
            
            unused.Add(t);

            UpdateTotalCount();
            
            t.OnInstantiate();
            
            t.Deactivate();

            return t;
        }

        /// <summary>
        /// Turns on a pool object.
        /// </summary>
        protected virtual void TurnOn(T0 key, T1 target)
        {
            if (SoftNullCheck(target)) return;

            target.key = key;

            SwitchToUsedList(key, target);

            if (getActivatesObject)
            {
                target.Activate();
            }

            if (getReparentsObject)
            {
                target.transform.SetParent(targetUsedParent);
            }
            
            target.OnGet();
        }

        private void SwitchToUnusedList(T0 key)
        {
            unused.Add(used[key]);
            
            used.Remove(key);
            
            keys.Remove(key);
        }

        private void SwitchToUnusedList(T1 target)
        {
            unused.Add(target);
            
            used.Remove(target.key);
            
            keys.Remove(target.key);
        }

        private void SwitchToUsedList(T0 key, T1 target)
        {
            unused.Remove(target);
            
            used.Add(key, target);
            
            keys.Add(key);
        }

        private void UpdateTotalCount()
        {
            totalCount = totalCount;
        }

        private void UpdatePoolLevel()
        {
            if (totalCount <= 0)
            {
                level = PoolLevel.Empty;
            }
            else if (totalCount == size)
            {
                level = PoolLevel.Full;
            }
            else if (totalCount > size)
            {
                level = PoolLevel.Overflow;
            }
            else
            {
                level = PoolLevel.Populated;
            }
        }
        
        public override void Reset() {}
    }
    
    // --------------------------------------------------------------------------------------------------- Mutations //

    // Limited //

    /// <summary>
    ///     This DictionaryPool will only allow up to a set maximum of created instances, upon which
    ///     active instances must be Return()'d before they can be used again.
    /// </summary>
    public class LimitedDictionaryPool<T0, T1, T2, T3> : VanillaDictionaryPool<T0, T1, T2, T3>
//      where T0 : struct                                                 // The key type
        where T1 : VanillaBehaviour, IDictionaryPoolable<T0, T1, T2, T3>  // The value type
        where T2 : SerializableDictionaryBase<T0, T1>, new()              // The dictionary type
        where T3 : VanillaDictionaryPool<T0,T1,T2,T3>                     // The pool type (this!)
    {
        private protected override T1 SourceNewInstance()
        {
            if (totalCount < size) return CreateNew();

            //            if (level == PoolLevel.Populated || level == PoolLevel.Empty) return CreateNew();
            
            Warning($"No new instances of [{prefab.name}] may be created; the pool is [{level}] with [{totalCount}] objects.");
            
            return null;
        }
    }
    
    // Limitless //

    /// <summary>
    ///     This DictionaryPool will always return a new instance of T1 if the unused list is empty.
    /// </summary>
    public class LimitlessDictionaryPool<T0, T1, T2, T3> : VanillaDictionaryPool<T0, T1, T2, T3>
//      where T0 : struct                                                 // The key type
        where T1 : VanillaBehaviour, IDictionaryPoolable<T0, T1, T2, T3>  // The value type
        where T2 : SerializableDictionaryBase<T0, T1>, new()              // The dictionary type
        where T3 : VanillaDictionaryPool<T0,T1,T2,T3>                     // The pool type (this!)
    {
        private protected override T1 SourceNewInstance()
        {
            return CreateNew();
        }
    }
    
    // Cyclical //
    
    /// <summary>
    ///     This DictionaryPool type will always return an instance of T1 by automatically Return()ing
    ///     whichever pre-existing instance has been active the longest and redistributing it.
    /// </summary>
    public class CyclicalDictionaryPool<T0, T1, T2, T3> : VanillaDictionaryPool<T0, T1, T2, T3>
//      where T0 : struct                                                 // The key type
        where T1 : VanillaBehaviour, IDictionaryPoolable<T0, T1, T2, T3>  // The value type
        where T2 : SerializableDictionaryBase<T0, T1>, new()              // The dictionary type
        where T3 : VanillaDictionaryPool<T0,T1,T2,T3>                     // The pool type (this!)
    {
        private protected override T1 SourceNewInstance()
        {
            if (totalCount < size) return CreateNew();

            Return(used[keys[0]]);

            return unused[0];
        }
    }
}