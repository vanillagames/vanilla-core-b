﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Vanilla
{
    /*
    VanillaPool<> is a generic object pool for prefabs with a VanillaBehaviour.
    
    If a more generic object pool is required for use with prefabs that don't need or have any
    kind of scripts attached, another kind of pool should be made based on this script.
    */

    public abstract class VanillaPool<T0, T1> : VanillaClass
        where T0 : VanillaBehaviour, IPoolable<T0, T1>
        where T1 : VanillaPool<T0, T1>
    {
        [Header("Object Prefab")] [Header("[ Vanilla Pool ]")]
        public GameObject prefab;

        [Header("Get / Return Switches")]
        [Tooltip("If true, GameObjects obtained via Get() will automatically be turned on (local to the hierarchy).")]
        public bool getActivatesObject;

        [Tooltip(
            "If true, GameObjects given back via Return() will automatically be turned off (local to the hierarchy).")]
        public bool returnDeactivatesObject;

        [Space(10),
         Tooltip(
             "If true, GameObjects obtained via Get() will automatically be reparented to the inUseParent transform.")]
        public bool getReparentsObject;

        [Tooltip(
            "If true, GameObjects given back via Return() will automatically be re-parented to this GameObjects Transform.")]
        public bool returnReparentsObject;

        [Space(10),
         Tooltip(
             "The parent that objects will be reparented to when accessed via Get(), if the option to do so is turned on.")]
        public Transform usedObjectParent;

        [Space(10),
         Tooltip(
             "The parent that objects will be reparented to when returned via Return(), if the option to do so is turned on.")]
        public Transform unusedObjectParent;


        [Space(10),
         Tooltip(
             "If true, GameObjects given back via Return() will automatically have their localPosition set to Vector3.zero.")]
        public bool returnResetsPosition;

        [Tooltip(
            "If true, GameObjects given back via Return() will automatically have their localEulerAngles set to Vector3.zero.")]
        public bool returnResetsRotation;

        [Tooltip(
            "If true, GameObjects given back via Return() will automatically have their localScale set to Vector3.one.")]
        public bool returnResetsScale;

        protected T0 cache;

        [Header("Count Settings")] public bool limitless;

        public int size = 10;

        [SerializeField, ReadOnly] private int _totalCount;

        [SerializeField, ReadOnly] protected PoolLevel level;

        protected int totalCount
        {
            get => _totalCount;
            set
            {
                _totalCount = Mathf.Clamp(value, 0, int.MaxValue);

                UpdatePoolLevel();
            }
        }

        [Header("Unused Objects")] [SerializeField, ReadOnly]
        private List<T0> _unused;

        public List<T0> unused => _unused ?? (_unused = new List<T0>());

        [Header("In-Use Objects")] [SerializeField, ReadOnly]
        private List<T0> _used;

        public List<T0> used => _used ?? (_used = new List<T0>());

        // -------------------------------------------------------------------------------- Pastables for inheritors //

        // Generic classes themselves can't use ContextMenu commands like this, but inheritors can!
        // Copy and paste these commands in for easy pre-filling and emptying outside of play mode.

        /*
        [ContextMenu("Fill")]
        public override void Fill()
        {
            base.Fill();
        }
    
        [ContextMenu("Empty")]
        public override void Empty()
        {
            base.Empty();
        }
    
        [ContextMenu("Trim")]
        public override void Trim()
        {
            base.Trim();
        }
        
        [ContextMenu("Get")]
        protected VanillaScrollEntry TestGet()
        {
            return Get();
        }
        
        [ContextMenu("Return")]
        protected void TestReturn()
        {
            if (used.Count > 0)
                Return(used[0]);
        }
        */

        // ---------------------------------------------------------------------------------------------- Initialization //

//    public virtual void OnEnable()
//    {
////        ((IPoolable<T0,T1>)T0).pool = (T1) this;}
//    }

        // -------------------------------------------------------------------------------------------- Level management //

        /// <summary>
        /// Spawns pool objects until the level is full.
        /// </summary>
        public virtual void Fill()
        {
            while (totalCount < size)
            {
                CreateNew();
            }
        }

        /// <summary>
        /// Destroys all tracked instances, used or unused.
        /// </summary>
        public virtual void Empty()
        {
            for (int i = used.Count; i > 0; i--)
            {
                DestroyPoolObject(used[0]);
            }

            for (int i = unused.Count; i > 0; i--)
            {
                DestroyPoolObject(unused[0]);
            }

//            while (GetUsedParent().childCount > 0)
//            {
//                Destroy(transform.GetChild(0).gameObject);
//            }
//            
//            while (GetUnusedParent().childCount > 0)
//            {
//                Destroy(transform.GetChild(0).gameObject);
//            }

            used.Clear();
            unused.Clear();

            totalCount = 0;
        }

        /// <summary>
        /// Repeatedly destroys instances until the pool is no longer overflowing.
        /// Preferences unused instances over used instances.
        /// </summary>
        public virtual void Trim()
        {
            while (totalCount > size && unused.Count > 0)
            {
                DestroyPoolObject(unused[0]);
            }

            while (totalCount > size && used.Count > 0)
            {
                DestroyPoolObject(used[0]);
            }
        }

        /// <summary>
        /// Establishes if the pool has too few or too many objects and then runs Fill() or Trim() accordingly.
        /// </summary>
        public virtual void BalanceTotal()
        {
            if (totalCount == size) return;

            if (totalCount < size)
            {
                Fill();
            }
            else
            {
                Trim();
            }
        }

        public virtual void BalanceTotal(int count)
        {
            if (count > size)
            {
                count = size;
            }

            if (totalCount == count) return;

            if (totalCount < count)
            {
                Fill();
            }
            else
            {
                Trim();
            }
        }

        public virtual void BalanceUsed(int amount)
        {
            if (used.Count == amount) return;

            if (used.Count < amount)
            {
                while (used.Count < amount)
                {
                    Get();
                }
            }
            else
            {
                if (amount < 0)
                {
                    amount = 0;
                }

                while (used.Count > amount)
                {
                    Return(used[0]);
                }
            }
        }

        // -------------------------------------------------------------------------------------------- Object borrowing //

        /// <summary>
        /// Returns a pre-existing unused instance of this pools object, or spawns a new one if the pool is not full.
        /// </summary>
        public virtual T0 Get()
        {
            T0 t;

            if (unused.Count > 0)
            {
                t = unused[0];

                TurnOn(t);
            }
            else
            {
                t = Spawn();

                if (t)
                {
                    TurnOn(t);
                }
            }

            return t;
        }

        /// <summary>
        /// Deactivates a given pool object and returns it to the unused list.
        /// </summary>
        public virtual void Return(T0 target)
        {
            if (SoftNullCheck(target)) return;

            SwitchToUnusedList(target);

            if (returnDeactivatesObject)
            {
                target.Deactivate();
            }

            if (returnReparentsObject)
            {
                target.transform.SetParent(unusedObjectParent);

                if (returnResetsPosition)
                {
                    target.transform.localPosition = Vector3.zero;
                }

                if (returnResetsRotation)
                {
                    target.transform.localEulerAngles = Vector3.zero;
                }
            }
            else
            {
                if (returnResetsPosition)
                {
                    target.transform.position = Vector3.zero;
                }

                if (returnResetsRotation)
                {
                    target.transform.eulerAngles = Vector3.zero;
                }
            }

            if (returnResetsScale)
            {
                target.transform.localScale = Vector3.one;
            }

            target.OnReturn();
        }

        /// <summary>
        /// Deactivates all pool objects currently in use and returns them to the unused list.
        /// </summary>
        public virtual void ReturnAll()
        {
            while (used.Count > 0)
            {
                Return(used[0]);
            }
        }

        // --------------------------------------------------------------------------------------------- Object creation //

        /// <summary>
        /// Creates a new instance of the pool prefab as long as it doesn't exceed any of the set limitations.
        /// </summary>
        public virtual T0 Spawn()
        {
            if (limitless || level == PoolLevel.Populated || level == PoolLevel.Empty) return CreateNew();

            Warning(
                $"No new instances of [{prefab.name}] may be created; the pool is [{level}] with [{totalCount}] objects.");
            return null;
        }

        /// <summary>
        /// Untracks and destroys a nominated pool object.
        /// </summary>
        public virtual void DestroyPoolObject(T0 target)
        {
            if (SoftNullCheck(target)) return;

            if (level == PoolLevel.Empty)
            {
                Warning($"[{target}] is about to be destroyed but the pool is already empty. How did this happen?");
            }

            if (unused.Contains(target))
            {
                unused.Remove(target);
            }
            else if (used.Contains(target))
            {
                used.Remove(target);
            }
            else
            {
                Warning(
                    $"This pool object [{target.gameObject.name}] is about to be destroyed but" +
                    " it is also not featured in either the used or unused lists. How did this happen?");
            }

            totalCount--;

            target.gameObject.Destroy();
        }

        // ---------------------------------------------------------------------------------------------------- Internal //

        /// <summary>
        /// Spawns a new GameObject instance of this pools prefab, without regard to the pool size or limits.
        /// Use Get() instead if use of the pools limitation system is desired.
        /// </summary>
        protected virtual T0 CreateNew()
        {
            // Is our prefab null?
            if (HardNullCheck(prefab)) return null;

            #if UNITY_EDITOR
            // Instantiate a copy of it.
            var g = Application.isPlaying
                ? Object.Instantiate(prefab, unusedObjectParent)
                : (GameObject) PrefabUtility.InstantiatePrefab(prefab, unusedObjectParent);
            #else
                GameObject g = Object.Instantiate(prefab, unusedObjectParent);
            #endif

            // Did that go wrong?
            if (SoftNullCheck(g))
            {
                Error($"Instantiation of prefab [{prefab.name}] failed");
                return null;
            }

            // Drill down through the new copys hierarchy looking for component of type T1.
            var t = g.GetComponentInChildren<T0>(true);

            // Did that go wrong?
            if (SoftNullCheck(t))
            {
                Error($"No component inheriting from this pools nominated generic type " +
                      $"[{typeof(T0).FullName}] could be found attached to the instantiated prefab. ");

                g.Destroy();

                return null;
            }

            unused.Add(t);

            t.pool = (T1) this;

            totalCount++;

            t.Deactivate();

            return t;
        }

        /// <summary>
        /// Turns on a pool object.
        /// </summary>
        protected virtual void TurnOn(T0 target)
        {
            if (SoftNullCheck(target)) return;

            SwitchToUsedList(target);

            if (getActivatesObject)
            {
                target.Activate();
            }

            if (getReparentsObject)
            {
                target.transform.SetParent(usedObjectParent);
            }

            target.OnGet();
        }

        /// <summary>
        /// Turns on all unused pool objects.
        /// </summary>
        protected virtual void TurnOnAll()
        {
            while (unused.Count > 0)
            {
                TurnOn(unused[0]);
            }
        }

        private void SwitchToUnusedList(T0 target)
        {
            used.Remove(target);
            unused.Add(target);
        }

        private void SwitchToUsedList(T0 target)
        {
            unused.Remove(target);

            used.Add(target);
        }

        private void UpdatePoolLevel()
        {
            if (totalCount <= 0)
            {
                level = PoolLevel.Empty;
            }
            else if (totalCount == size)
            {
                level = PoolLevel.Full;
            }
            else if (totalCount > size)
            {
                level = PoolLevel.Overflow;
            }
            else
            {
                level = PoolLevel.Populated;
            }
        }
    }
}