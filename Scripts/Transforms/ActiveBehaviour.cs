﻿using System;
using UnityEngine;

namespace Vanilla
{
    /// <summary>
    ///     This behaviour is intended for classes that refer to their own transform frequently, like in Update.
    ///
    ///     It has the cached Transform 't' which is 40% faster than Unity's internally cached 'transform' to access.
    /// </summary>
    public class ActiveBehaviour : VanillaBehaviour
    {
        [Header("Active Behaviour")]
        [SerializeField, ReadOnly] 
        public Transform t;

        protected override void InSceneValidation()
        {
            base.InSceneValidation();
            
            t = transform;
        }

        protected virtual void Awake()
        {
            t = transform;
        }
    }
}