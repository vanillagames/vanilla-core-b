﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla
{
    /// <summary>
    ///     This class will automatically handle its own deltaPosition comparisons and call TransformMoved() only
    ///     when the difference in global position between frames has been greater than 'threshold'.
    ///
    ///     It hijacks Update() in order to accomplish this, so TransformMoved() can essentially be considered a
    ///     gated version of Update().
    /// </summary>
    public class ChangeOnlyTransform : ActiveBehaviour
    {
        public PositionComparisonModule positionComparison;

        protected override void InSceneValidation()
        {
            base.InSceneValidation();

            positionComparison.Validate(this);
        }

        protected virtual void OnEnable()
        {
            positionComparison.onNewPosition += TransformMoved;
        }

        protected virtual void OnDisable()
        {
            positionComparison.onNewPosition -= TransformMoved;
        }

        protected virtual void Update()
        {
            positionComparison.Compare(t.position);
        }
        
        protected virtual void TransformMoved(Vector3 newPosition)
        {
            
        }
    }
}