﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Transforms
{

	public abstract class ActiveTargetBehaviour : ActiveBehaviour
	{
		[SerializeField]
		protected Transform _target;
		public Transform target
		{
			get =>
				_target != null ?
					_target :
					FindTarget();
			set => _target = value;
		}

		/// <summary>
		/// 	The behaviour will go and seek out a target transform.
		/// </summary>
		public abstract Transform FindTarget();
		
		protected virtual void OnEnable()
		{
			//if (HardNullCheck(target)) Disable();
		}
	}

}