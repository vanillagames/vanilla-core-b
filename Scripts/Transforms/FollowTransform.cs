﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Transforms
{

    public class FollowTransform : ActiveTargetBehaviour
    {

        public bool    followPosition;
        public Vector3 positionOffset;

        public bool    followRotation;
        public Vector3 rotationOffset;

        public bool    followScale;
        public Vector3 scaleOffset;


        public virtual void Update()
        {
            if (followPosition) t.position = target.position + positionOffset;
            if (followRotation) t.rotation = target.rotation * Quaternion.Euler(rotationOffset);
            if (followScale) t.localScale  = target.localScale + scaleOffset;
        }


        public override Transform FindTarget()
        {
            return _target;
        }

    }

}