﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SimulatedTouchSource : TouchSource
{

	[Serializable]
	public struct SimulatedTouch
	{

		[Range(0, 1)]
		public float screenPointX;

		[Range(0, 1)]
		public float screenPointY;

		[SerializeField]
		public int fingerId;

		[SerializeField, Vanilla.ReadOnly]
		public Vector2 _deltaPosition;
		
		public Vector2 GetScreenPoint() => new Vector2(x: Mathf.Lerp(a: 0,
		                                                             b: Screen.width,
		                                                             t: screenPointX),
		                                               y: Mathf.Lerp(a: 0,
		                                                             b: Screen.height,
		                                                             t: screenPointY));


		public Vector2 UpdateDelta()
		{
			var current = GetScreenPoint();

			var delta = current - _deltaPosition;
			
			_deltaPosition = current;

			return delta;
		}


		public Touch GetEnableTouch()
		{
			return new Touch
			       {
				       position      = GetScreenPoint(),
				       fingerId      = fingerId,
				       phase         = TouchPhase.Began,
				       deltaPosition = Vector2.zero
			       };
		}


		public Touch GetDisableTouch()
		{
			return new Touch
			       {
				       position      = GetScreenPoint(),
				       fingerId      = fingerId,
				       phase         = TouchPhase.Ended,
				       deltaPosition = UpdateDelta()
			       };
		}

		public Touch GetUpdateTouch()
		{
			var delta = UpdateDelta();
			
			return new Touch
			       {
				       position      = GetScreenPoint(),
				       fingerId      = fingerId,
				       phase         = delta.sqrMagnitude > Mathf.Epsilon ? TouchPhase.Moved : TouchPhase.Stationary,
				       deltaPosition = delta
			       };
		}

	}

	public SimulatedTouch[] simulatedTouches = new SimulatedTouch[0];

	void OnEnable()
	{
		for (var i = 0; i < simulatedTouches.Length; i++)
		{
			handler.HandleTouchBegan(t: simulatedTouches[i].GetEnableTouch());
		}
	}


	void OnDisable()
	{
		for (var i = 0; i < simulatedTouches.Length; i++)
		{
			handler.HandleTouchEnded(t: simulatedTouches[i].GetDisableTouch());

		}
	}


	void Update()
	{
		for (var i = 0; i < simulatedTouches.Length; i++) 
			handler.HandleTouch(t: simulatedTouches[i].GetUpdateTouch());
	}

}