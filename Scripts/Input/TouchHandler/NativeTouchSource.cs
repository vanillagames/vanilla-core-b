﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class NativeTouchSource : TouchSource
{

	public int fingerIdMax = 5;

	private void OnEnable()
	{
		for (var i = 0; i < Input.touches.Length && i < fingerIdMax; i++)
		{
			var t = Input.touches[i];
			
			if (t.phase != TouchPhase.Ended) handler.HandleTouchBegan(t);
		}
		
	}


	private void OnDisable()
	{
		for (var i = 0; i < Input.touches.Length && i < fingerIdMax; i++)
		{
			handler.HandleTouchEnded(Input.touches[i]);
		}
	}


	public void Update()
	{
		for (var i = 0; i < Input.touches.Length && i < fingerIdMax; i++)
		{
			handler.HandleTouch(Input.touches[i]);
		}

	}

}