﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla;
using Vanilla.Core.Inputs;

[Serializable]
public abstract class TouchSource : VanillaBehaviour
{

	[SerializeReference]
	public TouchHandlerBase handler;

}
