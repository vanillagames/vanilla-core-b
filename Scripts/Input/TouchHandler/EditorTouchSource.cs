﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Math;

public class EditorTouchSource : TouchSource
{

	public int fingerId = 5;
	
	public int editorTouchMouseButton = 0;

	private Vector2 _positionDelta;

	void Update()
	{
		#if UNITY_EDITOR
			if (!EditorTouchExists()) return;
	
			handler.HandleTouch(t: GetEditorTouch());
		#endif
	}

	void OnEnable()
	{
		#if UNITY_EDITOR
			// Lets check if the Editor Touch is already active this frame.
	
			// We don't want to accidentally start handling touches that have ended this frame.
			// They won't be properly ended next frame if we do!
	
			if ( Input.GetMouseButton(button: editorTouchMouseButton) &&
				 !Input.GetMouseButtonUp(button: editorTouchMouseButton))
			{
				handler.HandleTouchBegan(t: new Touch
											{
	
												fingerId      = fingerId,
												position      = Input.mousePosition,
												phase         = TouchPhase.Began,
												deltaPosition = Input.mousePosition
	
											});
			}
		#endif
	}

	void OnDisable()
	{
		#if UNITY_EDITOR
			if (!EditorTouchExists()) return;
	
			handler.HandleTouchEnded(t: new Touch
										{
											fingerId      = fingerId,
											position      = Input.mousePosition,
											phase         = TouchPhase.Ended,
											deltaPosition = Input.mousePosition.XYToXY() - _positionDelta
										});
	
			_positionDelta = Vector2.zero;
		#endif
	}

	private Touch GetEditorTouch()
	{

        #if UNITY_EDITOR
		var current = Input.mousePosition.XYToXY();

		var delta = current - _positionDelta;

		_positionDelta = current;

		TouchPhase phase;

		if (Input.GetMouseButtonDown(button: editorTouchMouseButton)) phase = TouchPhase.Began;
		else if (Input.GetMouseButtonUp(button: editorTouchMouseButton)) phase = TouchPhase.Ended;
		else if (delta.sqrMagnitude > Mathf.Epsilon) phase = TouchPhase.Moved;
		else phase = TouchPhase.Stationary;

		return new Touch
		{

			fingerId = 0,
			position = Input.mousePosition,
			phase = phase,
			deltaPosition = delta

		};
#else
 
		return new Touch{
		
		  fingerId = 0,
		  position = new Vector3(0,0,0),
		  phase = TouchPhase.Began,
		  deltaPosition = new Vector3(0,0,0)

		};

#endif


	}

#if UNITY_EDITOR
	private bool EditorTouchExists() => Input.GetMouseButton(button: editorTouchMouseButton) ||
											Input.GetMouseButtonUp(button: editorTouchMouseButton);
	#else
		private bool EditorTouchExists() => false;
	#endif

}