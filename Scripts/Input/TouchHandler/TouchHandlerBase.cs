﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla;

public abstract class TouchHandlerBase : VanillaBehaviour
{

	public abstract void HandleTouch(Touch t);
	public abstract void HandleTouchBegan(Touch t);
	public abstract void HandleTouchEnded(Touch t);
	public abstract void HandleTouchMoved(Touch t);
	public abstract void HandleTouchStationary(Touch t);

}