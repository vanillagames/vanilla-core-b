﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using Vanilla;
using Vanilla.Core.Inputs;

[Serializable]
public abstract class TouchHandler<T, H> : TouchHandlerBase
	where T : TouchBase
	where H : TouchHandler<T, H>
{

	[SerializeField]
	public T[] touches = new T[5];

	public Dictionary<int, T> activeTouches = new Dictionary<int, T>();


	public override void HandleTouch(Touch t)
	{
		switch (t.phase)
		{
			case TouchPhase.Began:
				HandleTouchBegan(t);
				break;

			case TouchPhase.Moved:
				HandleTouchMoved(t);
				break;

			case TouchPhase.Stationary:
				HandleTouchStationary(t);
				break;

			case TouchPhase.Ended:
			case TouchPhase.Canceled:
				HandleTouchEnded(t);
				break;
		}
	}


	public override void HandleTouchMoved(Touch t)
	{
//		Log("Moved!");

		if (!activeTouches.ContainsKey(t.fingerId)) return;

		var touch = activeTouches[t.fingerId];

		touch.screenPosition = t.position;

		touch.TouchMoved(ref t);
	}


	public override void HandleTouchStationary(Touch t)
	{
//		Log("Stationary!");

		if (!activeTouches.ContainsKey(t.fingerId)) return;

		activeTouches[t.fingerId].TouchStationary(ref t);
	}


	public override void HandleTouchBegan
	(
		Touch t)
	{
//		Log("Began!");
		
		var availableTouch = touches.FirstOrDefault(T => T.fingerId == -1);

		if (availableTouch == null)
		{
			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Warning("You've reached the multi-touch limit :( This touch won't be tracked.");
			#endif

			return;
		}

		availableTouch.fingerId = t.fingerId;

		availableTouch.screenPosition = t.position;

		activeTouches.Add(key: t.fingerId,
		                  value: availableTouch);

		availableTouch.TouchBegan(ref t);
	}


	public override void HandleTouchEnded(Touch t)
	{
//		Log("Ended!");

		var id = t.fingerId;

		if (!activeTouches.ContainsKey(id))
		{
			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Warning(
				"A Unity touch has ended but it didn't have a custom Touch class assigned. Maybe it was one that was already touching beyond the multi-touch limit?");
			#endif

			return;
		}

		var endingTouch = activeTouches[id];

		endingTouch.TouchEnded(ref t);

		endingTouch.fingerId = -1;
		
		activeTouches.Remove(id);
	}

}