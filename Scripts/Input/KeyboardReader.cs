﻿using System.Text;

using UnityEngine;

namespace Vanilla.Core.Inputs
{

	public class KeyboardReader : VanillaBehaviour
	{

		public StringBuilder chatter = new StringBuilder(512);

		[TextArea]
		public string output;

		public int chatterCount;


		void Update()
		{
//        frameText.Clear();

//        for (var i = 48; i < 58; i++)
//        {
//            var key = (KeyCode) i;
//            
//            if (!Input.GetKeyDown(key)) continue;
//            
//            frameText.Add(key);
//
////            Log($"You pressed {key}!");
//        }
//        
//        for (var i = 97; i < 123; i++)
//        {
//            var key = (KeyCode) i;
//            
//            if (!Input.GetKeyDown(key)) continue;
//            
//            frameText.Add(key);
//
////            Log($"You pressed {key}!");
//        }

//        Log("You said this! {");

//
//	    if (!string.IsNullOrWhiteSpace(Input.inputString))
//	    {
//		    foreach (var c in Input.inputString)
//		    {
//			    if (c == "\b"[0])
//			    {
//				    Log("It was a backspace");
//			    }
//		    }
//		    
//		    chatter.Append(Input.inputString);
//		    
//		    Log($"Input added!\n[{Input.inputString}]\n[{Input.inputString.Length}]");
//	    }

			if (Input.GetKeyDown(KeyCode.Backspace))
			{
				Backspace();
			}
			else if (Input.GetKeyDown(KeyCode.Return))
			{
				Return();
			}
			else if (!string.IsNullOrEmpty(Input.inputString))
			{
				Append();
			}
			else
			{
				return;
			}

			output = chatter.ToString();

			chatterCount = chatter.Length;
		}


		void Backspace()
		{
			if (chatter.Length > 0)
			{
				chatter.Length--;
			}
		}


		void Return()
		{
			chatter.Append(System.Environment.NewLine);
		}


		void Append()
		{
			if (chatter.Length < chatter.Capacity)
			{
				chatter.Append(Input.inputString);
			}
		}

	}

}