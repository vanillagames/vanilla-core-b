using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.EventSystems;

using Vanilla.Math;

namespace Vanilla.Core.Inputs
{

	[Serializable]
	public abstract class TouchBase : VanillaClass
	{

		[SerializeField, ReadOnly]
		public int fingerId = -1;

		[SerializeField, ReadOnly]
		public Vector2 screenPosition;

		public abstract void TouchBegan(ref Touch t);

		public abstract void TouchEnded(ref Touch t);

		public abstract void TouchStationary(ref Touch t);

		public abstract void TouchMoved(ref Touch t);

		public bool isOverUIElement => EventSystem.current.IsPointerOverGameObject(fingerId);

	}

	[Serializable]
	public abstract class MultiTouchBase<T> : VanillaBehaviour
		where T : TouchBase
	{

		[SerializeField]
		public T[] touches = new T[5];

		public Dictionary<int, T> activeTouches = new Dictionary<int, T>();

		[Header("Editor Touch")]
		public int editorTouchMouseButton = 0;
		
		private Vector2 _editorTouchDelta;

		public virtual void Update()
		{

			#if UNITY_EDITOR

			if (EditorTouchExists())
			{

				var d = GetEditorTouchDelta();

				var t = GetEditorTouch(delta: d);

				HandleTouchEvents(touches: new[] {t});

			}

			#else

			HandleTouchEvents(Input.touches);

			#endif

		}


		public void HandleTouchEvents(Touch[] touches)
		{
			foreach (var t in touches)
			{
				switch (t.phase)
				{
					case TouchPhase.Moved:

					{
						HandleTouchMoved(t);

						break;
					}

					case TouchPhase.Stationary:

					{
						HandleTouchStationary(t);

						break;
					}

					case TouchPhase.Began:

					{
						HandleTouchBegan(t);

						break;
					}

					case TouchPhase.Canceled:
					case TouchPhase.Ended:

					{
						HandleTouchEnded(t);

						break;
					}

				}
			}
		}


		protected virtual void HandleTouchMoved(Touch t)
		{
			if (!activeTouches.ContainsKey(t.fingerId)) return;

			var touch = activeTouches[t.fingerId];

			touch.screenPosition = t.position;

			touch.TouchMoved(ref t);
		}


		protected virtual void HandleTouchStationary(Touch t)
		{
			if (!activeTouches.ContainsKey(t.fingerId)) return;

			activeTouches[t.fingerId].TouchStationary(ref t);
		}


		protected virtual void HandleTouchBegan
		(
			Touch t)
		{
			T availableTouch = null;
			
			foreach (var T in touches)
			{
				Log(T.fingerId.ToString());
				
				if (T.fingerId == -1)
				{
					availableTouch = T;
				}
			}

//			try
//			{
//				var availableTouch = touches.FirstOrDefault(touch => touch.touch.fingerId == -1);
//			}
//			catch (Exception e) { }

			if (availableTouch == null)
			{
				#if UNITY_EDITOR || DEVELOPMENT_BUILD
				Warning("You've reached the multi-touch limit :( This touch won't be tracked.");
				#endif

				return;
			}

//			availableTouch.touch = t;
			
			availableTouch.fingerId = t.fingerId;

			availableTouch.screenPosition = t.position;

			activeTouches.Add(key: t.fingerId,
			                  value: availableTouch);

			availableTouch.TouchBegan(ref t);
		}


		protected virtual void HandleTouchEnded(Touch t)
		{
			var id = t.fingerId;

			if (!activeTouches.ContainsKey(id))
			{
				#if UNITY_EDITOR || DEVELOPMENT_BUILD
				Warning(
					"A Unity touch has ended but it didn't have a custom Touch class assigned. Maybe it was one that was already touching beyond the multi-touch limit?");
				#endif

				return;
			}

			var endingTouch = activeTouches[id];

//			endingTouch.touch = t;
			
			endingTouch.TouchEnded(ref t);

			endingTouch.fingerId = -1;

			activeTouches.Remove(id);
		}


		void OnEnable()
		{
			activeTouches.Clear();

			// Lets check if a valid touch (including the Editor Touch) is already active this frame.
			
			// We don't want to accidentally start tracking touches that have ended this frame.
			// They won't be properly de-registered next frame if we do!
			
			#if UNITY_EDITOR

			if (touches.Length != 1) touches = new T[1];

			if (EditorTouchExists())
				{
					var d = GetEditorTouchDelta();
	
					var p = GetEditorTouchPhase(movedThisFrame: d.sqrMagnitude > Mathf.Epsilon);
	
					if (p != TouchPhase.Ended) HandleTouchBegan(t: GetEditorTouch(delta: d));
				}
			
			#else

				foreach (var t in Input.touches)
				{
					if (t.phase != TouchPhase.Ended && t.phase != TouchPhase.Canceled) HandleTouchBegan(t);
				}
			
			#endif
		}


		void OnDisable()
		{
			
			// Force any active touches (including the Editor Touch) to be ended gracefully.
			
			#if UNITY_EDITOR

				if (EditorTouchExists())
				{
					var d = GetEditorTouchDelta();
	
					var t = GetEditorTouch(d);
	
					HandleTouchEnded(t);
				}

			#else
	
				foreach (var t in Input.touches) HandleTouchEnded(t);
			
			#endif
			

			activeTouches.Clear();
		}
		
		// -------------------------------------------------------------------------------------------- Editor Touch //

		private bool EditorTouchExists() => Input.GetMouseButton(button: editorTouchMouseButton) ||
		                                    Input.GetMouseButtonUp(button: editorTouchMouseButton);
		
		private Vector2 GetEditorTouchDelta()
		{
			var delta = Input.mousePosition.XYToXY() - _editorTouchDelta;

			_editorTouchDelta = Input.mousePosition.XYToXY();

			return delta;
		}

		private Touch GetEditorTouch(Vector2 delta) =>
			new Touch
			{

				fingerId      = 0,
				position      = Input.mousePosition,
				phase         = GetEditorTouchPhase(movedThisFrame: delta.sqrMagnitude > Mathf.Epsilon),
				deltaPosition = delta

			};


		private TouchPhase GetEditorTouchPhase(bool movedThisFrame)
		{
			if (!Input.GetMouseButton(editorTouchMouseButton)) 		return TouchPhase.Ended;
			if (Input.GetMouseButtonDown(button: editorTouchMouseButton)) 	return TouchPhase.Began;
			if (Input.GetMouseButtonUp(button: editorTouchMouseButton)) 	return TouchPhase.Ended;
			if (movedThisFrame)												return TouchPhase.Moved;
			else 															return TouchPhase.Stationary;
		}

	}

}


