﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Core.Inputs;
using Vanilla.Math;

[Serializable]
public class ExampleTouch :  TouchBase
{

	[SerializeField, Vanilla.ReadOnly]
	private Vector2 _deltaPosition;

	[SerializeField, Vanilla.ReadOnly]
	private Vector2 _delta;

	public bool drawDeltaRay = true;
	
	public override void TouchBegan(ref Touch touch)
	{
		_deltaPosition = screenPosition;

		Debug.Log($"Touch [{touch.fingerId}] Began!");
		
	}


	public override void TouchEnded(ref Touch touch)
	{

		_deltaPosition = screenPosition = Vector2.zero;

		Debug.Log($"Touch [{touch.fingerId}] Ended!");

	}


	public override void TouchStationary(ref Touch touch)
	{

		Debug.Log($"Touch [{touch.fingerId}] Stationary!");

	}


	public override void TouchMoved(ref Touch touch)
	{

		_delta = screenPosition - _deltaPosition;

		_deltaPosition = screenPosition;

		Debug.Log($"Touch [{touch.fingerId}] Moved!");

		if (drawDeltaRay)
		{
			Debug.DrawRay(start: Camera.main.ScreenToWorldPoint(screenPosition.XYToXY(10.0f)),
			              dir: _delta,
			              color: Color.Lerp(a: Color.yellow,
			                                b: Color.green,
			                                t: _delta.sqrMagnitude));
		}

	}
	
	

}

public class ExampleMultiTouch : MultiTouchBase<ExampleTouch> { }