﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//
//using UnityEngine;
//
//using Vanilla;
//using Vanilla.Math;
//
//[CreateAssetMenu(menuName = "Vanilla/Input/Unprotected Float Input Action", fileName = "New Unprotected Float Input Action")]
//public class UnprotectedFloatInputAction : FloatInputAction
//{
//    public override float state
//    {
//        get => _state;
//        set
//        {
//            _state = value;
//            
//            onValueChange.Invoke(value);
//        }
//    }
//
//}