﻿using System;

using UnityEngine;

namespace Vanilla.Core.Inputs
{
	#if UNITY_EDITOR
	[CreateAssetMenu(menuName = Core.Constants.VanillaSubPath +
	                            Core.Constants.CoreSubPath    +
	                            Core.Constants.InputSubPath   +
	                            Inputs.Constants.c_FloatInputActionAssetName,
					 fileName = "New " + Inputs.Constants.c_FloatInputActionAssetName)]
	#endif
	[Serializable]
	public class FloatInputAction : InputAction<float> { }

}