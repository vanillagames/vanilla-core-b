﻿using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Core.Inputs
{
	#if UNITY_EDITOR
	[CreateAssetMenu(menuName = Core.Constants.VanillaSubPath +
	                            Core.Constants.CoreSubPath    +
	                            Core.Constants.InputSubPath   +
	                            Constants.c_InputProfileAssetName,
		fileName = "New " + Constants.c_InputProfileAssetName)]
	#endif
	public class VanillaInputProfile : ScriptableObject
	{

		public List<AxisInputBlueprint>      axes      = new List<AxisInputBlueprint>();
		public List<ButtonInputBlueprint>    buttons   = new List<ButtonInputBlueprint>();
		public List<KeyCodeInputBlueprint>   keys      = new List<KeyCodeInputBlueprint>();
		public List<MouseAxisInputBlueprint> mouseAxes = new List<MouseAxisInputBlueprint>();

	}

}