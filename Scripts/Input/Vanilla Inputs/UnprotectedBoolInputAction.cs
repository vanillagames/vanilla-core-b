﻿//using UnityEngine;
//
//using Vanilla.Core;
//
//[CreateAssetMenu(menuName = Constants.VanillaSubPath +
//                            Constants.CoreSubPath    +
//                            Constants.InputSubPath   +
//                            "Unprotected Bool Input Action",
//				 fileName = "New Unprotected Bool Input Action")]
//public class UnprotectedBoolInputAction : BoolInputAction
//{
//
//	public override bool state
//	{
//		get => _state;
//		set
//		{
//			_state = value;
//
//			onValueChange.Invoke(value);
//		}
//	}
//
//}