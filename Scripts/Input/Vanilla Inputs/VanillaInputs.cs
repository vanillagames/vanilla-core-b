﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

namespace Vanilla.Core.Inputs
{

    public static class Constants
    {

        public const string c_InputProfileAssetName = "Input Profile";

        public const string c_BoolInputActionAssetName = "Bool Input Action";

        public const string c_FloatInputActionAssetName = "Float Input Action";

    }

    [Serializable]
    public class InputModule : VanillaClass
    {
        [SerializeField, ReadOnly] public string name;
        
        public override void Reset() {}

        public VanillaInputProfile profile;

        public List<KeyInputHandler> keyHandlers = new List<KeyInputHandler>();
        public List<ButtonInputHandler> buttonHandlers = new List<ButtonInputHandler>();
        public List<AxisInputHandler> axisHandlers = new List<AxisInputHandler>();
        public List<MouseAxisInputHandler> mouseAxisHandlers= new List<MouseAxisInputHandler>();

        public void CreateInputHandlers()
        {
            name = profile.name;
            
            keyHandlers.Clear();
            buttonHandlers.Clear();
            axisHandlers.Clear();
            mouseAxisHandlers.Clear();
            
            foreach (var k in profile.keys)
            {
                keyHandlers.Add(new KeyInputHandler(k));
            }

            foreach (var b in profile.buttons)
            {
                buttonHandlers.Add(new ButtonInputHandler(b));
            }

            foreach (var a in profile.axes)
            {
                axisHandlers.Add(new AxisInputHandler(a));
            }

            foreach (var m in profile.mouseAxes)
            {
                mouseAxisHandlers.Add(new MouseAxisInputHandler(m));
            }
        }

        public void CullUnusedHandlers()
        {
            for (var i = keyHandlers.Count - 1; i > -1; i--)
            {
                if (keyHandlers[i].inputActions.Length == 0)
                {
                    keyHandlers.RemoveAt(i);
                }
            }
            
            for (var i = buttonHandlers.Count - 1; i > -1; i--)
            {
                if (buttonHandlers[i].inputActions.Length == 0)
                {
                    buttonHandlers.RemoveAt(i);
                }
            }
            
            for (var i = axisHandlers.Count - 1; i > -1; i--)
            {
                if (axisHandlers[i].inputActions.Length == 0)
                {
                    axisHandlers.RemoveAt(i);
                }
            }
            
            for (var i = mouseAxisHandlers.Count - 1; i > -1; i--)
            {
                if (mouseAxisHandlers[i].inputActions.Length == 0)
                {
                    mouseAxisHandlers.RemoveAt(i);
                }
            }
        }

        public void CheckInputs()
        {
            foreach (var k in keyHandlers)
            {
                k.CheckInput();
            }

            foreach (var b in buttonHandlers)
            {
                b.CheckInput();
            }

            foreach (var a in axisHandlers)
            {
                a.CheckInput();
            }

            foreach (var m in mouseAxisHandlers)
            {
                m.CheckInput();
            }
        }
    }
    
    [Serializable]
    public class VanillaInputs : VanillaBehaviour
    {
        public bool cullUnusedInputs = true;
        
        [SerializeField]
        public InputModule[] inputModules = new InputModule[0];

        [ContextMenu("Update Input Handlers")]
        public void UpdateHandlers()
        {
            foreach (var i in inputModules)
            {
                i.CreateInputHandlers();
            }
        }

        void Awake()
        {
            if (cullUnusedInputs)
            {
                CullUnusedHandlers();
            }
        }

        private void CullUnusedHandlers()
        {
            foreach (var i in inputModules)
            {
                i.CullUnusedHandlers();
            }
        }
        
        void Update()
        {
            CheckInputModules();
        }

        void CheckInputModules()
        {
            foreach (var i in inputModules)
            {
                i.CheckInputs();
            }
        }
    }

    // ---------------------------------------------------------------------------------------- Input Action Sockets //

    [Serializable]
    public class InputActionSocket : VanillaClass
    {
        public string name;

        public override void Reset() {}
    }
    
    [Serializable]
    public class KeyCodeInputBlueprint : InputActionSocket
    {
        public KeyCode key;
    }
    
    [Serializable]
    public class ButtonInputBlueprint : InputActionSocket
    {
        public InputID buttonID;
    }

    [Serializable]
    public class AxisInputBlueprint : InputActionSocket
    {
        public InputID axisID;
    }

    [Serializable]
    public class MouseAxisInputBlueprint : InputActionSocket
    {
        public MouseAxisType axisType;
    }
    
    // ------------------------------------------------------------------------------------------- Run-time Handlers //

    [Serializable]
    public abstract class InputHandler : VanillaClass
    {
        [SerializeField, ReadOnly] protected string name;
        
        public override void Reset() {}

        public abstract void CheckInput();
    }
    
    [Serializable]
    public abstract class BoolInputHandler : InputHandler
    {
        [SerializeField, ReadOnly]
        public bool inputState;
        
        public BoolInputAction[] inputActions;

        public abstract override void CheckInput();
    }

    [Serializable]
    public abstract class FloatInputHandler : InputHandler
    {
        [SerializeField, ReadOnly]
        public float inputState;
        
        public FloatInputAction[] inputActions;
        
        public abstract override void CheckInput();
    }

    [Serializable]
    public class KeyInputHandler : BoolInputHandler
    {
        [SerializeField, ReadOnly]
        private KeyCode keyCode;

        public KeyInputHandler(KeyCodeInputBlueprint k)
        {
            name = k.name;
            
            keyCode = k.key;
        }
        
        public override void CheckInput()
        {
            inputState = Input.GetKey(keyCode);

            foreach (var a in inputActions)
            {
                if (!a.active) continue;

                a.slot.current = inputState;
                
                // a.state = inputState;
            }
        }
    }
    
    [Serializable]
    public class ButtonInputHandler : BoolInputHandler
    {
        [SerializeField, ReadOnly]
        private string inputString;
        
        public ButtonInputHandler(ButtonInputBlueprint b)
        {
            name = b.name;

            inputString = $"B{(int)b.buttonID}";
        }
        
        public override void CheckInput()
        {
            inputState = Input.GetButton(inputString);
            
            foreach (var a in inputActions)
            {
                if (!a.active) continue;

                a.slot.current = inputState;
                
                // a.state = inputState;
            }
        }
    }

    [Serializable]
    public class AxisInputHandler : FloatInputHandler
    {
        [SerializeField, ReadOnly]
        private string inputString;
        
        public AxisInputHandler(AxisInputBlueprint a)
        {
            name = a.name;

            inputString = $"A{(int)a.axisID}";
        }
        
        public override void CheckInput()
        {
            inputState = Input.GetAxis(inputString);
            
            foreach (var a in inputActions)
            {
                if (!a.active) continue;

                a.slot.current = inputState;
                
                // a.state = inputState;
            }
        }
    }

    [Serializable]
    public class MouseAxisInputHandler : FloatInputHandler
    {
        [SerializeField, ReadOnly]
        private string inputString;

        public MouseAxisInputHandler(MouseAxisInputBlueprint m)
        {
            name = m.name;

            switch (m.axisType)
            {
                case MouseAxisType.MouseX:
                    inputString = "Mouse X";
                    break;
                
                case MouseAxisType.MouseY:
                    inputString = "Mouse Y";
                    break;

                case MouseAxisType.ScrollWheel:
                    inputString = "Mouse ScrollWheel";
                    break;
            }
        }
        
        public override void CheckInput()
        {
            inputState = Input.GetAxis(inputString);
            
            foreach (var a in inputActions)
            {
                if (!a.active) continue;

                a.slot.current = inputState;

                // a.state = inputState;
            }
        }
    }
    
    // ------------------------------------------------------------------------------------------------------- Enums //
    
    public enum ButtonState {
        Released,
        Pressed,
        Held
    }

    public enum InputID {
        Zero = 0,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Eleven,
        Twelve,
        Thirteen,
        Fourteen,
        Fifteen,
        Sixteen,
        Seventeen,
        Eighteen,
        Nineteen,
        Twenty,
        TwentyOne,
        TwentyTwo,
        TwentyThree,
        TwentyFour,
        TwentyFive,
        TwentySix,
        TwentySeven,
        TwentyEight,
        TwentyNine,
        Thirty
    }

    public enum MouseAxisType {
        MouseX,
        MouseY,
        ScrollWheel
    }
}