﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Core.Inputs
{

    public abstract class InputAction<T> : ScriptableObject
        where T : struct
    {

        public bool active = true;

        protected readonly ValueSlot<T> _slot;
        public ValueSlot<T> slot => _slot;
//        
//        protected T _state = default;
//        public virtual T state
//        {
//            get => _state;
//            set => _state = value;
//        }
//
//        private GenericEvent<T> _onValueChange = new GenericEvent<T>();
//        public GenericEvent<T> onValueChange => _onValueChange;
    }

    


    
//    [CreateAssetMenu(menuName = Core.Constants.VanillaSubPath +
//                                Core.Constants.CoreSubPath    +
//                                Core.Constants.InputSubPath   +
//                                Inputs.Constants.c_BoolInputAction,
//                     fileName = "New " + Inputs.Constants.c_BoolInputAction)]
//    [Serializable]
//    public class BoolInputAction : InputAction
//    {
//
//        [SerializeField, ReadOnly]
//        protected bool _state;
//
//        public bool state
//        {
//            get => _state;
//            set
//            {
//                if (_state == value) return;
//
//                _state = value;
//
//                onValueChange.Invoke(value);
//            }
//        }
//
//        private BoolEvent _onValueChange;
//        public  BoolEvent onValueChange => _onValueChange ?? ( _onValueChange = new BoolEvent() );
//
//        public readonly ProtectedBoolSlot value = new ProtectedBoolSlot();
//
//    }

//    [CreateAssetMenu(menuName = Core.Constants.VanillaSubPath +
//                                Core.Constants.CoreSubPath    +
//                                Core.Constants.InputSubPath   +
//                                Inputs.Constants.c_FloatInputAction,
//                     fileName = "New "+Inputs.Constants.c_FloatInputAction)]
//    public class FloatInputAction : InputAction
//    {
//
//        [SerializeField, ReadOnly]
//        protected float _state;
//
//        public float state
//        {
//            get => _state;
//            set
//            {
//                // We use IsTheSameAs for more precision than IsRoughly
//                if (_state.IsTheSameAs(value)) return;
//
//                _state = value;
//
//                onValueChange.Invoke(value);
//            }
//        }
//
//        private FloatEvent _onValueChange;
//        public  FloatEvent onValueChange => _onValueChange ?? ( _onValueChange = new FloatEvent() );
//
//    }

    [Serializable]
    public abstract class GenericInputAction<T> : ScriptableObject
        where T : struct
    {

        public bool active = true;

        [SerializeField]
        public readonly ProtectedSlot<T> valueSlot = new ProtectedSlot<T>();

    }

}