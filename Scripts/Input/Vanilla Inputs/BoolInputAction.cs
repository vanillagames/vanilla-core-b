﻿using System;

using UnityEngine;

namespace Vanilla.Core.Inputs
{
	#if UNITY_EDITOR
	[CreateAssetMenu(menuName = Core.Constants.VanillaSubPath +
	                            Core.Constants.CoreSubPath    +
	                            Core.Constants.InputSubPath   +
	                            Constants.c_BoolInputActionAssetName,
		fileName = "New " + Constants.c_BoolInputActionAssetName)]
	#endif
	[Serializable]
	public class BoolInputAction : InputAction<bool> { }

}