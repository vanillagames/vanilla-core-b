﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Math.Examples
{
    public class IsFacingExamples : VanillaBehaviour
    {
        public Transform a;
        public Transform b;
        public Transform c;
        
        public float degrees = 30.0f;
        
        [SerializeField, ReadOnly]
        public float degreesToDot;

        [ReadOnly]
        public bool aIsFacingUp;
        [ReadOnly]
        public bool aIsFacingDown;
        [ReadOnly]
        public bool aIsFacingNorth;
        [ReadOnly]
        public bool aIsFacingSouth;
        [ReadOnly]
        public bool aIsFacingEast;
        [ReadOnly]
        public bool aIsFacingWest;
        [ReadOnly]
        public bool aIsFacingHorizon;

        [ReadOnly]
        public bool aForwardMatchesBForward;
        [ReadOnly]
        public bool aIsFacingB;
        
        [ReadOnly]
        public bool aForwardMatchesCForward;
        [ReadOnly]
        public bool aIsFacingC;

        void Update()
        {
            degreesToDot = degrees.GetDegreesToDotProduct();

            // A

            if (a)
            {
                aIsFacingUp = a.IsFacingUp(degrees);
                aIsFacingDown = a.IsFacingDown(degrees);

                aIsFacingNorth = a.IsFacingNorth(degrees);
                aIsFacingSouth = a.IsFacingSouth(degrees);

                aIsFacingEast = a.IsFacingEast(degrees);
                aIsFacingWest = a.IsFacingWest(degrees);

                aIsFacingHorizon = a.IsFacingHorizon(degrees);
                
                // B

                if (b)
                {
                    aForwardMatchesBForward = a.forward.IsAlignedWith(b.forward, degrees);

                    aIsFacingB = a.IsLookingAt(b, degrees);
                }
                
                // C

                if (c)
                {
                    aForwardMatchesCForward = a.forward.IsAlignedWith(c.forward, degrees);

                    aIsFacingC = a.IsLookingAt(c, degrees);
                }
            }
        }
    }
}