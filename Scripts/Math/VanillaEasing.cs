﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Vanilla.Math;

namespace Vanilla.Math
{

    /// <summary>
    ///     This is a C# implementation of Michealangelo007s incredible easing formula Javascript library.
    ///
    ///     That can be found at the following address:
    ///
    ///     https://github.com/Michaelangel007/easing
    ///
    ///     That library is itself based on Robert Penners infamous easing library from 2001, variations of
    ///     which can be found here:
    ///
    ///     http://robertpenner.com/easing/
    /// </summary>

    [Serializable]
    public struct EasingMethodSlot
    {

        [SerializeField]
        private VanillaEasing.EasingDirection _easingType;
        public VanillaEasing.EasingDirection easingType
        {
            get => _easingType;
            set
            {
                if (_easingType == value) return;

                _easingType = value;

                _method = GetCurrentMethod();
            }
        }

        [SerializeField]
        private VanillaEasing.EasingAlgorithmType _algorithm;
        public VanillaEasing.EasingAlgorithmType algorithm
        {
            get => _algorithm;
            set
            {
                if (_algorithm == value) return;

                _algorithm = value;

                _method = GetCurrentMethod();
            }
        }

        private VanillaEasing.EaseMethod _method;
        public  VanillaEasing.EaseMethod method => _method ?? ( _method = GetCurrentMethod() );


        public EasingMethodSlot
        (
            VanillaEasing.EasingDirection     easingDirection     = VanillaEasing.EasingDirection.InOut,
            VanillaEasing.EasingAlgorithmType easingAlgorithmType = VanillaEasing.EasingAlgorithmType.Quartic)
        {
            _easingType = easingDirection;
            _algorithm  = easingAlgorithmType;

            _method = VanillaEasing.GetEasingMethod(easingDirection: _easingType,
                                                    algorithmType: _algorithm);
        }


        private VanillaEasing.EaseMethod GetCurrentMethod()
        {
            return VanillaEasing.GetEasingMethod(easingDirection: _easingType,
                                                 algorithmType: _algorithm);
        }


//        public override void Reset()
//        {
//            _method = GetCurrentMethod();
//        }


        public void Validate<V>
        (
            V behaviour)
        {
            _method = GetCurrentMethod();
        }

    }

    public static class VanillaEasing
    {

        // -------------------------------------------------------------------------------------------------- Values //

        #region Constants



        const double k  = 1.70158;
        const double l0 = 7.5625;
        const double r  = 1 / 2.75; // reciprocal
        const double n  = 2.5949095;



        #endregion

        #region Enums



        /// <summary>
        ///     The different kinds of easing direction.
        /// </summary>
        public enum EasingDirection
        {

            In,
            Out,
            InOut

        }

        public enum EasingAlgorithmType
        {

            ConstantZero, // Returns a constant of 0
            ConstantOne,  // Returns a constant of 1
            Linear,       // Returns the input as-is
            Quadratic,    // Makes the output move towards/away from its target values, exaggerated to the power of 2
            Cubic,        // Makes the output move towards/away from its target values, exaggerated to the power of 3
            Quartic,      // Makes the output move towards/away from its target values, exaggerated to the power of 4
            Quintic,      // Makes the output move towards/away from its target values, exaggerated to the power of 5
            Sextic,       // Makes the output move towards/away from its target values, exaggerated to the power of 6
            Septic,       // Makes the output move towards/away from its target values, exaggerated to the power of 7
            Octic,        // Makes the output move towards/away from its target values, exaggerated to the power of 8
            Nonic,        // Makes the output move towards/away from its target values, exaggerated to the power of 9
            Decic,        // Makes the output move towards/away from its target values, exaggerated to the power of 10
            Back,         // Makes the output undershoot or overshoot its target values 
            Bounce,       // Makes the output bounce towards or away from its target values
            Circle,       // Makes the output follow a perfect circle
            Elastic       // Makes the output wobble towards or away from its target values

        }



        #endregion

        // ----------------------------------------------------------------------------------------- Delegate Easing //

        #region Delegate and delegate retrieval

        public delegate float EaseMethod(float i);


        /// <summary>
        ///     Returns a highly optimized float-returning method that corresponds with the given parameters.
        /// </summary>
        /// 
        /// <param name="easingDirection">
        ///     Does this easing happen on the way in, out, or both?
        /// </param>
        /// 
        /// <param name="algorithmType">
        ///     What kind of motion do we want applied? See the enum definition for details.
        /// </param>
        public static EaseMethod GetEasingMethod
        (
            EasingDirection     easingDirection,
            EasingAlgorithmType algorithmType)
        {
            switch (easingDirection)
            {
                case EasingDirection.In: return GetInMethod(algorithmType);

                case EasingDirection.Out: return GetOutMethod(algorithmType);

                case EasingDirection.InOut: return GetInOutMethod(algorithmType);
            }

            return null;
        }


        public static EaseMethod GetInMethod
        (
            EasingAlgorithmType subType)
        {
            switch (subType)
            {
                case EasingAlgorithmType.ConstantZero: return ConstantZero;

                case EasingAlgorithmType.ConstantOne: return ConstantOne;

                case EasingAlgorithmType.Linear: return Linear;

                case EasingAlgorithmType.Quadratic: return InQuadratic;

                case EasingAlgorithmType.Cubic: return InCubic;

                case EasingAlgorithmType.Quartic: return InQuartic;

                case EasingAlgorithmType.Quintic: return InQuintic;

                case EasingAlgorithmType.Sextic: return InSextic;

                case EasingAlgorithmType.Septic: return InSeptic;

                case EasingAlgorithmType.Octic: return InOctic;

                case EasingAlgorithmType.Nonic: return InNonic;

                case EasingAlgorithmType.Decic: return InDecic;

                case EasingAlgorithmType.Back: return InBack;

                case EasingAlgorithmType.Bounce: return InBack;

                case EasingAlgorithmType.Circle: return InBack;

                case EasingAlgorithmType.Elastic: return InBack;
            }

            return Linear;
        }


        public static EaseMethod GetOutMethod
        (
            EasingAlgorithmType subType)
        {
            switch (subType)
            {
                case EasingAlgorithmType.ConstantZero: return ConstantZero;

                case EasingAlgorithmType.ConstantOne: return ConstantOne;

                case EasingAlgorithmType.Linear: return Linear;

                case EasingAlgorithmType.Quadratic: return OutQuadratic;

                case EasingAlgorithmType.Cubic: return OutCubic;

                case EasingAlgorithmType.Quartic: return OutQuartic;

                case EasingAlgorithmType.Quintic: return OutQuintic;

                case EasingAlgorithmType.Sextic: return OutSextic;

                case EasingAlgorithmType.Septic: return OutSeptic;

                case EasingAlgorithmType.Octic: return OutOctic;

                case EasingAlgorithmType.Nonic: return OutNonic;

                case EasingAlgorithmType.Decic: return OutDecic;

                case EasingAlgorithmType.Back: return OutBack;

                case EasingAlgorithmType.Bounce: return OutBack;

                case EasingAlgorithmType.Circle: return OutBack;

                case EasingAlgorithmType.Elastic: return OutBack;
            }

            return Linear;
        }


        public static EaseMethod GetInOutMethod
        (
            EasingAlgorithmType subType)
        {
            switch (subType)
            {
                case EasingAlgorithmType.ConstantZero: return ConstantZero;

                case EasingAlgorithmType.ConstantOne: return ConstantOne;

                case EasingAlgorithmType.Linear: return Linear;

                case EasingAlgorithmType.Quadratic: return InOutQuadratic;

                case EasingAlgorithmType.Cubic: return InOutCubic;

                case EasingAlgorithmType.Quartic: return InOutQuartic;

                case EasingAlgorithmType.Quintic: return InOutQuintic;

                case EasingAlgorithmType.Sextic: return InOutSextic;

                case EasingAlgorithmType.Septic: return InOutSeptic;

                case EasingAlgorithmType.Octic: return InOutOctic;

                case EasingAlgorithmType.Nonic: return InOutNonic;

                case EasingAlgorithmType.Decic: return InOutDecic;

                case EasingAlgorithmType.Back: return InOutBack;

                case EasingAlgorithmType.Bounce: return InOutBack;

                case EasingAlgorithmType.Circle: return InOutBack;

                case EasingAlgorithmType.Elastic: return InOutBack;
            }

            return Linear;
        }



        #endregion

        // ---------------------------------------------------------------------------------------- Extension Easing //

        #region Extension easing (Self)



        public static void Ease
        (
            ref this float  input,
            EasingDirection easingDirection,
            int             polynomicStrength)
        {
            switch (polynomicStrength)
            {
                case 0:

                    input = ConstantOne(input);

                    break;

                case 1:

                    input = Linear(input);

                    break;

                case 2:

                    switch (easingDirection)
                    {
                        case EasingDirection.In:

                            input = InQuadratic(input);

                            break;

                        case EasingDirection.Out:

                            input = OutQuadratic(input);

                            break;

                        case EasingDirection.InOut:

                            input = InOutQuadratic(input);

                            break;
                    }

                    break;

                case 3:

                    switch (easingDirection)
                    {
                        case EasingDirection.In:

                            input = InCubic(input);

                            break;

                        case EasingDirection.Out:

                            input = OutCubic(input);

                            break;

                        case EasingDirection.InOut:

                            input = InOutCubic(input);

                            break;
                    }

                    break;

                case 4:

                    switch (easingDirection)
                    {
                        case EasingDirection.In:

                            input = InQuartic(input);

                            break;

                        case EasingDirection.Out:

                            input = OutQuartic(input);

                            break;

                        case EasingDirection.InOut:

                            input = InOutQuartic(input);

                            break;
                    }

                    break;

                case 5:

                    switch (easingDirection)
                    {
                        case EasingDirection.In:

                            input = InQuintic(input);

                            break;

                        case EasingDirection.Out:

                            input = OutQuintic(input);

                            break;

                        case EasingDirection.InOut:

                            input = InOutQuintic(input);

                            break;
                    }

                    break;

                case 6:

                    switch (easingDirection)
                    {
                        case EasingDirection.In:

                            input = InSextic(input);

                            break;

                        case EasingDirection.Out:

                            input = OutSextic(input);

                            break;

                        case EasingDirection.InOut:

                            input = InOutSextic(input);

                            break;
                    }

                    break;

                case 7:

                    switch (easingDirection)
                    {
                        case EasingDirection.In:

                            input = InSeptic(input);

                            break;

                        case EasingDirection.Out:

                            input = OutSeptic(input);

                            break;

                        case EasingDirection.InOut:

                            input = InOutSeptic(input);

                            break;
                    }

                    break;

                case 8:

                    switch (easingDirection)
                    {
                        case EasingDirection.In:

                            input = InOctic(input);

                            break;

                        case EasingDirection.Out:

                            input = OutOctic(input);

                            break;

                        case EasingDirection.InOut:

                            input = InOutOctic(input);

                            break;
                    }

                    break;

                case 9:

                    switch (easingDirection)
                    {
                        case EasingDirection.In:

                            input = InNonic(input);

                            break;

                        case EasingDirection.Out:

                            input = OutNonic(input);

                            break;

                        case EasingDirection.InOut:

                            input = InOutNonic(input);

                            break;
                    }

                    break;

                case 10:

                    switch (easingDirection)
                    {
                        case EasingDirection.In:

                            input = InDecic(input);

                            break;

                        case EasingDirection.Out:

                            input = OutDecic(input);

                            break;

                        case EasingDirection.InOut:

                            input = InOutDecic(input);

                            break;
                    }

                    break;
            }
        }



        #endregion

        #region Extension easing (Return)



        public static float GetEased
        (
            this float      input,
            EasingDirection easingDirection,
            int             polynomicStrength)
        {
            switch (polynomicStrength)
            {
                case 0: return ConstantOne(input);

                case 1: return Linear(input);

                case 2:

                    switch (easingDirection)
                    {
                        case EasingDirection.In: return InQuadratic(input);

                        case EasingDirection.Out: return OutQuadratic(input);

                        case EasingDirection.InOut: return InOutQuadratic(input);
                    }

                    break;

                case 3:

                    switch (easingDirection)
                    {
                        case EasingDirection.In: return InCubic(input);

                        case EasingDirection.Out: return OutCubic(input);

                        case EasingDirection.InOut: return InOutCubic(input);
                    }

                    break;

                case 4:

                    switch (easingDirection)
                    {
                        case EasingDirection.In: return InQuartic(input);

                        case EasingDirection.Out: return OutQuartic(input);

                        case EasingDirection.InOut: return InOutQuartic(input);
                    }

                    break;

                case 5:

                    switch (easingDirection)
                    {
                        case EasingDirection.In: return InQuintic(input);

                        case EasingDirection.Out: return OutQuintic(input);

                        case EasingDirection.InOut: return InOutQuintic(input);
                    }

                    break;

                case 6:

                    switch (easingDirection)
                    {
                        case EasingDirection.In: return InSextic(input);

                        case EasingDirection.Out: return OutSextic(input);

                        case EasingDirection.InOut: return InOutSextic(input);
                    }

                    break;

                case 7:

                    switch (easingDirection)
                    {
                        case EasingDirection.In: return InSeptic(input);

                        case EasingDirection.Out: return OutSeptic(input);

                        case EasingDirection.InOut: return InOutSeptic(input);
                    }

                    break;

                case 8:

                    switch (easingDirection)
                    {
                        case EasingDirection.In: return InOctic(input);

                        case EasingDirection.Out: return OutOctic(input);

                        case EasingDirection.InOut: return InOutOctic(input);
                    }

                    break;

                case 9:

                    switch (easingDirection)
                    {
                        case EasingDirection.In: return InNonic(input);

                        case EasingDirection.Out: return OutNonic(input);

                        case EasingDirection.InOut: return InOutNonic(input);
                    }

                    break;

                case 10:

                    switch (easingDirection)
                    {
                        case EasingDirection.In: return InDecic(input);

                        case EasingDirection.Out: return OutDecic(input);

                        case EasingDirection.InOut: return InOutDecic(input);
                    }

                    break;

                default: return Linear(input);
            }

            return Linear(input);
        }



        #endregion

        // ------------------------------------------------------------------------------------------ Easing Methods //

        #region Constant Methods



        /// <summary>
        ///     Returns a polynomial degree of 0, i.e. nothing.
        /// </summary>
        public static float ConstantZero
        (
            float p)
        {
            return 0;
        }


        /// <summary>
        ///     Returns a polynomial degree of 0, i.e. nothing.
        /// </summary>
        public static float ConstantOne
        (
            float p)
        {
            return 1;
        }



        #endregion

        // -------------------------------------------------------------------------------------------------- Linear //

        #region Linear Methods



        /// <summary>
        ///     Returns a polynomial degree of 1, i.e. the exact same value.
        /// </summary>
        public static float Linear
        (
            float p)
        {
            return p;
        }



        #endregion

        // ------------------------------------------------------------------------------------------------------ In //

        #region In Methods - 'Basic'



        // Basic //


        /// <summary>
        ///     Ease in with a polynomial degree of 2
        /// </summary>
        public static float InQuadratic
        (
            float p)
        {
            return p * p;
        }


        /// <summary>
        ///     Ease in with a polynomial degree of 3
        /// </summary>
        public static float InCubic
        (
            float p)
        {
            return p * p * p;
        }


        /// <summary>
        ///     Ease in with a polynomial degree of 4
        /// </summary>
        public static float InQuartic
        (
            float p)
        {
            return p * p * p * p;
        }


        /// <summary>
        ///     Ease in with a polynomial degree of 5
        /// </summary>
        public static float InQuintic
        (
            float p)
        {
            return p * p * p * p * p;
        }


        /// <summary>
        ///     Ease in with a polynomial degree of 6
        /// </summary>
        public static float InSextic
        (
            float p)
        {
            return p * p * p * p * p * p;
        }


        /// <summary>
        ///     Ease in with a polynomial degree of 7
        /// </summary>
        public static float InSeptic
        (
            float p)
        {
            return p * p * p * p * p * p * p;
        }


        /// <summary>
        ///     Ease in with a polynomial degree of 8
        /// </summary>
        public static float InOctic
        (
            float p)
        {
            return p * p * p * p * p * p * p * p;
        }


        /// <summary>
        ///     Ease in with a polynomial degree of 9
        /// </summary>
        public static float InNonic
        (
            float p)
        {
            return p * p * p * p * p * p * p * p * p;
        }


        /// <summary>
        ///     Ease in with a polynomial degree of 10
        /// </summary>
        public static float InDecic
        (
            float p)
        {
            return p * p * p * p * p * p * p * p * p * p;
        }



        #endregion

        #region In Methods - 'FX'



        // FX //


        /// <summary>
        ///     Ease in by dipping underneath the min value deliberately by 10%.
        /// </summary>
        public static float InBack
        (
            float p)
        {
            return (float) ( p * p * ( p * ( k + 1 ) - k ) );
        }


        /// <summary>
        ///     Ease in with a bounce-like animation.
        /// </summary>
        public static float InBounce
        (
            float p)
        {
            return 1 - OutBounce(1 - p);
        }


        /// <summary>
        ///     Ease in with the curvature of a perfect circle.
        /// </summary>
        public static float InCircle
        (
            float p)
        {
            return 1 - Mathf.Sqrt(1 - p * p);
        }


        /// <summary>
        ///     Ease in with an elastic wobble.
        /// </summary>
        public static float InElastic
        (
            float p)
        {
            var m = p - 1;

            return -Mathf.Pow(f: 2,
                              p: 10 * m) *
                   Mathf.Sin(f: ( m * 40 - 3 ) * Mathf.PI / 6);
        }



        #endregion

        // ----------------------------------------------------------------------------------------------------- Out //

        #region Out Methods - 'Basic'



        // Basic //


        /// <summary>
        ///     Ease out with a polynomial degree of 2
        /// </summary>
        public static float OutQuadratic
        (
            float p)
        {
            var m = p - 1;

            return 1 - m * m;
        }


        /// <summary>
        ///     Ease out with a polynomial degree of 3
        /// </summary>
        public static float OutCubic
        (
            float p)
        {
            var m = p - 1;

            return 1 + m * m * m;
        }


        /// <summary>
        ///     Ease out with a polynomial degree of 4
        /// </summary>
        public static float OutQuartic
        (
            float p)
        {
            var m = p - 1;

            return 1 - m * m * m * m;
        }


        /// <summary>
        ///     Ease out with a polynomial degree of 5
        /// </summary>
        public static float OutQuintic
        (
            float p)
        {
            var m = p - 1;

            return 1 + m * m * m * m * m;
        }


        /// <summary>
        ///     Ease out with a polynomial degree of 6
        /// </summary>
        public static float OutSextic
        (
            float p)
        {
            var m = p - 1;

            return 1 - m * m * m * m * m * m;
        }


        /// <summary>
        ///     Ease out with a polynomial degree of 7
        /// </summary>
        public static float OutSeptic
        (
            float p)
        {
            var m = p - 1;

            return 1 + m * m * m * m * m * m * m;
        }


        /// <summary>
        ///     Ease out with a polynomial degree of 8
        /// </summary>
        public static float OutOctic
        (
            float p)
        {
            var m = p - 1;

            return 1 - m * m * m * m * m * m * m * m;
        }


        /// <summary>
        ///     Ease out with a polynomial degree of 9
        /// </summary>
        public static float OutNonic
        (
            float p)
        {
            var m = p - 1;

            return 1 + m * m * m * m * m * m * m * m * m;
        }


        /// <summary>
        ///     Ease out with a polynomial degree of 10
        /// </summary>
        public static float OutDecic
        (
            float p)
        {
            var m = p - 1;

            return 1 + m * m * m * m * m * m * m * m * m;
        }



        #endregion

        #region Out Methods - 'FX'



        // FX //


        /// <summary>
        ///     Ease out by overshooting our max value deliberately by 10%.
        /// </summary>
        public static float OutBack
        (
            float p)
        {
            var m = p - 1;

            return (float) ( 1 + m * m * ( m * ( k + 1 ) + k ) );
        }


        /// <summary>
        ///     Ease out with a bounce-like animation.
        /// </summary>
        public static float OutBounce
        (
            float p)
        {
            #region Old Mode



//            var k1 = r; // 36.36%
//            var k2 = 2 * r; // 72.72%
//            var k3 = 1.5 * r; // 54.54%
//            var k4 = 2.5 * r; // 90.90%
//            var k5 = 2.25 * r; // 81.81%
//            var k6 = 2.625 * r; // 95.45%
//            var k0 = 7.5625;

//            if (p < k1)
//            {
//                return (float) (k0 * p * p);
//            }
//
//            if (p < k2)
//            {
//                // 48 / 64
//                
//                t = p - k3;
//
//                return (float) (k0 * t * t + 0.75);
//            }
//
//            if (p < k4)
//            {
//                // 60 / 64
//                
//                t = p - k5;
//                
//                return (float) (k0 * t * t + 0.9375);
//            }
//
//            // 63 / 64
//
//            t = p - k6;
//            
//            return (float) (k0 * t * t + 0.984375);



            #endregion

            double t;

            if (p < r)
            {
                return (float) ( l0 * p * p );
            }

            var l1 = 2   * r; // 72.72%
            var l2 = 1.5 * r; // 54.54%

            if (p < l1)
            {
                // 48 / 64

                t = p - l2;

                return (float) ( l0 * t * t + 0.75 );
            }

            l1 = 2.5  * r; // 90.90%
            l2 = 2.25 * r; // 81.81%

            if (p < l1)
            {
                // 60 / 64

                t = p - l2;

                return (float) ( l0 * t * t + 0.9375 );
            }

            l1 = 2.625 * r; // 95.45%

            // 63 / 64

            t = p - l1;

            return (float) ( l0 * t * t + 0.984375 );
        }


        /// <summary>
        ///     Ease out with the curvature of a perfect circle.
        /// </summary>
        public static float OutCircle
        (
            float p)
        {
            var m = p - 1;

            return Mathf.Sqrt(1 - m * m);
        }


        /// <summary>
        ///     Ease out with an elastic wobble.
        /// </summary>
        public static float OutElastic
        (
            float p)
        {
            return 1 +
                   ( Mathf.Pow(f: 2,
                               p: 10 * -p) *
                     Mathf.Sin(f: ( -p * 40 - 3 ) * Mathf.PI / 6) );
        }



        #endregion

        // ------------------------------------------------------------------------------------------------ In / Out //

        #region In/Out Methods - 'Basic'



        // Basic //


        /// <summary>
        ///     Ease in and out with a polynomial degree of 2
        /// </summary>
        public static float InOutQuadratic
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            if (t < 1) return p * t;

            return 1 - m * m * 2;
        }


        /// <summary>
        ///     Ease in and out with a polynomial degree of 3
        /// </summary>
        public static float InOutCubic
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            if (t < 1) return p * t * t;

            return 1 + m * m * m * 4;
        }


        /// <summary>
        ///     Ease in and out with a polynomial degree of 4
        /// </summary>
        public static float InOutQuartic
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            if (t < 1) return p * t * t * t;

            return 1 - m * m * m * m * 8;
        }


        /// <summary>
        ///     Ease in and out with a polynomial degree of 5
        /// </summary>
        public static float InOutQuintic
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            if (t < 1) return p * t * t * t * t;

            return 1 + m * m * m * m * m * 16;
        }


        /// <summary>
        ///     Ease in and out with a polynomial degree of 6
        /// </summary>
        public static float InOutSextic
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            if (t < 1) return p * t * t * t * t * t;

            return 1 - m * m * m * m * m * m * 32;
        }


        /// <summary>
        ///     Ease in and out with a polynomial degree of 7
        /// </summary>
        public static float InOutSeptic
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            if (t < 1) return p * t * t * t * t * t * t;

            return 1 + m * m * m * m * m * m * m * 64;
        }


        /// <summary>
        ///     Ease in and out with a polynomial degree of 8
        /// </summary>
        public static float InOutOctic
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            if (t < 1) return p * t * t * t * t * t * t * t;

            return 1 - m * m * m * m * m * m * m * m * 128;
        }


        /// <summary>
        ///     Ease in and out with a polynomial degree of 9
        /// </summary>
        public static float InOutNonic
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            if (t < 1) return p * t * t * t * t * t * t * t * t;

            return 1 + m * m * m * m * m * m * m * m * m * 256;
        }


        /// <summary>
        ///     Ease in and out with a polynomial degree of 10
        /// </summary>
        public static float InOutDecic
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            if (t < 1) return p * t * t * t * t * t * t * t * t * t;

            return 1 - m * m * m * m * m * m * m * m * m * m * 512;
        }



        #endregion

        #region In/Out Methods - 'FX'



        // FX //


        /// <summary>
        ///     Ease in and out by dipping under and then overshooting our min and max values respectively by 10%.
        /// </summary>
        public static float InOutBack
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            return (float) ( p < 0.5f ?
                                 p * t * ( t * ( n + 1 ) - n ) :
                                 1 + 2 * m * m * ( 2 * m * ( n + 1 ) + n ) );
        }


        /// <summary>
        ///     Ease in and out with a bounce-like animation.
        /// </summary>
        public static float InOutBounce
        (
            float p)
        {
            var t = p * 2;

            return (float) ( t < 1 ?
                                 0.5 - 0.5 * OutBounce(1 - t) :
                                 0.5 + 0.5 * OutBounce(t - 1) );
        }


        /// <summary>
        ///     Ease in and then out with the curvature of a perfect circle.
        /// </summary>
        public static float InOutCircle
        (
            float p)
        {
            var m = p - 1;

            var t = p * 2;

            return (float) ( t < 1 ?
                                 ( 1 - Mathf.Sqrt(1 - t     * t) )     * 0.5 :
                                 ( Mathf.Sqrt(1     - 4 * m * m) + 1 ) * 0.5 );
        }


        /// <summary>
        ///     Ease in and out with an elastic wobble.
        /// </summary>
        public static float InOutElastic
        (
            float p)
        {
            var s = 2              * p - 1;         // remap: [0.0, 0.5] -> [-1.0, 0.0]
            var l = ( 80 * s - 9 ) * Mathf.PI / 18; // and    [0.5, 1.0] -> [0.0, 1.0]

            return (float) ( s < 0 ?
                                 ( -0.5 *
                                   Mathf.Pow(f: 2,
                                             p: 10 * s) *
                                   Mathf.Sin(f: l) ) :
                                 ( 1 +
                                   0.5 *
                                   Mathf.Pow(f: 2,
                                             p: -10 * s) *
                                   Mathf.Sin(f: l) ) );
        }



        #endregion

    }

}