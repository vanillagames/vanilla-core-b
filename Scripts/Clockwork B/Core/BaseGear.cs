﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using RotaryHeart.Lib;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

using Vanilla.Math;

namespace Vanilla.Clockwork.B
{

    [Serializable]
    public class BaseGearList : ProtectedList<BaseGear> { }

    [Serializable]
    public abstract class BaseGear : VanillaBehaviour
    {

        [Header("Timing")] [Tooltip("How many seconds should this gear run for?")] [SerializeField]
        protected float _duration = 3.0f;
        public float duration
        {
            get => _duration;
            set
            {
                _duration = Mathf.Abs(value);

                if (_duration < _localTime)
                {
                    localTime = _duration;
                }
            }
        }

        [Tooltip("How long has this gear been running?")] [SerializeField]
        protected float _localTime;
        public float localTime
        {
            get => _localTime;
            set
            {
                _localTime = value.GetClamp(min: 0.0f,
                                            max: _duration);

                _localNormal = invertNormal ?
                                   1.0f - _localTime / _duration :
                                          _localTime / _duration;
            }
        }

        [Tooltip(
            "No matter how long duration is, this value will always represent its progress between 0 - 1. This is useful for standardizing many kinds of transition.")]
        [SerializeField]
        protected float _localNormal;
        public float localNormal => _localNormal;


        [Tooltip("This can be used to modify the amount of time this gear will process each frame.")]
        [SerializeField]
        [Range(0.1f, 8.0f)]
        public float localTimeMultiplier = 1.0f;
        
        [Header("Settings")]
        [Tooltip("Which gear passed control to this one?")] 
        [ReadOnly]
        public BaseGear _initiate;
        
        public List<BaseGear> startedBy = new List<BaseGear>();
        
        [Tooltip("If true, Time.deltaTime will be added to localTime on Update.\n\nHaving this " +
                 "value set to false is the same as letting Run() happen indefinitely, like Update().\n\n" +
                 "This can be useful for letting a gear observe for some kind of dynamic conditions, like " +
                 "if a user is looking at a certain object.")] 
        [SerializeField]
        [FormerlySerializedAs("_processTime")]
        public bool processTime = true;

        [Tooltip("If true, this gear will execute its Run() function.\n\nHaving this value set to " +
                 "false may be beneficial if its Run() code is no longer needed but the length of time it " +
                 "takes is.")] 
        [SerializeField]
        protected bool runnable = true;

        [Tooltip("If true, this gear will invert it's normal value.\n\nThis can be beneficial " +
                 "for running certain pieces of logic as though they were inverted without actually " +
                 "changing the direction of time.")]
        [SerializeField]
        public bool invertNormal;

        [Tooltip(
            "If true, this gear will snap localTime to either 0 or duration when it gets activated by " +
            "another gear, depending on if time is flowing forwards or backwards respectively.\n\n" +
            "This is useful if the gear is part of a greater execution loop.")]
        [SerializeField]
        public bool snapLocalTimeOnEnter = true;

        [Header("Editor Options")]
        [SerializeField]
        public bool automaticNaming = true;
        
        [Header("Enter Forwards")] 
        [FormerlySerializedAs("onEnterForwards")]
        public UnityEvent onEnter = new UnityEvent();

        [Header("Exit Forwards")]
        [FormerlySerializedAs("onExitForwards")]
        public UnityEvent onExit = new UnityEvent();

        [Header("Next Gears")] 
        
        [SerializeField]
        [HideInInspector]
        [FormerlySerializedAs("_nextGears")]
        public BaseGearList nextGears = new BaseGearList();

        // Okay, you caught me.
        // This is the real list of gears in use currently. The old version (nextGears) was in a container class
        // because Unity used to not be able to serialize generic collections. Now they can!
        // So I'm sneakily (and most importantly, safely) porting any and all gears from the old collection
        // to the new one and using that instead.
        [SerializeField]
//        [HideInInspector]
        public List<BaseGear> next = new List<BaseGear>();
        
//        public readonly static BaseGearList activeGears = new BaseGearList();

        // ---------------------------------------------------------------------------------------------- Validation //


        public void ValidateSelf() => InSceneValidation();

        protected override void InSceneValidation()
        {
            #if UNITY_EDITOR
                base.InSceneValidation();

                // nextGears is deprecated and 'next' is taking its place.
                // To not confuse people... I'm just updating and hiding this data for now.
                if (next.Count == 0)
                {
                    next = nextGears.contents;
                }

                AddValidGearConnections();

                RemoveInvalidGearConnections();
                
                localTime = _localTime;

                if (automaticNaming) gameObject.name = GetAutomaticGearName();

                if (GetComponents<BaseGear>().Length > 1)
                {
                    Error("You have more than one gear attached to the same GameObject. This is against the law in at least 4 countries.");
                }

                #endif
        }


        protected virtual string GetAutomaticGearName() => gameObject.name;

        protected virtual void AddValidGearConnections()
        {
            foreach (var g in next)
            {
                if (!SilentNullCheck(g) && !g.startedBy.Contains(this))
                {
                    g.startedBy.Add(this);
                }
            }
        }


        protected virtual void RemoveInvalidGearConnections()
        {
            startedBy.RemoveNullEntries();

            // startedBy is a list of any gears that can activate this one.
            // However, it may be possible for a removal to not be properly updated during validation.
            // For this reason, we'll confirm by iterating through each gear and removing it if
            // this gear isn't connected at all.

            // Remove gears from startedBy that
            // aren't a conditional (if/else)
            // aren't a switch (0/1/2/3/etc)
            // and don't have this gear in their nextGears list.
            startedBy.RemoveAll(match: g => !g.GetType().IsSubclassOf(c: typeof(ConditionalGear)) &&
                                            !g.GetType().IsSubclassOf(c: typeof(SwitchGear))      &&
                                            !g.next.Contains(item: this));

            // conditional gears that don't contain this gear in either true or false list.
            startedBy.RemoveAll(match: g => g.GetType().IsSubclassOf(c: typeof(ConditionalGear))    &&
                                            !( (ConditionalGear) g ).trueGears.Contains(item: this) &&
                                            !( (ConditionalGear) g ).falseGears.Contains(item: this));

            // switch gears that don't contain this gear in its switchGearOptions array.
            startedBy.RemoveAll(match: g => g.GetType().IsSubclassOf(c: typeof(SwitchGear)) &&
                                            !( (SwitchGear) g ).switchGearOptions.Contains(this));
        }


        // ---------------------------------------------------------------------------------------------- Activation //

        protected virtual void OnEnable()
        {
            if (snapLocalTimeOnEnter)
            {
                localTime = 0.0f;
            }

            if (!runnable) return;

            onEnter.Invoke();
                
            Handle_Enter();

        }


        protected virtual void OnDisable()
        {
            if (runnable)
            {
                Handle_Exit();

                onExit.Invoke();
            }
            
            foreach (var g in next)
            {
                g.Activate();
            }
        }

        // --------------------------------------------------------------------------------------------- Update Loop //

        internal void Update()
        {
            if (processTime) localTime += Time.deltaTime * localTimeMultiplier;

            if (runnable) Run();
        }
        
        protected virtual void Run() { }

        internal void LateUpdate()
        {
            if (processTime && _localTime.IsRoughly(_duration))
            {
                Deactivate();
            }
        }
        
        // --------------------------------------------------------------------------------------------------- Enter //

        protected virtual void Handle_Enter() => Update();

        // ---------------------------------------------------------------------------------------------------- Exit //
        
        protected virtual void Handle_Exit() { }

        // ------------------------------------------------------------------------------------------------- Helpers //

        // Set switches
        
        public void SetInvertNormal(bool value) => invertNormal = value;

        public void SetProcessTime(bool value) => processTime = value;

        public void SetRunnable(bool value) => runnable = value;

        // Toggle switches
        
        public void ToggleInvertNormal() => invertNormal = !invertNormal;

        public virtual void ToggleProcessTime() => processTime = !processTime;

        public void ToggleRunnable() => runnable = !runnable;

        // --------------------------------------------------------------------------------------------------------- //

    }

}