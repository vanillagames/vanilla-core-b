﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vanilla.Clockwork.B
{
    public enum ClockworkEnterActions
    {
        Continue,     // Calls Receive_Control_Forwards/Backwards().
        Stop,         // No further action occurs.
        FastForward   // Calls Run_Internal() followed by Handle_Exit_Attempt_Forwards_Internal().
                      // This allows for gears to be either skipped or fast-forwarded depending on if Running is true.
    }

    public enum ClockworkExitActions
    {
        Continue,             // Calls Pass_Control_Forwards/Backwards_Internal()

        Stop,                 // No further action occurs.
        
        Bounce,               // Flips the value of invertTime, effectively reversing time for this script.
        
        Nothing               // Does nothing.
    }
}
