﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;

using UnityEditor;

using UnityEngine;
using UnityEngine.AddressableAssets;

using Vanilla;
using Vanilla.Clockwork.B;
using Vanilla.SceneWorks;

public class SceneSourceGear : BaseGear, ISceneSource
{

	[Header("Scene Source")]
	[Tooltip(
		"If true, the scene this component is found in will automatically be removed from SceneLoader.targetScenes.")]
	[SerializeField]
	private bool removeThisSceneOnUse = false;
	public bool RemoveThisSceneOnUse
	{
		get => removeThisSceneOnUse;
		set => removeThisSceneOnUse = value;
	}

	[SerializeField]
	internal AssetReference thisSceneAssetReference;
	public AssetReference ThisSceneAssetReference
	{
		get => thisSceneAssetReference;
		set => thisSceneAssetReference = value;
	}

	[SerializeField]
	private bool clearTargetScenesOnUse;
	public bool ClearTargetScenesOnUse
	{
		get => clearTargetScenesOnUse;
		set => clearTargetScenesOnUse = value;
	}
	
	[SerializeField]
	private bool unloadUnusedAssets;
	public bool UnloadUnusedAssets
	{
		get => unloadUnusedAssets;
		set => unloadUnusedAssets = value;
	}

	[SerializeField]
	private List<AssetReference> loadSceneReferences = new List<AssetReference>();
	public List<AssetReference> LoadSceneReferences
	{
		get => loadSceneReferences;
		set => loadSceneReferences = value;
	}

	[SerializeField]
	private List<AssetReference> unloadSceneReferences = new List<AssetReference>();
	public List<AssetReference> UnloadSceneReferences
	{
		get => unloadSceneReferences;
		set => unloadSceneReferences = value;
	}


	protected override void InSceneValidation()
	{
		#if UNITY_EDITOR
		base.InSceneValidation();

		thisSceneAssetReference.SetEditorAsset(removeThisSceneOnUse ?
			                                       AssetDatabase.LoadAssetAtPath<SceneAsset>(
				                                       assetPath: gameObject.scene.path) :
			                                       null);

		_duration = 0.01f;
		processTime = false;
	
		// Load scene string

		var loadName = string.Empty;

		if (loadSceneReferences.Count > 0)
		{
			for (var index = 0; index < loadSceneReferences.Count && index < 3; index++)
			{
				var s = loadSceneReferences[index];

				if (s.editorAsset == null) continue;

				if (index > 0) loadName += ", ";

				loadName += s.GetShortAssetName();
			}
			
			loadName = $"Load [{loadName}] scene{(loadSceneReferences.Count > 1 ? "s" : string.Empty)}";
		}

		// Unload scene string
		
		var unloadName = string.Empty;

		if (unloadSceneReferences.Count > 0)
		{
			for (var index = 0; index < unloadSceneReferences.Count && index < 3; index++)
			{
				var s = unloadSceneReferences[index];

				if (s.editorAsset == null) continue;

				var sceneName = s.editorAsset.name;

				var indexOfLastDash = sceneName.LastIndexOf(value: "-",
				                                            comparisonType: StringComparison.Ordinal) +
				                      2;

				if (index > 0) unloadName += ", ";

				unloadName += sceneName.Substring(startIndex: indexOfLastDash,
				                                length: sceneName.Length - indexOfLastDash);
			}
			
			unloadName = $"{(string.IsNullOrEmpty(loadName) ? "U" : "u")}nload [{unloadName}] scene{(unloadSceneReferences.Count > 1 ? "s" : string.Empty)}";
		}

		// Format the results
		
		gameObject.name = string.IsNullOrEmpty(loadName) ?
			                  string.IsNullOrEmpty(unloadName) ?
				                  $"Do nothing - no scene changes listed" :
				                  $"{unloadName}" :
			                  string.IsNullOrEmpty(unloadName) ?
				                  $"{loadName}" :
				                  $"{loadName}, {unloadName}";
		#endif
	}


	protected override void Handle_Enter()
	{
//		if (loadSceneReferences.Count + unloadSceneReferences.Count == 0) return;

		if (!removeThisSceneOnUse) SceneLoader.i.onLoadOperationCompletion += ToggleProcessTime;

		SceneLoader.i.ApplySceneSetupAndBeginLoad(this);
	}


	public override void ToggleProcessTime()
	{
		base.ToggleProcessTime();

		SceneLoader.i.onLoadOperationCompletion -= ToggleProcessTime;
	}

	protected override void Run() { }


	private void OnDestroy()
	{
		
	}

}