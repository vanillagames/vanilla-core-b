﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

namespace Vanilla.Clockwork.B
{

	public class TapOrClickInputGear : BaseGear
	{

		protected override void OnEnable()
		{
			processTime = false;
			
			base.OnEnable();
		}


		protected override void Run()
		{
			#if UNITY_EDITOR
			// If no click, return.
			if (!Input.GetMouseButtonDown(0)) return;

			// If the pointer is over a UI element, return.
			if (EventSystem.current.IsPointerOverGameObject(-1)) return;
			#else
			// If no tap, return.
			if (!FirstTouchBegan()) return;

			// If the tap is over a UI element, return.
			if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) return;
			#endif

			processTime = true;
		}

	}

}