﻿using System;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Clockwork.B
{

	[Serializable]
	public abstract class ConditionalGear : BaseGear
	{

		public bool flipConditionInEditor = false;
		
		[SerializeField]
		public List<BaseGear> trueGears = new List<BaseGear>();

		[SerializeField]
		public List<BaseGear> falseGears = new List<BaseGear>();

		protected override void OnDisable()
		{
			#if UNITY_EDITOR
			if (flipConditionInEditor)
			{
				next = Evaluate() ?
					       falseGears :
					       trueGears;
			}
			else
			{
				next = Evaluate() ?
					       trueGears :
					       falseGears;
			}	
			#else
			next = Evaluate() ?
				       trueGears :
				       falseGears;
			#endif

			base.OnDisable();
		}

		protected override string GetAutomaticGearName() => $"If [{EvaluateDescription()}]...";

		protected abstract bool Evaluate();

		protected abstract string EvaluateDescription();

		protected override void AddValidGearConnections()
		{
			foreach (var g in trueGears)
			{
				if (!SilentNullCheck(o: g) && !g.startedBy.Contains(item: this)) g.startedBy.Add(this);
			}

			foreach (var g in falseGears)
			{
				if (!SilentNullCheck(o: g) && !g.startedBy.Contains(item: this)) g.startedBy.Add(this);
			}
		}

	}

}