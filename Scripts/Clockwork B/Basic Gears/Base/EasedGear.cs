// Creation Date                                                                              [ 31/01/2020 8:24:47 PM ]
// Project                                                                                               [ Rewild2020 ]
// Developer                                                                                                 [ PHORIA ]

using Vanilla.Math;

using static Vanilla.Math.VanillaEasing;

namespace Vanilla.Clockwork.B
{
	public abstract class EasedGear : BaseGear
	{

		// ----------------------------------------------------------------------------------------------- Variables //

		public EasingMethodSlot easing = new EasingMethodSlot(easingDirection: EasingDirection.InOut,
		                                                      easingAlgorithmType: EasingAlgorithmType.Quadratic);
		
		// ---------------------------------------------------------------------------------------------- Validation //


		protected override void InSceneValidation()
		{
			base.InSceneValidation();
			
			easing.Validate(behaviour: this);
		}

		// ----------------------------------------------------------------------------------------------- Clockwork //

		protected abstract override void Run();

	}
}