﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla.Clockwork.B;

public class DestructionGear : BaseGear
{
	public List<GameObject> objectsToDestroy = new List<GameObject>();

	protected override void Run() { }
	
	public void DestroyAllInList()
	{
		for (var i = objectsToDestroy.Count - 1; i >= 0; i--)
		{
			Destroy(objectsToDestroy[i]);
		}
	}

}
