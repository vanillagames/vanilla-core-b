﻿using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

namespace Vanilla.Clockwork.B
{

	[Serializable]
	public abstract class SwitchGear : BaseGear
	{

		[SerializeReference]
		[SerializeField]
		public BaseGear[] switchGearOptions = new BaseGear[3];
		
		protected override void OnDisable()
		{
			var result = switchGearOptions[Evaluate()];

			if (result != null && !next.Contains(item: result)) next.Add(result);

			base.OnDisable();
		}

		protected abstract int Evaluate();

		protected abstract string EvaluateDescription();

		protected override string GetAutomaticGearName() => $"If [{EvaluateDescription()}]...";

		protected override void AddValidGearConnections()
		{
			foreach (var g in switchGearOptions)
			{
				if (!SilentNullCheck(g) && !g.startedBy.Contains(this))
				{
					g.startedBy.Add(this);
				}
			}
		}

	}

}