//using System.Collections;
//using System.Collections.Generic;
//
//using UnityEngine;
//
//namespace Vanilla.Clockwork.B
//{
//
//    public class ObjectGroupGear : BaseGear
//    {
//
//        [Tooltip("All GameObjects in this list will be automatically activated when this gear starts.")]
//        public List<GameObject> startObjects = new List<GameObject>();
//
//        [Tooltip("All GameObjects in this list will be automatically deactivated when this gear ends.")]
//        public List<GameObject> endObjects = new List<GameObject>();
//
//
//        protected override void Handle_Enter()
//        {
//            base.Handle_Enter();
//            
//            foreach (var go in startObjects)
//            {
//                go.SetActive(true);
//            }
//        }
//
//        protected override void Handle_Exit()
//        {
//            base.Handle_Exit();
//            
//            foreach (var go in endObjects)
//            {
//                go.SetActive(false);
//            }
//        }
//
//        protected override void Run() { }
//
//    }
//
//}