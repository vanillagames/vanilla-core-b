﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AddressableAssets;

using Vanilla.Clockwork.B;
using Vanilla.Math;

namespace Vanilla
{

	public class FadeMusicGear : BaseGear
	{

		[Tooltip("If this is populated, it will immediately swap out the current playing audio clip.")]
		public AssetReference audioClip;
		
		[SerializeField]
		[Range(0, 1)]
		public float from;

		[SerializeField]
		[Range(0, 1)]
		public float to;

		[Tooltip("If true, Music.Play or Music.Stop will automatically be called if the volume is 0 on enter or exit respectively.")]
		public bool autoPlayAndStop = true;

		[Tooltip("If true, the audio clip in Music.source.clip will be released on exit. This will cause errors if the clip is not addressable!")]
		public bool releaseClipOnExit = false;

		protected override void Handle_Enter()
		{
			if (audioClip.RuntimeKeyIsValid())
			{
				// Does this work if you've unloaded and reloaded the containing scene?
				// Hm... it might get loaded twice this way. Careful!
				if (audioClip.IsValid())
				{
					Music.SwapClip(clip: audioClip.Asset as AudioClip);

					if (autoPlayAndStop && from.IsZero()) Music.Play();
				}
				else
				{
					audioClip.LoadAssetAsync<AudioClip>().Completed += handle =>
					                                                   {
						                                                   Music.SwapClip(clip: handle.Result);

						                                                   if (autoPlayAndStop && from.IsZero())
							                                                   Music.Play();
					                                                   };
				}
			}

			base.Handle_Enter();
		}


		protected override void Run() => Music.source.volume = Volume;


		protected override void Handle_Exit()
		{
			if (autoPlayAndStop && to.IsZero())
				Music.Stop();

			if (releaseClipOnExit) Music.ReleaseClip();
		}

		private float Volume => Mathf.Lerp(a: from,
		                                   b: to,
		                                   t: localNormal);


		protected override string GetAutomaticGearName()
		{
			#if UNITY_EDITOR
			var n = audioClip.editorAsset != null ? 
				             $"Play [{audioClip.GetShortAssetName()}] clip, " : 
				             string.Empty;

			n += $"{(string.IsNullOrEmpty(n) ? "F" : "f")}ade music from [{from:0.#}] to [{to:0.#}] over [{duration:0.#}]s";

			return n;
			#else
			return string.Empty;
			#endif
		}

	}

}