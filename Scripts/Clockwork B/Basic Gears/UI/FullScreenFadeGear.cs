﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vanilla.Clockwork.B;
using Vanilla.SceneWorks;

public class FullScreenFadeGear : BaseGear
{
//    public UnityEvent fadeBegun = new UnityEvent();
//    public UnityEvent fadeFinished = new UnityEvent();

    public void TurnOffSystemCam()
    {
        SystemCam.i.Deactivate();
    }

    public void TurnOnSystemCam()
    {
        SystemCam.i.Activate();
    }
    
//    public override void Handle_Enter_Forwards(BaseGear sender)
//    {
//        fadeBegun.Invoke();
//    }

//    public override void Handle_Enter_Backwards(BaseGear sender)
//    {
//        fadeBegun.Invoke();
//    }

//    public override void Handle_Exit_Forwards()
//    {
//        if (!invertNormal)
//        {
//            SceneLoader.i.BeginLoadOperation();
//        }
//        else
//        {
//            fadeFinished.Invoke();
//        }
//    }

//    public override void Handle_Exit_Backwards()
//    {
//        fadeFinished.Invoke();
//    }

    protected override void Run()
    {
        FullScreenFade.i.Fade(localNormal);
    }

    protected override void InSceneValidation()
    {
        base.InSceneValidation();
        
        NameSelf();
    }


    public override void NameSelf()
    {
        gameObject.name = $"Fade [{( invertNormal ? "In" : "Out" )}]";
    }

}
