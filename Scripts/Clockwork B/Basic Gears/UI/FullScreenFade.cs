﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class FullScreenFade : MonoBehaviour
{
    public static FullScreenFade i;

    public CanvasRenderer image;
    
    void OnEnable()
    {
        i = this;
    }

    public void Fade(float normal)
    {
        image.SetAlpha(normal);
    }
}
