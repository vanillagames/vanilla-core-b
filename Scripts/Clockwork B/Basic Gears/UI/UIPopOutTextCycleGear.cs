﻿//using System.Collections;
//using System.Collections.Generic;
//
//using UnityEngine.UI;
//
//using Vanilla.Clockwork.B;
//using Vanilla.Math;
//
//public class UIPopOutTextCycleGear : UIPopOutGear
//{
//	public List<string> textBank = new List<string>();
//
//	public Text text;
//
//	public int currentText = 0;
//
//	protected override void Awake()
//	{
//		base.Awake();
//		
//		NextText();
//	}
//
//	protected override void Handle_Bounce_Off_Start()
//	{
//		base.Handle_Bounce_Off_Start();
//
//		NextText();
//	}
//
//
//	public void Previous()
//	{
//		currentText = ( currentText - 1 ).GetClamp(min: 0,
//		                                           max: textBank.Count - 1);
//	}
//
//	public void Next()
//	{
//		currentText = ( currentText + 1 ).GetClamp(min: 0,
//		                                           max: textBank.Count - 1);
//	}
//
//	void NextText()
//	{
//		if (textBank.Count == 0)
//		{
//			Deactivate();
//
//			return;
//		}
//		
//		text.text = textBank[0];
//
//		textBank.RemoveAt(0);
//	}
//}