﻿// Creation Date                                                                               [ 1/02/2020 7:29:21 PM ]
// Project                                                                                               [ Rewild2020 ]
// Developer                                                                                                 [ PHORIA ]

using System;

using UnityEngine;

namespace Vanilla.Clockwork.B
{

	public class FadeUIGroupGear : EasedGear
	{

		// ---------------------------------------------------------------------------------------------- Subclasses //

		#region Subclasses


		[Serializable]
		public class CanvasGroupFadeSlot : VanillaClass
		{

			[SerializeField]
			public CanvasGroup group;

			[SerializeField]
			[Range(0, 1)]
			public float from;

			[SerializeField]
			[Range(0, 1)]
			public float to;

			public CanvasGroupFadeSlot
			(
				CanvasGroup renderer,
				float            from       = 0.0f,
				float            to         = 1.0f)
			{
				this.group = renderer;
				this.from     = from;
				this.to       = to;
			}

			public void Fade(float normal)
			{
				var f = Mathf.Lerp(a: from,
				                   b: to,
				                   t: normal);
				
				group.alpha = f;
			}


			public void ToggleActive(float normal)
			{
				var f = Mathf.Lerp(a: from,
				                   b: to,
				                   t: normal);
				
				group.gameObject.SetActive(f > Mathf.Epsilon);
			}

		}

		#endregion
		
		// ----------------------------------------------------------------------------------------------- Variables //

		#region Variables

		public CanvasGroupFadeSlot[] fadeGroupArray = new CanvasGroupFadeSlot[0];

		#endregion

		// ---------------------------------------------------------------------------------------------- Validation //

		#region Validation



		protected override void InSceneValidation()
		{
			base.InSceneValidation();

			easing.Validate(behaviour: this);

			var n = easing.method(i: _localNormal);
			
			foreach (var f in fadeGroupArray)
			{
				if (f.group == null) continue;

				f.Validate(behaviour: this);
				f.Fade(normal: n);
				f.ToggleActive(normal: n);
			}

		}

		#endregion

		// ----------------------------------------------------------------------------------------------- Overrides //

		#region Overrides

		protected override void Handle_Enter()
		{
			base.Handle_Enter();

			ToggleElementsActiveState();
		}

		protected override void Handle_Exit()
		{
			base.Handle_Exit();

			ToggleElementsActiveState();
		}


		private void ToggleElementsActiveState()
		{
			var n = easing.method(i: _localNormal);

			foreach (var f in fadeGroupArray) f.ToggleActive(n);
		}


		protected override void Run()
		{
			var n = easing.method(i: _localNormal);

			foreach (var f in fadeGroupArray) f.Fade(normal: n);
		}

		protected override string GetAutomaticGearName()
		{
			var fromString    = string.Empty;
			var toString      = string.Empty;
			var elementString = string.Empty;

			if (fadeGroupArray.Length == 0)
			{
				fromString = toString = elementString = "?";
			}
			else
			{
				if (invertNormal)
				{
					fromString = $"{fadeGroupArray[0].to:0.##}";
					toString   = $"{fadeGroupArray[0].from:0.##}";
				}
				else
				{
					fromString = $"{fadeGroupArray[0].from:0.##}";
					toString   = $"{fadeGroupArray[0].to:0.##}";
				}
				
				elementString = fadeGroupArray[0].group == null ? "?" : fadeGroupArray[0].group.gameObject.name;
			}
			
			return $"Fade [{elementString}] UI from [{fromString}] to [{toString}] over [{duration}]s";
		}



		#endregion

	}

}