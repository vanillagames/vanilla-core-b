// Creation Date                                                                               [ 1/02/2020 7:29:21 PM ]
// Project                                                                                               [ Rewild2020 ]
// Developer                                                                                                 [ PHORIA ]

using System;
using System.Collections.Generic;
using System.Reflection;

using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Clockwork.B
{

	public class FadeUIGear : EasedGear
	{

		// ---------------------------------------------------------------------------------------------- Subclasses //

		#region Subclasses



		[Serializable]
		public class CanvasRendererFadeSlot : VanillaClass
		{

			[SerializeField]
			public CanvasRenderer renderer;

			[SerializeField]
			[Range(0,
				1)]
			public float from;

			[SerializeField]
			[Range(0,
				1)]
			public float to;


			public CanvasRendererFadeSlot
			(
				CanvasRenderer renderer,
				float          from = 0.0f,
				float          to   = 1.0f)
			{
				this.renderer = renderer;
				this.from     = from;
				this.to       = to;
			}


			public void Fade(float normal)
			{
				var f = Mathf.Lerp(a: from,
				                   b: to,
				                   t: normal);

				renderer.SetAlpha(alpha: f);
			}


			public void ToggleActive(float normal)
			{
				var f = Mathf.Lerp(a: from,
				                   b: to,
				                   t: normal);

				renderer.gameObject.SetActive(f > Mathf.Epsilon);
			}

		}



		#endregion

		// ----------------------------------------------------------------------------------------------- Variables //

		#region Variables



		public CanvasRendererFadeSlot[] fadeArray = new CanvasRendererFadeSlot[0];



		#endregion

		// ---------------------------------------------------------------------------------------------- Validation //

		#region Validation



		protected override void InSceneValidation()
		{

			base.InSceneValidation();

			var n = easing.method(i: _localNormal);

			foreach (var f in fadeArray)
			{
				if (f.renderer == null) continue;

				f.Validate(behaviour: this);
				f.Fade(normal: n);
				f.ToggleActive(normal: n);
			}

		}



		#endregion

		// ----------------------------------------------------------------------------------------------- Overrides //

		#region Overrides



		protected override void Handle_Enter()
		{
			base.Handle_Enter();

			ToggleElementsActiveState();
		}



		protected override void Handle_Exit()
		{
			base.Handle_Exit();

			ToggleElementsActiveState();
		}


		private void ToggleElementsActiveState()
		{
			var n = easing.method(i: _localNormal);

			foreach (var f in fadeArray) f.ToggleActive(n);
		}


		protected override void Run()
		{
			var n = easing.method(i: _localNormal);

			foreach (var f in fadeArray) f.Fade(normal: n);
		}


		protected override string GetAutomaticGearName()
		{
			var fromString    = string.Empty;
			var toString      = string.Empty;
			var elementString = string.Empty;

			if (fadeArray.Length == 0) { fromString = toString = elementString = "?"; }
			else
			{
				if (invertNormal)
				{
					fromString = $"{fadeArray[0].to:0.##}";
					toString   = $"{fadeArray[0].from:0.##}";
				}
				else
				{
					fromString = $"{fadeArray[0].from:0.##}";
					toString   = $"{fadeArray[0].to:0.##}";
				}

				elementString = fadeArray[0].renderer == null ?
					                "?" :
					                fadeArray[0].renderer.gameObject.name;
			}

			return $"Fade [{elementString}] from [{fromString}] to [{toString}]";
		}



		#endregion

	}

}