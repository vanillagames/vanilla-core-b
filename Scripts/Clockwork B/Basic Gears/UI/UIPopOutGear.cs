﻿using Vanilla.UI;

namespace Vanilla.Clockwork.B
{

	public class UIPopOutGear : EasedGear
	{

		public UIPopOut popOut;


		protected override void Run()
		{
			popOut.PopOutFrame(n: easing.method(i: localNormal));
		}


		protected override void Handle_Exit()
		{
			base.Handle_Exit();
		}

	}

}