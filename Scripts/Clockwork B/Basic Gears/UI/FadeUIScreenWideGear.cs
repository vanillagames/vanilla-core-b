// Creation Date                                                                             [ 23/03/2020 11:20:44 AM ]
// Project                                                                                                   [ Rewild ]
// Developer                                                                                                 [ PHORIA ]

using UnityEngine;

using Vanilla.Clockwork.B;
using Vanilla.Math;

namespace Vanilla.SceneWorks
{
	public class FadeUIScreenWideGear : BaseGear
	{
		
		// ----------------------------------------------------------------------------------------------- Variables //
		
		#region Variables
		
		public CanvasRenderer screenImage;
		
		#endregion
		
        
		// ---------------------------------------------------------------------------------------------- Validation //
		
		#region Validation
		
		protected override void InSceneValidation()
		{
			base.InSceneValidation();

			Run();

			ToggleElementsActiveState();

//			NameSelf("Screen Fader");
		}
		
		#endregion

		// ----------------------------------------------------------------------------------------------- Overrides //

		#region Overrides
		
		protected override void Handle_Enter()
		{
			base.Handle_Enter();
			
			ToggleElementsActiveState();
		}


		protected override void Handle_Exit()
		{
			ToggleElementsActiveState();
		}


		private void ToggleElementsActiveState()
		{
			// Turn the elements off if the normal is 0.0f
			screenImage.gameObject.SetActive(!_localNormal.IsRoughly(0.0f));
		}


		protected override void Run()
		{
			screenImage.SetAlpha(_localNormal);
		}
		
		
		#endregion

	}
}