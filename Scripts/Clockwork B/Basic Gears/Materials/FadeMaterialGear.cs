// Creation Date                                                                              [ 24/03/2020 7:08:33 PM ]
// Project                                                                                                   [ Rewild ]
// Developer                                                                                                 [ PHORIA ]

using System;

using UnityEngine;

using Vanilla.Clockwork.B;



namespace Vanilla.Clockwork
{
	public class FadeMaterialGear : BaseGear
	{

		public Material material;

		public String _CustomColorID = "_PlaneColor";

		public enum MaterialColorAccessMethod
		{

			MaterialDotColor,
			ColorShaderProperty,
			TintShaderProperty,
			CustomColorID

		}

		public MaterialColorAccessMethod accessMethod;

		[Range(0.0f, 1.0f)]
		public float fadeFactor = 1.0f;
		
		private static readonly int Color = Shader.PropertyToID("_Color");
		private static readonly int Tint = Shader.PropertyToID("_Tint");


		protected override void Run()
		{
			var c = GetColor();

			c.a = localNormal * fadeFactor;

			SetColor(c);
		}


		private Color GetColor()
		{
			switch (accessMethod)
			{
				case MaterialColorAccessMethod.MaterialDotColor: return material.color;

				case MaterialColorAccessMethod.ColorShaderProperty: return material.GetColor(nameID: Color);

				case MaterialColorAccessMethod.TintShaderProperty: return material.GetColor(nameID: Tint);

				case MaterialColorAccessMethod.CustomColorID: return material.GetColor(_CustomColorID);

				default: throw new ArgumentOutOfRangeException();
			}
		}


		private void SetColor(Color c)
		{
			switch (accessMethod)
			{
				case MaterialColorAccessMethod.MaterialDotColor:

					material.color = c;

					break;

				case MaterialColorAccessMethod.ColorShaderProperty:

					material.SetColor(nameID: Color,
					                  value: c);

					break;

				case MaterialColorAccessMethod.TintShaderProperty:

					material.SetColor(nameID: Tint,
					                  value: c);

					break;

				case MaterialColorAccessMethod.CustomColorID:

					
					material.SetColor(_CustomColorID,
						value: c);

					break;

				default: throw new ArgumentOutOfRangeException();
			}
		}
	}
}