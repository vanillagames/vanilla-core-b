﻿//using UnityEngine;
//
//using Vanilla.Clockwork.B;
//
//namespace Vanilla.Clockwork
//{
//
//	/// <summary>
//	/// 	This gear can be used in junction with a GearAsset to remotely Run() a gear in another scene.
//	/// </summary>
//	public class RemoteGear : BaseGear
//	{
//
//		public GearAsset remoteGear;
//
//		protected override void Run()
//		{
//			var g = remoteGear.gear;
//
//			g.localTime = Mathf.Lerp(a: 0,
//			                         b: g.duration,
//			                         t: _localNormal);
//
//			g.Run_Internal();
//		}
//
//	}
//
//}