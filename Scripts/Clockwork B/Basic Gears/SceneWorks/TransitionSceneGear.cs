// Creation Date                                                                              [ 12/03/2020 8:12:57 PM ]
// Project                                                                                                   [ Rewild ]
// Developer                                                                                                 [ PHORIA ]

using UnityEngine;

namespace Vanilla.SceneWorks
{

	public abstract class TransitionSceneGear : TransitionGear
	{

//		[Tooltip("Is this transition registered for event responses?")] [ReadOnly] [SerializeField]
//		private bool registeredWithSceneLoader = false;


//		protected virtual void Start()
//		{
//			RegisterWithSceneLoader();
//		}
//
//
//		protected virtual void OnDestroy()
//		{
//			if (applicationIsQuitting) return;
//			
//			DeregisterWithSceneLoader();
//		}


//		public virtual void RegisterWithSceneLoader()
//		{
//			if (registeredWithSceneLoader) return;
//
//			if (curtainsDownTransition)
//			{
//				onExitForwards.AddListener(SceneLoader.i.BeginLoadOperation);
//
//				if (curtainsUpTransition)
//				{
//					onExitBackwards.AddListener(SceneLoader.i.PostLoadTransitionEnded);
//				}
//			}
//			else
//			{
//				if (curtainsUpTransition)
//				{
//					onExitForwards.AddListener(SceneLoader.i.PostLoadTransitionEnded);
//				}
//			}
//
//			registeredWithSceneLoader = true;
//		}
//
//
//		public virtual void DeregisterWithSceneLoader()
//		{
//			if (!registeredWithSceneLoader) return;
//
//			if (curtainsDownTransition)
//			{
//				onExitForwards.RemoveListener(SceneLoader.i.BeginLoadOperation);
//
//				if (curtainsUpTransition)
//				{
//					onExitBackwards.RemoveListener(SceneLoader.i.PostLoadTransitionEnded);
//				}
//			}
//			else
//			{
//				if (curtainsUpTransition)
//				{
//					onExitForwards.RemoveListener(SceneLoader.i.PostLoadTransitionEnded);
//				}
//			}
//
//			registeredWithSceneLoader = false;
//		}
		
		protected abstract override void Run();


//		public override void NameSelf
//		(
//			string subtitle)
//		{
//			gameObject.name =
//				$"[ Transition Scene Gear ] [ {subtitle} ] [ {( curtainsDownTransition ? "Down" : "-" )} | {( curtainsUpTransition ? "Up" : "-" )} ]";
//		}

	}

}