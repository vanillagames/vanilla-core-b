﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanilla.Clockwork.B;

namespace Vanilla.Clockwork.B
{
    public class AnimatorGear : BaseGear
    {
        public Animator animator;

        public string animationLayer = "Base Layer";

        public AnimationClip clip;

        public bool durationLockedToClipLength = true;

        public bool playBackwards;
        
        [SerializeField, ReadOnly]
        private int stateHash = 0;

        public void Awake()
        {
            if (!animator || !clip) return;

            stateHash = Animator.StringToHash($"{animationLayer}.{clip.name}");

            if (durationLockedToClipLength)
            {
                _duration = clip.length;
            }

//            if (!_initialGear)
//            {
//                activeGears.Remove(this);
//
////                TurnOff();
//            }
        }
        
        protected override void Handle_Enter()
        {
            base.Handle_Enter();
            
            if (!animator) return;
            
            animator.enabled = true;
        }

        protected override void Handle_Exit()
        {
            base.Handle_Exit();
            
            if (!animator) return;

            animator.enabled = false;
        }

        protected override void Run()
        {
            if (!animator) return;

            if (playBackwards)
            {
                animator.Play(
                    stateNameHash: stateHash,
                    layer: -1,
                    normalizedTime: 1-_localNormal);
            }
            else
            {
                animator.Play(
                    stateNameHash: stateHash,
                    layer: -1,
                    normalizedTime: _localNormal);
            }
        }
    }
}