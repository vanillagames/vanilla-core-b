﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanilla;
using Vanilla.Clockwork.B;
using Object = System.Object;

[Serializable]
public class AnimationGear : BaseGear
{
    public Animation animation;

    public AnimationClip clip;

    protected override string GetAutomaticGearName() => $"Play [{clip.name}] anim over [{duration}]s";

    public void Awake()
    {
        animation.clip              = clip;
        animation[clip.name].weight = 1;
        animation[clip.name].speed  = 0;
    }


    protected override void Handle_Enter()
    {
        base.Handle_Enter();

        animation.Play();
    }


    protected override void Handle_Exit()
    {
        animation.Stop();

        base.Handle_Exit();
    }


    protected override void Run() => animation[clip.name].normalizedTime = _localNormal;

}