// Creation Date                                                                              [ 12/03/2020 8:12:57 PM ]
// Project                                                                                                   [ Rewild ]
// Developer                                                                                                 [ PHORIA ]

using UnityEngine;

using Vanilla.Clockwork.B;

namespace Vanilla.SceneWorks
{
	public abstract class TransitionGear : BaseGear
	{
//		[Tooltip("If true, this transition is considered to have a blacked out screen by the time it ends aka 'curtains down'.")]
//		public bool curtainsDown = true;

		protected abstract override void Run();
	}
}