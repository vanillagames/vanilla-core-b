// Creation Date                                                                              [ 31/01/2020 8:21:57 PM ]
// Project                                                                                               [ Rewild2020 ]
// Developer                                                                                                 [ PHORIA ]

using UnityEngine;

namespace Vanilla.Clockwork.B
{

	public class PositionGear : BaseGear
	{

		// ----------------------------------------------------------------------------------------------- Variables //

		#region Variables



		public Transform input;

		public Vector3 from;
		public Vector3 to;

		public bool local;



		#endregion


		// ----------------------------------------------------------------------------------------------- Overrides //

		#region Overrides



		protected override void Run()
		{
			if (local)
			{
				input.localPosition = Vector3.Lerp(a: from,
				                                   b: to,
				                                   t: localNormal);
			}
		}



		#endregion

	}

}