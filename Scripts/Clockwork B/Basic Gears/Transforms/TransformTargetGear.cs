// Creation Date                                                                              [ 31/01/2020 8:18:54 PM ]
// Project                                                                                               [ Rewild2020 ]
// Developer                                                                                                 [ PHORIA ]

using UnityEngine;

namespace Vanilla.Clockwork.B
{

	public class TransformTargetGear : EasedGear
	{

		// ----------------------------------------------------------------------------------------------- Variables //

		#region Variables



		[Header("Transform References")] [SerializeField]
		private Transform _input;
		public Transform input
		{
			get
			{
				if (_input != null) return _input;

				return _input = SourceInputTransform();
			}
			set => _input = value;
		}

		public Transform fromTarget;
		public Transform toTarget;

		[Header("Target Options")]
		public bool targetsAreLocal;

		[Header("Position Options")]
		public bool translatePosition;
		public bool positionIsLocal;

		[Header("Rotation Options")]
		public bool translateRotation;
		public bool rotationIsLocal;

		[Header("Scale Options")]
		public bool translateScale;



		#endregion

		// ---------------------------------------------------------------------------------------------- Validation //

		#region Validation



		protected override void InSceneValidation()
		{
			base.InSceneValidation();

			easing.Validate(this);

//			if (!_input)
//			{
//				Warning("I haven't been assigned an input transform, so I won't have anything to move.");
//			}

			if (!fromTarget)
			{
				Warning("I haven't been assigned a 'from' target transform, so I won't have anything to move/rotate/scale the input transform from.");

				return;
			}

			if (!toTarget)
			{
				Warning("I haven't been assigned a 'to' target transform, so I won't have anything to move/rotate/scale the input transform to.");
				
				return;
			}
		}


		protected override string GetAutomaticGearName() =>
			$"Translate [{( _input == null ? "?" : _input.name )}] over [{duration:0.#}] seconds";



		#endregion


		// -------------------------------------------------------------------------------------------- Inheritables //

		#region Inheritables



		protected virtual Transform SourceInputTransform()
		{
			return null;
		}



		#endregion

		// ----------------------------------------------------------------------------------------------- Overrides //

		#region Overrides



		protected override void Run()
		{
			if (!input) return;

			var n = easing.method(localNormal);

			if (translatePosition)
			{
				if (positionIsLocal)
				{
					input.localPosition = targetsAreLocal ?
						                      Vector3.Lerp(a: fromTarget.localPosition,
						                                   b: toTarget.localPosition,
						                                   t: n) :
						                      Vector3.Lerp(a: fromTarget.position,
						                                   b: toTarget.position,
						                                   t: n);
				}
				else
				{
					input.position = Vector3.Lerp(a: fromTarget.position,
					                              b: toTarget.position,
					                              t: n);
				}
			}

			if (translateRotation)
			{
				if (rotationIsLocal)
				{
					input.localRotation = targetsAreLocal ?
						                      Quaternion.Lerp(a: fromTarget.localRotation,
						                                      b: toTarget.localRotation,
						                                      t: n) :
						                      Quaternion.Lerp(a: fromTarget.rotation,
						                                      b: toTarget.rotation,
						                                      t: n);
				}
				else
				{
					input.rotation = Quaternion.Lerp(a: fromTarget.rotation,
					                                 b: toTarget.rotation,
					                                 t: n);
				}
			}

			if (translateScale)
			{
				input.localScale = targetsAreLocal ?
					                   Vector3.Lerp(a: fromTarget.localScale,
					                                b: toTarget.localScale,
					                                t: n) :
					                   Vector3.Lerp(a: fromTarget.lossyScale,
					                                b: toTarget.lossyScale,
					                                t: n);
			}
		}



		#endregion

	}

}