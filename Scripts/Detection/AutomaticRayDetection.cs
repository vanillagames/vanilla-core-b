﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Basic
{

    public class AutomaticRayDetection<T0, T1> : RayDetection<T0, T1>
        where T0 : MonoBehaviour, IDetectable<T0, T1>, new()
        where T1 : DetectionSystem<T0, T1>
    {

        public Space raySpace = Space.Self;

        public Vector3 rayDirection = Vector3.forward;

        [Range(0.0f, 60.0f)]
        public float checkInterval = 1.0f;

        [SerializeField, ReadOnly]
        private float rayTimer = 0.0f;


        public virtual void Update()
        {
            if (!rayTimer.HasElapsed(timerLength: checkInterval)) return;

            Check(origin: t.position,
                  direction: raySpace == Space.World ?
                                 rayDirection :
                                 t.TransformDirection(direction: rayDirection));
        }

    }

}