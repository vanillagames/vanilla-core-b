﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Basic
{

    [Serializable]
    public abstract class RayDetection<T0, T1> : DetectionSystem<T0, T1>
        where T0 : MonoBehaviour, IDetectable<T0, T1>, new()
        where T1 : DetectionSystem<T0, T1>
    {

        [Header("Ray Settings")]
        public LayerMask layerMask;

        [Range(0.1f, 100.0f)]
        public float rayLength = 5.0f;

        private Color _detectionStateColor = Color.red;

        public float debugRayLifetime = 1.0f;

        // -------------------------------------------------------------------------------------------------- Checks //


        protected void Check
        (
            Vector3 origin,
            Vector3 direction)
        {
            Physics.Raycast(origin: origin,
                            direction: direction,
                            hitInfo: out var hit,
                            maxDistance: rayLength,
                            layerMask: layerMask);

            gameObjectSlot.current = hit.collider ?
                                         hit.collider.gameObject :
                                         null;

            #if UNITY_EDITOR
            DoDebugRay(origin: origin,
                       direction: direction);
            #endif
        }


        protected void Check(Ray ray)
        {
            Physics.Raycast(ray: ray,
                            hitInfo: out var hit,
                            maxDistance: rayLength,
                            layerMask: layerMask);

            gameObjectSlot.current = hit.collider ?
                                         hit.collider.gameObject :
                                         null;

            #if UNITY_EDITOR
            DoDebugRay(origin: ray.origin,
                       direction: ray.direction);
            #endif
        }


        // ------------------------------------------------------------------------------ Detection System Overrides //


        protected override void HandleDetectedObjectChange
        (
            GameObject old,
            GameObject @new)
        {
            Log($"Detection changed! [{@old}] [{@new}]");
        }


        protected override void HandleDetectionChange
        (
            T0 old,
            T0 @new)
        {
            #if UNITY_EDITOR
            _detectionStateColor = componentDetected ?
                                       Color.green :
                                       Color.red;
            #endif
        }


        // --------------------------------------------------------------------------------------------------- Debug //

        #if UNITY_EDITOR
        void DoDebugRay
        (
            Vector3 origin,
            Vector3 direction)
        {
            if (debugFlags.HasFlag(flag: DebugFlags.Gizmos))
                DrawDebugRay(rayOrigin: origin,
                             rayDir: direction,
                             rayLength: rayLength,
                             color: _detectionStateColor,
                             duration: Mathf.Max(a: debugRayLifetime,
                                                 b: Time.deltaTime));
        }
        #endif

    };

}