﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Basic
{

    public interface IDetectable<in T0, T1>
        where T0 : MonoBehaviour, IDetectable<T0, T1>, new()
        where T1 : DetectionSystem<T0, T1>
    {

        T1 detectionSystem { get; set; }

        void DetectionBegun(T0 replaced);

        void DetectionEnded(T0 replacedBy);

    }

    [Serializable]
    public abstract class DetectionSystem<T0, T1> : ActiveBehaviour
        where T0 : MonoBehaviour, IDetectable<T0, T1>, new()
        where T1 : DetectionSystem<T0, T1>
    {

        [Header("Current Detection")] [SerializeField]
        public ReferenceSlot<GameObject> gameObjectSlot = new ReferenceSlot<GameObject>();

        [SerializeField, ReadOnly]
        public bool gameObjectDetected;

        [SerializeField]
        public ReferenceSlot<T0> componentSlot = new ReferenceSlot<T0>();

        [SerializeField, ReadOnly]
        public bool componentDetected;

        public GetComponentStyle GetComponentStyle = GetComponentStyle.OnObject;

        public void Awake() { Initialize(); }


        public void Initialize()
        {
            gameObjectSlot.onSlotChange.AddListener(HandleDetectedObjectChange_Internal);
            componentSlot.onSlotChange.AddListener(HandleDetectedChange_Internal);
        }


        private void HandleDetectedObjectChange_Internal
        (
            GameObject @old,
            GameObject @new)
        {
            gameObjectDetected = !SilentNullCheck(@new);

            componentSlot.current = gameObjectDetected ?
                                        @new.GetComponentDynamic<T0>(GetComponentStyle) :
                                        null;

            HandleDetectedObjectChange(old: @old,
                                       @new: @new);
        }


        private void HandleDetectedChange_Internal
        (
            T0 @old,
            T0 @new)
        {
            // Before we update componentDetected, is it still set from the last assignment?
            // This saves us from having to null check @old a second time.
            if (componentDetected)
            {
                @old.DetectionEnded(@new);

                @old.detectionSystem = null;
            }

            componentDetected = !SilentNullCheck(@new);

            if (componentDetected)
            {
                @new.detectionSystem = this as T1;

                @new.DetectionBegun(@old);
            }
            else if (gameObjectDetected)
                Error($"A GameObject has been passed into this DetectionSystem that doesn't have the expected "
                      + $"detection component type [{typeof(T0)}]. We can handle this gracefully, but it has likely "
                      + "occurred in error. Check your setup!");

            HandleDetectionChange(old: @old,
                                  @new: @new);
        }


        protected abstract void HandleDetectedObjectChange
        (
            GameObject @old,
            GameObject @new);


        protected abstract void HandleDetectionChange
        (
            T0 @old,
            T0 @new);

    }

}