﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Basic
{
    public class TouchRayDetection<T0, T1> : RayDetection<T0, T1>
        where T0 : MonoBehaviour, IDetectable<T0, T1>, new()
        where T1 : DetectionSystem<T0, T1>
    {
        public Camera tapCamera;

        [Tooltip("Which phase do we want the ray to fire on?")]
        public TouchPhase touchRayPhase = TouchPhase.Began;

        public bool useScreenBounds;

        public Range xRange;
        public Range yRange;

        public void Update()
        {
            #if UNITY_EDITOR
                EditorUpdate();
            #else
                MobileUpdate();
            #endif
        }

        private void EditorUpdate()
        {
            switch (touchRayPhase)
            {
                default: return;
                
                case TouchPhase.Began: if (!Input.GetMouseButtonDown(0)) return;
                    break;

                case TouchPhase.Ended: if (!Input.GetMouseButtonUp(0)) return;
                    break;
            }
            
            FireCheckRay();
        }

        private void MobileUpdate()
        {
            foreach (var touch in Input.touches)
            {
                if (touch.phase != touchRayPhase) continue;
                
                FireCheckRay();
            }
        }

        protected virtual void FireCheckRay()
        {
            var p = Input.mousePosition;

            if (useScreenBounds && (!xRange.InclusivelyContains(p.x) || !yRange.InclusivelyContains(p.y))) 
            {
                Warning("Ray cancelled: Touch position is outside of nominated screen bounds.");
                return;
            }

            Check(tapCamera.ScreenPointToRay(p));
        }
    }
}