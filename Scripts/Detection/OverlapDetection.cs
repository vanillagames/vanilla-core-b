﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//
//using Vanilla;
//
//using Vanilla.Basic;
//
//namespace Vanilla.Detection
//{
//    public class OverlapDetection<T0, T1> : DetectionSystem<T0, T1>
//        where T0 : MonoBehaviour, IDetectable<T0, T1>
//        where T1 : DetectionSystem<T0, T1>
//    {
//        public enum OverlapPrimitiveType
//        {
//            Box,
//            Sphere,
//            Capsule
//        }
//
//        public OverlapPrimitiveType overlapShapeType = OverlapPrimitiveType.Sphere;
//
//        [Range(0.0f, 60.0f)] 
//        public float checkInterval = 1.0f;
//
//        [SerializeField, ReadOnly]
//        private float checkTimer = 0.0f;
//        
//        public LayerMask layerMask;
//        
//        public float shapeSize = 1.0f;
//        
//        public List<Collider> detectionList = new List<Collider>();
//
//        public Collider[] overlapObjects = new Collider[10];
//        
//        public void CheckSphere()
//        {
//            Physics.OverlapSphereNonAlloc(
//                position:        transform.position, 
//                radius:          shapeSize, 
//                results:         overlapObjects,
//                layerMask:       layerMask);
//        }
//
//        public void CheckBox()
//        {
//            Physics.OverlapBoxNonAlloc(
//                center:          transform.position,
//                halfExtents:     new Vector3(shapeSize * 0.5f, shapeSize * 0.5f, shapeSize * 0.5f),
//                results:         overlapObjects, 
//                orientation:     Quaternion.identity, 
//                mask:            layerMask);
//        }
//
//        public void CheckCapsule(Vector3 pointA, Vector3 pointB)
//        {
//            Physics.OverlapCapsuleNonAlloc(
//                point0:          pointA,
//                point1:          pointB, 
//                radius:          shapeSize, 
//                results:         overlapObjects, 
//                layerMask:       layerMask);
//        }
//
//        public override void DetectionBegun(T0 newDetection)
//        {
//            
//        }
//
//        public override void DetectionHeld()
//        {
//
//        }
//
//        public override void DetectionEnded(T0 oldDetection)
//        {
//            
//        }
//    }
//}