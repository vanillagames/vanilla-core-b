﻿using System.Text;

using UnityEngine;
using UnityEngine.Events;

namespace Vanilla.Core.Debugging
{

	public class DebugKeyEvents : VanillaSingleton<DebugKeyEvents>
	{

		public KeyCode[] debugKeys =
		{
			KeyCode.Escape,
			KeyCode.F1,
			KeyCode.F2,
			KeyCode.F3,
			KeyCode.F4,
			KeyCode.F5,
			KeyCode.F6,
			KeyCode.F7,
			KeyCode.F8,
			KeyCode.F9,
			KeyCode.F10,
			KeyCode.F11,
			KeyCode.F12
		};

		public UnityEvent[] debugEvents = new UnityEvent[13];

		protected override void InSceneValidation()
		{
			base.InSceneValidation();

			if (debugEvents.Length == debugKeys.Length) return;

			if (debugKeys.Length > debugEvents.Length)
			{
				var newEvents = new UnityEvent[debugKeys.Length];

				debugEvents.CopyTo(array: newEvents,
				                   index: 0);

				debugEvents = newEvents;
			}
			else
			{
				var newEvents = new UnityEvent[debugKeys.Length];

				for (var i = 0; i < debugKeys.Length; i++)
				{
					newEvents[i] = debugEvents[i];
				}
				
				debugEvents = newEvents;
			}
		}


		void Update()
		{
			for (var k = 0; k < debugKeys.Length; k++)
			{
				if (!Input.GetKeyDown(debugKeys[k])) continue;

				Log($"Invoking DebugKeyEvent for [{debugKeys[k]}]");
					
				debugEvents[k].Invoke();
			}
		}

	}

}