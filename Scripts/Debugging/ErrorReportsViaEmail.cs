﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using UnityEngine;

using Vanilla;

using static Vanilla.Vanilla;

public class ErrorReportsViaEmail : VanillaBehaviour
{

	private const string c_Email = "sasquatchbeta7@gmail.com"; 

	private const string c_Pass = "Simple123D";

	private readonly StringBuilder bodyBuilder = new StringBuilder(1024); // lol

	private readonly StringBuilder logBuilder = new StringBuilder(8196);

	public enum ReportSendStyle
	{

		EmailBody = 0,
		TextFileEmailAttachment = 1,
//		JSONFile = 2 // Not implemented.

	}

	[Header("Report destination")]

	public ReportSendStyle reportSendStyle = ReportSendStyle.TextFileEmailAttachment;
	
	[Header("Device Diagnostics")]

	public bool ReportIncludesApplicationInfo = true;
	public bool ReportIncludesDeviceInfo = true;
	public bool ReportIncludesOperatingSystemInfo = true;
	public bool ReportIncludesProcessorsInfo = true;
	public bool ReportIncludesMemoryInfo = true;
	public bool ReportIncludesDiskSpaceInfo = true;
	public bool ReportIncludesBatteryInfo = true;
	public bool ReportIncludesAudioSupportInfo = true;
	public bool ReportIncludesHardwareSupportInfo = true;
	public bool ReportIncludesRenderingSupportInfo = true;
	public bool ReportIncludesGPUSupportInfo = true;
	public bool ReportIncludesShaderSupportInfo = true;
	public bool ReportIncludesComputeSupportInfo = true;
	public bool ReportIncludesTextureSupportInfo = true;
	public bool ReportIncludesMiscInfo = true;
	
	[Header("Logs")]

	public bool ReportIncludesLogs = false;
	public bool ReportIncludesWarnings = false;
	public bool ReportIncludesErrors = true;
	public bool ReportIncludesExceptions = true;
	
	public void Start()
	{
		#if DEVELOPMENT_BUILD && !UNITY_EDITOR

		bodyBuilder.Append($"App Session Report for [{Application.productName}]\n");

		bodyBuilder.Append("\n// -------------------- Device Report\n\n");
		
		if (ReportIncludesApplicationInfo) 		AppendSystemInfoForApplication(bodyBuilder);
		if (ReportIncludesDeviceInfo) 			AppendSystemInfoForDevice(bodyBuilder);
		if (ReportIncludesOperatingSystemInfo) 	AppendSystemInfoForOperatingSystem(bodyBuilder);
		if (ReportIncludesProcessorsInfo) 		AppendSystemInfoForProcessors(bodyBuilder);
		if (ReportIncludesMemoryInfo) 			AppendSystemInfoForMemory(bodyBuilder);
		if (ReportIncludesDiskSpaceInfo) 		AppendSystemInfoForDiskSpace(bodyBuilder);
		if (ReportIncludesBatteryInfo) 			AppendSystemInfoForBattery(bodyBuilder);
		if (ReportIncludesAudioSupportInfo) 	AppendSystemInfoForAudioSupport(bodyBuilder);
		if (ReportIncludesHardwareSupportInfo) 	AppendSystemInfoForHardwareSupport(bodyBuilder);
		if (ReportIncludesRenderingSupportInfo) AppendSystemInfoForRenderingSupport(bodyBuilder);
		if (ReportIncludesGPUSupportInfo) 		AppendSystemInfoForGPUSupport(bodyBuilder);
		if (ReportIncludesShaderSupportInfo) 	AppendSystemInfoForShaderSupport(bodyBuilder);
		if (ReportIncludesComputeSupportInfo) 	AppendSystemInfoForComputeSupport(bodyBuilder);
		if (ReportIncludesTextureSupportInfo) 	AppendSystemInfoForTextureSupport(bodyBuilder);
		if (ReportIncludesMiscInfo) 			AppendSystemInfoForMisc(bodyBuilder);

		bodyBuilder.Append("\n// -------------------- Log History\n\n");

//		logBuilder = new StringBuilder(logBuilderCharacterMax);
		
		Application.logMessageReceivedThreaded += NewLogWithTraces;

		#endif
	}


//	public void Update()
//	{
////		#if DEVELOPMENT_BUILD && UNITY_EDITOR
//		if (Input.touchCount > 4 && Input.GetTouch(4).phase == TouchPhase.Ended) SendErrorReportEmail();
////		#endif
//	}

	public void NewLogWithTraces
	(
		string  condition,
		string  stackTrace,
		LogType type)
	{
		#if DEVELOPMENT_BUILD && !UNITY_EDITOR

		switch (type)
		{
			case LogType.Log:
				if (ReportIncludesLogs) 
					logBuilder.Append($"\nLog\n\n{condition}\n\n{stackTrace}\n"); 
					break;

			case LogType.Warning:
				if (ReportIncludesWarnings) 
					logBuilder.Append($"\nWarning!\n\n{condition}\n\n{stackTrace}\n"); 
					break;

			case LogType.Error:
				if (ReportIncludesErrors) 
					logBuilder.Append($"\nError!\n\n{condition}\n\n{stackTrace}\n"); 
					break;

			case LogType.Exception:
				if (ReportIncludesExceptions) 
					logBuilder.Append($"\nEXCEPTION\n\n{condition}\n\n{stackTrace}\n"); 
					break;
		}

		#endif
	}

	// The Application Quit/Focus/Pause situation is really confusing.
	// Basically Quit never fires for Android or iOS, and Focus/Pause both work properly.
	// However, Focus fires under some unusual circumstances in Android like when the input keyboard appears,
	// so Pause seems to be the preferred winner by default.
	
//	private void OnApplicationPause(bool pauseStatus)
//	{
//		if (pauseStatus)
//		{
//			SendErrorReportEmail();
//		}
//	}

	void Update()
	{
		if (Input.touchCount < 2 || Input.GetTouch(1).phase != TouchPhase.Began) return;

		var w = (float) Screen.width / 10;
		var h = (float) Screen.height / 10;
		
		var p1 = Input.GetTouch(0).position;

		var p2 = Input.GetTouch(1).position;
		
		// Back out if touch 1 isn't in the bottom (?) left corner
		if (p1.x > w || p1.y > h) return;

		// Back out if touch 2 isn't in the opposite corner
		if (p2.x < Screen.width - w || p2.y < Screen.height - h) return;
		
		SendErrorReportEmail();
	}

	[ContextMenu("Send Error Report Email")]
	public void SendErrorReportEmail()
	{
		
		#if DEVELOPMENT_BUILD
		Log("Sending session log...");
		#endif
		
		// Make a mail instance

		var mail = new MailMessage(from: c_Email,
		                           to: c_Email)
		           {
			           Subject =
				           $"{Application.productName} - {Application.version} - {Application.platform} - {SystemInfo.deviceName}",
			           Body = reportSendStyle == ReportSendStyle.EmailBody ?
				                  bodyBuilder.ToString() + logBuilder.ToString() :
				                  string.Empty
		           };

		// Keep a screenshot
		
		var screenShotPath = Application.temporaryCachePath + "ErrorReportScreenshot";
		
		ScreenCapture.CaptureScreenshot(screenShotPath);

		if (File.Exists(path: screenShotPath))
			mail.Attachments.Add(item: new Attachment(fileName: screenShotPath)); 

		// Write the report text file

		if (reportSendStyle == ReportSendStyle.TextFileEmailAttachment)
		{
			var reportPath = Application.persistentDataPath + "/report.txt";

			File.WriteAllText(path: reportPath,
			                  contents: bodyBuilder.ToString() + logBuilder.ToString());

			if (File.Exists(path: reportPath)) mail.Attachments.Add(item: new Attachment(fileName: reportPath));
		}

		// Fly, you fools!
		
		var c = new SmtpClient
		        {
			        Host           = "smtp.gmail.com",
			        Port           = 587,
			        DeliveryMethod = SmtpDeliveryMethod.Network,
			        Credentials = new NetworkCredential(userName: c_Email,
			                                            password: c_Pass),
			        EnableSsl = true
		        };

		ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

		c.Send(mail);
	}
	
}