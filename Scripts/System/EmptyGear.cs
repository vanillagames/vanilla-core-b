﻿using Vanilla.Clockwork.B;

namespace Vanilla.Clockwork.B
{

    public class EmptyGear : BaseGear
    {

        protected override void Run() { }

        protected override string GetAutomaticGearName()
        {
            var newName = string.Empty;

            if (onEnter.GetPersistentEventCount() > 0)
            {
                newName += "Call ";

                for (var e = 0; e < onEnter.GetPersistentEventCount(); e++)
                {
                    newName += $"[{onEnter.GetPersistentTarget(e).name}.{onEnter.GetPersistentMethodName(e)}] ";
                }
            }

            if (duration > 0.1) newName += $"{( string.IsNullOrEmpty(newName) ? "W" : "w" )}ait [{duration}]s";

            if (onExit.GetPersistentEventCount() > 0)
            {
                newName += " then call";

                for (var e = 0; e < onExit.GetPersistentEventCount(); e++)
                {
                    newName += $" [{onExit.GetPersistentTarget(e).name}.{onExit.GetPersistentMethodName(e)}]";
                }

            }

            return newName;
        }

    }

}