// Created                                                                                    [ 27/01/2020 5:22:53 PM ]
// Project                                                                                               [ Rewild2020 ]
// Developed by                                                                                             [ Vanilla ]

using System;

namespace Vanilla.SceneWorks
{
	public abstract class BaseVanillaScene : VanillaBehaviour
	{
		
		// ---------------------------------------------------------------------------------------------- Validation //

		#region Validation

		protected override void InSceneValidation()
		{
			#if UNITY_EDITOR
				base.InSceneValidation();
				
				NameSelf();
			#endif
		}

		#endregion
		
		// ----------------------------------------------------------------------------------------------- Overrides //

		#region Overrides

		public override void NameSelf()
		{
#if UNITY_EDITOR
			gameObject.name = $"+ {gameObject.scene.name}";
#endif
		}
		
		public override void NameSelf(string subtitle)
		{
#if UNITY_EDITOR
			gameObject.name = $"+ {gameObject.scene.name} [{subtitle}]";
#endif
		}

		#endregion

	}
}