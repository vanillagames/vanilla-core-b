﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

using Object = UnityEngine.Object;

namespace Vanilla.SceneWorks
{
//
//	[Serializable]
//	public sealed class SmartSceneAssetSlot : VanillaClass
//	{
//		[SerializeField]
//		private Object sceneAsset;
//
//		[SerializeField, ReadOnly]
//		private string _name = string.Empty;
//		public string name
//		{
//			get => _name;
//		}
//
//		[SerializeField, ReadOnly]
//		private int _buildIndex = -1;
//		public int buildIndex
//		{
//			get => _buildIndex;
//		}
//
//
//		public override void Validate<V>
//		(
//			V behaviour)
//		{
//			base.Validate(behaviour);
//
//			// XNullChecks don't work here? They return false, as in the slot isn't null...
//			if (sceneAsset == null)
//			{
//				_name = string.Empty;
//				
//				_buildIndex = -1;
//				
//				return;
//			}
//
//			if (!( sceneAsset is SceneAsset ))
//			{
//				Warning("This SmartSceneAsset is populated with an asset that isn't a Unity scene asset.");
//
//				_name       = string.Empty;
//				
//				_buildIndex = -1;
//				
//				return;
//			}
//
//			_name       = ( (SceneAsset) sceneAsset ).name;
//			_buildIndex = SceneWorksUtility.GetBuildIndexOfSceneAsset(sceneAsset);
//		}
//
//	}

	public static class SceneWorksUtility
	{


		// -------------------------------------------------------------------------------------------------- Scene Slot //


		public static int GetBuildIndexOfSceneAsset
		(
			Object assetFile)
		{
			if (assetFile == null)
			{
				return -1;
			}

			#if UNITY_EDITOR
			for (var i = 0; i < EditorBuildSettings.scenes.Length; i++)
			{
				var path = EditorBuildSettings.scenes[i].path;

				var lastSlashIndex = path.LastIndexOf('/') + 1;

				var output = path.Substring(startIndex: lastSlashIndex,
				                            length: path.LastIndexOf(value: ".unity",
				                                                     comparisonType: StringComparison.Ordinal) -
				                                    lastSlashIndex);

				if (!string.Equals(a: assetFile.name,
				                   b: output,
				                   comparisonType: StringComparison.Ordinal))
					continue;

				return i;
			}
			#endif

			Vanilla.Warning(
				$"This asset file [{assetFile.name}] could not be found in the build settings scene index. Ensure the scene is present first!");

			return -1;
		}

	}

}