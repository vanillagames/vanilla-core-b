﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.Scripting;
using UnityEngine.Serialization;

using Vanilla.Clockwork.B;

namespace Vanilla.SceneWorks
{
    
    [RequireComponent(typeof(SceneSource))]
    [Serializable]
    public class SceneLoader : VanillaSingleton<SceneLoader>
    {

        // ---------------------------------------------------------------------------------------------- References //

        [SerializeField, ReadOnly]
        private SceneSource _initialSceneSource;
        public SceneSource initialSceneSource => _initialSceneSource;

        // ----------------------------------------------------------------------------------------------- Variables //
        
        [Header("Settings")]
        public bool startWithCurrentlyLoadedEditorScenes;
        
        [Header("Status")]
        [SerializeField, ReadOnly]
        private bool _loadRequested;
        public bool loadRequested => _loadRequested;
        
        [SerializeField, ReadOnly]
        private bool _loadInProgress;
        public bool loadInProgress => _loadInProgress;

        private AsyncOperation async;

        private IEnumerator loadingOperation;

//        public UnityAction onPreLoadTransitionBegun;
        public UnityAction onLoadOperationBegun;
        public UnityAction onLoadOperationCompletion;
//        public UnityAction onPostLoadTransitionEnded;

//        [SerializeField, ReadOnly]
//        private BaseGear preLoadTransitionGear;

//        [SerializeField, ReadOnly]
//        private BaseGear postLoadTransitionGear;


        private static bool initialReload = false;

        [SerializeField, ReadOnly]
        private bool unloadUnusedAssets = false;
        
//        /// <summary>
//        ///     This list represents the current target configuration of scenes to be loaded.
//        ///
//        ///     Any smart scene asset included in this list when a 'loading operation' is executed
//        ///     will be ensured to be loaded.
//        /// </summary>
//        [SerializeField]
//        public HashSet<SmartSceneAsset> currentScenes = new HashSet<SmartSceneAsset>();

//        public HashSetQueue<SmartSceneAsset> loadQueue = new HashSetQueue<SmartSceneAsset>();
//        public HashSetQueue<SmartSceneAsset> unloadQueue = new HashSetQueue<SmartSceneAsset>();

        [SerializeField]
        public HashSet<AssetReference> currentScenes = new HashSet<AssetReference>();

        [SerializeField]
        public HashSetQueue<AssetReference> loadQueue   = new HashSetQueue<AssetReference>();

        [SerializeField]
        public HashSetQueue<AssetReference> unloadQueue = new HashSetQueue<AssetReference>();

        // ---------------------------------------------------------------------------------------------- Subclasses //

        protected override void InSceneValidation()
        {
            base.InSceneValidation();
            
            _initialSceneSource = GetComponent<SceneSource>();
        }


        // ---------------------------------------------------------------------------------------------- Subclasses //

//        void Awake()
//        {
//            foreach (var g in gameObject.scene.GetRootGameObjects())
//            {
//                DontDestroyOnLoad(g);
//            }
//        }

        void Start()
        {
            #if UNITY_EDITOR

            if (startWithCurrentlyLoadedEditorScenes)
            {
                for (var s = 0; s < SceneManager.sceneCount; s++)
                {
//                    var smartSceneAsset = GetContainingSmartSceneAsset(SceneManager.GetSceneAt(s));
//
//                    if (smartSceneAsset != null)
//                    {
//                        currentScenes.Add(smartSceneAsset);
//                    }

                    currentScenes.Add(new AssetReference(AssetDatabase.AssetPathToGUID(gameObject.scene.path)));
                }

                return;
            }

            // The best way to truly clear things out is to do a 'destructive' load of the System scene.
            // But we'll get into a loading loop if we ask System to load itself in Start...
            // Let's tick a static bool to make sure this can only happen once.
            if (!initialReload)
            {
                initialReload = true;

                SceneManager.LoadSceneAsync(sceneName: gameObject.scene.name,
                                            mode: LoadSceneMode.Single);

                return;
            }

            #endif


            // The initial scene source is in a unique position.
            // We want it to be able to have a pre-load transition in case it's being called upon from the 'end'
            // of an app - i.e. the credits.
            // But we also don't want it to run the first time around because the app is loading from 'nothing'.
            // So we cache and null it just for the first load, and then repopulate it for use any time afterwards.
            
//            var preTransitionTempCache = initialSceneSource.preLoadTransitionGear;

//            initialSceneSource.preLoadTransitionGear = null;
            
            initialSceneSource.ApplyAndBeginLoad();

//            initialSceneSource.preLoadTransitionGear = preTransitionTempCache;
        }


//        public void ApplyTransitionSource
//        (
//            TransitionSource source)
//        {
//            if (source.preLoadTransitionGear)
//            {
//                preLoadTransitionGear = source.preLoadTransitionGear.gear;
//            }

//            if (source.postLoadTransitionGear)
//            {
//                postLoadTransitionGear = source.postLoadTransitionGear.gear;
//            }
//        }


        public void ApplySceneSetupAndBeginLoad
        (
            ISceneSource source)
        {
            _loadRequested = true;
            
            ApplySceneSource(source: source);

            BeginLoadOperation();

//            if (source.preLoadTransitionGear)
//            {
//                preLoadTransitionGear = source.preLoadTransitionGear.gear;
//            }

//            if (source.postLoadTransitionGear)
//            {
//                postLoadTransitionGear = source.postLoadTransitionGear.gear;
//            }

//            if (SilentNullCheck(preLoadTransitionGear))
//            {
//                If no preLoadTransitionGear is present, we should simply start the loading operation.
                
//                BeginLoadOperation();
 
//                return;
//            }

//            preLoadTransitionGear.onExit.AddListener(BeginLoadOperation);

//            preLoadTransitionGear.Activate();

//            onPreLoadTransitionBegun?.Invoke();

        }
        
        [ContextMenu("Begin Load Operation")]
        public void BeginLoadOperation()
        {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            Log("Loading operation requested.");
            #endif

//            if (preLoadTransitionGear)
//            {
//                preLoadTransitionGear.onExit.RemoveListener(BeginLoadOperation);
//            }

            if (_loadInProgress)
            {
                WaitForLoadToComplete();

                return;
            }

            LoadOperation();
        }


        private async void WaitForLoadToComplete()
        {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            Warning("A loading operation is already currently in progress. Any changes to TargetScenes will automatically be administered at the soonest available moment.");
            #endif
            
            while (_loadInProgress) await Task.Yield();
            
            LoadOperation();
        }


        private async Task LoadOperation()
        {
            _loadInProgress = true;
            
            onLoadOperationBegun?.Invoke();
            
            // Report

            #if UNITY_EDITOR
            LogLoadOperations();
            #endif

            // Unload

            while (unloadQueue.Count > 0) /* await unloadQueue.Dequeue().UnLoadScene().Task; */
            {
                var assRef = unloadQueue.Dequeue();

//                Log($"Before unloading, is this asset reference valid? [{assRef.IsValid()}]");
                
                await assRef.UnLoadScene().Task;

//                Log($"After unloading, is this asset reference still valid? [{assRef.IsValid()}]");
                
                if (!assRef.IsValid())
                {
                    currentScenes.Remove(assRef);
                }
            }

            // Memory management

            if (unloadUnusedAssets)
            {
                unloadUnusedAssets = false;
                
                var cleanupOp = Resources.UnloadUnusedAssets();

                while (!cleanupOp.isDone) await Task.Yield();
            }

            // Load

            while (loadQueue.Count > 0) /*await loadQueue.Dequeue().Load();*/
            {
                var assRef = loadQueue.Dequeue();

                Log($"Before loading, is this asset reference valid? [{assRef.IsValid()}]");

                await assRef.LoadSceneAsync(loadMode: LoadSceneMode.Additive,
                                            activateOnLoad: true,
                                            priority: 100).Task;

//                Log($"After loading, is this asset reference still valid? [{assRef.IsValid()}]");

                if (assRef.IsValid())
                {
                    currentScenes.Add(assRef); 
                }
            }

            onLoadOperationCompletion?.Invoke();

            // If there's no post-load transition, wrap up here.
//            if (SilentNullCheck(postLoadTransitionGear))
//            {
//                preLoadTransitionGear = null;

                _loadInProgress = false;

                _loadRequested = false;

                #if UNITY_EDITOR
                var output = "At the end of this load op, the current scenes are:\n\n";

                for (var s = 0; s < SceneManager.sceneCount; s++)
                {
                    output += SceneManager.GetSceneAt(s).name + "\n";
                }
                
                Log(output);
                #endif

//                return;
//            }
//
//            postLoadTransitionGear.onExit.AddListener(PostLoadTransitionEnded);

//            postLoadTransitionGear.Activate();
        }


        private void LogLoadOperations()
        {
            #if UNITY_EDITOR
            var operations = "The following scenes are flagged for unloading:\n";

            foreach (var a in unloadQueue)
            {
                operations += $"{a.editorAsset.name}\n";
            }

            operations += "\nThe following scenes are flagged for loading:\n";

            foreach (var a in loadQueue)
            {
                operations += $"{a.editorAsset.name}\n";
            }
                
            Log(operations);
            #endif
        }

/*
        private IEnumerator LoadOperationOld()
        {
            _loadInProgress = true;

            onLoadOperationBegun?.Invoke();
            
            // While there are any operations at all...
            while (unloadOperations.contents.Count + loadOperations.contents.Count > 0)
            {
                #if UNITY_EDITOR || DEVELOPMENT_BUILD
                
                var operations = "The following scenes are flagged for unloading:\n";

                foreach (var a in unloadOperations.contents)
                {
                    operations += $"{a.SceneName}\n";
                }

                operations += "\nThe following scenes are flagged for loading:\n";

                foreach (var a in loadOperations.contents)
                {
                    operations += $"{a.SceneName}\n";
                }
                
                Log(operations);
                
                Log("Commencing...");
                #endif

                SmartSceneAsset assetCache = null;
                
                
                // ----- Unloads
                
                
                while (unloadOperations.contents.Count > 0)
                {
                    assetCache = unloadOperations.contents[0];

                    if (assetCache.CannotBeUnloaded())
                    {
                        unloadOperations.contents.RemoveAt(0);
                        
                        continue;
                    }

                    #if UNITY_EDITOR || DEVELOPMENT_BUILD
                    Log($"Unload begun for [{assetCache.SceneName}]");
                    #endif

                    async = SceneManager.UnloadSceneAsync(sceneBuildIndex: assetCache.buildIndex,
                                                          options: UnloadSceneOptions.None);

                    while (async.progress < 0.99f)
                    {
                        #if UNITY_EDITOR || DEVELOPMENT_BUILD
//                        Log($"Progress [ {async.progress * 100.0f}% ]");
                        #endif
                        
                        yield return null;
                    }

                    // Lets just hang here until we're certain the scene has unloaded and recorded as much.
                    while (assetCache.UpdateLoadStatus()) yield return null;
                    
                    #if UNITY_EDITOR || DEVELOPMENT_BUILD
                    Log($"Unload complete for [{assetCache.SceneName}]");
                    #endif

                    currentScenes.Remove(assetCache);
                    
                    unloadOperations.contents.RemoveAt(0);
                }
                
                // ----- Native memory clearance calls
                
                // According to section 2.7. Resource lifecycle of this official primer on Assets and memory,
                // https://learn.unity.com/tutorial/assets-resources-and-assetbundles#5c7f8528edbc2a002053b5a6
                // Unitys native unused asset clean-up call is only invoked automatically if a Scene is loaded
                // 'destructively' - i.e. not additive. In other words, for additive scene loading like this,
                // Unity needs you to invoke this manually. I have no idea what affect this may have on
                // pre-existing use-cases, but we're about to find out. Thats why its optional behind a bool,
                // and may even need to be optional at a SceneSource level.

                if (useNativeUnloadUnusedAssetsCall)
                {
                    yield return Resources.UnloadUnusedAssets();
                }

                // ----- Loads
                
                
                while (loadOperations.contents.Count > 0)
                {
                    assetCache = loadOperations.contents[0];
                 
                    if (assetCache.CannotBeLoaded())
                    {
                        loadOperations.contents.RemoveAt(0);
                        
                        continue;
                    }

                    #if UNITY_EDITOR || DEVELOPMENT_BUILD
                    Log($"Load begun for [{assetCache.SceneName}]");
                    #endif

                    async = SceneManager.LoadSceneAsync(sceneBuildIndex: assetCache.buildIndex,
                                                        mode: LoadSceneMode.Additive);

                    while (async.progress < 0.99f)
                    {
                        #if UNITY_EDITOR || DEVELOPMENT_BUILD
//                        Log($"Progress [ {async.progress * 100.0f}% ]");
                        #endif
                        
                        yield return null;
                    }

                    // Lets just hang here until we're certain the scene has loaded and recorded as much.
                    while (!assetCache.UpdateLoadStatus()) yield return null;
                    
                    #if UNITY_EDITOR || DEVELOPMENT_BUILD
                    Log($"Load complete for [{assetCache.SceneName}]");
                    #endif

                    currentScenes.Add(assetCache);

                    loadOperations.contents.RemoveAt(0);
                }
            }

            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            Log("Load operations completed.");
            #endif

            // If there's no post-load transition, wrap up here.
            if (SilentNullCheck(postLoadTransitionGear))
            {
                preLoadTransitionGear = null;

                _loadInProgress = false;
    
                onLoadOperationCompletion?.Invoke();

                yield break;
            }

            onLoadOperationCompletion?.Invoke();

            postLoadTransitionGear.onExitForwards.AddListener(PostLoadTransitionEnded);
            
            postLoadTransitionGear.Activate();
        }
*/
//

//        private void PostLoadTransitionEnded()
//        {
//            Log("PostLoadTransitionEnded");
            
//            postLoadTransitionGear.onExit.RemoveListener(PostLoadTransitionEnded);

//            _loadInProgress = false;
//            _loadRequested = false;

//            preLoadTransitionGear  = null;
//            postLoadTransitionGear = null;

//            onPostLoadTransitionEnded?.Invoke();
//        }
        
        
        
        public void ApplySceneSource
        (
            ISceneSource source)
        {
            if (source.UnloadUnusedAssets) unloadUnusedAssets = true;

            if (source.ClearTargetScenesOnUse)
            {
                foreach (var s in currentScenes)
                {
                    unloadQueue.Enqueue(s);
                }
            }
            else
            {
                // Otherwise, are we removing the scene that contains this SceneSource?
                if (source.RemoveThisSceneOnUse && source.ThisSceneAssetReference != null)
                {
                    SearchForCurrentSceneAssetMatch(source.ThisSceneAssetReference);
                }
                
                // Remove any other scenes as requested
                foreach (var s in source.UnloadSceneReferences)
                {
                    SearchForCurrentSceneAssetMatch(s);
                }
            }

            // Finally, add the new target scenes
            foreach (var s in source.LoadSceneReferences)
            {
                loadQueue.Enqueue(s);
            }
        }


        public void SearchForCurrentSceneAssetMatch(AssetReference target)
        {
            foreach (var c in currentScenes)
            {
                // AssetReferences can technically be unique even if they refer to the same asset.
                // So we need to compare their Asset guid's to see if there's a match.
                // This does that pretty quickly.
                if (string.Equals(a: c.AssetGUID,
                                  b: target.AssetGUID,
                                  comparisonType: StringComparison.Ordinal))
                {
                    // Queue up the one from currentScenes, not sceneSource.
                    unloadQueue.Enqueue(c);
                }
            }
        }
//
//        public bool AddScene
//        (
//            SmartSceneAsset asset)
//        {
////            return loadQueue.Enqueue(asset);
//        }
//
//
//        public bool RemoveScene
//        (
//            SmartSceneAsset asset)
//        {           
////            return unloadQueue.Enqueue(asset);
//        }

    }

}