// Creation Date                                                                               [ 3/02/2020 9:08:23 PM ]
// Project                                                                                               [ Rewild2020 ]
// Developer                                                                                                 [ PHORIA ]

using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Vanilla
{

	[RequireComponent(typeof(AudioSource))]
	public class Music : VanillaBehaviour
	{

		public static AudioSource source;

		protected void OnEnable() => source = GetComponent<AudioSource>();

		public static void SwapClip(AudioClip clip) => source.clip = clip;

		public static void Play() => source.Play();

		public static void Stop() => source.Stop();
		
		public static void Pause() => source.Pause();

		public static void UnPause() => source.UnPause();

		public static void ReleaseClip()
		{
			if (source.clip == null) return;
			
			Addressables.Release(source.clip);
		}
	}

}