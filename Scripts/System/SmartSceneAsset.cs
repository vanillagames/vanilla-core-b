﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEditor;
using UnityEditor.Experimental;

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

using Vanilla.AddressableAssets;

using Object = UnityEngine.Object;

namespace Vanilla.SceneWorks
{
	#if UNITY_EDITOR
	[CreateAssetMenu(fileName = Core.Constants.NewKeyword +
	                            c_SmartSceneAssetName, 
				     menuName = Core.Constants.VanillaSubPath +
				                Core.Constants.CoreSubPath +
				                Core.Constants.SceneWorksSubPath +
				                c_SmartSceneAssetName)]
	#endif
	public class SmartSceneAsset : VanillaScriptable
	{
		public const string c_SmartSceneAssetName = "Smart Scene Asset";

//		[SerializeField]
//		private Object sceneAsset;

		// Temporarily making this reference public so we can port current data! Make it private after.

		[SerializeField, ReadOnly]
		public string SerializedName;
		
		[SerializeField]
		public Object sceneAsset;

		public AssetReference sceneAssetReference;

		public LoadSceneMode loadSceneMode = LoadSceneMode.Additive;
		
		public bool becomeActiveScene = false;
		
		public int loadOperationPriority = 100;

		public AsyncOperationHandle<SceneInstance> handle = new AsyncOperationHandle<SceneInstance>();

		public void OnValidate()
		{	
			#if UNITY_EDITOR
			if (sceneAsset == null)
			{
				SerializedName = string.Empty;
				
				return;
			}

			sceneAssetReference.SetEditorAsset(sceneAsset);

			SerializedName = sceneAssetReference.editorAsset.name;

			if (SerializedName.StartsWith("Scene"))
			{
				NameSelf(name: "SmartSceneAsset"
				               + SerializedName.Remove(startIndex: 0,
				                                       count: 5));
			}
			
			EditorUtility.SetDirty(this);
			#endif
		}


		public async Task<bool> Load()
		{
			if (!sceneAssetReference.RuntimeKeyIsValid())
			{
				Error("AssetReference key is invalid.");

				return false;
			}

			if (sceneAssetReference.IsValid())
			{
				Warning($"The scene [{sceneAssetReference}] is already loaded.");

				return false;
			}

			handle = sceneAssetReference.LoadSceneAsync(loadMode: loadSceneMode,
			                                            activateOnLoad: true,
			                                            priority: loadOperationPriority);

			await handle.Task;

			if (handle.Status == AsyncOperationStatus.Failed)
			{
				Exception(handle.OperationException);

				// If the load operation failed, do we still need to release its handle? Lets do it anyway and find out!

				Addressables.Release(handle);

				return false;
			}

			if (becomeActiveScene)
			{
				if (!SceneManager.SetActiveScene(handle.Result.Scene))
				{
					Warning(
						$"Despite being flagged to do so, the scene [{handle.Result.Scene.name}] could not become the active scene for some reason.");
				}
			}

//			Log($"Is this sceneAssetReference valid? [{sceneAssetReference.IsValid()}]");

//			Log($"Is this handle valid? [{handle.IsValid()}]");

//			SceneLoader.i.currentScenes.Add(this);

			return true;
		}


		public async Task<bool> Unload()
		{
			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Log($"Unload begun for [{SerializedName}]");
			#endif
			
//			SceneLoader.i.currentScenes.Remove(this);

//			if (!sceneAssetReference.RuntimeKeyIsValid())
//			{
//				Error("AssetReference key is invalid.");
//
//				return false;
//			}
			
			if (!handle.IsValid())
			{
				Warning($"The scene [{SerializedName}] is not loaded to begin with.");

				return false;
			}

			var unloadOp = Addressables.UnloadSceneAsync(handle: handle, 
			                                             autoReleaseHandle: false);
			
//			var unloadOp = sceneAssetReference.UnLoadScene();

			await unloadOp.Task;

			if (handle.Status == AsyncOperationStatus.Failed)
			{
				Exception(handle.OperationException);
			}
			
			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Log($"Unload complete for [{SerializedName}]");
			#endif

			Addressables.Release(handle);
			
			return true;
		}

	}

}