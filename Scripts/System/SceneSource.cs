// Creation Date                                                                              [ 12/03/2020 1:45:04 PM ]
// Project                                                                                                   [ Rewild ]
// Developer                                                                                                 [ PHORIA ]

using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Vanilla.SceneWorks
{
	
	[Serializable]
	public class SceneSource : VanillaBehaviour, ISceneSource
	{

		// ----------------------------------------------------------------------------------------------- Variables //

		#region Variables

		[Tooltip(
			"If true, the scene this component is found in will automatically be removed from SceneLoader.targetScenes.")]
		[SerializeField]
		private bool removeThisSceneOnUse = false;
		public bool RemoveThisSceneOnUse
		{
			get => removeThisSceneOnUse;
			set => removeThisSceneOnUse = value;
		}

		[SerializeField]
		internal AssetReference thisSceneAssetReference;
		public AssetReference ThisSceneAssetReference
		{
			get => thisSceneAssetReference;
			set => thisSceneAssetReference = value;
		}

		[SerializeField]
		private bool clearTargetScenesOnUse;
		public bool ClearTargetScenesOnUse
		{
			get => clearTargetScenesOnUse;
			set => clearTargetScenesOnUse = value;
		}
		
		
		[SerializeField]
		private bool unloadUnusedAssets;
		public bool UnloadUnusedAssets
		{
			get => unloadUnusedAssets;
			set => unloadUnusedAssets = value;
		}

		[SerializeField]
		private List<AssetReference> loadSceneReferences;
		public List<AssetReference> LoadSceneReferences
		{
			get => loadSceneReferences;
			set => loadSceneReferences = value;
		}

		[SerializeField]
		private List<AssetReference> unloadSceneReferences;
		public List<AssetReference> UnloadSceneReferences
		{
			get => unloadSceneReferences;
			set => unloadSceneReferences = value;
		}
		
		// This will work as of Unity 2020.1!
//		[SerializeField]
//		public AssetReferenceT<SceneAsset> thing; 
		
		#endregion

		// ---------------------------------------------------------------------------------------------- Validation //

		#region Validation



		protected override void InSceneValidation()
		{
			#if UNITY_EDITOR
			base.InSceneValidation();

			thisSceneAssetReference.SetEditorAsset(removeThisSceneOnUse ?
				                                       AssetDatabase.LoadAssetAtPath<SceneAsset>(
					                                       assetPath: gameObject.scene.path) :
				                                       null);
			#endif
		}



		#endregion

		// -------------------------------------------------------------------------------------------------- Public //

		#region Public


		[ContextMenu("Apply")]
		public virtual void Apply()
		{
			SceneLoader.i.ApplySceneSource(this);
		}


		[ContextMenu("Apply And Load")]
		public virtual void ApplyAndBeginLoad()
		{
			SceneLoader.i.ApplySceneSetupAndBeginLoad(this);
		}


		public void BeginSceneLoader()
		{
			SceneLoader.i.BeginLoadOperation();
		}
		
		
		#endregion
		
	}

}