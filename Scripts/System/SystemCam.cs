﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla;
using Vanilla.Core;

namespace Vanilla.SceneWorks
{

    public class SystemCam : VanillaSingleton<SystemCam>
    {

        [SerializeField, ReadOnly]
        public Camera cam;


        protected override void InSceneValidation()
        {
            base.InSceneValidation();

            cam = GetComponent<Camera>();
        }

    }

}