﻿using UnityEngine;

using Vanilla.Clockwork.B;

namespace Vanilla.Clockwork.B
{

    public class InstantGear : BaseGear
    {

        [Header("AutoNaming Settings")]
        public bool includeGameObject = false;
        public bool includeComponent = false;
        
        protected override void InSceneValidation()
        {
            #if UNITY_EDITOR
            base.InSceneValidation();

            duration = 0.01f;
            #endif
        }

        

        protected override string GetAutomaticGearName()
        {
            var newName = string.Empty;

            if (onEnter.GetPersistentEventCount() > 0)
            {
                newName += "Call ";

                for (var e = 0; e < onEnter.GetPersistentEventCount(); e++)
                {
                    newName += "[";
                    
                    var o = onEnter.GetPersistentTarget(e);

                    // Is the caller a GameObject?
                    
                    if (o is GameObject)
                    {
                        newName += $"{(o.GetInstanceID() == gameObject.GetInstanceID() ? "this" : o.name)}.";
                    }
                    else
                    {
                        // Otherwise, it's a component!
                        
                        var c = (Component) o;

                        if (includeGameObject)
                        {
                            newName += $"{(c.gameObject.GetInstanceID() == gameObject.GetInstanceID() ? "this" : o.name)}.";
                        }

                        if (includeComponent)
                        {
                            newName += $"{c.GetType().Name}.";
                        }
                    }

                    newName += $"{onEnter.GetPersistentMethodName(e)}";
                    
                    newName += "]";

                }
            }

            if (onExit.GetPersistentEventCount() > 0)
            {
                if (!string.IsNullOrEmpty(newName)) newName += " then ";

                for (var e = 0; e < onExit.GetPersistentEventCount(); e++)
                {
                    newName += "[";
                    
                    var o = onExit.GetPersistentTarget(e);
                    var c = (Component) o;

                    if (includeGameObject)
                    {
                        newName += $"{(c.gameObject.GetInstanceID() == gameObject.GetInstanceID() ? "this" : o.name)}.";
                    }

                    if (includeComponent)
                    {
                        newName += $"{c.GetType().Name}.";
                    }

                    newName += $"{onExit.GetPersistentMethodName(e)}";
                    
                    newName += "]";
                }

            }

            return newName;
        }

    }

}