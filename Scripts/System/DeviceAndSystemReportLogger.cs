﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vanilla
{

    public class DeviceAndSystemReportLogger : VanillaBehaviour
    {

        void Start() => Vanilla.LogSystemInfoForAll();

    }

}