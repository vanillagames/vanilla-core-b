//// Creation Date                                                                              [ 10/04/2020 4:41:37 PM ]
//// Project                                                                                                   [ Rewild ]
//// Developer                                                                                                 [ PHORIA ]
//
//using UnityEngine;
//
//using Vanilla.Clockwork.B;
//
//namespace Vanilla.SceneWorks
//{
//	public class TransitionSource : VanillaBehaviour
//	{
//		// ----------------------------------------------------------------------------------------------- Variables //
//		
//		#region Variables
//		
//		[Tooltip("If populated, this gear will be ran to completion before the SceneLoader's next load operation starts.")]
//		public GearAsset preLoadTransitionGear;
//
//		[Tooltip("If populated, this gear will be ran to completion immediately after the SceneLoader's next load operation has ended.")]
//		public GearAsset postLoadTransitionGear;
//		
//		#endregion
//        
//		// ---------------------------------------------------------------------------------------------- Validation //
//		
//		#region Validation
//		
//		
//		
//		#endregion
//		
//		// -------------------------------------------------------------------------------------------------- Public //
//		
//		#region Public
//
//
//
//		public void Apply()
//		{
//			SceneLoader.i.ApplyTransitionSource(this);
//		}
//		
//		#endregion
//		
//	}
//}