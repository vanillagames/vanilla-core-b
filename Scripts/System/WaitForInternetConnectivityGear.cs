﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Clockwork.B
{

	public class WaitForInternetConnectivityGear : BaseGear
	{

		public BaseGear noInternetGear;
		
		protected override void Handle_Enter() => processTime = false;

		protected override void Run() => processTime = Application.internetReachability != NetworkReachability.NotReachable;

	}

}