﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using Vanilla.Math;

namespace Vanilla.Sequencing
{
    public abstract class VanillaSequenceScrub<T0,T1,T2> : VanillaBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
        where T0 : VanillaChapter<T0,T1,T2>
        where T1 : VanillaEpoch<T1>
        where T2 : VanillaSequence<T0,T1,T2>
    {
        public T2 sequence;

        public RectTransform marker;
        
        private Vector2 pos;

        private float lineSpaceHalf;
        
        void Awake()
        {
            pos = marker.anchoredPosition;

            if (sequence) sequence.onGlobalNormalUpdate.AddListener(ReceiveNormal);
        }

        void Start()
        {
            lineSpaceHalf = (Screen.width * 0.9f) / 2;
        }
        
        public void OnPointerDown(PointerEventData eventData)
        {
            sequence.running = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData.delta.x.IsZero()) return;

            pos.x += eventData.delta.x;

            pos.x.Clamp(-lineSpaceHalf, lineSpaceHalf);

            marker.anchoredPosition = pos;

            SendNormal((pos.x + lineSpaceHalf) / (lineSpaceHalf * 2.0f));
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            sequence.running = true;
        }
        
        public void SendNormal(float normal)
        {
            sequence.globalTime = Mathf.Lerp(0, sequence.globalDuration, normal);
        }
        
        public void ReceiveNormal(float normal)
        {
            pos.x = Mathf.Lerp(-lineSpaceHalf, lineSpaceHalf, normal);
        
            marker.anchoredPosition = pos;
        }
    }
}