﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Vanilla.Sequencing
{
    [Serializable]
    public abstract class VanillaChapter<T0, T1, T2> : VanillaBehaviour
        where T0 : VanillaChapter<T0, T1, T2>
        where T1 : VanillaEpoch<T1>
        where T2 : VanillaSequence<T0, T1, T2>
    {
        [SerializeField, ReadOnly]
        private bool initialized;
        
        [SerializeField, ReadOnly]
        public bool active;

        [SerializeField]
        public bool ignore = false;
        
        [SerializeField]
        public EpochTimeline timeline = new EpochTimeline();

        public T0 dependencyChapter;
        
        [Tooltip("If true, this chapters GameObject will automatically make itself active when the timelines start epoch occurs.")]
        public bool toggleGameObjectAtStart = true;
        [Tooltip("If true, this chapters GameObject will automatically make itself inactive when the timelines end epoch occurs.")]
        public bool toggleGameObjectAtEnd = true;

        [Tooltip("All GameObjects in this array will be SetActive(true) at the beginning of this chapter.")]
        public GameObject[] startToggleObjects = new GameObject[0];

        [Tooltip("All GameObjects in this array will be SetActive(false) at the end of this chapter.")]
        public GameObject[] endToggleObjects = new GameObject[0];
        
        public bool eventsUseLocalTime = true;
        
        public bool selectSelfInEditorWhenActivated = true;

        public bool pauseSequenceOnStart;
        public bool pauseSequenceOnEnd;
        
        // --------------------------------------------------------------------------------------------------- Debug //
        // This is a dumb work-around for scrubbing through localTime from the inspector.
        // If you replace it with a screen-space version, you could take all of these calls out.
        
//        #if UNITY_EDITOR
//        public bool updateLocalTimeFromInspector;
//
//        private void Update()
//        {
//            if (updateLocalTimeFromInspector)
//            {
//                timeline.UpdateViaInspector();
//            }
//        }
//        #endif

        // ---------------------------------------------------------------------------------------------------- Init //

        public virtual void Awake()
        {
            Initialize_Internal();
        }

        internal void Initialize_Internal()
        {
            if (initialized) return;

            timeline.Initialize();

            timeline.OnTimeUpdate.AddListener(Run_Internal);
            timeline.start.OnTranspire.AddListener(Started_Internal);
            timeline.end.OnTranspire.AddListener(Ended_Internal);

            foreach (var e in events)
            {
                e.OnTranspire.AddListener(EventTranspired);
            }
            
            initialized = true;
        }

        internal void Analyze_Internal(T2 sequence)
        {
            if (dependencyChapter)
            {
                timeline.start.time = dependencyChapter.timeline.end.time;

                var oldDuration = timeline.end.time;

                timeline.end.time = timeline.start.time + oldDuration;
            }
            
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
                gameObject.name = $"{timeline.startTime:00.00} ~ {timeline.endTime:00.00} - {gameObject.name}";
            #endif
        }

        #if UNITY_EDITOR
        private void SelectSelf()
        {
            Selection.activeGameObject = gameObject;
        } 
        #endif

        // ----------------------------------------------------------------------------------------- Incoming Update //

        public void ProcessGlobalTime(float globalTime, bool timePositive)
        {
            if (ignore) return;
            
            timeline.ProcessGlobalTime_Internal(globalTime, timePositive);

//            if (!ignore)
//            {
                CheckEvents(eventsUseLocalTime ? timeline.localTime : globalTime, timePositive);
//            }
        }

        // ----------------------------------------------------------------------------------------- Outgoing Update //
        // If a change in the timeline is detected as a result of the above incoming update, the following will be called:

        private void Run_Internal(Timeline t)
        {
            if (ignore) return;
            {
                Run();
            }
        }
        
        public abstract void Run();

        // ----------------------------------------------------------------------------------------- Active Handling //
        
        /// <summary>
        ///     Returns true if this Chapter's active state changed this frame.
        /// </summary>
        public bool CheckActiveState(float currentTime)
        {
            bool startElapsed = timeline.start.HasElapsedInclusive(currentTime); // Did we pass over start.time this frame?
            bool endElapsed = timeline.end.HasElapsedInclusive(currentTime); // Did we pass over end.time this frame?

            // We can save another bool check by branching the && statement above
            if (startElapsed)
            {
                if (endElapsed)
                {
                    // If both epochs elapsed in one frame, this chapter was skipped.
                    Skipped(true);
                    
                    // Technically, it's ActiveState is the same as the start of the frame and won't be different
                    // by the end of it either, so we consider this to return false.
                }
                else
                {
                    // If only startElapsed happened this frame, lets set active based on both epochs .past flags.
                    bool newActive = timeline.start.past && !timeline.end.past;

                    if (newActive == active) return false;
            
                    active = newActive;
                    
                    return true;
                }
            } 
            else if (endElapsed)
            {
                // If only endElapsed happened this frame, lets set active based on both epochs .past flags.
                bool newActive = timeline.start.past && !timeline.end.past;

                if (newActive == active) return false;
            
                active = newActive;
                    
                return true;
            }

            return false;
        }

        public bool CompareActiveStateForwards(float currentTime)
        {
            var startElapsed = timeline.start.HasElapsedInclusive(currentTime); // Did we pass over start.time this frame?
            var endElapsed = timeline.end.HasElapsedInclusive(currentTime); // Did we pass over end.time this frame?

            if (startElapsed)
            {
                if (endElapsed)
                {
                    // If both epochs elapsed in one frame, this chapter was skipped.
                    Skipped(true);
                    
                    // Technically, it's ActiveState is the same as the start of the frame and won't be different
                    // by the end of it either, so we consider this to return false.
                }
                else
                {
                    // If only startElapsed happened this frame, lets set active based on both epochs .past flags.
                    bool newActive = timeline.start.past && !timeline.end.past;

                    if (newActive == active) return false;
            
                    active = newActive;
                    
                    return true;
                }
            } 
            else if (endElapsed)
            {
                // If only endElapsed happened this frame, lets set active based on both epochs .past flags.
                bool newActive = timeline.start.past && !timeline.end.past;

                if (newActive == active) return false;
            
                active = newActive;
                    
                return true;
            }

            return false;
        }
        
        public bool CompareActiveStateBackwards(float currentTime)
        {
            var endElapsed = timeline.end.HasElapsedInclusive(currentTime); // Did we pass over end.time this frame?
            var startElapsed = timeline.start.HasElapsedInclusive(currentTime); // Did we pass over start.time this frame?

            if (endElapsed)
            {
                if (startElapsed)
                {
                    // If both epochs elapsed in one frame, this chapter was skipped.
                    Skipped(false);
                    
                    // Technically, it's ActiveState is the same as the start of the frame and won't be different
                    // by the end of it either, so we consider this to return false.
                }
                else
                {
                    // If only startElapsed happened this frame, lets set active based on both epochs .past flags.
                    bool newActive = timeline.start.past && !timeline.end.past;

                    if (newActive == active) return false;
            
                    active = newActive;
                    
                    return true;
                }
            } 
            else if (startElapsed)
            {
                // If only startElapsed happened this frame, lets set active based on both epochs .past flags.
                bool newActive = timeline.start.past && !timeline.end.past;

                if (newActive == active) return false;
            
                active = newActive;
                    
                return true;
            }

            return false;
        }
        
        internal void Started_Internal(BasicEpoch @event)
        {
            if (ignore) return;

            bool startActiveState = @event.past;
            
            if (toggleGameObjectAtStart)
            {
                gameObject.SetActive(startActiveState);
            }
            
            foreach (var g in startToggleObjects)
            {
                g.SetActive(startActiveState);
            }
            
            if (!@event.past)
            {
                ProcessGlobalTime(timeline.startTime, @event.past);
            }
            
            #if UNITY_EDITOR
            if (selectSelfInEditorWhenActivated)
            {
                SelectSelf();
            }
            #endif
            
            Started(@event);
        }

        internal void Ended_Internal(BasicEpoch @event)
        {
            if (ignore) return;
            
            if (@event.past)
            {
                ProcessGlobalTime(timeline.endTime, @event.past);
            }
            
            bool endActiveState = !@event.past;

            if (toggleGameObjectAtEnd)
            {
                gameObject.SetActive(endActiveState);
            }

            foreach (var g in endToggleObjects)
            {
                g.SetActive(endActiveState);
            }

            Ended(@event);
        }
        
        // -------------------------------------------------------------------------------------------------- Events //
        
        public List<T1> events = new List<T1>();

        public void CheckEvents(float currentTime, bool timePositive)
        {
            foreach (T1 e in events)
            {
                e.HasElapsedInclusive(currentTime);
//                if (e.HasElapsedInclusive(currentTime))
//                {
//                    EventTranspired(e, timePositive);
//                }
            }
        }
        
        public abstract void EventTranspired(T1 @event);
        
        // ----------------------------------------------------------------------------------------------- Abstracts //

        public abstract void Analyze(T2 sequence);
        public abstract void Started(BasicEpoch epoch);
        public abstract void Ended(BasicEpoch epoch);
        public abstract void Skipped(bool timePositive);
    }
}