﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.Linq;

using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Sequencing
{
    public class VanillaSequence<T0, T1, T2> : VanillaBehaviour
        where T0 : VanillaChapter<T0, T1, T2>
        where T1 : VanillaEpoch<T1>
        where T2 : VanillaSequence<T0, T1, T2>
    {
        public bool running = true;

        public float timeMultiplier = 1.0f;

        public float globalTime;

        [ReadOnly] public float globalNormal;

        private float deltaGlobalTime;

        [SerializeField, ReadOnly]
        private bool _timePositive = true;
        public bool timePositive
        {
            get => _timePositive;
            set
            {
                if (_timePositive == value) return;

                _timePositive = value;
                
                onTimePositiveChange.Invoke(value);
            }
        }

        [ReadOnly] public float delta;

        [ReadOnly] public float globalDuration;

        public FloatEvent onGlobalTimeUpdate = new FloatEvent();
        public FloatEvent onGlobalNormalUpdate = new FloatEvent();

        public BoolEvent onTimePositiveChange = new BoolEvent();

        public KeyCode toggleMotorKey = KeyCode.Space;

        public float fastForwardAndRewindAmount = 2.5f;
        public int fastForwardMultiplier = 2;
        
        public KeyCode rewindKey = KeyCode.Comma;
        public KeyCode fastForwardKey = KeyCode.Period;

        public KeyCode skipToNearestEpochKey = KeyCode.LeftControl;
        
        public List<T0> chapters = new List<T0>();

        public List<T0> activeChapters = new List<T0>();

        // ---------------------------------------------------------------------------------------------------- Init //

        public virtual void Awake()
        {
            Initialize_Internal();
        }

        protected void Initialize_Internal()
        {
            chapters = GetComponentsInChildren<T0>(true).ToList();

            foreach (var c in chapters)
            {
                c.Initialize_Internal();
            }

            Analyze_Internal();
        }

        protected void Analyze_Internal()
        {
            timePositive = timeMultiplier.IsPositive();

            foreach (var c in chapters)
            {
                c.Analyze_Internal(this as T2);

                if (c.timeline.end.time > globalDuration)
                {
                    globalDuration = c.timeline.end.time;
                }
            }
        }

        // ----------------------------------------------------------------------------------------------- Time Loop //

        void Update()
        {
            CheckDebugKeys();

            UpdateGlobalTime();

            if (Mathf.Approximately(globalTime, deltaGlobalTime)) return;

            onGlobalTimeUpdate.Invoke(globalTime);

            globalNormal = globalTime / globalDuration;

            onGlobalNormalUpdate.Invoke(globalNormal);

            CheckAllChaptersActivity();

            RunActiveChapters();

            deltaGlobalTime = globalTime;
        }

        void UpdateGlobalTime()
        {
            if (running)
            {
                globalTime += Time.deltaTime * timeMultiplier;
            }

            globalTime.Clamp(0, globalDuration);

            delta = globalTime - deltaGlobalTime;

            timePositive = delta.IsPositive();
        }

        void RunActiveChapters()
        {
            if (timePositive)
            {
                for (int i = 0; i < activeChapters.Count; i++)
                {
                    activeChapters[i].ProcessGlobalTime(globalTime, timePositive);
                }
            }
            else
            {
                for (int i = activeChapters.Count - 1; i > -1; i--)
                {
                    activeChapters[i].ProcessGlobalTime(globalTime, timePositive);
                }
            }
        }

        // -------------------------------------------------------------------------------------- Chapter Management //

        void CheckAllChaptersActivity()
        {
            if (timePositive)
            {
                for (var i = 0; i < chapters.Count; i++)
                {
                    if (CheckChapterActivityForwards(chapters[i]))
                    {
                        running = false;

                        return;
                    }
                }
            }
            else
            {
                for (var i = chapters.Count - 1; i > -1; i--)
                {
                    if (CheckChapterActivityBackwards(chapters[i]))
                    {
                        running = false;
                        
                        return;
                    }
                }
            }
        }
        
//        void CheckChapterActivityForwards(T0 chapter)
//        {
//            if (chapter.ignore) return;
//            
//            if (chapter.ignore || !chapter.CompareActiveStateForwards(globalTime)) return;
//
//            if (chapter.active)
//            {
//                activeChapters.Add(chapter);
//            }
//            else
//            {
//                activeChapters.Remove(chapter);
//            }
//        }
        
        /// <summary>
        ///     Returns true if a chapter nominated to pause the sequence when starting/ending.
        /// </summary>
        /// <param name="chapter"></param>
        /// <returns></returns>
        bool CheckChapterActivityForwards(T0 chapter)
        {
            if (chapter.ignore || !chapter.CompareActiveStateForwards(globalTime)) return false;

            if (chapter.active)
            {
                activeChapters.Add(chapter);

                return chapter.pauseSequenceOnStart;
            }

            activeChapters.Remove(chapter);
                
            return chapter.pauseSequenceOnEnd;
        }
        
        bool CheckChapterActivityBackwards(T0 chapter)
        {
            if (chapter.ignore || !chapter.CompareActiveStateBackwards(globalTime)) return false;

            if (chapter.active)
            {
                activeChapters.Add(chapter);
                
                return chapter.pauseSequenceOnEnd;
            }

            activeChapters.Remove(chapter);

            return chapter.pauseSequenceOnStart;
        }

        // ---------------------------------------------------------------------------------------------- Navigation //

        public void Toggle()
        {
            if (running)
            {
                Pause();
            }
            else
            {
                Play();
            }
        }

        public void Play()
        {
            running = true;
        }

        public void Pause()
        {
            running = false;
        }
        
        private void CheckDebugKeys()
        {
            #if UNITY_EDITOR
            if (Input.GetKeyDown(toggleMotorKey)) Toggle();

            if (Input.GetKeyDown(rewindKey))
            {
                if (Input.GetKey(skipToNearestEpochKey))
                {
                    JumpToPreviousEpoch();

                    // If there's been any change in time, we should do a second jump back to compensate.
                    // This gets checked before global time is updated, so we can only really know by
                    // checking if time _will_ be added.
                    if (running && !timeMultiplier.IsZero())
                    {
                        JumpToPreviousEpoch();
                    }
                }
                else
                {
                    Rewind();
                }
            }

            if (Input.GetKeyDown(fastForwardKey))
            {
                if (Input.GetKey(skipToNearestEpochKey))
                {
                    JumpToNextEpoch();
                }
                else
                {
                    FastForward();
                }
            }
            #endif
        }
        
        public void JumpToPreviousEpoch()
        {
            if (chapters == null || chapters.Count == 0) return;

                float nearestDifference = float.MaxValue;
                float nearestPreviousEpoch = -1.0f;

                float currentDifference = float.MaxValue;
                float currentEpoch = float.MaxValue;

                foreach (var chapter in chapters)
                {
                    currentEpoch = chapter.timeline.startTime;

                    if (currentEpoch < globalTime)
                    {
                        currentDifference = currentEpoch.GetAbsoluteDifference(globalTime);

                        if (currentDifference < nearestDifference)
                        {
                            nearestDifference = currentDifference;
                            nearestPreviousEpoch = currentEpoch;
                        }
                    }

                    currentEpoch = chapter.timeline.endTime;

                    if (currentEpoch < globalTime)
                    {
                        currentDifference = currentEpoch.GetAbsoluteDifference(globalTime);

                        if (currentDifference < nearestDifference)
                        {
                            nearestDifference = currentDifference;
                            nearestPreviousEpoch = currentEpoch;
                        }
                    }

                    foreach (var e in chapter.events)
                    {
                        currentEpoch = chapter.eventsUseLocalTime ? e.time + chapter.timeline.startTime : e.time;

                        if (currentEpoch < globalTime)
                        {
                            currentDifference = currentEpoch.GetAbsoluteDifference(globalTime);

                            if (currentDifference < nearestDifference)
                            {
                                nearestDifference = currentDifference;
                                nearestPreviousEpoch = currentEpoch;
                            }
                        }
                    }
                }

                if (Mathf.Approximately(nearestPreviousEpoch, -1.0f)) return;

                globalTime = nearestPreviousEpoch;
        }
        
        public void JumpToNextEpoch()
        {
            if (chapters == null || chapters.Count == 0) return;

            float nearestDifference = float.MaxValue;
            float nearestNextEpoch = -1.0f;

            float currentDifference = float.MaxValue;
            float currentEpoch = float.MaxValue;

            foreach (var c in chapters)
            {
                currentEpoch = c.timeline.startTime;

                if (currentEpoch > globalTime)
                {
                    currentDifference = currentEpoch.GetAbsoluteDifference(globalTime);

                    if (currentDifference < nearestDifference)
                    {
                        nearestDifference = currentDifference;
                        nearestNextEpoch = currentEpoch;
                    }
                }

                currentEpoch = c.timeline.endTime;

                if (currentEpoch > globalTime)
                {
                    currentDifference = currentEpoch.GetAbsoluteDifference(globalTime);

                    if (currentDifference < nearestDifference)
                    {
                        nearestDifference = currentDifference;
                        nearestNextEpoch = currentEpoch;
                    }
                }

                foreach (var e in c.events)
                {
                    currentEpoch = c.eventsUseLocalTime ? e.time + c.timeline.startTime : e.time;

                    if (currentEpoch > globalTime)
                    {
                        currentDifference = currentEpoch.GetAbsoluteDifference(globalTime);

                        if (currentDifference < nearestDifference)
                        {
                            nearestDifference = currentDifference;
                            nearestNextEpoch = currentEpoch;
                        }
                    }
                }
            }

            if (Mathf.Approximately(nearestNextEpoch, -1.0f)) return;

            globalTime = nearestNextEpoch;
        }
        
        public void Rewind()
        {
            globalTime -= fastForwardAndRewindAmount;
        }

        public void FastForward()
        {
            globalTime += fastForwardAndRewindAmount * fastForwardMultiplier;
        }
    }
}