﻿using System.Collections.Generic;
using System.ComponentModel;

using JetBrains.Annotations;

using RotaryHeart.Lib.SerializableDictionary;

using SimpleJSON;

using UnityEngine.AddressableAssets;

namespace Vanilla
{

//    public abstract class VanillaPool2<T> : VanillaBehaviour 
//        where T : VanillaBehaviour, IPoolable<VanillaBehaviour>
//    {
//        public List<T> unused;
//        public List<T> used;
//        
//        public T Get()
//        {
//            T t = unused[0];
//            
//            t.Activate();
//            
//            return t;
//        }
//    }

	public interface ISceneSource
	{
		
		bool RemoveThisSceneOnUse { get; set; }

		AssetReference ThisSceneAssetReference { get; set; }

		bool ClearTargetScenesOnUse { get; set; }

		bool UnloadUnusedAssets { get; set; }
		
		List<AssetReference> LoadSceneReferences { get; set; }
		
		List<AssetReference> UnloadSceneReferences { get; set; }

	}

	public interface IPoolable<T0, T1>
		where T0 : VanillaBehaviour, IPoolable<T0, T1>
		where T1 : VanillaPool<T0, T1>
	{

//        VanillaPool<T0> pool { get; set; }
		T1 pool { get; set; }

		void OnGet();

		void OnReturn();

	}

//    public interface IPoolable<T> where T : VanillaBehaviour
//    {
//        VanillaPool2<T0> pool<T0> { get; set; } 
//        
//        T value { get; set; }
//
//        void OnGet();
//
//        void OnReturn();
//    }

//    public interface IPoolable
//    {
//        VanillaPool<T> GetVanillaPool<T>()
//            where T : VanillaBehaviour, IPoolable;
//        
//        void SetVanillaPool<T>(VanillaPool<T> pool)
//            where T : VanillaBehaviour, IPoolable;
//        
//        void OnGet<T>(VanillaPool<T> pool) 
//            where T : VanillaBehaviour, IPoolable;
//
//        void OnReturn<T>(VanillaPool<T> pool) 
//            where T : VanillaBehaviour, IPoolable;
//    }

	public interface IDictionaryPoolable<T0, T1, T2, T3>

//        where T0 : struct                                                  // The dictionary key type
		where T1 : VanillaBehaviour, IDictionaryPoolable<T0, T1, T2, T3> // The dictionary value type (this!)
		where T2 : SerializableDictionaryBase<T0, T1>, new()             // The dictionary type
		where T3 : VanillaDictionaryPool<T0, T1, T2, T3>                 // The pool type
	{

		T3 pool { get; set; }

		T0 key { get; set; }


        /// <summary>
        ///     This is called when the poolable is instantiated.
        /// 
        ///     No callbacks occur natively in Unity upon instantiation outside of runtime (!) so
        ///     we have our own. Note that this may occur inside or outside of run-time, check using
        ///     Application.IsPlaying if needed.
        /// </summary>
        void OnInstantiate();


		void OnGet();

		void OnReturn();

	}

	public interface IWritable
	{

		void Read();

		void Write();

	}

	public interface IJsonData
	{

		JSONNode ToJson();

		void FromJson(JSONNode json);

	}

//    /// <summary>
//    ///     A data class that can invoke a callback any time a value is changed.
//    /// </summary>
//    public abstract class TrackableClass : VanillaClass, INotifyPropertyChanged
//	{
//
//		public event PropertyChangedEventHandler PropertyChanged;
//
//
//		[NotifyPropertyChangedInvocator]
//		protected internal void OnPropertyChanged()
//		{
//			PropertyChanged?.Invoke(sender: this,
//			                        e: null);
//		}
//
//	}

//	public abstract class SettingsFileBase<T0> : TrackableClass
//		where T0 : SettingsFileBase<T0>, new()
//	{
//
//		public abstract string ToJSON();
//
//		public abstract void FromJSON(JSONObject json);
//
//	}

//    public interface IDictionaryPoolable<T> where T : struct
//    {
//        T GetKey();
//
//        void SetKey(T Key);
//        
////        void OnGet<T1>(VanillaDictionaryPool<T, T1> pool, T Key)
////            where T1 : VanillaBehaviour, IDictionaryPoolable<T>;
//
//        void OnGet<T1>(VanillaDictionaryPool<T, T1> pool)
//            where T1 : VanillaBehaviour, IDictionaryPoolable<T>;
//        
//        void OnReturn<T1>(VanillaDictionaryPool<T, T1> pool)
//            where T1 : VanillaBehaviour, IDictionaryPoolable<T>;
//    }

	

	public interface ISelector<TSelectable, TSelector>
		where TSelectable : ISelectable<TSelectable, TSelector>
		where TSelector : ISelector<TSelectable, TSelector>
	{

		

	}

	public interface ISelectable<TSelectable, TSelector>
		where TSelectable : ISelectable<TSelectable, TSelector>
		where TSelector : ISelector<TSelectable, TSelector>
	{
		// Call this when you 'hover' over the object.
		void RequestHover();

		void Hovered();

		void Dehovered();

		// Call this when you have 'selected' the object.
		void RequestSelect();
		
		void Selected();

		void Deselected();

	}
	
	public interface ISelfSelectable<T>
		where T : ISelfSelectable<T>
	{
		SelfHoverSlot<T> HoverSlot();
		
		void TryHover();
		
		void Hovered();

		void Dehovered();
		
		SelfSelectionSlot<T> SelectionSlot();
		
		void TrySelect();
		
		void Selected();

		void Deselected();

	}

}