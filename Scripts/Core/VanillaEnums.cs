﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vanilla
{
    [Flags]
    public enum DebugFlags {
        Logs = 1,
        Warnings = 2,
        Errors = 4,
        Gizmos = 8,
    }
    
    // ----------------------------------------------------------------------------------------------------- Pooling //

    public enum PoolLevel
    {
        Empty,
        Populated,
        Full,
        Overflow
    }
    
    // -------------------------------------------------------------------------------------------------------- Time //

    public enum TimeDeltaMode
    {
        Update,
        FixedUpdate,
        Manual
    }
    
    // --------------------------------------------------------------------------------------------- Validation Mode //

    public enum ValidationMode
    {
        InScene,
        Asset,
        PrefabEditor,
        Unknown
    }
    
    // ------------------------------------------------------------------------------------------------------ Fading //

    public enum FadeState
    {
        CurtainsOpening,
        CurtainsOpen,
        CurtainsClosing,
        CurtainsClosed
    }
    
    // --------------------------------------------------------------------------------------------------- Scripting //

    public enum GetComponentStyle
    {
        OnObject,
        InChildren,
        InParent
    }
    
    // ----------------------------------------------------------------------------------------------------- Spatial //
    
    public enum Axis3D
    {
        X,
        Y,
        Z,
        None
    }

    public enum Axis2D
    {

        X,
        Y,
        None

    }
    
    [Flags]
    public enum AxisFlags
    {
        X = 1,
        Y = 2,
        Z = 4
    }
    
    [Flags]
    public enum TransformComponentFlags
    {
        Position = 1,
        Rotation = 2,
        Scale = 4
    }
    
    [Flags]
    public enum TransformComponentPerAxisFlags
    {
        PositionX = 1,
        PositionY = 2,
        PositionZ = 4,
        
        EulerX = 8,
        EulerY = 16,
        EulerZ = 32,

        ScaleX = 64,
        ScaleY = 128,
        ScaleZ = 256
    }

    public enum CardinalDirection
    {
        North,
        East,
        South,
        West
    }
    
    public enum Handedness {
        None,
        Left,
        Right
    }

    // ---------------------------------------------------------------------------------------------- Generic / Data //

    public enum DataType
    {
        Bool,
        Byte,
        String,
        Char,
        Int,
        Float,
        Vector2,
        Vector3,
        Struct
    }
    
    public enum Polarity {
        Positive,
        Neutral,
        Negative
    }
}