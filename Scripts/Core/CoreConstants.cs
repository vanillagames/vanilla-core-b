﻿#if UNITY_EDITOR

namespace Vanilla.Core
{
	public static class Constants
	{
		// ----------------------------------------------------------------------------------------------- Sub-paths //

		public const string NewKeyword = "New ";
		
		public const string AssetsSubPath = "Assets/";

		public const string SubmodulesSubPath = "+Modules/";

		public const string ProjectSubPath = "+Project/";

		public const string VanillaSubPath = "Vanilla/";

		public const string CoreSubPath = "Core/";

		public const string PrefabsSubPath = "Prefabs/";

		public const string EditorExtensionsSubPath = "EditorExtensions/";

		public const string ScriptTemplatesSubPath = "Script Templates/";

		public const string GameObjectSubPath = "GameObject/";

		public const string CreateSubPath = "Create/";

		public const string DebugSubPath = "Debug/";
		
		public const string InputSubPath = "Input/";

		public const string UISubPath = "UI/";

		public const string SceneWorksSubPath = "SceneWorks/";

		// ------------------------------------------------------------------------------------ Compound Directories //

		internal const string ProjectPath =
			AssetsSubPath + ProjectSubPath;

		internal const string CoreModulePath =
			AssetsSubPath + SubmodulesSubPath + VanillaSubPath + CoreSubPath;

		internal const string PrefabsPath =
			CoreModulePath + PrefabsSubPath;

		internal const string ScriptTemplatesPath =
			CoreModulePath + EditorExtensionsSubPath + ScriptTemplatesSubPath;

		// --------------------------------------------------------------------------- Hierarchy Window Context Menu //

		internal const string CoreHierarchyMenuPath =
			GameObjectSubPath + VanillaSubPath + CoreSubPath;

		// ----------------------------------------------------------------------------- Project Window Context Menu //
	}
}

#endif