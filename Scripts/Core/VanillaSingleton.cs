﻿using System;

using UnityEngine;

namespace Vanilla
{
    [Serializable]
    public abstract class VanillaSingleton<T> : VanillaBehaviour where T : VanillaBehaviour
    {
        private static T _i;

        public static T i
        {
            get
            {
                if (_i != null) return _i;

                if (applicationIsQuitting) return null;
                
                if (_i == null)
                {
                    return _i = FindObjectOfType<T>();
                }

//#if UNITY_EDITOR
//                if (_i == null)
//                {
//                    foreach (var s in Resources.FindObjectsOfTypeAll<T>())
//                    {
//                        return _i = s;
//                    }
//                }
//#endif

                if (_i == null)
                {
                    Debug.LogError($"No instance of the singleton type [{typeof(T).FullName}] could be found.");
                }

                return _i;
            }
            private set => _i = value;
        }

        public static Transform t;

        protected virtual void Awake()
        {
            _i = this as T;
            
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            Log($"Singleton assigned [ {GetType().FullName} ]");
            #endif
            
            t = transform;
        }

        protected override void InSceneValidation()
        {
            base.InSceneValidation();

            t = transform;
        }
    }
}