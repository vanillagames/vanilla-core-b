﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

using Vanilla.Core;
using Vanilla.SceneWorks;

using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
#endif

namespace Vanilla
{

	public abstract class VanillaBehaviour : MonoBehaviour
	{

		public static bool applicationIsQuitting;

		// ---------------------------------------------------------------------------------------------------- Data //

		[Header("[ Vanilla Behaviour ]")] [EnumFlags(4)]
		public DebugFlags debugFlags = (DebugFlags) 31;

		// ---------------------------------------------------------------------------------------------- Validation //


		private void OnValidate()
		{
			#if !UNITY_EDITOR
			
			#else
			if (Selection.activeGameObject != gameObject) return;

			if (PrefabStageUtility.GetCurrentPrefabStage() == null)
			{
//				Log($"Running InScene Validation for [{gameObject.name}]");

				InSceneValidation();
			}
			else
			{
//				Log($"Running Prefab Editor Validation for [{gameObject.name}]");

				PrefabEditorValidation();
			}
			#endif
		}


		protected virtual void PrefabEditorValidation() { }

		protected virtual void InSceneValidation() { }

		protected virtual void AssetFileValidation() { }

		// --------------------------------------------------------------------------------------------- Editor Only //

		public static List<T> GetAssetsOfType<T>()
			where T : Object
		{
			#if UNITY_EDITOR

			return AssetDatabase.FindAssets(filter: $"t:{typeof(T)}").
			                     Select(AssetDatabase.GUIDToAssetPath).
			                     Select(AssetDatabase.LoadAssetAtPath<T>).
			                     Where(asset => asset != null).ToList();

			#endif

			return null;
		}
		
		public SmartSceneAsset GetContainingSmartSceneAsset
		(
			GameObject target)
		{
			#if UNITY_EDITOR
			return GetContainingSmartSceneAsset(target.scene);
			#endif

			return null;
		}


		public SmartSceneAsset GetContainingSmartSceneAsset
		(
			Scene scene)
		{
			#if UNITY_EDITOR
			var asset = GetAssetsOfType<SmartSceneAsset>()
				?.FirstOrDefault(s => string.Equals(a: s.name,
				                                    b: scene.name));

			if (asset == null)
			{
				Error($"No SmartSceneAsset was found to be associated with this scene [{scene.name}].");
			}

			return asset;
			#endif

			return null;
		}


		public string GetAssemblyName()
		{
			#if UNITY_EDITOR
			return Assembly.GetExecutingAssembly()
			               .GetName()
			               .Name;
			#endif

			return string.Empty;
		}


		public bool IsAProjectScript()
		{
			#if UNITY_EDITOR
			return GetPathToScript()
				.StartsWith(Constants.ProjectPath);
			#endif

			return false;
		}


		public string GetPathToScript()
		{
			#if UNITY_EDITOR
			return AssetDatabase.GUIDToAssetPath(GetScriptFileGUID());
			#endif

			return string.Empty;
		}


		public string GetScriptFileGUID()
		{
			#if UNITY_EDITOR
			foreach (var g in AssetDatabase.FindAssets(GetType()
				                                          .Name))
				return g;
			#endif

			return string.Empty;
		}


		// ------------------------------------------------------------------------------------------------- Helpers //


		public void LogHierarchy() => transform.LogHierarchy();


		public virtual void Reset() { }


		protected bool SilentNullCheck(Object o) => ReferenceEquals(objA: o,
		                                                            objB: null);


		protected bool SilentNullCheck(VanillaClass c) => ReferenceEquals(objA: c,
		                                                                  objB: null);


		protected bool SoftNullCheck
		(
			Object o)
		{
			if (!ReferenceEquals(objA: o,
			                     objB: null))
				return false;

			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Warning("Null detected!");
			#endif

			return true;
		}


		protected bool SoftNullCheck
		(
			VanillaClass c)
		{
			if (!ReferenceEquals(objA: c,
			                     objB: null))
				return false;

			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Warning("Null detected!");
			#endif

			return true;
		}


		protected bool HardNullCheck
		(
			Object o)
		{
			if (!ReferenceEquals(objA: o,
			                     objB: null))
				return false;

			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Error("Null detected!");
			#endif

			return true;
		}


		protected bool HardNullCheck
		(
			VanillaClass c)
		{
			if (!ReferenceEquals(objA: c,
			                     objB: null))
				return false;

			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Error("Null detected!");
			#endif

			return true;
		}


		public T GetComponentDynamic<T>
		(
			GetComponentStyle style)
			where T : MonoBehaviour
		{
			switch (style)
			{
				default: return GetComponent<T>();

				case GetComponentStyle.InChildren: return GetComponentInChildren<T>();

				case GetComponentStyle.InParent: return GetComponentInParent<T>();
			}
		}


		public T[] GetComponentsDynamic<T>
		(
			GetComponentStyle style)
			where T : MonoBehaviour
		{
			switch (style)
			{
				default: return GetComponents<T>();

				case GetComponentStyle.InChildren: return GetComponentsInChildren<T>();

				case GetComponentStyle.InParent: return GetComponentsInParent<T>();
			}
		}


		// --------------------------------------------------------------------------------------------------- Input //

		/// <summary>
		/// 	Returns false if either the nominated mouse button isn't clicked, no touch is occurring, or
		/// 	either is but they're over a UI element.
		/// </summary>
		/// 
		/// <param name="mouseButton">Which mouse button? 0 - Left, 1 - Middle, 2 - Right</param>
		/// <returns></returns>
		protected bool ValidClickOrTouch(int mouseButton)
		{
			#if UNITY_EDITOR
				return Input.GetMouseButton(mouseButton) && !EventSystem.current.IsPointerOverGameObject(-1);
			#else
				return Input.touchCount > 0 && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
			#endif
		}

		protected bool AnyTouchBegan() => Input.touchCount > 0 && 
		                                  Input.touches.Any(t => t.phase == TouchPhase.Began);

		protected bool AnyTouchDoubleTapped() => Input.touchCount > 0 && 
		                                         Input.touches.Any(t => t.tapCount == 2);

		
		protected bool FirstTouchBegan() => Input.touchCount > 0 &&
		                                    Input.GetTouch(index: 0).phase == TouchPhase.Began;

		protected bool FirstTouchDoubleTapped() => Input.touchCount > 0 && 
		                                           Input.GetTouch(index: 0).tapCount == 2;

		
		protected bool SecondTouchBegan() => Input.touchCount > 1 && 
		                                     Input.GetTouch(index: 1).phase == TouchPhase.Began;
		
		protected bool SecondTouchDoubleTapped() => Input.touchCount > 1 && 
		                                            Input.GetTouch(index: 1).tapCount == 2;

		
		protected bool ThirdTouchBegan() => Input.touchCount > 2 && 
		                                    Input.GetTouch(index: 2).phase == TouchPhase.Began;
		
		protected bool ThirdTouchDoubleTapped() => Input.touchCount > 2 && 
		                                           Input.GetTouch(index: 2).tapCount == 2;

		// ------------------------------------------------------------------------------------- Gizmos / Debug Draw //


		protected virtual bool CanDrawGizmos() => Application.isPlaying && debugFlags.HasFlag(DebugFlags.Gizmos);

		protected void DrawDebugRay
		(
			Vector3 rayOrigin,
			Vector3 rayDir,
			float   rayLength,
			Color   color,
			float   duration)
		{
			#if UNITY_EDITOR
			if (!CanDrawGizmos()) return;

			Debug.DrawRay(start: rayOrigin,
			              dir: rayDir * rayLength,
			              color: color,
			              duration: duration);
			#endif
		}


		// ---------------------------------------------------------------------------------------------------- Logs //


		protected void Log(string message)
		{
			#if DEVELOPMENT_BUILD || UNITY_EDITOR
			if (debugFlags.HasFlag(DebugFlags.Logs))
				Vanilla.Log(culprit: gameObject.name,
				            message: message);

//				Debug.Log($"[{gameObject.name}] Frame [{Time.frameCount}] {message}");
			#endif
		}


		protected void Warning
		(
			string message)
		{
			#if DEVELOPMENT_BUILD || UNITY_EDITOR
			if (debugFlags.HasFlag(DebugFlags.Warnings))
				Vanilla.Warning(culprit: gameObject.name,
				                message: message);

//				Debug.LogWarning($"[{gameObject.name}] Frame [{Time.frameCount}] {message}");
			#endif
		}


		protected void Error
		(
			string message)
		{
			#if DEVELOPMENT_BUILD || UNITY_EDITOR
			if (debugFlags.HasFlag(DebugFlags.Errors))
				Vanilla.Error(culprit: gameObject.name,
				              message: message);

//				Debug.LogError($"[{gameObject.name}] Frame [{Time.frameCount}] {message}");
			#endif
		}


		protected void Exception(Exception e) => Debug.LogException(exception: e,
		                                                            context: this);

		// ---------------------------------------------------------------------------------------------- Activation // 

		public virtual void Enable() => enabled = true;

		public virtual void Disable() => enabled = false;
		public virtual void Activate() => gameObject.SetActive(true);

		public virtual void Deactivate() => gameObject.SetActive(false);

		public virtual void ToggleActive() => gameObject.SetActive(!gameObject.activeSelf);

		// --------------------------------------------------------------------------------------------- Convenience // 


		public virtual void NameSelf()
		{
			#if UNITY_EDITOR
			gameObject.name = $"{GetType().Name}";
			#endif
		}


		public virtual void NameSelf(string subtitle)
		{
			#if UNITY_EDITOR
			gameObject.name = $"{GetType().Name} [ {subtitle} ]";
			#endif
		}


		// ------------------------------------------------------------------------------------------------ App Flow //

		public virtual void OnApplicationQuit() => applicationIsQuitting = true;

	}

}