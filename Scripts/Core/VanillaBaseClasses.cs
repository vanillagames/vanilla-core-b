using System;

using UnityEngine;

using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Vanilla
{

	// ----------------------------------------------------------------------------------------------- Vanilla Class //

	[Serializable]
	public abstract class VanillaClass
	{

//		[EnumFlags(4)]
//		public DebugFlags debugFlags = (DebugFlags) 31;


		protected bool SilentNullCheck<T>(T input)
		{
			return Vanilla.SilentNullCheck(input);
		}


		protected bool SoftNullCheck<T>(T input)
		{
			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			return Vanilla.SoftNullCheck(culprit: input.GetType().Name,
			                             thing: input);
			#else
            return Vanilla.SilentNullCheck(input);
			#endif
		}


		protected bool HardNullCheck<T>(T input)
		{
			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			return Vanilla.HardNullCheck(culprit: GetType().Name,
			                             thing: input);
			#else
            return Vanilla.SilentNullCheck(input);
			#endif
		}


		protected void Log(string message)
		{
			#if DEVELOPMENT_BUILD || UNITY_EDITOR

//			if (!debugFlags.HasFlag(DebugFlags.Logs)) return;

			Vanilla.Log(culprit: GetType().FullName,
			            message: message);

			#endif
		}


		protected void Warning(string message)
		{
			#if DEVELOPMENT_BUILD || UNITY_EDITOR

//			if (!debugFlags.HasFlag(DebugFlags.Warnings)) return;

			Vanilla.Warning(culprit: GetType().FullName,
			                message: message);

			#endif
		}


		protected void Error(string message)
		{
			#if DEVELOPMENT_BUILD || UNITY_EDITOR

//			if (!debugFlags.HasFlag(DebugFlags.Errors)) return;

			Vanilla.Error(culprit: GetType().FullName,
			              message: message);

			#endif
		}


		// ----------------------------------------------------------------------------------- Unity Callback Mimics //


		public virtual void OnEnable<V>
		(
			V behaviour)
			where V : VanillaBehaviour { }


		public virtual void OnDisable<V>
		(
			V behaviour)
			where V : VanillaBehaviour { }


		public virtual void Validate<V>
		(
			V behaviour)
			where V : VanillaBehaviour { }


		public virtual void Reset() { }

	}

	// ------------------------------------------------------------------------------------------ Vanilla Scriptable //

	public abstract class VanillaScriptable : ScriptableObject
	{

		protected virtual void OnValidate() { }

		public virtual void Reset() { }


		protected void Log(string message)
		{
			#if DEVELOPMENT_BUILD || UNITY_EDITOR
			Vanilla.Log(culprit: GetType().FullName,
			            message: message);
			#endif
		}


		protected void Warning(string message)
		{
			#if DEVELOPMENT_BUILD || UNITY_EDITOR
			Vanilla.Warning(culprit: GetType().FullName,
			                message: message);
			#endif
		}


		protected void Error(string message)
		{
			#if DEVELOPMENT_BUILD || UNITY_EDITOR
			Vanilla.Error(culprit: GetType().FullName,
			              message: message);
			#endif
		}


		protected void Exception
		(
			Exception e)
		{
			Debug.LogException(exception: e, 
			                   context: this);
		}


		// Helpers //


		protected bool SilentNullCheck(Object o)
		{
			return o == null;
		}


		protected bool SoftNullCheck(Object o)
		{
			if (o != null) return false;

			Warning("Null detected!");

			return true;
		}


		protected bool HardNullCheck(Object o)
		{
			if (o != null) return false;

			Error("Null detected!");

			return true;
		}


		// Convenience //

		[ContextMenu("Name Self")]
		protected virtual void NameSelf(string name)
		{
			#if UNITY_EDITOR
			AssetDatabase.RenameAsset(pathName: AssetDatabase.GetAssetPath(GetInstanceID()),
			                          newName: name);
			#endif
		}
		
		[ContextMenu("Name Self")]
		public virtual void NameSelfByType()
		{
			#if UNITY_EDITOR
			NameSelf(name: GetType().Name);
			#endif
		}


		public virtual void NameSelfByType(string subtitle)
		{
			#if UNITY_EDITOR
			NameSelf(name: $"{GetType().Name} [ {subtitle} ]");
			#endif
		}


		[ContextMenu("Save")]
		protected void Save()
		{
			#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
			AssetDatabase.SaveAssets();
			#endif
		}

	}

}