﻿using System;

using UnityEngine;

using Vanilla.Math;

namespace Vanilla.Sequencing
{

	// ------------------------------------------------------------------------------------------------------- Epoch //

    /// <summary>
    ///     This class represents a moment in time that we want to react to dynamically.
    /// 
    ///     It may be with an inherited callback, event or used as part of a greater class
    ///     structure.
    /// 
    ///     Simply pass some kind of chronological time value (like Time.time) in to either
    ///     HasElapsed() method and Transpire() will be called only when the time value has
    ///     been elapsed.
    /// 
    ///     If different functionality is required depending on the 'direction of time', i.e.
    ///     heading backwards or forwards through time, simply check the current state of 'past'
    ///     in Transpire().
    /// </summary>
    [Serializable]
	public abstract class VanillaEpoch<T> : VanillaClass
		where T : VanillaEpoch<T>
	{

		public GenericEvent<T> OnTranspire = new GenericEvent<T>();

		[Tooltip("Is this event in the past?")] [SerializeField] [ReadOnly]
		public bool past;
		[Tooltip("What time does this event happen?")] [SerializeField]
		public float time = 1.0f;


        /// <summary>
        ///     Asks the epoch to compare itself against currentTime exclusively
        ///     (i.e. _not_ including the value of currentTime itself).
        /// </summary>
        public bool HasElapsedExclusive(float currentTime)
		{
			return HasElapsed(currentTime > time);
		}


        /// <summary>
        ///     Asks the epoch to compare itself against currentTime inclusively
        ///     (i.e. including the value of currentTime itself).
        /// </summary>
        public bool HasElapsedInclusive(float currentTime)
		{
			return HasElapsed(currentTime >= time);
		}


		protected virtual bool HasElapsed(bool pastThisFrame)
		{
			if (pastThisFrame == past) return false; // Nothing changed

			past = pastThisFrame;

			OnTranspire.Invoke(this as T);

			return true;
		}

	}

	// ------------------------------------------------------------------------------------------------- Basic Epoch //

    /// <summary>
    ///     This Epoch simply seals off the generic type so it can be serialized and seen in the inspector panel.
    /// </summary>
    [Serializable]
	public class BasicEpoch : VanillaEpoch<BasicEpoch>
	{

		public override void Reset() { }

	}

	// ------------------------------------------------------------------------------------------- Toggle-able Epoch //

    /// <summary>
    ///     This Epoch adds a bool for making the epoch toggle-able. If ignore is true, the epoch
    ///     never invokes OnTranspire.
    /// </summary>
    [Serializable]
	public class ToggleableEpoch<T> : VanillaEpoch<T>
		where T : ToggleableEpoch<T>
	{

		[SerializeField]
		public bool ignore;


		protected override bool HasElapsed(bool pastThisFrame)
		{
			if (pastThisFrame == past) return false; // Nothing changed

			past = pastThisFrame;

			if (!ignore) OnTranspire.Invoke(this as T);

			return true;
		}


		public override void Reset() { }

	}

	// ---------------------------------------------------------------------------------------------------- Timeline //

	[Serializable]
	public abstract class Timeline : VanillaClass
	{

		[SerializeField] [ReadOnly]
		private float _localNormal;
		[SerializeField]
		private float _localTime = -1.0f;

		public float localTime
		{
			get => _localTime;
			set
			{
				value.Clamp(min: 0,
				            max: duration);

				if (Mathf.Approximately(a: _localTime,
				                        b: value)) return;

				_localTime = value;

				_localNormal = ( _localTime / duration ).GetBetween01();

				TimeUpdate_Internal();
			}
		}

		public float localNormal
		{
			get => _localNormal;
			set
			{
				value = ( _localTime / duration ).GetBetween01();

				if (Mathf.Approximately(a: _localNormal,
				                        b: value)) return;

				_localNormal = value;

				_localTime = Mathf.Lerp(a: 0,
				                        b: duration,
				                        t: _localNormal);

				TimeUpdate_Internal();
			}
		}

		public GenericEvent<Timeline> OnTimeUpdate { get; } = new GenericEvent<Timeline>();

		public virtual float startTime { get; set; }
		public virtual float endTime   { get; set; }

		public float duration => endTime - startTime;


		public void Initialize()
		{
			localTime = localTime;
		}


		internal void ProcessGlobalTime_Internal
		(
			float globalTime,
			bool  timePositive)
		{
			localTime = globalTime - startTime;

			ProcessGlobalTime(globalTime: globalTime,
			                  timePositive: timePositive);
		}


		public abstract void ProcessGlobalTime
		(
			float globalTime,
			bool  timePositive);


		private void TimeUpdate_Internal()
		{
			TimeUpdate();

			OnTimeUpdate.Invoke(this);
		}


		public void TimeUpdate() { }

		public override void Reset() { }


		public void UpdateViaInspector()
		{
			// Because the values are get/set/reactive, of course, Unity can't properly update the values based on
			// changes in the inspector. As a result, we have to make a goofy little method like this that constantly
			// sets the values to themselves in order to run the setter :(

			#if UNITY_EDITOR
			var cache = _localNormal;

			_localNormal = ( localTime / duration ).GetBetween01();

			localTime = localTime;

			if (Mathf.Approximately(a: cache,
			                        b: _localNormal)) return;

			TimeUpdate_Internal();
			#endif
		}

	}

	// ---------------------------------------------------------------------------------------------- Epoch Timeline //

    /// <summary>
    ///     Epoch Timeline extends the standard Timeline class by including two BasicEpochs; 'start' and 'end'.
    /// 
    ///     These epochs are used to fire OnTranspire when the start or end epochs elapse.
    /// </summary>
    [Serializable]
	public class EpochTimeline : Timeline
	{

		[SerializeField]
		public BasicEpoch end = new BasicEpoch();
		[SerializeField]
		public BasicEpoch start = new BasicEpoch();

		public override float startTime
		{
			get => start.time;
			set =>
				start.time = Mathf.Min(a: end.time,
				                       b: value);
		}

		public override float endTime
		{
			get => end.time;
			set =>
				end.time = Mathf.Max(a: start.time,
				                     b: value);
		}


		public override void ProcessGlobalTime
		(
			float globalTime,
			bool  timePositive) { }

	}

	[Serializable]
	public class FixedTimeline : Timeline
	{

		[SerializeField]
		private float _endTime = 1.0f;
		[SerializeField]
		private float _startTime;

		public float startTime
		{
			get => _startTime;
			set =>
				_startTime = Mathf.Min(a: _endTime,
				                       b: value);
		}

		public float endTime
		{
			get => _endTime;
			set =>
				_endTime = Mathf.Max(a: _startTime,
				                     b: value);
		}


		public override void ProcessGlobalTime
		(
			float globalTime,
			bool  timePositive) { }

	}

}