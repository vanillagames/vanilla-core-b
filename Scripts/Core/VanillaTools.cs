using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;

using Vanilla.Math;

using Debug = UnityEngine.Debug;
using Object = System.Object;

//using UnityEngine.Experimental.PlayerLoop;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Vanilla
{
    //Allen here doing God's work
	//Doing more God's work
	// -------------------------------------------------------------------------------------------------------- vuid //

	[Serializable]
	public class FakeDownload : VanillaClass
	{

		[SerializeField]
		[Range(1.0f, 1000.0f)]
		private long _downloadTotalMB = 50; // 50Mb

		[SerializeField]
		[Range(0.0f, 100.0f)]
		private float _downloadSpeedInMbps = 20.0f;
		
		[SerializeField]
		private FakeDownloadConnection _connectionQuality = FakeDownloadConnection.Average;

		private enum FakeDownloadConnection
		{

			Perfect,
			Great,
			Good,
			Average,
			Bad,
			Awful

		}

		public KeyCode skipKey = KeyCode.F5;

		public async Task StartDownload
		(
			UnityAction<long>  onDownloadTotalUpdate,
			UnityAction<float> onDownloadNormalUpdate)
		{
			#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Log("Beginning fake download...");

			onDownloadTotalUpdate?.Invoke(_downloadTotalMB * 1000000);

			var t = 0.0f;

			while (t <= 1.0f && !Input.GetKeyDown(skipKey))
			{
				var f = Time.frameCount;

				switch (_connectionQuality)
				{
					case FakeDownloadConnection.Perfect:
						t += GetFakeDownloadFrameBytes();

						break;

					case FakeDownloadConnection.Great:
						if (f % 4 != 0) t += GetFakeDownloadFrameBytes();

						break;

					case FakeDownloadConnection.Good:
						if (f % 3 != 0) t += GetFakeDownloadFrameBytes();

						break;

					case FakeDownloadConnection.Average:
						if (f % 3 == 0) t += GetFakeDownloadFrameBytes();

						break;

					case FakeDownloadConnection.Bad:
						if (f % 3 == 0 && f.IsEven() && f % 12 != 0) t += GetFakeDownloadFrameBytes();

						break;

					case FakeDownloadConnection.Awful:
						var s = Mathf.Sin(Time.time);

						if (s > 0.6f && s < 0.8f && f % 3 == 0 && f.IsEven() && f % 12 != 0)
							t += GetFakeDownloadFrameBytes();

						break;
				}

				onDownloadNormalUpdate?.Invoke(t);

				await Task.Yield();
			}

			onDownloadNormalUpdate?.Invoke(1.0f);

			Log("Fake download complete!");
			
			#endif
		}


		private float GetFakeDownloadFrameBytes() =>
			Time.deltaTime * ( _downloadSpeedInMbps * 1000000.0f / 8.0f / ( _downloadTotalMB * 1000000 ) );

	}

	// --------------------------------------------------------------------------------------------------- Slot List //

	[Serializable]
	public abstract class VanillaList<T> : VanillaClass
	{
        [SerializeField]
		public List<T> contents = new List<T>();

		[SerializeField]
		public GenericEvent<T> onAddition = new GenericEvent<T>();

		[SerializeField]
		public GenericEvent<T> onRemoval  = new GenericEvent<T>();

		public abstract bool Add(T newItem);

		public abstract bool Remove(T newItem);

		public override void Reset() { }

	}

	// --- Unprotected --- //

	[Serializable]
	public class UnprotectedList<T> : VanillaList<T>
	{

		public override bool Add(T newItem)
		{
			if (SoftNullCheck(newItem)) return false;

			contents.Add(newItem);

			onAddition.Invoke(newItem);

			return true;
		}


		public override bool Remove(T newItem)
		{
			if (SoftNullCheck(newItem)) return false;

			onRemoval.Invoke(newItem);

			contents.Remove(newItem);

			return true;
		}


		public override void Validate<V>(V behaviour) { }

	}

	// --- Protected --- //

	[Serializable]
	public class ProtectedList<T> : VanillaList<T>
	{

		public override bool Add(T newItem)
		{
			if (SoftNullCheck(newItem)) return false;

			if (contents.Contains(newItem))
			{
				#if UNITY_EDITOR || DEVELOPMENT_BUILD
				Log($"The item {newItem} is already contained in this ProtectedList and can't be added again.");
				#endif

				return false;
			}

			contents.Add(newItem);

			onAddition.Invoke(newItem);

			return true;
		}


		public override bool Remove(T newItem)
		{
			if (SoftNullCheck(newItem)) return false;

			if (!contents.Contains(newItem))
			{
				#if UNITY_EDITOR || DEVELOPMENT_BUILD
				Log($"The item {newItem} could not be found in this ProtectedList and can't be removed.");
				#endif

				return false;
			}

			onRemoval.Invoke(newItem);

			contents.Remove(newItem);

			return true;
		}


		public int Count()
		{
			return contents.Count;
		}


		public void Clear()
		{
			contents.Clear();
		}


		public override void Validate<V>(V behaviour) { }

	}

	// --- Vanilla Collection --- //

	[Serializable]
	public class VanillaCollection<T0, T1> : VanillaClass
		where T0 : ICollectible<T0, T1>
		where T1 : VanillaCollection<T0, T1>
	{

		public ProtectedList<T0> collected;

//        public Dictionary<ushort, T0> collectionDictionary = new Dictionary<ushort,T0>();

		public GenericEvent<T0>  onCollect = new GenericEvent<T0>();
		public ProtectedList<T0> uncollected;

		public int total => collected.Count() + uncollected.Count();

		public float completionPercentage => (float) collected.Count() / total;


		public void Collect(T0 item)
		{
			if (uncollected.Remove(item))
			{
				collected.Add(item);

				item.Collect();

				onCollect.Invoke(item);
			}
		}


		public void Uncollect(T0 item)
		{
			if (collected.Remove(item))
			{
				item.Uncollect();

				uncollected.Add(item);
			}
		}


		public virtual void InitializeItem(T0 item)
		{
			item.id = (byte) uncollected.Count();

			uncollected.Add(item);
		}


		public virtual void DeinitializeItem(T0 item)
		{
			Uncollect(item);

			uncollected.Remove(item);
		}


		public override void Validate<V>(V behaviour) { }

		public override void Reset() { }

	}

	// --- Collectible Interface --- //

	public interface ICollectible<T0, T1>
	{

        /// <summary>
        ///     The collection this collectible belongs to.
        /// 
        ///     I recommend making the getter point to a static T1!
        /// </summary>
        T1 collection { get; set; }

        /// <summary>
        ///     A unique ID for this item.
        /// 
        ///     It could be set to the length of the list it gets added to (kinda pointless...)
        ///     or you could use the getter to redirect to an attached Object.GetUniqueID
        /// </summary>
        byte id { get; set; }

        /// <summary>
        ///     Has this item been collected?
        /// </summary>
        bool collected { get; set; }


        /// <summary>
        ///     Call this on Awake, and use it to call collection.InitializeItem(T0 item).
        /// 
        ///     If you want a persistent collectible (ala Banjo Kazooie), you could check if a unique
        ///     id matches whatever persistent data management has recorded before deciding whether to
        ///     run AddToUncollectedList() or not.
        /// </summary>
        void Initialize();


        /// <summary>
        ///     This will be called automatically by the T1 collection when the time is right...
        /// </summary>
        void Collect();


        /// <summary>
        ///     This will be called automatically by the T1 collection when the time is right...
        /// </summary>
        void Uncollect();


        /// <summary>
        ///     Be careful! This should only be used to call collection.Collect(this);
        /// </summary>
        void GetCollected();


        /// <summary>
        ///     Be careful! This should only be used to call collection.Uncollect(this);
        /// </summary>
        void GetUncollected();

	}

	public abstract class VanillaCollectible<T0, T1> : VanillaClass, ICollectible<T0, T1>
		where T0 : class, ICollectible<T0, T1>
		where T1 : VanillaCollection<T0, T1>
	{

		public static T1 _collection;

		public T1 collection
		{
			get => _collection;
			set => _collection = value;
		}

		[field: SerializeField]
		[field: ReadOnly]
		public byte id { get; set; } = 0;

		[field: SerializeField]
		[field: ReadOnly]
		public bool collected { get; set; }


		public void Initialize()
		{
			collection.InitializeItem(this as T0);
		}


		public void Collect()
		{
			collection.Collect(this as T0);
		}


		public void Uncollect()
		{
			collection.Uncollect(this as T0);
		}


		public abstract void GetCollected();

		public abstract void GetUncollected();


		public override void Reset()
		{
			collection.DeinitializeItem(this as T0);
		}

	}

	// ------------------------------------------------------------------------------------------- Threshold Modules //
	
	/// <summary>
	/// 	This class is used to compare any kind of struct data against incoming updates and
	/// 	alert us only about significant changes. This can be greatly beneficial in situations
	/// 	where an expensive operation needs to happen frequently but not necessarily constantly.
	///
	/// 	A great example is firing a raycast from the users mouse position. We could fire one
	/// 	every frame but many more invocations will occur than we really need. We could put it
	/// 	on a timer... but there may be incidental frames that miss their target as a result.
	///
	/// 	If we only fire the ray when the users input position has changed by some amount, we're
	/// 	only really firing the ray when we need to which saves us a lot. But what if something
	/// 	moves in front of the ray while the user doesn't update their pose? Hm...!
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	public abstract class BaseThresholdModule<T> : VanillaClass
		where T : struct
	{
		[SerializeField, ReadOnly]
		protected T _delta;

		/// <summary>
		/// 	The current cached value. When this value is changed, its compared against its old
		/// 	value and an event is invoked if the value difference is significant enough.
		///
		/// 	This is a great and easy optimization for ensuring things don't happen any more
		/// 	frequently than they need to. Of course there's a small cost associated with comparing
		/// 	in the first place... so always do your own benchmarks using a Stopwatch!
		/// </summary>
		public T delta
		{
			get => _delta;
			set
			{
				if (!SignificantChange(value)) return;

				_delta = value;

				onThresholdBreach?.Invoke(_delta);
			}
		}
		
		/// <summary>
		/// 	This event will only been invoked when a 'significant' difference is detected in the old and
		/// 	new values.
		/// </summary>
		public UnityAction<T> onThresholdBreach;

		public void Compare
		(
			T newValue)
		{
			delta = newValue;
		}
		
		/// <summary>
		/// 	This function internally checks for 'significant difference' between the delta and new values.
		///
		/// 	What constitutes 'significance' is highly contextually subjective, so it's left to be overridden.
		/// </summary>
		/// <param name="newValue"></param>
		/// <returns></returns>
		protected abstract bool SignificantChange(T newValue);

	}
	
	/// <summary>
	/// 	This class is used to compare any kind of struct data against incoming updates and
	/// 	alert us only about significant changes. This can be greatly beneficial in situations
	/// 	where an expensive operation needs to happen frequently but not necessarily constantly.
	///
	/// 	A great example is firing a raycast from the users mouse position. We could fire one
	/// 	every frame but many more invocations will occur than we really need. We could put it
	/// 	on a timer... but there may be incidental frames that miss their target as a result.
	///
	/// 	If we only fire the ray when the users input position has changed by some amount, we're
	/// 	only really firing the ray when we need to which saves us a lot. But what if something
	/// 	moves in front of the ray while the user doesn't update their pose? Hm...!
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	public abstract class BaseRecordableThresholdModule<T> : VanillaClass
		where T : struct
	{
		[SerializeField, ReadOnly]
		protected T _delta;

		/// <summary>
		/// 	The current cached value. When this value is changed, its compared against its old
		/// 	value and an event is invoked if the value difference is significant enough.
		///
		/// 	This is a great and easy optimization for ensuring things don't happen any more
		/// 	frequently than they need to. Of course there's a small cost associated with comparing
		/// 	in the first place... so always do your own benchmarks using a Stopwatch!
		/// </summary>
		public T delta
		{
			get => _delta;
			set
			{
				if (allowMultipleBreachesPerFrame)
				{
					while (SignificantChange(value))
					{
						UpdateDelta(value);
					}
				}
				else
				{
					if (SignificantChange(value))
					{
						UpdateDelta(value);
					}
				}
			}
		}
		
		/// <summary>
		/// 	This event will only been invoked when a 'significant' difference is detected in the old and
		/// 	new values.
		/// </summary>
		public UnityAction<T> onThresholdBreach;

		/// <summary>
		/// 	If true, delta will step towards new values (based on the threshold) rather than be set instantly.
		///
		/// 	This is useful if the intermediary values need reacting to as well as the target value itself.
		/// </summary>
		public bool clampDeltaUpdates = true;
		
		/// <summary>
		/// 	If true, delta will be clamped by threshold each time it breaches. As a result, it will be
		/// 	possible for onThresholdBreach to be invoked multiple times per frame. This is useful for higher
		/// 	precision and recording of all the intermediary values delta moves through on its way to a new
		/// 	value, although it is potentially more expensive to perform as a result.
		/// </summary>
		public bool allowMultipleBreachesPerFrame = true;
		
		public void Compare
		(
			T newValue)
		{
			delta = newValue;
		}
		
		/// <summary>
		/// 	This function internally checks for 'significant difference' between the delta and new values.
		///
		/// 	What constitutes 'significance' is highly contextually subjective, so it's left to be overridden.
		/// </summary>
		/// <param name="newValue"></param>
		/// <returns></returns>
		protected abstract bool SignificantChange(T newValue);
		
		protected abstract T StepTowardsNewValue(T newValue);

		/// <summary>
		/// 	This function quietly sets the backing field for delta without triggering any events.
		///
		/// 	This is useful if you need to turn the module on and off again without leaving an undesired
		/// 	delta record trail.
		/// </summary>
		/// 
		/// <param name="newValue">
		///		The new value for _delta.
		/// </param>
		public void ResetDelta
		(
			T newValue)
		{
			_delta = newValue;
		}
		
		private void UpdateDelta
		(
			T newValue)
		{
			_delta = clampDeltaUpdates ?
				         StepTowardsNewValue(newValue) :
				         newValue;

			onThresholdBreach?.Invoke(_delta);
		}
	}

	/// <summary>
	/// 	When comparing positional vectors, a distance check can be executed quickly by comparing the square
	/// 	magnitude of their difference. This works for both 2D and 3D vectors, so we share a base class
	/// 	that allows us to reuse the overlapping functionality. 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	public abstract class PositionVectorThresholdModule<T> : BaseRecordableThresholdModule<T>
		where T : struct
	{
		public const float _worldSpaceThresholdMin = 0.001f;
		public const float _worldSpaceThresholdMax = 10.0f;
		
		[SerializeField, ReadOnly]
		protected float _sqrMagThreshold;

		[SerializeField]
		protected float _worldSpaceThreshold = 0.01f;
		public float worldSpaceThreshold
		{
			get => _worldSpaceThreshold;
			set
			{
				_worldSpaceThreshold = value.GetClamp(min: _worldSpaceThresholdMin, 
				                                      max: _worldSpaceThresholdMax);

				_sqrMagThreshold = _worldSpaceThreshold * _worldSpaceThreshold;
			}
		}
		
		public override void Validate<V>
		(
			V behaviour)
		{
			#if UNITY_EDITOR
			base.Validate(behaviour);

			worldSpaceThreshold = _worldSpaceThreshold;
			#endif
		}
	}
	
	/// <summary>
	/// 	When comparing directional vectors, it can be intuitive to think of the difference we want to
	/// 	observe as measured in degrees. This works for both 2D and 3D directional vector comparisons,
	/// 	so we reuse the overlap with this base class.
	/// </summary>
	/// <typeparam name="T">
	///		A vector that can be treated as a direction.
	/// </typeparam>
	[Serializable]
	public abstract class DirectionVectorThresholdModule<T> : BaseRecordableThresholdModule<T>
		where T : struct
	{
		public const float _degreeThresholdMin = 0.01f;
		public const float _degreeThresholdMax = 90.0f;
		
		[SerializeField]
		protected float _thresholdInDegrees = 0.1f;
		public float thresholdInDegrees
		{
			get => _thresholdInDegrees;
			set => _thresholdInDegrees = value.GetClamp(min: _degreeThresholdMin,
			                                            max: _degreeThresholdMax);
		}
		
		public override void Validate<V>
		(
			V behaviour)
		{
			base.Validate(behaviour);

			thresholdInDegrees = _thresholdInDegrees;
		}
	}

	/// <summary>
	/// 	Bug : Recorded values aren't correctly normalized leading to inaccurate data. Unity
	/// 	Bug : doesn't have a built-in Vector2.RotateTowards() so I have to make my own and I'm
	/// 	Bug : very tired.
	/// </summary>
	[Serializable]
	public class DirectionVector2DThresholdModule : DirectionVectorThresholdModule<Vector2>
	{

		protected override bool SignificantChange
		(
			Vector2 newValue)
		{
			return !_delta.IsAlignedWith(direction: newValue,
			                             degrees: _thresholdInDegrees);
		}

		
		protected override Vector2 StepTowardsNewValue
		(
			Vector2 newValue)
		{
			return _delta + ( newValue - _delta ).normalized * ( _thresholdInDegrees * Mathf.Deg2Rad );

//			return delta + (( newValue - _delta ).normalized * _thresholdInDegrees);

//			return delta.GetRotated(_thresholdInDegrees);



//			return Vector2.MoveTowards(current: _delta,
//			                           target: newValue,
//			                           maxDistanceDelta: _thresholdInDegrees * Mathf.Deg2Rad);

//			return Vector2.RotateTowards(current: _delta,
//			                             target: newValue,
//			                             maxRadiansDelta: _thresholdInDegrees * Mathf.Deg2Rad,
//			                             maxMagnitudeDelta: 1.0f);
		}

	}

	[Serializable]
	public class DirectionVector3DThresholdModule : DirectionVectorThresholdModule<Vector3>
	{

		protected override bool SignificantChange
		(
			Vector3 newValue)
		{
			return !_delta.IsAlignedWith(direction: newValue, 
			                                degrees: _thresholdInDegrees);
		}
		
		protected override Vector3 StepTowardsNewValue
		(
			Vector3 newValue)
		{
			return Vector3.RotateTowards(current: _delta,
			                             target: newValue,
			                             maxRadiansDelta: _thresholdInDegrees * Mathf.Deg2Rad,
			                             maxMagnitudeDelta: 1.0f).normalized;
		}
	}

		
	/// <summary>
	/// 	This module will take in a Vector2, treated as a position, and invoke the onThresholdBreach
	/// 	event automatically when a change in distance greater than worldSpaceThreshold is detected.
	/// </summary>
	[Serializable]
	public class Position2DThresholdModule : PositionVectorThresholdModule<Vector2>
	{

		protected override bool SignificantChange
		(
			Vector2 newValue)
		{
			return ( newValue - _delta ).sqrMagnitude > _sqrMagThreshold;
		}

		protected override Vector2 StepTowardsNewValue
		(
			Vector2 newValue)
		{
			return _delta + ( newValue - _delta ).normalized * _worldSpaceThreshold;
		}
	}
	
	/// <summary>
	/// 	This module will take in a Vector3, treated as a position, and invoke the onThresholdBreach
	/// 	event automatically when a change in distance greater than worldSpaceThreshold is detected.
	/// </summary>
	[Serializable]
	public class Position3DThresholdModule : PositionVectorThresholdModule<Vector3>
	{
		protected override bool SignificantChange
		(
			Vector3 newValue)
		{
			return ( newValue - _delta ).sqrMagnitude > _sqrMagThreshold;
		}

		protected override Vector3 StepTowardsNewValue
		(
			Vector3 newValue)
		{
			return _delta + ( newValue - _delta ).normalized * _worldSpaceThreshold;
		}
	}

	/// <summary>
	///		This module is the exact same as Position3DThresholdModule except that each time the new value
	/// 	breaches the delta, the delta is updated, radially clamped and the onNewDeltaRecord event
	/// 	is invoked. This can happen multiple times a frame if the delta breach is significant enough.
	///
	/// 	For example, if thresholdInMetres was 1.0f and a new value 10.0f metres away from delta was
	/// 	recorded, this event would be invoked 10 times, each time with delta a metre closer to the new
	/// 	value. This is useful for systems that require extra accuracy like navigation, analytics,
	/// 	replay systems, etc.
	/// </summary>
 	[Serializable]
	public class RecordedPosition3DThresholdModule : Position3DThresholdModule
	{

		public Action<Vector3> onNewDeltaRecord;

		protected override bool SignificantChange
		(
			Vector3 newPosition)
		{
			var breachFrame = false;

			while (( newPosition - _delta ).sqrMagnitude > _sqrMagThreshold)
			{
				_delta += ( newPosition - _delta ).normalized * _worldSpaceThreshold;

				onNewDeltaRecord?.Invoke(_delta); // We want to invoke this as many times as necessary.

				breachFrame = true;
			}

			if (breachFrame) onThresholdBreach?.Invoke(newPosition); // We only want to invoke this once.

			return breachFrame;
		}

	}

	/// <summary>
    ///     This module will invoke its onNewPosition event when a given position Vector3 is far enough away from a new value.
    /// 	In other words... subscribe to onThresholdBreached, feed in new values in some kind of update loop and you'll only hear back when that position is further than
    ///     the threshold in metres away! It's a great way of doing something using a transforms
    ///     position without having to worry about it happening every single frame.
    /// </summary>
    [Serializable]
	public class PositionComparisonModule : VanillaClass
	{

		protected Vector3 _deltaPosition;

		[SerializeField] [ReadOnly]
		protected float _sqrMagThreshold;
		[SerializeField]
		[Range(min: 0.01f,
			max: 10.0f)]
		private float _thresholdInMetres = 0.05f;

		public Action<Vector3> onNewPosition;

		public float thresholdInMetres
		{
			get => _thresholdInMetres;
			set
			{
				_thresholdInMetres = value;

				_sqrMagThreshold = value * value;
			}
		}


		public override void Validate<V>(V behaviour)
		{
			thresholdInMetres = _thresholdInMetres;
		}


		public virtual bool Compare(Vector3 newPosition)
		{
			if (( newPosition - _deltaPosition ).sqrMagnitude < _sqrMagThreshold) return false;

			_deltaPosition = newPosition;

			onNewPosition?.Invoke(newPosition);

			return true;
		}

	}

    /// <summary>
    ///     This module is the exact same as PositionDiffModule except that when the delta position is
    ///     breached and recorded, the new value will be radially clamped to the last value. In other words,
    ///     you always know that each breach position can only ever be exactly thresholdInMetres away from
    ///     the last. This is useful for wayfinding like measuring and visualising navigation paths.
    /// 
    ///     This module is the exact same as PositionDiffModule except for that it takes the extra effort to
    ///     invoke all the exact delta positions taken on the way to reach newPosition when breach occurs.
    /// 
    ///     This would be useful for wayfinding systems, like visualising a navigated path or recorded the
    ///     exact distance a transform has travelled in metres.
    /// </summary>
    [Serializable]
	public class PrecisePositionComparisonModule : PositionComparisonModule
	{

		public Action<Vector3> onNewDeltaRecord;


		public override bool Compare(Vector3 newPosition)
		{
			var breachFrame = false;

			while (( newPosition - _deltaPosition ).sqrMagnitude > _sqrMagThreshold)
			{
				_deltaPosition += ( newPosition - _deltaPosition ).normalized * thresholdInMetres;

				onNewDeltaRecord?.Invoke(_deltaPosition); // We want to invoke this as many times as necessary.

				breachFrame = true;
			}

			if (breachFrame) onNewPosition?.Invoke(newPosition); // We only want to invoke this once.

			return breachFrame;
		}

	}
    
    // ----------------------------------------------------------------------------------------- Render Texture Slot //

    /// <summary>
    /// 	A class containing the basic necessities for creating and wielding a custom RenderTexture at run-time.
    /// </summary>
    [Serializable]
    public class RenderTextureSlot : VanillaClass
    {

	    [SerializeField, ReadOnly]
	    private RenderTexture _texture;
	    public RenderTexture texture
	    {
		    get
		    {
			    if (_texture != null) return _texture;

			    _texture = new RenderTexture(width: pixelSize,
			                                 height: pixelSize,
			                                 depth: depth,
			                                 format: format);

			    if (!_texture.Create()) Error("Creating RenderTexture failed.");

			    Fill(color: defaultColor);
			    
			    return _texture;
		    }
	    }

	    [SerializeField]
	    public int pixelSize = 1024;

	    [SerializeField]
	    public int depth = 24;

	    [SerializeField]
	    public RenderTextureFormat format;

	    [SerializeField]
	    public Color defaultColor = Color.black;

	    public RenderTextureSlot
	    (
		    int                 pixelSize = 1024,
		    int                 depth     = 24,
		    RenderTextureFormat format    = RenderTextureFormat.ARGB32)
	    {
		    this.pixelSize = pixelSize;
		    this.depth     = depth;
		    this.format    = format;
	    }


	    public override void Validate<V>
	    (
		    V behaviour)
	    {
		    if (depth != 0  ||
		        depth != 16 ||
		        depth != 24)
		    {
			    // Run this weird obtuse snapping to the only valid values

			    if (depth      < 8) { depth  = 0; }
			    else if (depth < 20) { depth = 16; }
			    else if (depth < 28) { depth = 24; }
			    else { depth                 = 32; }
		    }

		    pixelSize.ToNearestPowerOfTwo();
	    }



	    public void Fill(Color color)
	    {
		    RenderTexture.active = texture;

		    GL.Clear(clearDepth: true,
		             clearColor: true,
		             backgroundColor: color);

		    RenderTexture.active = null;
	    }

	    public void FillBlack() => Fill(Color.black);

	    public void FillWhite() => Fill(Color.white);
    }

    // ----------------------------------------------------------------------------------------------- Material Slot //

	// Allows for easy handling of Material instantiation or re-use

	[Serializable]
	public class VanillaMaterialSlot : VanillaClass
	{

		[SerializeField] [ReadOnly]
		private Material _materialInstance;

		public bool     createInstance = true;
		public Material sourceMaterial;
		public Material materialInstance
		{
			get
			{
				if (_materialInstance != null && sourceMaterial != null) return _materialInstance;

				if (createInstance)
				{
					_materialInstance = new Material(sourceMaterial);

					onMaterialInstanceCreated.Invoke(_materialInstance);
				}
				else
				{
					_materialInstance = sourceMaterial;
				}
				
				return _materialInstance;
			}
		}
		
		public GenericEvent<Material> onMaterialInstanceCreated = new GenericEvent<Material>();

		public override void Validate<V>(V behaviour) { }


		public override void Reset()
		{
			_materialInstance = null;
		}

	}
	
	// -------------------------------------------------------------------------------------------------------- Slot //

    /// <summary>
    ///     Only one 'T' fits at a time.
    ///     We're able to listen for when an old T is outgoing or when a new T is incoming to the slot.
    /// </summary>
    [Serializable]
    [Obsolete("VanillaSlot<T> and ProtectedSlot<T> have been deprecated due to value comparison complications. Going forwards, please use ReferenceSlot<T> for reference types and ValueSlot<T> for value types.")]
	public abstract class VanillaSlot<T> : VanillaClass
	{

		[SerializeField]
		public T _current;
		public virtual T current
		{
			get => _current;
			set
			{
				onSlotChange.Invoke(arg0: _current,
				                    arg1: value);

				_current = value;
			}
		}
		
		public GenericEvent<T, T> onSlotChange = new GenericEvent<T, T>();

		public override void Reset()
		{
			_current = default;

			onSlotChange.RemoveAllListeners();
		}

	}
    
    // ---------------------------------------------------------------------------------------------------- Ref Slot //

    /// <summary>
    ///     Only one 'T' fits at a time, where T is a reference type.
    /// 
    ///     We're able to listen for when an old T is outgoing or when a new T is incoming to the slot.
    /// </summary>
    [Serializable]
    public class ReferenceSlot<T> : VanillaClass
		where T : class, new()
    {

	    [SerializeField]
	    public T _current;
	    public virtual T current
	    {
		    get => _current;
		    set
		    {
			    if (ReferenceEquals(objA: _current,
			                        objB: value))
				    return;

			    onSlotChange.Invoke(arg0: _current,
			                        arg1: value);

			    _current = value;
		    }
	    }
		
	    public GenericEvent<T, T> onSlotChange = new GenericEvent<T, T>();

	    public override void Reset()
	    {
		    _current = default;

		    onSlotChange.RemoveAllListeners();
	    }

    }

    /// <summary>
    /// 	
    /// </summary>
    /// 
    /// <typeparam name="T">
    ///		Some kind of value type.
    /// </typeparam>
    [Serializable]
    public abstract class ValueSlot<T> : VanillaClass
		where T : struct
    {

	    [SerializeField]
	    public T _current;
	    public virtual T current
	    {
		    get => _current;
		    set
		    {
			    if (EqualityComparer<T>.Default.Equals(x: _current,
			                                           y: value))
				    return;

			    onSlotChange.Invoke(arg0: _current,
			                        arg1: value);

			    _current = value;
		    }
	    }
		
	    [SerializeField]
	    public GenericEvent<T, T> onSlotChange = new GenericEvent<T, T>();

	    public override void Reset()
	    {
		    _current = default;

		    onSlotChange.RemoveAllListeners();
	    }

    }
    
    
    

    /// <summary>
    ///     Only one 'T' fits in .current at a time.
    /// 
    ///     For the protected slot, only objects of type T that are comparably different are allowed to replace .current.
    /// 
    ///     If .current is assigned to the same object, the set returns early and nothing happens.
    /// </summary>

    // ---------------------------------------------------------------------------------------------- Protected Slot //
	
    [Serializable]
    [Obsolete("VanillaSlot<T> and ProtectedSlot<T> have been deprecated due to value comparison complications. Going forwards, please use ReferenceSlot<T> for reference types and ValueSlot<T> for value types.")]
    public class ProtectedSlot<T> : VanillaSlot<T>
	{

		public override T current
		{
			get => _current;
			set
			{
				// There are so many ways to skin a cat here and some comparisons are type-specific.
				// For example, if someone wants a ProtectedSlot<float>, this should be using Mathf.Approximately().

				if (TheSame(a: _current,
				            b: value))
					return;

				onSlotChange.Invoke(arg0: _current,
				                    arg1: value);

				_current = value;
			}
		}

		public override void Validate<V>(V behaviour) { }


		protected virtual bool TheSame
		(
			T a,
			T b)
		{
			return Equals(objA: a,
			              objB: b);
		}

	}
    
	[Serializable]
	public class ProtectedTransformSlot : ProtectedSlot<Transform> { }

	[Serializable]
	public class ProtectedGameObjectSlot : ProtectedSlot<GameObject> { }

	[Serializable]
	public class ProtectedIntSlot : ProtectedSlot<int>
	{

		protected override bool TheSame
		(
			int a,
			int b)
		{
			return a == b;
		}

	}

	[Serializable]
	public class ProtectedFloatSlot : ProtectedSlot<float>
	{

		protected override bool TheSame
		(
			float a,
			float b)
		{
			return a.IsRoughly(b);
		}

	}

	[Serializable]
	public class ProtectedBoolSlot : ProtectedSlot<bool>
	{

		protected override bool TheSame
		(
			bool a,
			bool b)
		{
			return a == b;
		}

	}
	
	[Serializable]
	public class SelfHoverSlot<T> : ProtectedSlot<T>
		where T : ISelfSelectable<T>
	{

		public override T current
		{
			get => _current;
			set
			{
				if (TheSame(a: _current,
				            b: value))
					return;

				_current?.Dehovered();
				
				_current = value;
				
				_current?.Hovered();
				
				onSlotChange.Invoke(arg0: _current,
				                    arg1: value);
			}
		}

	}
	

	[Serializable]
	public class SelfSelectionSlot<T> : ProtectedSlot<T>
		where T : ISelfSelectable<T>
	{

		public override T current
		{
			get => _current;
			set
			{
				if (TheSame(a: _current,
				            b: value))
					return;

				_current?.Deselected();

				_current = value;
				
				_current?.Selected();
				
				onSlotChange.Invoke(arg0: _current,
				                    arg1: value);
			}
		}

	}
	
	// ----------------------------------------------------------------------------------------------- HashSet Queue //

	/// <summary>
	/// 	This collection features both a HashSet and a Queue and uses one or the other depending
	/// 	on the operation for best performance.
	/// </summary>
	[Serializable]
	public class HashSetQueue<T> : IEnumerable<T>
		where T : class
	{
		[SerializeField]
		private HashSet<T> _hashSet = new HashSet<T>();

		[SerializeField]
		private Queue<T> _queue = new Queue<T>();

		public int Count
			=> _hashSet.Count;
		
		public bool Contains
			(T item) =>
				_hashSet.Contains(item);

		public void Clear()
		{
			_hashSet.Clear();
			_queue.Clear();
		}

		public bool Enqueue
		(
			T item)
		{
			if (!_hashSet.Add(item)) return false;

			_queue.Enqueue(item);

			return true;
		}


		public bool Remove
		(
			T item)
		{
			if (!Contains(item)) return false;

			_hashSet.Remove(item);
			_queue = new Queue<T>(_queue.Where(s => s != item));

			return true;
		}

		public T Dequeue()
		{
			var item = _queue.Dequeue();

			_hashSet.Remove(item);

			return item;
		}

		public T Peek() {
			return _queue.Peek();
		}

		public IEnumerator<T> GetEnumerator() {
			return _queue.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return _queue.GetEnumerator();
		}
	}
	
	// ------------------------------------------------------------------------------------------------------- Queue //

	
	
	[Serializable]
	public class VanillaQueue<T> : VanillaClass
		where T : class
	{

		[SerializeField]
		public List<T> contents = new List<T>();
		[SerializeField]
		public bool IsEmpty = true;


		public virtual void Queue(T newItem)
		{
			contents.Add(newItem);

			IsEmpty = contents.Count < 1;
		}


		public void PushIn
		(
			T   newItem,
			int index)
		{
			contents.Insert(index: index,
			                item: newItem);

			IsEmpty = contents.Count < 1;
		}


		public void Dequeue(T oldItem)
		{
			while (contents.Contains(oldItem)) contents.Remove(oldItem);

			IsEmpty = contents.Count < 1;
		}


		public T Process()
		{
			if (contents.Count < 1) return default;

			var item = contents[0];

			contents.RemoveAt(0);

			IsEmpty = contents.Count < 1;

			return item;
		}


		public T Peek()
		{
			return contents.Count < 1 ? null : contents[0];
		}


		public bool Contains(T item)
		{
			return contents.Contains(item);
		}


		public void LogContents()
		{
			foreach (var t in contents) Debug.Log(t);
		}


		public override void Validate<V>(V behaviour) { }


		public override void Reset()
		{
			contents.Clear();

			IsEmpty = true;
		}

	}

	// --------------------------------------------------------------------------------------------- Protected Queue //

	public class ProtectedQueue<T> : VanillaQueue<T>
		where T : class
	{

		public override void Queue(T newItem)
		{
			if (contents.Contains(newItem)) return;

			base.Queue(newItem);
		}

	}

	// ----------------------------------------------------------------------------------------------- Limited Queue //

	public class LimitedQueue<T> : VanillaQueue<T>
		where T : class
	{

		[Tooltip("The maximum allowed length of the queue. "               +
		         "Items that are queued past this limit will be ignored. " +
		         "-1 represents infinite.")]
		public int queueMaximum = -1;


		public override void Queue(T newItem)
		{
			if (contents.Count >= queueMaximum) return;

			base.Queue(newItem);
		}

	}

	// ------------------------------------------------------------------------------------ Limited No Doubles Queue //

	public class LimitedNoDoubleQueue<T> : VanillaQueue<T>
		where T : class
	{

		[Tooltip("The maximum allowed length of the queue. " +
		         "Items that are queued past this limit will be ignored.")]
		public int queueMaximum = -1;


		public override void Queue(T newItem)
		{
			if (contents.Contains(newItem) || contents.Count >= queueMaximum) return;

			base.Queue(newItem);
		}

	}

	// --------------------------------------------------------------------------------------------- Vanilla Routine //

	// This really needs to be split up!
	// The below class should be called VanillaNormalRoutine and inherit from a class below it called VanillaRoutine
	// Then you can also have another kind of routine called VanillaUntimedRoutine.
	// Or maybe VanillaRoutine simply works that way until you inherit from it?
	// Regardless, you need a kind of routine that doesn't care about normal stuff or timings!
	// Some routines don't know how long they're supposed to go for and shouldn't care either, like the Typewriter.

	[Serializable]
	public abstract class VanillaManualRoutine : VanillaClass
	{

		protected YieldInstruction endOfFrameAction;
		[SerializeField] [ReadOnly]
		public bool isRunning; // Is the routine currently running?
		[SerializeField] [ReadOnly]
		protected bool keepRunning; // The routine will run until this is false.

		private IEnumerator routine;


		public virtual void Start(MonoBehaviour coroutineSurrogate)
		{
			Stop(coroutineSurrogate);

			routine = Routine();

			isRunning   = true;
			keepRunning = true;

			coroutineSurrogate.StartCoroutine(routine);
		}


		public virtual void Stop(MonoBehaviour coroutineSurrogate)
		{
			if (!isRunning) return;

			coroutineSurrogate.StopCoroutine(routine);

			isRunning = false;
		}


		protected virtual IEnumerator Routine()
		{
			Began();

			while (keepRunning)
			{
				PreRun();

				Run();

				PostRun();

				yield return endOfFrameAction;
			}

			Ended();

			isRunning = false;
		}


        /// <summary>
        ///     Runs once on the frame the coroutine is started.
        /// </summary>
        protected virtual void Began() { }


        /// <summary>
        ///     Intended for inherited classes that want to get something done before Run()
        /// </summary>
        protected virtual void PreRun() { }


        /// <summary>
        ///     This is the part where you get this done frame to frame!
        /// </summary>
        protected abstract void Run();


        /// <summary>
        ///     Intended for inherited classes that want to get something done after Run()
        /// </summary>
        protected virtual void PostRun() { }


        /// <summary>
        ///     Intended for inherited classes that want to get something done after the yield instruction
        /// </summary>
        protected virtual void PostYield() { }


        /// <summary>
        ///     Runs once on the frame the coroutine ends.
        /// </summary>
        protected virtual void Ended() { }

	}

    /// <summary>
    ///     This heavily controlled routine runs automatically for the nominated amount of runningTime.
    ///     While this happens, it also ticks up a 'normal' value (a float between 0 and 1) which can
    ///     be used for easily moving and interpolating something.
    /// </summary>
    [Serializable]
	public abstract class VanillaNormalRoutine : VanillaClass
	{

		public float normal;

		private IEnumerator routine;

		[SerializeField] [ReadOnly]
		private bool running;
		public float runningTime = 1.0f;

		protected WaitForSeconds waitPerFrame;


		public void Start_In
		(
			VanillaBehaviour coroutineSurrogate,
			bool             resume)
		{
			if (running) coroutineSurrogate.StopCoroutine(routine);

			if (!resume) normal = 0.0f;

			routine = InRoutine();

			coroutineSurrogate.StartCoroutine(routine);
		}


		public void Start_Out
		(
			VanillaBehaviour coroutineSurrogate,
			bool             resume)
		{
			if (running) coroutineSurrogate.StopCoroutine(routine);

			if (!resume) normal = 1.0f;

			routine = OutRoutine();

			coroutineSurrogate.StartCoroutine(routine);
		}


		private IEnumerator InRoutine()
		{
			running = true;

			if (normal.IsZero()) InStarted();

			var rate = 1.0f / runningTime;

			while (normal < 1.0f)
			{
				normal += Time.deltaTime * rate;

				Run_Internal();

				Run();

				yield return waitPerFrame;
			}

			InEnded();

			running = false;
		}


		private IEnumerator OutRoutine()
		{
			running = true;

			if (normal.IsTheSameAs(1.0f)) OutStarted();

//            var n = 1.0f;
//            normal = 1.0f;

			var rate = 1.0f / runningTime;

			while (normal > 0.0f)
			{
				normal -= Time.deltaTime * rate;

				Run_Internal();

				Run();

				yield return null;
			}

			OutEnded();

			running = false;
		}


		public void Stop(VanillaBehaviour coroutineSurrogate)
		{
			if (!running) return;

			coroutineSurrogate.StopCoroutine(routine);

			running = false;
		}


		// Pause() ?

		// Resume() ?

		protected abstract void Run_Internal();

		protected abstract void Run();

		protected virtual void InStarted() { }

		protected virtual void InEnded() { }

		protected virtual void OutStarted() { }

		protected virtual void OutEnded() { }

	}

	// ------------------------------------------------------------------------------------------- Routine Mutations //

	[Serializable]
	public abstract class BasicRoutine : VanillaNormalRoutine
	{

		protected sealed override void Run_Internal() { }

		protected abstract override void Run();

		protected override void InStarted() { }

		protected override void InEnded() { }

		protected override void OutStarted() { }

		protected override void OutEnded() { }

		public override void Reset() { }

	}

	[Serializable]
	public abstract class BasicEventRoutine : VanillaNormalRoutine
	{

		[SerializeField] [ReadOnly]
		private VanillaNormal vanillaNormal;


		protected sealed override void Run_Internal()
		{
			vanillaNormal.normal = normal;
		}


		protected abstract override void Run();

		protected override void InStarted() { }

		protected override void InEnded() { }

		protected override void OutStarted() { }

		protected override void OutEnded() { }

		public override void Reset() { }

	}

	[Serializable]
	public abstract class EasedRoutine : VanillaNormalRoutine
	{

		[SerializeField] [ReadOnly]
		public float easedNormal;
		[SerializeField]
		public EasingMethodSlot easing;


		protected sealed override void Run_Internal()
		{
			easedNormal = easing.method(normal);
		}


		public override void Reset() { }

		protected abstract override void Run();

		protected override void InStarted() { }

		protected override void InEnded() { }

		protected override void OutStarted() { }

		protected override void OutEnded() { }

	}

	[Serializable]
	public abstract class EasedEventRoutine : VanillaNormalRoutine
	{

		private UnityEvent _onInEnded;

		private UnityEvent _onOutEnded;
		[SerializeField]
		public EasedNormal easedNormal;
		public UnityEvent onInEnded  => _onInEnded  ?? ( _onInEnded = new UnityEvent() );
		public UnityEvent onOutEnded => _onOutEnded ?? ( _onOutEnded = new UnityEvent() );


		protected sealed override void Run_Internal()
		{
			easedNormal.normal = normal;
		}


		public override void Reset() { }

		protected abstract override void Run();

		protected override void InStarted() { }


		protected override void InEnded()
		{
			onInEnded.Invoke();
		}


		protected override void OutStarted() { }


		protected override void OutEnded()
		{
			onOutEnded.Invoke();
		}

	}


	// ---------------------------------------------------------------------------------------------------- Smoothie //

	[Serializable]
	public abstract class VanillaSmoothie<T>
		where T : struct
	{

		[SerializeField] [ReadOnly]
		protected List<T> _contents = new List<T>();
		[SerializeField] [ReadOnly]
		public T output;

		public VanillaSmoothie(int capacity) => _contents.Capacity = capacity;

		public List<T> contents => _contents;


        /// <summary>
        ///     Add some data for the smoothie.
        /// </summary>
        /// <param name="input"></param>
        public void Add(T input)
		{
			contents.Cycle(input);
		}


        /// <summary>
        ///     Averages the contents of all contained data and stores it in output.
        /// </summary>
        public abstract void Blend();


        /// <summary>
        ///     Adds some data, blends it, updates output and returns it immediately.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public T Smooth(T input)
		{
			contents.Cycle(input);

			Blend();

			return output;
		}

	}

	// ---------------------------------------------------------------------------------------------- Float Smoothie //

	[Serializable]
	public class FloatSmoothie : VanillaSmoothie<float>
	{

		public FloatSmoothie(int capacity) : base(capacity) { }


		public override void Blend()
		{
			output = contents.Average();
		}

	}

	// ------------------------------------------------------------------------------------------------ Int Smoothie //

	[Serializable]
	public class IntSmoothie : VanillaSmoothie<int>
	{

		public IntSmoothie(int capacity) : base(capacity) { }


		public override void Blend()
		{
			output = contents.GetAverage();
		}

	}

	// ------------------------------------------------------------------------------------------------ Int Smoothie //

	[Serializable]
	public class LongSmoothie : VanillaSmoothie<long>
	{

		public LongSmoothie(int capacity) : base(capacity) { }


		public override void Blend()
		{
			output = contents.Sum() / contents.Count;
		}

	}

	// -------------------------------------------------------------------------------------------- Vector2 Smoothie //

	[Serializable]
	public class Vector2Smoothie : VanillaSmoothie<Vector2>
	{

		public Vector2Smoothie(int capacity) : base(capacity) { }


		public override void Blend()
		{
			output = contents.GetAverage();
		}

	}

	// -------------------------------------------------------------------------------------------- Vector3 Smoothie //

	[Serializable]
	public class Vector3Smoothie : VanillaSmoothie<Vector3>
	{

		public Vector3Smoothie(int capacity) : base(capacity) { }


		public override void Blend()
		{
			output = contents.GetAverage();
		}

	}

	// ----------------------------------------------------------------------------------------- Quaternion Smoothie //

	[Serializable]
	public class QuaternionSmoothie : VanillaSmoothie<Quaternion>
	{

		[SerializeField]
		public bool normalize;

		public QuaternionSmoothie(int capacity) : base(capacity) { }


		public override void Blend()
		{
			output = contents.GetAverage1();
		}

	}

	// ------------------------------------------------------------------------------------------- Vanilla StopWatch //

	[Serializable]
	public class VanillaStopwatch : VanillaClass
	{

		[SerializeField]
		private bool _running;
		[SerializeField] [ReadOnly]
		public long milliseconds;
		[SerializeField] [ReadOnly]
		private LongSmoothie millisecondSmoothie = new LongSmoothie(32);

		[SerializeField] [ReadOnly]
		public long ticks;
		[SerializeField] [ReadOnly]
		private LongSmoothie tickSmoothie = new LongSmoothie(32);

		[SerializeField]
		public Stopwatch watch;

		public bool running
		{
			get => _running;
			set
			{
				_running = value;

				if (_running)
				{
					if (watch == null) Start();
				}
				else
				{
					if (watch != null) Stop();
				}
			}
		}


		public void Start()
		{
			running = true;

			watch = Stopwatch.StartNew();
		}


		public void Stop()
		{
			var t = watch.ElapsedTicks;
			var m = watch.ElapsedMilliseconds;

			watch.Stop();

			ticks        = tickSmoothie.Smooth(t);
			milliseconds = millisecondSmoothie.Smooth(m);

			running = false;
		}


		public override void Validate<V>(V behaviour)
		{
			base.Validate(behaviour);

			if (Application.isPlaying) running = _running;

			tickSmoothie.contents.Capacity        = 16;
			millisecondSmoothie.contents.Capacity = 16;
		}

	}

	// ----------------------------------------------------------------------------------------------------- Min/Max //

	[Serializable]
	public struct Range
	{

		[SerializeField]
		private float _min;

		public float min
		{
			get => _min;
			set
			{
				if (value > _max)
				{
					Vanilla.Warning(culprit: "Range",
					                message: "Min cannot be set to a value higher than max.\n" +
					                         $"i.e. [{value}] is higher than [{_max}].");

					_min = _max;

					return;
				}

				_min = value;
			}
		}

		[SerializeField]
		private float _max;

		public float max
		{
			get => _max;
			set
			{
				if (value < _min)
				{
					Vanilla.Warning(culprit: "Range",
					                message: "Max cannot be set to a value lower than min.\n" +
					                         $"i.e. [{value}] is lower than [{_min}].");

					_max = _min;

					return;
				}

				_max = value;
			}
		}


		public Range
		(
			float min,
			float max)
		{
			_min = min;
			_max = max;
		}


        /// <summary>
        ///     Returns a float interpolated between min and max based on the given value.
        /// </summary>
        public float Lerp(float value)
		{
			return Mathf.Lerp(a: _min,
			                  b: _max,
			                  t: value);
		}


		public float Random()
		{
			return UnityEngine.Random.Range(min: _min,
			                                max: _max);
		}


		public bool InclusivelyContains(float value)
		{
			return value.IsWithinInclusiveRange(min: _min,
			                                    max: _max);
		}


		public bool ExclusivelyContains(float value)
		{
			return value.IsWithinExclusiveRange(min: _min,
			                                    max: _max);
		}

	}

	[Serializable]
	public struct IntRange
	{

		[SerializeField]
		private int _min;

		public int min
		{
			get => _min;
			set
			{
				if (value > _max)
				{
					Vanilla.Warning(culprit: "Range",
					                message: "Min cannot be set to a value higher than max.\n" +
					                         $"i.e. [{value}] is higher than [{_max}].");

					_min = _max;

					return;
				}

				_min = value;
			}
		}

		[SerializeField]
		private int _max;

		public int max
		{
			get => _max;
			set
			{
				if (value < _min)
				{
					Vanilla.Warning(culprit: "Range",
					                message: "Max cannot be set to a value lower than min.\n" +
					                         $"i.e. [{value}] is lower than [{_min}].");

					_max = _min;

					return;
				}

				_max = value;
			}
		}


		public IntRange
		(
			int min,
			int max)
		{
			_min = min;
			_max = max;
		}


        /// <summary>
        ///     Returns a float interpolated between min and max based on the given value.
        /// </summary>
        public int Lerp(float value)
		{
			return (int) Mathf.Lerp(a: _min,
			                        b: _max,
			                        t: value);
		}


		public int Random()
		{
			return UnityEngine.Random.Range(min: _min,
			                                max: _max);
		}


		public bool InclusivelyContains(int value)
		{
			return value.IsWithinInclusiveRange(min: _min,
			                                    max: _max);
		}


		public bool ExclusivelyContains(int value)
		{
			return value.IsWithinExclusiveRange(min: _min,
			                                    max: _max);
		}

	}

	// ---------------------------------------------------------------------------------------------- Vanilla Normal //

//    // Custom Editor //
//    #if UNITY_EDITOR
//    [CustomEditor(typeof(VanillaNormal), true)]
//    public class NormalEditor : Editor
//    {
//        private SerializedProperty _normal;
//
//        private void OnEnable()
//        {
//            _normal = serializedObject.FindProperty("_normal");
//        }
//
//        public override void OnInspectorGUI()
//        {
//            serializedObject.Update();
//
//            base.OnInspectorGUI();
//
//            EditorGUILayout.HelpBox("This slider lets us test out the normal in the inspector.", MessageType.Info);
//
//            EditorGUILayout.Slider(_normal, 0, 1, "Normal");
//            
//            serializedObject.ApplyModifiedProperties();
//        }
//    }
//    #endif

	[Serializable]
	public abstract class VanillaNormal : VanillaClass
	{

		[SerializeField] [ReadOnly]
		protected float _normal;

		[SerializeField]
		private FloatEvent _onNewValue;

		[SerializeField] [ReadOnly]
		protected float _value;

		[SerializeField]
		public bool ignoreEvent;

		[SerializeField]
		public bool protectValue = true;

		public virtual float normal
		{
			get => _normal;
			set
			{
				if (protectValue && _normal.IsTheSameAs(value)) return;

				_normal = value.GetBetween01();

				_value = NormalToValue();

				if (!ignoreEvent) onNewValue.Invoke(_value);
			}
		}

		public float value => _value;

		public FloatEvent onNewValue => _onNewValue ?? ( _onNewValue = new FloatEvent() );

		protected abstract float NormalToValue();


		public override void Reset()
		{
			normal = 0.0f;
		}

	}

	// ------------------------------------------------------------------------------------------------ Basic Normal //

	[Serializable]
	public class BasicNormal : VanillaNormal
	{

		protected override float NormalToValue()
		{
			return _normal;
		}


		public override void Validate<V>(V behaviour) { }

	}

	// ----------------------------------------------------------------------------------------------- Master Normal //

    /// <summary>
    ///     This class will automatically apply its normal value (when changed) to all other normals in its
    ///     'slave' list, allowing multiple normal classes to be driven with just one normal.
    /// 
    ///     Initialize() must be called on this class before it can do its job!
    /// </summary>
    [Serializable]
	public class MasterNormal : VanillaNormal
	{

		[SerializeField]
		public List<VanillaNormal> slaveNormals = new List<VanillaNormal>();


		protected override float NormalToValue()
		{
			return _normal;
		}


		public void Initialize()
		{
			onNewValue.AddListener(DriveSlaveNormals);
		}


		private void DriveSlaveNormals(float n)
		{
			foreach (var s in slaveNormals) s.normal = n;
		}


		public override void Validate<V>(V behaviour) { }

	}

	// ----------------------------------------------------------------------------------------- Eased Master Normal //

	[Serializable]
	public class EasedMasterNormal : MasterNormal
	{

		[SerializeField]
		public EasingMethodSlot easing = new EasingMethodSlot();


		protected override float NormalToValue()
		{
			return easing.method(_normal);
		}

	}

	// ----------------------------------------------------------------------------------------------- Custom Normal //

	[Serializable]
	public class CustomNormal : VanillaNormal
	{

		public Range range = new Range(min: 0.0f,
		                               max: 1.0f);


		protected override float NormalToValue()
		{
			return Mathf.Lerp(a: range.min,
			                  b: range.max,
			                  t: _normal);
		}


		public override void Validate<V>(V behaviour) { }

	}

	// ------------------------------------------------------------------------------------------------ Curve Normal //

	[Serializable]
	public class CurveNormal : VanillaNormal
	{

		[SerializeField]
		public AnimationCurve curve = AnimationCurve.EaseInOut(timeStart: 0.0f,
		                                                       valueStart: 0.0f,
		                                                       timeEnd: 1.0f,
		                                                       valueEnd: 1.0f);


		protected override float NormalToValue()
		{
			return curve.Evaluate(_normal);
		}


		public override void Validate<V>(V behaviour) { }

	}

	// ----------------------------------------------------------------------------------------- Custom Curve Normal //

	[Serializable]
	public class CustomCurveNormal : CustomNormal
	{

		[SerializeField]
		public AnimationCurve curve = AnimationCurve.EaseInOut(timeStart: 0.0f,
		                                                       valueStart: 0.0f,
		                                                       timeEnd: 1.0f,
		                                                       valueEnd: 1.0f);


		protected override float NormalToValue()
		{
			return Mathf.Lerp(a: range.min,
			                  b: range.max,
			                  t: curve.Evaluate(_normal));
		}

	}

	// ------------------------------------------------------------------------------------------------ Eased Normal //

    /// <summary>
    ///     This class establishes a value interpolated or 'eased' between two custom values.
    /// 
    ///     When working with Vector3 interpolations, consider using Vector3.LerpUnclamped!
    /// </summary>
    [Serializable]
	public class EasedNormal : VanillaNormal
	{

		[SerializeField]
		public EasingMethodSlot easing = new EasingMethodSlot();


		protected override float NormalToValue()
		{
			return easing.method(_normal);
		}


		public override void Validate<V>(V behaviour) { }

	}

	// ----------------------------------------------------------------------------------------- Eased Custom Normal //

    /// <summary>
    ///     This class establishes a value interpolated or 'eased' between two custom values.
    /// 
    ///     When working with Vector3 interpolations, consider using Vector3.LerpUnclamped!
    /// </summary>
    [Serializable]
	public class CustomEasedNormal : CustomNormal
	{

		[SerializeField]
		public EasingMethodSlot easing = new EasingMethodSlot();


		protected override float NormalToValue()
		{
			return Mathf.Lerp(a: range.min,
			                  b: range.max,
			                  t: easing.method(_normal));
		}

	}

	// -------------------------------------------------------------------------------------------------- Sin Module //

	[Serializable]
	public class VanillaSinModule : VanillaClass
	{

		public float rate  = 1.0f;
		public float scale = 1.0f;

		[ReadOnly]
		public float sin;
		public float timeOffset;
		public float valueOffset;


		public float Update()
		{
			return sin = ( valueOffset + Mathf.Sin(( Time.time + timeOffset ) * rate) ) * scale;
		}


		public float Update(float time)
		{
			return sin = ( valueOffset + Mathf.Sin(( time + timeOffset ) * rate) ) * scale;
		}


		public override void Reset() { }

		public override void Validate<V>(V behaviour) { }

	}

	// ------------------------------------------------------------------------------------------------ Timer Module //

	[Serializable]
	public abstract class VanillaTimer : VanillaClass
	{

		public float currentTime;

		public float timerMax = 10.0f;


		public void Initialize()
		{
			currentTime = timerMax;
		}


		public bool HasElapsed(float delta)
		{
			if (( currentTime -= delta ).IsPositive()) return false;

			HandleElapse();

			return true;
		}


		protected abstract void HandleElapse();

	}

	[Serializable]
	public class VanillaTimerBasic : VanillaTimer
	{

		protected override void HandleElapse()
		{
			currentTime = timerMax;
		}


		public override void Validate<V>(V behaviour) { }

	}

	[Serializable]
	public class VanillaTimerWrapped : VanillaTimer
	{

		protected override void HandleElapse()
		{
			currentTime.Wrap(min: 0.0f,
			                 max: timerMax);
		}


		public override void Validate<V>(V behaviour) { }

	}

}