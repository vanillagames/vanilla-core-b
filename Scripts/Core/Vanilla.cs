﻿using System;
using System.IO;
using System.Linq;
using System.Text;

using UnityEngine;

using Debug = UnityEngine.Debug;
using Format = Vanilla.VanillaFormatting;

namespace Vanilla
{
    public static partial class Vanilla
    {
        // ----------------------------------------------------------------------------------------------- Constants //
        
        const string c_SystemInfoCulprit = "System & Device Report";
        
        // -------------------------------------------------------------------------------------------- Addressables //

        public static string ReleasePath => Debug.isDebugBuild ?
                                                "Dev" :
                                                "Live";
        
        // ------------------------------------------------------------------------------------------------ Logs //

        public static void Log(string message)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            Debug.Log($"[Vanilla] Frame [{Time.frameCount}]\n{message}");
            #endif
        }

        public static void Warning(string message)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            Debug.LogWarning($"[Vanilla] Frame [{Time.frameCount}]\n{message}");
            #endif
        }

        public static void Error(string message)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            Debug.LogError($"[Vanilla] Frame [{Time.frameCount}]\n{message}");
            #endif
        }

        public static void Log
        (
            string culprit,
            string message)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            Debug.Log($"[{culprit}] Frame [{Time.frameCount}]\n{message}");
            #endif
        }

        public static void Warning
        (
            string culprit,
            string message)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            Debug.LogWarning($"[{culprit}] Frame [{Time.frameCount}]\n{message}");
            #endif
        }

        public static void Error
        (
            string culprit,
            string message)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            Debug.LogError($"[{culprit}] Frame [{Time.frameCount}]\n{message}");
            #endif
        }


        public static void LogSystemInfo
        (
            StringBuilder sb)
        {
            sb.Append($"\n");

            Log(culprit: c_SystemInfoCulprit,
                message: sb.ToString());
        }
        
        // Device & System Info

        public static StringBuilder AppendSystemInfoForApplication(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            sb.Append($"\n[ Application ]\n\n");

            sb.Append($"Product name          [{Application.productName}]\n");
            sb.Append($"Company name          [{Application.companyName}]\n");
            sb.Append($"Version               [{Application.version}]\n");
            sb.Append($"Platform              [{Application.platform}]\n");
            sb.Append($"Can confirm genuinity [{Application.genuineCheckAvailable}]\n");
            if (Application.genuineCheckAvailable) 
                sb.Append($"Genuine               [{Application.genuine}]\n");
            sb.Append($"Install mode          [{Application.installMode}]\n");
            sb.Append($"Identifier            [{Application.identifier}]\n");
            sb.Append($"Made with Unity       [{Application.unityVersion}]\n");
            sb.Append($"Build Tags\n");
            Application.GetBuildTags().ToList().ForEach(t => sb.Append($"    {t}"));
            sb.Append($"Development build     [{Debug.isDebugBuild}]\n");

            return sb;
            
            #else            
            return null;
            #endif
        }

        public static StringBuilder AppendSystemInfoForDevice(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            sb.Append($"\n[ Device ]\n\n");

            sb.Append($"Name  [{SystemInfo.deviceName}]\n");
            sb.Append($"Model [{SystemInfo.deviceModel}]\n");
            sb.Append($"Type  [{SystemInfo.deviceType}]\n"  );
            sb.Append($"ID    [{SystemInfo.deviceUniqueIdentifier}]\n" );

            return sb;
            
            #else            
            return null;
            #endif
        }
        
        public static StringBuilder AppendSystemInfoForOperatingSystem(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            sb.Append($"\n[ Operating System ]\n\n" );

            sb.Append($"Name   [{SystemInfo.operatingSystem}]\n"       );
            sb.Append($"Family [{SystemInfo.operatingSystemFamily}]\n" );
         
            return sb;

            #else            
            return null;
            #endif
        }
        
        public static StringBuilder AppendSystemInfoForProcessors(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            
            sb.Append($"\n[ Processor/s ]\n\n" );

            sb.Append($"Type         [{SystemInfo.processorType}]\n"      );
            sb.Append($"Speed in MHz [{(SystemInfo.processorFrequency * Format.MHz).FormatAsFrequency()}]\n" );
            sb.Append($"Count        [{SystemInfo.processorCount}]\n"     );

            return sb;

            #else            
            return null;
            #endif
        }
        
        public static StringBuilder AppendSystemInfoForMemory(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            
            sb.Append($"\n[ Memory ]\n\n" );

            sb.Append($"System Memory [{(SystemInfo.systemMemorySize * Format.Mb).FormatAsBytes()}]\n" );

            return sb;

            #else            
            return null;
            #endif
        }
        
        public static StringBuilder AppendSystemInfoForDiskSpace(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            
            sb.Append($"\n[ Storage & Caching ]\n\n" );
            
            // Android //
            
            #if UNITY_ANDROID
            
            sb.Append($"Free disk-space [Android]   [{GetAvailableDiskSpace().FormatAsBytes()}]\n" );

            #endif
            
            // Windows //

            #if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN

            sb.Append($"Free disk-space [Windows]\n" );

            var allDrives = DriveInfo.GetDrives();

            foreach (var d in allDrives)
            {
                sb.Append($"Drive [{d.Name}]");
                sb.Append($"    Drive type:                      [{d.DriveType}]");

                if (!d.IsReady) continue;

                sb.Append($"    Volume label:                    [{d.VolumeLabel}]");
                sb.Append($"    File system:                     [{d.DriveFormat}]");
                sb.Append($"    Available space to current user: [{d.AvailableFreeSpace.FormatAsBytes()}]");
                sb.Append($"    Total available space:           [{d.TotalFreeSpace.FormatAsBytes()}]");
                sb.Append($"    Total size of drive:             [{d.TotalSize.FormatAsBytes()}]");
            }
            
            #endif
            
            // ----------------------------------------------------------------------------------------------------- //

            sb.Append($"Caching system ready        [{Caching.ready}]\n" );

            if (Caching.ready && Caching.currentCacheForWriting.valid)
            {
                sb.Append($"Cache count                 [{Caching.cacheCount}]\n");
                sb.Append($"Current cache path          [{Caching.currentCacheForWriting.path}]\n");
                sb.Append($"Current cache maximum space [{Caching.currentCacheForWriting.maximumAvailableStorageSpace.FormatAsBytes()}]\n");
                sb.Append($"Current cache read-only     [{Caching.currentCacheForWriting.readOnly}]\n");
                sb.Append($"Current cache bytes used    [{Caching.currentCacheForWriting.spaceOccupied.FormatAsBytes()}]\n");
                sb.Append($"Current cache bytes free    [{Caching.currentCacheForWriting.spaceFree.FormatAsBytes()}]\n");
            }
         
            return sb;

            #else            
            return null;
            #endif

        }

        public static StringBuilder AppendSystemInfoForBattery(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            
            sb.Append($"\n[ Power ]\n\n" );

            sb.Append($"Battery level  [{SystemInfo.batteryLevel * 100.0f}%]\n"  );
            sb.Append($"Battery status [{SystemInfo.batteryStatus}]\n" );
         
            return sb;

            #else            
            return null;
            #endif

        }
        
        public static StringBuilder AppendSystemInfoForAudioSupport(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            
            sb.Append($"\n[ Audio ]\n\n" );
         
            sb.Append($"Audio device available [{SystemInfo.supportsAudio}]\n" );
            
            return sb;

            #else            
            return null;
            #endif

        }
        
        public static StringBuilder AppendSystemInfoForHardwareSupport(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR
            
            sb.Append($"\n[ Hardware features ]\n\n" );

            sb.Append($"Location Services [{SystemInfo.supportsLocationService}]\n" );
            sb.Append($"Gyroscope         [{SystemInfo.supportsGyroscope}]\n"       );
            sb.Append($"Accelerometer     [{SystemInfo.supportsAccelerometer}]\n"   );
            sb.Append($"Vibration         [{SystemInfo.supportsVibration}]\n"       );
         
            return sb;

            #else            
            return null;
            #endif

        }


        public static StringBuilder AppendSystemInfoForRenderingSupport(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            sb.Append($"\n[ Rendering ]\n\n");

            sb.Append($"Graphics Tier                            [{Graphics.activeTier}]\n");
            sb.Append($"Active color gamut                       [{Graphics.activeColorGamut}]\n");
            sb.Append($"Rendering threading mode                 [{SystemInfo.renderingThreadingMode}]\n");
            sb.Append($"Simultaneous render targets (MRTs) count [{SystemInfo.supportedRenderTargetCount}]\n");
            sb.Append($"Separated render target blending         [{SystemInfo.supportsSeparatedRenderTargetsBlend}]\n");

            return sb;

            #else            
            return null;
            #endif

        }


        public static StringBuilder AppendSystemInfoForGPUSupport(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            sb.Append($"\n[ GPU ]\n\n" );

            sb.Append($"Name                           [{SystemInfo.graphicsDeviceName}]\n" );
            sb.Append($"Type                           [{SystemInfo.graphicsDeviceType}]\n" );
            sb.Append($"Memory                         [{(SystemInfo.graphicsMemorySize * Format.Mb).FormatAsBytes()}]\n" );
            sb.Append($"ID                             [{SystemInfo.graphicsDeviceID}]\n" );
            sb.Append($"Vendor                         [{SystemInfo.graphicsDeviceVendor}]\n" );
            sb.Append($"Vendor ID                      [{SystemInfo.graphicsDeviceVendorID}]\n" );
            sb.Append($"Version                        [{SystemInfo.graphicsDeviceVersion}]\n" );
            sb.Append($"GPU multi-threaded rendering   [{SystemInfo.graphicsMultiThreaded}]\n" );
            sb.Append($"DrawCall instancing            [{SystemInfo.supportsInstancing}]\n" );
            sb.Append($"Shadows                        [{SystemInfo.supportsShadows}]\n"                );
            sb.Append($"Raw Shadow Depth sampling      [{SystemInfo.supportsRawShadowDepthSampling}]\n" );
            sb.Append($"Quad topology                  [{SystemInfo.supportsHardwareQuadTopology}]\n"   );
            sb.Append($"32-bit index buffers           [{SystemInfo.supports32bitsIndexBuffer}]\n"      );
            sb.Append($"Motion vectors                 [{SystemInfo.supportsMotionVectors}]\n"          );
            sb.Append($"Asynchronous GPU data readback [{SystemInfo.supportsAsyncGPUReadback}]\n"       );
            sb.Append($"RayTracing                     [{SystemInfo.supportsRayTracing}]\n"             );
            sb.Append($"Hidden surface removal         [{SystemInfo.hasHiddenSurfaceRemovalOnGPU}]\n"   );
            
            return sb;

            #else            
            return null;
            #endif

        }
        
        public static StringBuilder AppendSystemInfoForShaderSupport(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            sb.Append($"\n[ Shader support ]\n\n" );

            sb.Append($"Shader level                                       [{SystemInfo.graphicsShaderLevel}]\n"                             );
            sb.Append($"Geometry                                           [{SystemInfo.supportsGeometryShaders}]\n"                         );
            sb.Append($"Tessellation                                       [{SystemInfo.supportsTessellationShaders}]\n"                     );
            sb.Append($"Dynamic Uniform Array-Indexing In Fragment Shaders [{SystemInfo.hasDynamicUniformArrayIndexingInFragmentShaders}]\n" );
            sb.Append($"Support for binding constant buffers directly      [{SystemInfo.supportsSetConstantBuffer}]\n"                       );
         
            return sb;

            #else            
            return null;
            #endif

        }
        
        public static StringBuilder AppendSystemInfoForComputeSupport(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            sb.Append($"\n[ Compute ]\n\n" );

            sb.Append($"Compute support      [{SystemInfo.supportsComputeShaders}]\n" );
            sb.Append($"Async Compute queues [{SystemInfo.supportsAsyncCompute}]\n"   );
            sb.Append($"GraphicsFences       [{SystemInfo.supportsGraphicsFence}]\n"  );

            return sb;

            #else            
            return null;
            #endif

        }
        
        public static StringBuilder AppendSystemInfoForTextureSupport(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            sb.Append($"\n[ Texture support ]\n\n" );

            sb.Append($"Maximum texture size               [{SystemInfo.maxTextureSize}]\n" );
            sb.Append($"Maximum CubeMap size               [{SystemInfo.maxCubemapSize}]\n" );
            sb.Append($"Multi-sampled textures             [{SystemInfo.supportsMultisampledTextures == 1}]\n" );
            sb.Append($"Multi-sampled texture auto-resolve [{SystemInfo.supportsMultisampleAutoResolve}]\n" );
            sb.Append($"UV Starts at top                   [{SystemInfo.graphicsUVStartsAtTop}]\n" );
            sb.Append($"Sparse textures                    [{SystemInfo.supportsSparseTextures}]\n" );
            sb.Append($"Texture wrap mirror once           [{SystemInfo.supportsTextureWrapMirrorOnce == 1}]\n" );
            sb.Append($"GPU supports partial mipmap chains [{SystemInfo.hasMipMaxLevel}]\n" );
            sb.Append($"Streaming of texture mip maps      [{SystemInfo.supportsMipStreaming}]\n"           );
            sb.Append($"2D Array textures                  [{SystemInfo.supports2DArrayTextures}]\n"        );
            sb.Append($"3D (volume) textures               [{SystemInfo.supports3DTextures}]\n"             );
            sb.Append($"3D (volume) RenderTextures         [{SystemInfo.supports3DRenderTextures}]\n"       );
            sb.Append($"CubeMap Array textures             [{SystemInfo.supportsCubemapArrayTextures}]\n"   );
            sb.Append($"NPOT (Nearest power-of-two)        [{SystemInfo.npotSupport}]\n"                    );

            return sb;

            #else            
            return null;
            #endif

        }
        
        public static StringBuilder AppendSystemInfoForMisc(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            sb.Append($"\n[ Misc ]\n\n" );

            sb.Append($"Random write target count       [{SystemInfo.supportedRandomWriteTargetCount}]\n" );
            sb.Append($"Reversed Z buffer               [{SystemInfo.usesReversedZBuffer}]\n" );
            sb.Append($"RenderBuffer Load/Store actions [{SystemInfo.usesLoadStoreActions}]\n" );

            return sb;

            #else            
            return null;
            #endif

        }


        public static StringBuilder AppendSystemInfoForAll(StringBuilder sb)
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            AppendSystemInfoForApplication(sb);
            AppendSystemInfoForDevice(sb);
            AppendSystemInfoForOperatingSystem(sb);
            AppendSystemInfoForProcessors(sb);
            AppendSystemInfoForMemory(sb);
            AppendSystemInfoForDiskSpace(sb);
            AppendSystemInfoForBattery(sb);
            AppendSystemInfoForAudioSupport(sb);
            AppendSystemInfoForHardwareSupport(sb);
            AppendSystemInfoForRenderingSupport(sb);
            AppendSystemInfoForGPUSupport(sb);
            AppendSystemInfoForShaderSupport(sb);
            AppendSystemInfoForComputeSupport(sb);
            AppendSystemInfoForTextureSupport(sb);
            AppendSystemInfoForMisc(sb);

            return sb;

            #else            
            return null;
            #endif

        }


        public static void LogSystemInfoForAll()
        {
            #if DEVELOPMENT_BUILD || UNITY_EDITOR

            var sb = new StringBuilder(2048);

            AppendSystemInfoForAll(sb);
            
            LogSystemInfo(sb);
            
            #endif
        }
        
        // ----------------------------------------------------------------------------------------- Platform Checks //

        
        public static long GetAvailableDiskSpace()
        {
            #if UNITY_EDITOR
                return 1000000000000000000;
            #elif UNITY_ANDROID
                var javaClass = new AndroidJavaClass(className: "android.os.Environment");
                var file      = javaClass.CallStatic<AndroidJavaObject>(methodName: "getDataDirectory");
                var path      = file.Call<string>(methodName: "getAbsolutePath");
    
                var stat = new AndroidJavaObject(className: "android.os.StatFs",
                                                 path);
    
                var blocks    = stat.Call<long>(methodName: "getAvailableBlocksLong");
                var blockSize = stat.Call<long>(methodName: "getBlockSizeLong");
    
                return blocks * blockSize;
            #elif UNITY_IOS
                return ???
            #endif
        }


        // ----------------------------------------------------------------------------------------- Null-checks //

        // Objects //

        public static bool SilentNullCheck<T>(T thing)
        {
            return ReferenceEquals(objA: thing, 
                                   objB: null);
        }

        public static bool SoftNullCheck<T>(T thing)
        {
            if (!ReferenceEquals(objA: thing, 
                                 objB: null)) return false;

            Warning("Null detected!");

            return true;
        }

        public static bool HardNullCheck<T>(T thing)
        {
            if (!ReferenceEquals(objA: thing, 
                                 objB: null)) return false;

            Error("Null detected!");

            return true;
        }

        public static bool SoftNullCheck<T>
        (
            string culprit,
            T      thing)
        {
            if (!ReferenceEquals(objA: thing, objB: null)) return false;

            Warning(culprit: culprit, 
                    message: "Null detected!");

            return true;
        }

        public static bool HardNullCheck<T>
        (
            string culprit,
            T      thing)
        {
            if (!ReferenceEquals(objA: thing, 
                                 objB: null)) return false;

            Error(culprit: culprit, 
                  message: "Null detected!");

            return true;
        }
    }
}