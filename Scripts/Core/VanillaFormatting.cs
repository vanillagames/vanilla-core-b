﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

using UnityEngine;

namespace Vanilla
{

	public static class VanillaFormatting
	{
		
		// ---------------------------------------------------------------------------------------------------- Time //


		public static string FormatAsTimeFromHours
		(
			this int hours,
			bool     verbose = true) =>
			FormatAsTimeFromMilliseconds(milliseconds: hours * 360000,
			                             verbose: verbose);


		public static string FormatAsTimeFromMinutes
		(
			this int minutes,
			bool     verbose = true) =>
			FormatAsTimeFromMilliseconds(milliseconds: minutes * 6000,
			                             verbose: verbose);


		public static string FormatAsTimeFromSeconds
		(
			this int seconds,
			bool     verbose = true) =>
			FormatAsTimeFromMilliseconds(milliseconds: seconds * 1000,
			                             verbose: verbose);


		public static string FormatAsTimeFromMilliseconds
		(
			this long milliseconds,
			bool      verbose = true)
		{
			if (milliseconds == 0) return "0";

			if (verbose)
			{
				// If we're only talking about milliseconds, spit out a basic string early on.
				if (milliseconds < 1000 && milliseconds > -1000)
					return milliseconds == 1  ? "1 millisecond" :
					       milliseconds == -1 ? "-1 millisecond" :
					                            $"{milliseconds} milliseconds";
			}
			else
			{
				// If we're only talking about milliseconds, spit out a basic string early on.
				if (milliseconds < 1000 && milliseconds > -1000) return $"{milliseconds}ms";
			}

			// Oh wise guy eh...

			var sb = new StringBuilder(capacity: 64);

			if (milliseconds < 0)
			{
				sb.Append("-");

				milliseconds = -milliseconds;
			}

			var t = TimeSpan.FromMilliseconds(milliseconds);

			if (verbose)
			{
				if (t.Days > 0)
					sb.Append(t.Days > 1 ?
						          $"{t.Days} days, " :
						          "1 day, ");

				if (t.Hours > 0)
					sb.Append(t.Hours > 1 ?
						          $"{t.Hours} hours, " :
						          "1 hour, ");

				if (t.Minutes > 0)
					sb.Append(t.Minutes > 1 ?
						          $"{t.Minutes} minutes, " :
						          "1 minute, ");

				if (t.Seconds > 0)
					sb.Append(t.Seconds > 1 ?
						          $"{t.Seconds} seconds, " :
						          "1 second, ");

				sb.Remove(startIndex: sb.Length - 2,
				          length: 2);
			}
			else
			{
				if (t.Days    > 0) sb.Append($"{t.Days}d ");
				if (t.Hours   > 0) sb.Append($"{t.Hours}h ");
				if (t.Minutes > 0) sb.Append($"{t.Minutes}m ");
				if (t.Seconds > 0) sb.Append($"{t.Seconds}s ");

				sb.Remove(startIndex: sb.Length - 1,
				          length: 1);
			}

			return sb.ToString();
		}


		// --------------------------------------------------------------------------------------------------- Bytes //
		
		public const long Kb = 1  * 1024;
		public const long Mb = Kb * 1024;
		public const long Gb = Mb * 1024;
		public const long Tb = Gb * 1024;
		public const long Pb = Tb * 1024;
		public const long Eb = Pb * 1024;


		public static string FormatAsBytes
		(
			this long bytes)
		{
			if (bytes <  Kb) 			   return $"{bytes:0.##}b";
			if (bytes >= Kb && bytes < Mb) return $"{(double) bytes / Kb:0.##}Kb";
			if (bytes >= Mb && bytes < Gb) return $"{(double) bytes / Mb:0.##}Mb";
			if (bytes >= Gb && bytes < Tb) return $"{(double) bytes / Gb:0.##}Gb";
			if (bytes >= Tb && bytes < Pb) return $"{(double) bytes / Tb:0.##}Tb";
			if (bytes >= Pb && bytes < Eb) return $"{(double) bytes / Pb:0.##}Pb";
			if (bytes >= Eb) 			   return $"{(double) bytes / Eb:0.##}Eb";

			return "a whole bunch";
		}


		// --------------------------------------------------------------------------------------------------- Hertz //

		public const long kHz = 1000;
		public const long MHz = kHz * 1000;
		public const long GHz = MHz * 1000;
		public const long THz = GHz * 1000;
		public const long PHz = THz * 1000;
		public const long EHz = PHz * 1000;
		
		public static string FormatAsFrequency
		(
			this long hertz)
		{
			var negative = hertz < 0;

			if (negative) hertz = -hertz;
			
			if (hertz <  kHz) 				 return $"{(negative ? "-" : string.Empty)}{hertz:0.##}hz";
			if (hertz >= kHz && hertz < MHz) return $"{(negative ? "-" : string.Empty)}{(double) hertz / kHz:0.##}kHz";
			if (hertz >= MHz && hertz < GHz) return $"{(negative ? "-" : string.Empty)}{(double) hertz / MHz:0.##}MHz";
			if (hertz >= GHz && hertz < THz) return $"{(negative ? "-" : string.Empty)}{(double) hertz / GHz:0.##}GHz";
			if (hertz >= THz && hertz < PHz) return $"{(negative ? "-" : string.Empty)}{(double) hertz / THz:0.##}THz";
			if (hertz >= PHz && hertz < EHz) return $"{(negative ? "-" : string.Empty)}{(double) hertz / PHz:0.##}PHz";
			if (hertz >= EHz) 				 return $"{(negative ? "-" : string.Empty)}{(double) hertz / EHz:0.##}EHz";

			return "a whole bunch";
		}

//		// ---------------------------------------------------------------------------------------------- Encryption //
//
//
//		public static string XOREncrypt
//		(
//			this string input,
//			int         key) =>
//			new string(input.Select(c => (char) ( c ^ key )).ToArray());

	}
}