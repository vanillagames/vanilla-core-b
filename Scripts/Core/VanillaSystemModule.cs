//// Created         [ 24/01/2020 2:57:09 AM ]
//// Project         [ ManualAlignmentTest ]
//// Developed by    [ Vanilla ]
//
//using System;
//using UnityEngine;
//
//using Vanilla;
//
//namespace Vanilla.Core
//{
//	[Serializable]
//	public class VanillaSystemModule<T> : VanillaSingleton<T> where T : VanillaBehaviour
//	{
//		// ---------------------------------------------------------------------------------------------- Subclasses //
//		
//		#region Subclasses
//		
//		
//		
//		#endregion
//		
//		// ----------------------------------------------------------------------------------------------- Variables //
//		
//		#region Variables
//		
//		
//		
//		#endregion
//
//		// ---------------------------------------------------------------------------------------------- Validation //
//		
//		#region Validation
//		
//		
//		
//		#endregion
//		
//		// ------------------------------------------------------------------------------------------ Initialization //
//
//		#region Initialization
//
//		public override void Awake()
//		{
//			base.Awake();
//			
//			DontDestroyOnLoad(gameObject);
//		}
//
//		#endregion
//		
//		// ----------------------------------------------------------------------------------------------- Listeners //
//		
//		#region Listeners
//		
//		
//		
//		#endregion
//				
//		// -------------------------------------------------------------------------------------------- Update Loops //
//		
//		#region Update Loops
//		
//		
//		
//		#endregion
//				
//		// ------------------------------------------------------------------------------------------------- Helpers //
//		
//		#region Helpers
//		
//		
//		
//		#endregion
//		
//		// ----------------------------------------------------------------------------------------------- Overrides //
//
//		#region Overrides
//		
//		
//		
//		#endregion
//		
//	}
//}