﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using UnityEngine.Events;

namespace Vanilla
{
    [Serializable]
    public class GenericEvent<T> : UnityEvent<T> { }

    [Serializable]
    public class GenericEvent<T0, T1> : UnityEvent<T0, T1> { }

    [Serializable]
    public class GenericEvent<T0, T1, T2> : UnityEvent<T0, T1, T2> { }

    [Serializable]
    public class BoolEvent : UnityEvent<bool> { }

    [Serializable]
    public class FloatEvent : UnityEvent<float> { }

    [Serializable]
    public class IntEvent : UnityEvent<int> { }

    [Serializable]
    public class Vector2Event : UnityEvent<Vector2> { }

    [Serializable]
    public class Vector3Event : UnityEvent<Vector3> { }

    [Serializable]
    public class TransformEvent : UnityEvent<Transform> { }

    [Serializable]
    public class GameObjectEvent : UnityEvent<GameObject> { }
}