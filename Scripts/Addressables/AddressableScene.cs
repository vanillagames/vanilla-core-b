﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace Vanilla.AddressableAssets
{

	[Serializable]
	public class AddressableScene : Addressable
	{

		// ---------------------------------------------------------------------------------------- Load Scene Async //


		///  <summary>
		///  	This asynchronous call will load the asset associated with this asset reference.
		///  </summary>
		///   
		///  <param name="successCallback">
		///  	What method should be called if the process succeeds? Can be null!
		///  </param>
		///   
		///  <param name="failCallback">
		///  	What method should be called if the process fails? Can be null!
		///  </param>
		/// 
		///  <param name="loadSceneMode">
		/// 		Should this scene be loaded in addition to the current scenes or should it replace them entirely?
		///  </param>
		/// 
		///  <param name="activateOnLoad">
		///		When this scene has finished loading, should it become the 'active' scene?
		/// 	Unity nominates one scene at a time as the 'active' scene; this scene is where any new
		/// 	GameObject instantiations occur and also dictates which lighting settings are in effect.
		/// </param>
		///
		/// <param name="priority">
		///		I have no idea what this does and theres no documentation on it.
		/// 	Something about AsyncOperation priority.
		/// 	It defaults to 100, maybe don't touch it.
		/// </param>
		public void LoadScene
		(
			UnityAction   successCallback = default,
			UnityAction   failCallback    = default,
			LoadSceneMode loadSceneMode   = LoadSceneMode.Additive,
			bool          activateOnLoad  = false,
			int           priority        = 100)
		{
			if (ReferenceKeyIsInvalid()) return;

			// When is an AssetReference valid for a scene..?
			// For that matter... what happens if you just call LoadAssetAsync by itself?

//		    if (assetRef.IsValid())
//		    {
//			    #if UNITY_EDITOR || DEVELOPMENT_BUILD
//			    Debug.LogWarning("This asset is already loaded in memory.");
//			    #endif
//
//			    successCallback?.Invoke();
//
//			    return;
//		    }

			assetRef.LoadSceneAsync(loadMode: loadSceneMode,
			                        activateOnLoad: activateOnLoad,
			                        priority: priority)
			        .Completed += HandleCompletion;

			void HandleCompletion
			(
				AsyncOperationHandle<SceneInstance> handle)
			{
				handle.Completed -= HandleCompletion;

				switch (handle.Status)
				{
					case AsyncOperationStatus.Succeeded:
						successCallback?.Invoke();

						break;

					case AsyncOperationStatus.Failed:
						Debug.LogException(handle.OperationException);
						failCallback?.Invoke();

						break;
				}

				// Does this actually become true?
				UpdateLoadedState();

				// Does this work?
				// Can you call LoadSceneAsync without having called Load or DownloadDependencies first?
				UpdateDownloadSizeAsync(successCallback: null,
				                        failCallback: null);
			}
		}


		// -------------------------------------------------------------------------------------- Unload Scene Async //


		public void UnloadScene
		(
			UnityAction successCallback = default,
			UnityAction failCallback    = default)
		{
			if (ReferenceKeyIsInvalid()) return;

			assetRef.UnLoadScene().Completed += HandleCompletion;

			void HandleCompletion
			(
				AsyncOperationHandle<SceneInstance> handle)
			{
				handle.Completed -= HandleCompletion;

				switch (handle.Status)
				{
					case AsyncOperationStatus.Succeeded:
						successCallback?.Invoke();

						break;

					case AsyncOperationStatus.Failed:
						Debug.LogException(handle.OperationException);
						failCallback?.Invoke();

						break;
				}

				// Does this actually become true?
				UpdateLoadedState();

				// Does this work?
				// Can you call LoadSceneAsync without having called Load or DownloadDependencies first?
				UpdateDownloadSizeAsync(successCallback: null,
				                        failCallback: null);
			}
		}

	}

}