﻿using System;

using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;

using Object = UnityEngine.Object;

namespace Vanilla.AddressableAssets
{

	[Serializable]
	public class AddressableObject : Addressable
	{

		// ---------------------------------------------------------------------------------------------------- Load //

		/// <summary>
		/// 	This asynchronous call will load the asset associated with this asset reference.
		/// </summary>
		/// 
		/// <param name="successCallback">
		///		What method should be called if the process succeeds? Can be null!
		/// </param>
		/// 
		/// <param name="failCallback">
		///		What method should be called if the process fails? Can be null!
		/// </param>
		public void Load<TAssetType>
		(
			UnityAction<TAssetType> successCallback = default,
			UnityAction failCallback    = default)
			where TAssetType : Object
		{
			if (ReferenceKeyIsInvalid()) return;

			if (assetRef.IsValid())
			{
				#if UNITY_EDITOR || DEVELOPMENT_BUILD
				Debug.LogWarning("This asset is already loaded in memory.");
				#endif

				successCallback?.Invoke(assetRef.Asset as TAssetType);

				return;
			}

			assetRef.LoadAssetAsync<TAssetType>().Completed += HandleCompletion;

			void HandleCompletion
			(
				AsyncOperationHandle<TAssetType> handle)
			{
				handle.Completed -= HandleCompletion;

				switch (handle.Status)
				{
					case AsyncOperationStatus.Succeeded:
						successCallback?.Invoke(assetRef.Asset as TAssetType);
						break;

					case AsyncOperationStatus.Failed:
						Debug.LogException(handle.OperationException);
						failCallback?.Invoke();
						break;
				}

				UpdateLoadedState();
				
				UpdateDownloadSizeAsync(successCallback: null,
				                        failCallback: null);
			}
		}


		/// <summary>
		/// 	This asynchronous call will load the asset associated with this asset reference.
		/// </summary>
		/// 
		/// <param name="progressCallback">
		///		What method should be called every frame the progress of this task is updated? Can be null!
		/// </param>
		/// 
		/// <param name="successCallback">
		///		What method should be called if the process succeeds? Can be null!
		/// </param>
		/// 
		/// <param name="failCallback">
		///		What method should be called if the process fails? Can be null!
		/// </param>
		public async void Load<TAssetType>
		(
			UnityAction<float> progressCallback              = default,
			UnityAction        successCallback               = default,
			UnityAction        failCallback                  = default)
			where TAssetType : Object
		{
			if (ReferenceKeyIsInvalid()) return;

			// At the time of writing, IsValid essentially translates to IsLoaded.
			// It is only true if the handle has had LoadAssetAsync completed.
			// AssetReference.IsValid also funnels directly through to the managed internal handles IsValid value.
			if (assetRef.IsValid())
			{
				#if UNITY_EDITOR || DEVELOPMENT_BUILD
				Debug.LogWarning("This asset is already loaded in memory.");
				#endif

				progressCallback?.Invoke(1.0f);

				successCallback?.Invoke();

				return;
			}

			var handle = assetRef.LoadAssetAsync<TAssetType>();
			
			// For whatever magical reasons, anything that happens before Task.Yield() will be executed
			// twice only on the first frame the method is called. This can lead to crucial inaccuracies
			// or unexpected behaviour and should be avoided completely. Please ensure await Task.Yield()
			// is called before anything else important in the loop.

			// More information about this issue can be found here:
			// https://gametorrahod.com/unity-and-async-await/

			while (!handle.IsDone)
			{
				await Task.Yield();

				progressCallback?.Invoke(handle.PercentComplete);
			}

			progressCallback?.Invoke(1.0f);

			switch (handle.Status)
			{
				case AsyncOperationStatus.Succeeded:
					successCallback?.Invoke();
					break;

				case AsyncOperationStatus.Failed:
					Debug.LogException(handle.OperationException);
					failCallback?.Invoke();
					break;
			}

			UpdateLoadedState();

			UpdateDownloadSizeAsync(successCallback: null,
			                        failCallback: null);
		}

		// -------------------------------------------------------------------------------------------------- Unload //

		/// <summary>
		/// 	This asynchronous call will unload the asset from memory.
		/// </summary>
		/// 
		/// <param name="successCallback">
		///		What method should be called if the process succeeds? Can be null!
		/// </param>
		/// 
		/// <param name="failCallback">
		///		What method should be called if the process fails? Can be null!
		/// </param>
		public virtual void Unload
		(
			UnityAction successCallback = default,
			UnityAction failCallback    = default)
		{
			if (ReferenceKeyIsInvalid()) return;

			if (!assetRef.IsValid())
			{
				#if UNITY_EDITOR || DEVELOPMENT_BUILD
				Debug.LogWarning("This asset is not loaded to begin with.");
				#endif

				failCallback?.Invoke();

				return;
			}
			
			assetRef.ReleaseAsset();

			UpdateLoadedState();

			successCallback?.Invoke();
		}
		
	}

}