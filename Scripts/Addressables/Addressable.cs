﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

namespace Vanilla.AddressableAssets
{

	[Serializable]
	public class Addressable
	{

		// Although it is possible to have strongly typed AssetReferences like below:
		
		// public AssetReferenceT<SomeType> assetRef;
		
		// They seem to be more trouble than they're worth and only serve to help prevent
		// incorrect asset assignment. Outside of that, they also present a lot of double-handling
		// since their type-specific calls need to be explicitly stated every time they're each use.
		
		public AssetReference assetRef;

		[SerializeField]
		private bool _IsLoaded;
		public bool IsLoaded
		{
			get => _IsLoaded;
			protected set
			{
				if (_IsLoaded == value) return;

				const string AssetLoadedMessage = "This asset was just loaded into memory!";
				const string AssetUnloadedMessage = "This asset was just unloaded from memory!";

				#if UNITY_EDITOR || DEVELOPMENT_BUILD
				Debug.Log(value ?
					          AssetLoadedMessage :
					          AssetUnloadedMessage);
				#endif

				_IsLoaded = value;
			}
		}
		

		[SerializeField]
		private long _DownloadSize = -1;
		public long DownloadSize
		{
			get => _DownloadSize;
			protected set
			{
				_DownloadSize = value;
				
				// -1 is used to signify that DownloadSize has not yet been checked and a
				// result of 0 bytes means no downloading is required.
				Downloadable = value > 0;
			}
		}


		[SerializeField]
		private bool _Downloadable = false;
		public bool Downloadable
		{
			get => _Downloadable;
			private set => _Downloadable = value;
		}


		[SerializeField]
		private IList<IResourceLocation> _Locations = null;
		public IList<IResourceLocation> Locations
		{
			get => _Locations;
			protected set => _Locations = value;
		}

		// ------------------------------------------------------------------------------------ Update Download Size //
		
		/// <summary>
		/// 	This asynchronous call will fetch the total number of bytes needed to fully download this asset.
		/// </summary>
		/// 
		/// <param name="successCallback">
		///		What method should be called if the process succeeds? Can be null!
		/// </param>
		/// 
		/// <param name="failCallback">
		///		What method should be called if the process fails? Can be null!
		/// </param>
		public void UpdateDownloadSizeAsync
		(
			UnityAction<long>  successCallback               = default,
			UnityAction        failCallback                  = default)
		{
			if (ReferenceKeyIsInvalid()) return;

			Addressables.GetDownloadSizeAsync(assetRef).Completed += HandleCompletion;

			void HandleCompletion
			(
				AsyncOperationHandle<long> handle)
			{
				handle.Completed -= HandleCompletion;
				
				switch (handle.Status)
				{
					case AsyncOperationStatus.Succeeded:
						DownloadSize = handle.Result;
						successCallback?.Invoke(DownloadSize);
						break;

					case AsyncOperationStatus.Failed:
						DownloadSize = -1;
						Debug.LogException(handle.OperationException);
						failCallback?.Invoke();
						break;
				}

				Addressables.Release(handle);
			}
			
		}

		// ---------------------------------------------------------------------------------- Get Resource Locations //

		/// <summary>
		/// 	This asynchronous call will fetch the resource location that can be used to retrieve this asset.
		/// </summary>
		/// 
		/// <param name="progressCallback">
		///		What method should be called every frame the progress of this task is updated? Can be null!
		/// </param>
		/// 
		/// <param name="successCallback">
		///		What method should be called if the process succeeds? Can be null!
		/// </param>
		///
		/// <param name="onlyInvokeProgressIfDifferent">
		///		Should the progressCallback method be invoked only if the progress value has changed? This kind
		/// 	of callback is typically used for loading animations; some benefit from only hearing about changes
		/// 	while others expect to be ran every frame the task is running.
		/// </param>
		public void GetLocations
		(
			UnityAction<IList<IResourceLocation>> successCallback = default)
		{
			if (ReferenceKeyIsInvalid()) return;

			// Hypothetically, if this were to change at run-time, it wouldn't be updated.
			// In the name of futuristic crazy asset access options (runtime asset updates!)
			// ...lets disable it for now.
			
//			if (Locations != null)
//			{
//				#if UNITY_EDITOR || DEVELOPMENT_BUILD
//				Debug.LogWarning("The download size for this asset reference has already been fetched");
//				#endif
//
//				successCallback?.Invoke(Locations);
//
//				return;
//			}

			Addressables.LoadResourceLocationsAsync(assetRef).Completed += HandleCompletion;

			void HandleCompletion
			(
				AsyncOperationHandle<IList<IResourceLocation>> handle)
			{
				handle.Completed -= HandleCompletion;

				// As of Addressables 1.9:
				// 'LoadResourceLocationsAsync will always return success, with a valid IList of results.
				// If nothing matches keys, IList will be empty.'

				Locations = handle.Result;

				Addressables.Release(handle);

				successCallback?.Invoke(Locations);
			}
		}

		// ----------------------------------------------------------------------------------- Download Dependencies //


		/// <summary>
		/// 	This asynchronous call will download all assets and their respective bundles required to load
		/// 	this asset.
		/// </summary>
		/// 
		/// <param name="progressCallback">
		///		What method should be called every frame the progress of this task is updated? Can be null!
		/// </param>
		/// 
		/// <param name="successCallback">
		///		What method should be called if the process succeeds? Can be null!
		/// </param>
		/// 
		/// <param name="failCallback">
		///		What method should be called if the process fails? Can be null!
		/// </param>
		///
		/// <param name="onlyInvokeProgressIfDifferent">
		///		Should the progressCallback method be invoked only if the progress value has changed? This kind
		/// 	of callback is typically used for loading animations; some benefit from only hearing about changes
		/// 	while others expect to be ran every frame the task is running.
		/// </param>
		public async void DownloadDependencies
		(
			UnityAction<float> progressCallback              = default,
			UnityAction        successCallback               = default,
			UnityAction        failCallback                  = default,
			bool               onlyInvokeProgressIfDifferent = false)
		{
			if (ReferenceKeyIsInvalid()) return;

			if (!Downloadable)
			{
				#if UNITY_EDITOR || DEVELOPMENT_BUILD
				Debug.LogWarning(
					"This asset is not downloadable. Either it's download size has not yet been assessed or it has and no further download is required.");
				#endif

				progressCallback?.Invoke(1.0f);

				successCallback?.Invoke();

				return;
			}

			var handle = Addressables.DownloadDependenciesAsync(assetRef);

			// For whatever magical reasons, anything that happens before Task.Yield() will be executed
			// twice only on the first frame the method is called. This can lead to crucial inaccuracies
			// or unexpected behaviour and should be avoided completely. Please ensure await Task.Yield()
			// is called before anything else important in the loop.

			// More information about this issue can be found here:
			// https://gametorrahod.com/unity-and-async-await/

			if (onlyInvokeProgressIfDifferent)
			{
				var completion = 0.0f;

				while (!handle.IsDone)
				{
					await Task.Yield();

					var newCompletion = handle.PercentComplete;

					if (newCompletion > completion)
					{
						completion = newCompletion;

						progressCallback?.Invoke(newCompletion);
					}
				}
			}
			else
			{
				while (!handle.IsDone)
				{
					await Task.Yield();

					progressCallback?.Invoke(handle.PercentComplete);
				}
			}

			progressCallback?.Invoke(1.0f);

			switch (handle.Status)
			{
				case AsyncOperationStatus.Succeeded:
					successCallback?.Invoke();
					break;

				case AsyncOperationStatus.Failed:
					Debug.LogException(handle.OperationException);
					failCallback?.Invoke();
					break;
			}

			Addressables.Release(handle);

			UpdateDownloadSizeAsync(successCallback: null,
			                        failCallback: null);
		}

		// -------------------------------------------------------------------------------------------- Clear Cached //

		/// <summary>
		/// 	Clears any cache data associated with this asset.
		///
		/// 	Caution! Calling this function before loading the asset will cause an error and there
		/// 	does not appear to be a way to check for this condition ahead of time currently.
		/// </summary>
		public void ClearCache()
		{
			if (assetRef.IsValid())
			{
				Debug.LogWarning("Unable to clear this assets cached data while asset is still loaded.");

				return;
			}

			Addressables.ClearDependencyCacheAsync(assetRef);

			UpdateDownloadSizeAsync(successCallback: null,
			                        failCallback: null);
		}

		// --------------------------------------------------------------------------------------------- Validations //

		protected bool ReferenceKeyIsInvalid()
		{
			if (assetRef.RuntimeKeyIsValid()) return false;

			Debug.LogWarning("This AssetReference's key is invalid.");

			return true;
		}

		protected void UpdateLoadedState()
		{
			IsLoaded = assetRef.IsValid();
		}
		
		// ---------------------------------------------------------------------------------------------- Converters //


		public string BytesToISUFormat
		(
			long size)
		{
			const long Kb = 1  * 1024;
			const long Mb = Kb * 1024;
			const long Gb = Mb * 1024;
			const long Tb = Gb * 1024;
			const long Pb = Tb * 1024;
			const long Eb = Pb * 1024;

			if (size < Kb) return size.ToString(format: "0.##") + " byte";

			if (size >= Kb &&
			    size < Mb)
				return ( (double) size / Kb ).ToString(format: "0.##") + " Kb";

			if (size >= Mb &&
			    size < Gb)
				return ( (double) size / Mb ).ToString(format: "0.##") + " Mb";

			if (size >= Gb &&
			    size < Tb)
				return ( (double) size / Gb ).ToString(format: "0.##") + " Gb";

			if (size >= Tb &&
			    size < Pb)
				return ( (double) size / Tb ).ToString(format: "0.##") + " Tb";

			if (size >= Pb &&
			    size < Eb)
				return ( (double) size / Pb ).ToString(format: "0.##") + " Pb";

			if (size >= Eb) return ( (double) size / Eb ).ToString(format: "0.##") + " Eb";

			return "a lot";
		}


		public string GetFormattedDownloadSize
		()
		{
			const string c_unknownDownloadSize = "Unknown";

			return DownloadSize == -1 ?
				       c_unknownDownloadSize :
				       BytesToISUFormat(DownloadSize);
		}

	}

}