﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Vanilla.AddressableAssets
{

	[Serializable]
	public class AddressableGameObject : AddressableObject
	{

		[SerializeField]
		private HashSet<GameObject> _instances = new HashSet<GameObject>();
		public HashSet<GameObject> instances => _instances;

		// --------------------------------------------------------------------------------------------- Instantiate //

		/// <summary>
		/// 	This asynchronous call will load the asset associated with this asset reference.
		/// </summary>
		///  
		/// <param name="successCallback">
		/// 	What method should be called if the process succeeds? Can be null!
		/// </param>
		///  
		/// <param name="failCallback">
		/// 	What method should be called if the process fails? Can be null!
		/// </param>
		/// 
		/// <param name="position">
		/// 	Where in world-space should this instance be instantiated?
		/// </param>
		///
		/// <param name="rotation">
		/// 	What rotation should this instance have?
		/// </param>
		///
		/// <param name="parent">
		/// 	What transform should this GameObject be childed to?
		/// </param>
		public void Instantiate
		(
			UnityAction<GameObject> successCallback,
			UnityAction             failCallback,
			Vector3                 position = default,
			Quaternion              rotation = default,
			Transform               parent   = null)
		{
			if (ReferenceKeyIsInvalid())
			{
				failCallback?.Invoke();

				return;
			}
			
			// It is technically possible to instantiate if the asset hasn't been loaded (IsValid) before.
			// However, the instances created from doing so will not be associated with the assetRef
			// internally and thus won't be able to be released from memory properly.
			// To prevent this from happening in the first place, we only allow valid AssetReference calls.

			if (!assetRef.IsValid())
			{
				Debug.LogWarning("This asset hasn't been loaded yet.");

				failCallback?.Invoke();

				return;
			}

			assetRef.InstantiateAsync(position: position,
			                          rotation: rotation,
			                          parent: parent)
			        .Completed += HandleCompletion;

			void HandleCompletion
			(
				AsyncOperationHandle<GameObject> handle)
			{
				handle.Completed -= HandleCompletion;

				switch (handle.Status)
				{
					case AsyncOperationStatus.Succeeded:
						instances.Add(handle.Result);
						successCallback?.Invoke(handle.Result);

						break;

					case AsyncOperationStatus.Failed:
						Debug.LogException(handle.OperationException);
						failCallback?.Invoke();

						break;
				}
				
				Debug.Log($"There are now {instances.Count} instances.");

				Debug.Log($"Is this AssetRef valid? {assetRef.IsValid()}");

				if (!IsLoaded)
				{
					UpdateLoadedState();

					UpdateDownloadSizeAsync();
				}
			}
		}


		/// <summary>
		/// 	This asynchronous call will load the asset associated with this asset reference.
		/// </summary>
		///  
		/// <param name="progressCallback">
		/// 	What method should be called every frame the progress of this task is updated? Can be null!
		/// </param>
		///  
		/// <param name="successCallback">
		/// 	What method should be called if the process succeeds? Can be null!
		/// </param>
		///  
		/// <param name="failCallback">
		/// 	What method should be called if the process fails? Can be null!
		/// </param>
		/// 
		/// <param name="position">
		/// 	Where in world-space should this instance be instantiated?
		/// </param>
		///
		/// <param name="rotation">
		/// 	What rotation should this instance have?
		/// </param>
		///
		/// <param name="parent">
		/// 	What transform should this GameObject be childed to?
		/// </param>
		public virtual async void Instantiate
		(
			UnityAction<float>      progressCallback,
			UnityAction<GameObject> successCallback,
			UnityAction             failCallback,
			Vector3                 position = default,
			Quaternion              rotation = default,
			Transform               parent   = null)
		{
			if (ReferenceKeyIsInvalid())
			{
				failCallback?.Invoke();

				return;
			}

			// It is technically possible to instantiate if the asset hasn't been loaded (IsValid) before.
			// However, the instances created from doing so will not be associated with the assetRef
			// internally and thus won't be able to be released from memory properly.
			// To prevent this from happening in the first place, we only allow valid AssetReference calls.
			
			if (!assetRef.IsValid())
			{
				Debug.LogWarning("This asset hasn't been loaded yet.");

				failCallback?.Invoke();

				return;
			}
			
			var handle = assetRef.InstantiateAsync(position: position,
			                                       rotation: rotation,
			                                       parent: parent);

			// For whatever magical reasons, anything that happens before Task.Yield() will be executed
			// twice only on the first frame the method is called. This can lead to crucial inaccuracies
			// or unexpected behaviour and should be avoided completely. Please ensure await Task.Yield()
			// is called before anything else important in the loop.

			// More information about this issue can be found here:
			// https://gametorrahod.com/unity-and-async-await/

			while (!handle.IsDone)
			{
				await Task.Yield();

				progressCallback?.Invoke(handle.PercentComplete);
			}

			progressCallback?.Invoke(1.0f);

			switch (handle.Status)
			{
				case AsyncOperationStatus.Succeeded:
					instances.Add(handle.Result);
					successCallback?.Invoke(handle.Result);

					break;

				case AsyncOperationStatus.Failed:
					Debug.LogException(handle.OperationException);
					failCallback?.Invoke();

					break;
			}
			
			Debug.Log($"There are now {instances.Count} instances.");
			
			Debug.Log($"Is this AssetRef valid? {assetRef.IsValid()}");
			
			if (!IsLoaded)
			{
				Debug.Log("This happens");
				
				UpdateLoadedState();

				UpdateDownloadSizeAsync();
			}
		}


		// ------------------------------------------------------------------------------------------- Deinstantiate //

		public void Deinstantiate
		(
			GameObject instance,
			UnityAction successCallback,
			UnityAction failCallback)
		{
			if (!assetRef.IsValid())
			{
				Debug.LogError("This GameObject cannot be de-instantiated because its AssetReference isn't loaded to begin with.");

				failCallback?.Invoke();

				return;
			}

			if (instances.Count == 0)
			{
				Debug.LogError("This GameObject cannot be de-instantiated because there are no tracked instances of it. It is likely that this GameObject was not tracked properly when created, or the instances list has been modified improperly.");
				
				failCallback?.Invoke();
				
				return;
			}
			
			if (instance == null)
			{
				Debug.LogError("This GameObject cannot be de-instantiated because it is null.");

				failCallback?.Invoke();

				return;
			}
			
			if (!instances.Contains(instance))
			{
				Debug.LogError("This GameObject cannot be de-instantiated as an Addressable because it is not a tracked instance to begin with.");
				
				failCallback?.Invoke();
				
				return;
			}
			
			instances.Remove(instance);

			assetRef.ReleaseInstance(instance);
			
			Debug.Log($"There are now {instances.Count} instances.");

			successCallback?.Invoke();
		}
		
		// --------------------------------------------------------------------------------------- Deinstantiate All //


		public void DeinstantiateAll
		(
			UnityAction perInstanceSuccessCallback,
			UnityAction perInstanceFailCallback,
			UnityAction completeSuccessCallback)
		{
			while (instances.Count > 0)
			{
				Deinstantiate(instance: instances.First(),
				              successCallback: perInstanceSuccessCallback,
				              failCallback: perInstanceFailCallback);
			}
			
			completeSuccessCallback?.Invoke();
		}


		// ----------------------------------------------------------------------------------------------- Overrides //


		public override void Unload
		(
			UnityAction successCallback = default,
			UnityAction failCallback    = default)
		{
			if (instances.Count > 0)
			{
				Debug.LogWarning("Can't unload an asset reference with remaining instances! Consider using DeinstantiateAll first.");
				
				failCallback?.Invoke();
				
				return;
			}
			
			base.Unload(successCallback: successCallback,
			            failCallback: failCallback);
		}

	}

}