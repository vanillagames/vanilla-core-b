﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using Vanilla.Math;

namespace Vanilla.Clockwork
{
    [Serializable]
    public abstract class VanillaMotor<T0, T1, T2> : VanillaSingleton<T1>
        where T0 : VanillaGear<T0, T1, T2>
        where T1 : VanillaMotor<T0, T1, T2>
        where T2 : ProtectedList<T0>, new()
    {
        [Header("Controls")] 
//        [SerializeField]
//        private bool atStart;
        
        public bool running = true;
        
        public float speed = 1.0f;

        protected float _timeDelta;
        public virtual float timeDelta
        {
            get => _timeDelta;
            protected set => _timeDelta = value;
        }

        public bool stopMotorOnNoActiveGears = false;

        [Header("Gears")]
        [SerializeField]
        public T2 defaultGears = new T2();
        
        [SerializeField]
        public T2 activeGears = new T2();

        [Header("Settings")] 
        [Tooltip("If true, when a gear passes control forwards or backwards, it will also automatically be added to the new gears corresponding next/previous gear list. For example, if Gear A passes control forward to Gear B, A will automatically be added to Gear B's 'exit backwards' gear list.")]
        public bool autoFlow;
        
        [Header("Events")]
        public UnityEvent onMotorStart = new UnityEvent();
        public UnityEvent onMotorRestart = new UnityEvent();
        public UnityEvent onMotorStop = new UnityEvent();
        
        public UnityEvent onEnd = new UnityEvent();

        public void Awake()
        {
            Reset();
        }

        void Start()
        {
            StartMotor();
        }
        
        public override void Reset()
        {
//            if (atStart) return;
            
            running = false;
            
//            atStart = true;

            var timePositive = speed.IsPositive();
            
            while (activeGears.contents.Count > 0)
            {
                activeGears.contents[0].Handle_Deactivation(this as T1, timePositive);
            }

            activeGears.Clear();

            onMotorRestart.Invoke();
        }

        public void StartMotor()
        {
            if (running) return;

            running = true;

//            if (!atStart) return;
            
            onMotorStart.Invoke();
            
            var timePositive = speed.IsPositive();

            if (timePositive)
            {
                foreach (var g in defaultGears.contents)
                {
                    g.Handle_Enter_Forwards(this as T1, timeDelta, null);
                }
            }
            else
            {
                foreach (var g in defaultGears.contents)
                {
                    g.Handle_Enter_Backwards(this as T1, timeDelta, null);
                }
            }

//            atStart = false;
        }

        public void StopMotor()
        {
            if (!running) return;

            running = false;
            
            onMotorStop.Invoke();
        }

        public void FlipMotorSpeed()
        {
            speed = -speed;
        }
        
        void Update()
        {
            if (!running) return;

            EstablishTimeDelta();

            foreach (var g in activeGears.contents)
            {
                Log($"Running gear [{g.gameObject.name}]");
            }
            
            Run();
        }

        protected virtual void EstablishTimeDelta()
        {
            timeDelta = Time.deltaTime * speed;

//            timePositive = timeDelta.IsZeroOrHigher();
        }
        
        public virtual void Run()
        {
            for (var g = 0; g < activeGears.contents.Count; g++)
            {
                activeGears.contents[g].ProcessTime(this as T1, timeDelta);
            }
        }

        protected internal virtual void GearActivated(T0 gear)
        {
            activeGears.Add(gear);
        }
        
        protected internal virtual void GearDectivated(T0 gear)
        {
            Log($"Removing gear [{gear.gameObject.name}]");
            
            activeGears.Remove(gear);

            if (stopMotorOnNoActiveGears)
            {
                if (activeGears.contents.Count < 1)
                {
                    StopMotor();
                }
            }
        }

        public void AddDefaultGear(T0 gear)
        {
            defaultGears.Add(gear);
        }
        
        public void RemoveDefaultGear(T0 gear)
        {
            defaultGears.Remove(gear);
        }
    }
}