﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Vanilla.Clockwork
{
    public class AnimationGear<T0, T1, T2> : VanillaGear<T0, T1, T2>
        where T0 : VanillaGear<T0, T1, T2>
        where T1 : VanillaMotor<T0, T1, T2>
        where T2 : ProtectedList<T0>, new()
    {
        public Animation animation;

        public AnimationClip clip;
        
        private void Awake()
        {
            animation[clip.name].weight = 1;
            animation[clip.name].speed = 0;
        }

        protected override void Run(T1 motor, bool timePositive)
        {
            animation[clip.name].normalizedTime = localNormal;
        }

        protected override void EnteredForwards(T1 motor)
        {
            animation.Play();
        }

        protected override void EnteredBackwards(T1 motor)
        {
            animation.Play();
        }

        protected override void ExitedForwards(T1 motor)
        {
//            animation.Stop();
        }

        protected override void ExitedBackwards(T1 motor)
        {
//            animation.Stop();
        }
    }
}