﻿using System;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.Events;
using Vanilla.Math;

namespace Vanilla.Clockwork
{
    public abstract class VanillaGear<T0, T1, T2> : VanillaBehaviour
        where T0 : VanillaGear<T0, T1, T2>
        where T1 : VanillaMotor<T0, T1, T2>
        where T2 : ProtectedList<T0>, new()
    {
        [Header("Status")] 
        [SerializeField, ReadOnly]
        protected internal bool active;
        
        [SerializeField]
        public float duration = 1.0f;
        
        [Tooltip("While true, all incoming Run() calls are dropped.")]
        [SerializeField, ReadOnly]
        private float _localTime;
        public float localTime
        {
            get => _localTime;
            set => _localTime = value.GetClamp(0, duration);
        }
        
        public float localNormal => (_localTime / duration).GetBetween01();

        [Header("Update Flags")]
        [Tooltip("While true, all incoming Run() calls are dropped.")]
        public bool pause = false;
        
        [Tooltip("While true, the motors timeDelta will be added to localTime.")]
        public bool processLocalTime = true;
        
        [Tooltip("While true, Run() will be executed.")]
        public bool execute = true;
        
        [Tooltip("While true, any incoming time will be inverted. " +
                 "For example, a 0.1 second delta will be treated as -0.1 seconds.")]
        [SerializeField, ReadOnly]
        private bool localTimeFlip;

        private GenericEvent<T0> _onEnterForwards;
        public GenericEvent<T0> onEnterForwards => _onEnterForwards ?? (_onEnterForwards = new GenericEvent<T0>());

        private GenericEvent<T0> _onExitForwards;
        public GenericEvent<T0> onExitForwards => _onExitForwards ?? (_onExitForwards = new GenericEvent<T0>());
        
        private GenericEvent<T0> _onEnterBackwards;
        public GenericEvent<T0> onEnterBackwards => _onEnterBackwards ?? (_onEnterBackwards = new GenericEvent<T0>());

        private GenericEvent<T0> _onExitBackwards;
        public GenericEvent<T0> onExitBackwards => _onExitBackwards ?? (_onExitBackwards = new GenericEvent<T0>());

        [Header("Enter/Exit Actions")]
        [Tooltip("What should happen when this gear is entered backwards?")]
        public ClockworkFlowAction enterBackwardsAction = ClockworkFlowAction.Continue;
        
        [Tooltip("What should happen when this gear is exited backwards?")]
        public ClockworkFlowAction exitBackwardsAction = ClockworkFlowAction.Continue;

        [Tooltip("What should happen when this gear is entered forwards?")]
        public ClockworkFlowAction enterForwardsAction = ClockworkFlowAction.Continue;
        
        [Tooltip("What should happen when this gear is exited forwards?")]
        public ClockworkFlowAction exitForwardsAction = ClockworkFlowAction.Continue;

        [Header("Connected Gears")]
        [Tooltip("If we exit backwards, which gears should control be handed to?")]
        public List<T0> backwardsGears = new List<T0>();
        
        [Tooltip("If we exit forwards, which gears should control be handed to?")]
        public List<T0> forwardsGears = new List<T0>();

        [Header("Editor Options")]
        public bool activateGameObjectWhenActive;        
        public bool selectGameObjectWhenActive;        

        // ---------------------------------------------------------------------------------------------------- Init //

        public override void Reset()
        {
            _localTime = 0;
        }

        // -------------------------------------------------------------------------------------------------- Enters //

        /// <summary>
        ///     This function will add this gear to a Motor, causing it to start running. This is the correct function
        ///     to call if you want to add a gear outside of the regular flow of time.
        /// </summary>
        public void AddToMotor(T1 motor)
        {
            if (active) return;

            if (motor.timeDelta.IsPositive())
            {
                Handle_Enter_Forwards(motor, motor.timeDelta, null);
            }
            else
            {
                Handle_Enter_Backwards(motor, motor.timeDelta, null);
            }
        }

        public void Remove(T1 motor)
        {
            if (!active) return;
            
            Handle_Deactivation(motor, motor.timeDelta.IsPositive());
        }
        
        // -------------------------------------------------------------------------------------------------- Enters //

        /// <summary>
        ///     This is used to handle entering the gear.
        /// </summary>
        protected internal bool Handle_Enter_Forwards(T1 motor, float delta, T0 fromGear)
        {
            Log("Handle_Enter_Forwards");
            
            if (active) return false;
            
            // We need to inherit the timeFlip of the gear that handed over control.
            // This bool is true whenever the local flow of time has been 'bounced' (inverted)
            // somewhere along the chain. Since the bool state is local to each gear, it needs
            // passing on.
            if (fromGear)
            {
                localTimeFlip = fromGear.localTimeFlip;

                if (motor.autoFlow)
                {
                    if (!backwardsGears.Contains(fromGear))
                    {
                        backwardsGears.Add(fromGear);
                    }
                }
            }

            switch (enterForwardsAction)
            {
                default:
                case ClockworkFlowAction.Continue:
                case ClockworkFlowAction.Loop:
                    Handle_Activation(motor, true);

                    EnteredForwards_Internal(motor);

                    return true;
                
                case ClockworkFlowAction.StopLocal:
                    return false;
                
                case ClockworkFlowAction.StopGlobal:
                    motor.StopMotor();
                    return false;
                
                case ClockworkFlowAction.Skip:
                    // If we don't do this, skipped chapters may behave unexpectedly in the following cases:
                    // - Enter doesn't occur
                    // - Exit doesn't occur
                    EnteredForwards_Internal(motor);
                    
                    Handle_Exit_Forwards(motor, delta);
                    
                    return false;
                
                case ClockworkFlowAction.BounceLocal:
                    // Bounce upon entry is a weird one...
                    // Both Handle_Enters inherit the timeFlip state of the fromGear, so we need to ensure
                    // that it gets flipped even though this gears isn't!
                    if (fromGear)
                    {
                        fromGear.Handle_Enter_Backwards(motor, -delta, this as T0);
                        
                        fromGear.localTimeFlip = true;

                        fromGear.ProcessTime(motor, delta);
                    }
                    return false;
                
                case ClockworkFlowAction.BounceGlobal:
                    motor.FlipMotorSpeed();

                    if (fromGear)
                    {
                        fromGear.Handle_Enter_Backwards(motor, -delta, this as T0);
                    }
                    return false;
                
                case ClockworkFlowAction.RestartMotor:
                    motor.Reset();
                    motor.StartMotor();
                    return false;
            }
        }

        protected internal virtual bool Handle_Enter_Backwards(T1 motor, float delta, T0 fromGear)
        {
            if (active) return false;
            
            // We need to inherit the timeFlip of the gear that handed over control.
            // This bool is true whenever the local flow of time has been 'bounced' (inverted)
            // somewhere along the chain. Since the bool state is local to each gear, it needs
            // passing on.
            if (fromGear)
            {
                localTimeFlip = fromGear.localTimeFlip;
                
                if (motor.autoFlow)
                {
                    if (!forwardsGears.Contains(fromGear))
                    {
                        forwardsGears.Add(fromGear);
                    }
                }
            }

            switch (enterBackwardsAction)
            {
                default:
                case ClockworkFlowAction.Continue:
                case ClockworkFlowAction.Loop:
                    Handle_Activation(motor, false);

                    EnteredBackwards_Internal(motor);

                    return true;
                
                case ClockworkFlowAction.StopLocal:
                    return false;
                
                case ClockworkFlowAction.StopGlobal:
                    motor.StopMotor();
                    return false;
                
                case ClockworkFlowAction.Skip:
                    // If we don't do this, skipped chapters may behave unexpectedly in the following cases:
                    // - Activation doesn't occur
                    // - Enter doesn't occur
                    // - Exit doesn't occur
                    // - Deactivation doesn't occur
                    EnteredBackwards_Internal(motor);
                    
                    Handle_Exit_Backwards(motor, delta);
                    
                    return false;
                
                case ClockworkFlowAction.BounceLocal:
                    // Bounce upon entry is a weird one...
                    // Both Handle_Enters inherit the timeFlip state of the fromGear, so we need to ensure
                    // that it gets flipped even though this gears isn't!
                    if (fromGear)
                    {
                        fromGear.Handle_Enter_Forwards(motor, -delta, this as T0);
                        
                        fromGear.localTimeFlip = false;

                        fromGear.ProcessTime(motor, delta);
                    }

                    return false;
                
                case ClockworkFlowAction.BounceGlobal:
                    motor.FlipMotorSpeed();

                    if (fromGear)
                    {
                        fromGear.Handle_Enter_Forwards(motor, -delta, this as T0);
                    }

                    return false;
                
                case ClockworkFlowAction.RestartMotor:
                    motor.Reset();
                    motor.StartMotor();
                    return false;
            }
        }

        // ----------------------------------------------------------------------------------------------------- Run //
        
        protected internal virtual void ProcessTime(T1 motor, float delta)
        {
            if (pause) return;
            
            if (HardNullCheck(o: motor)) return;
            
            var d = localTimeFlip ? -delta : delta;

            var deltaPositive = d.IsPositive();

            if (processLocalTime)
            {
                if (deltaPositive)
                {
                    // Would adding d to localTime elapse duration?
                    if (_localTime + d >= duration)
                    {
                        // Add the difference between now and the end of the gears duration to the delta
                        d += _localTime - duration;

                        Handle_Exit_Forwards(
                            motor: motor, 
                            delta: d);

                        return;
                    }

                    localTime += d;
                }
                else
                {
                    // Would adding d (which is negative) to localTime drop it below 0?
                    if (_localTime + d <= 0)
                    {
                        // Add localTime to the delta (which is negative) subtracting the difference from it
                        d += localTime;

                        Handle_Exit_Backwards(
                            motor: motor, 
                            delta: d);

                        return;
                    }

                    localTime += d;
                }
            }

            if (execute)
            {
                Run(motor: motor, 
                    timePositive: deltaPositive);
            }
        }

        protected abstract void Run(T1 motor, bool timePositive);

        // --------------------------------------------------------------------------------------------------- Exits //
        
        protected virtual void Handle_Exit_Forwards(T1 motor, float delta)
        {
            switch (exitForwardsAction)
            {
                case ClockworkFlowAction.Continue:
                    ExitedForwards_Internal(motor);

                    PassControlForwards(motor, delta);
                    
                    return;
                
                case ClockworkFlowAction.StopLocal:
                    ExitedForwards_Internal(motor);

                    return;
                
                case ClockworkFlowAction.StopGlobal:
                    ExitedForwards_Internal(motor);

                    motor.StopMotor();

                    return;
                
                case ClockworkFlowAction.BounceLocal:
                    // Turn on/off the timeflip.
                    // This bool is contagious! Its value will be copied into all following gears.
                    ToggleTimeFlip();

                    if (execute)
                    {
                        BounceOffEnd_Internal(motor, delta);

                        ProcessTime(motor, delta);
                    }

                    return;
                
                case ClockworkFlowAction.BounceGlobal:
                    motor.FlipMotorSpeed();
                    
                    if (execute)
                    {
                        // The delta isn't going to come in flipped until the next frame, so
                        // we'll flip it ourselves just for this frame.
                        BounceOffEnd_Internal(motor, -delta);

                        ProcessTime(motor, -delta);
                    }
                    
                    return;
                
                case ClockworkFlowAction.Loop:
                    if (execute)
                    {
                        LoopedAtEnd_Internal();
                    }
                    
                    _localTime = 0;
                    
                    if (execute)
                    {
                        ProcessTime(motor, delta);
                    }
                    
                    return;
                
                case ClockworkFlowAction.RestartMotor:
                    motor.Reset();
                    motor.StartMotor();
                    return;
            }
        }
        
        protected virtual void Handle_Exit_Backwards(T1 motor, float delta)
        {
            switch (exitBackwardsAction)
            {
                case ClockworkFlowAction.Continue:
                    ExitedBackwards_Internal(motor);

                    PassControlBackwards(motor, delta);
                    
                    return;
                
                case ClockworkFlowAction.StopLocal:
                    ExitedBackwards_Internal(motor);

                    return;
                
                case ClockworkFlowAction.StopGlobal:
                    ExitedBackwards_Internal(motor);
                    
                    motor.StopMotor();
                    
                    return;
                
                case ClockworkFlowAction.BounceLocal:
                    // Turn on/off the timeflip.
                    // This bool is contagious! Its value will be copied into all following gears.
                    ToggleTimeFlip();

                    if (execute)
                    {
                        BounceOffStart_Internal(motor, delta);

                        ProcessTime(motor, delta);
                    }

                    return;
                
                case ClockworkFlowAction.BounceGlobal:
                    motor.FlipMotorSpeed();
                    
                    if (execute)
                    {
                        // The delta isn't going to come in flipped until the next frame, so
                        // we'll flip it ourselves just for this frame.
                        BounceOffStart_Internal(motor, -delta);

                        ProcessTime(motor, -delta);
                    }
                    
                    return;
                
                case ClockworkFlowAction.Loop:
                    if (execute)
                    {
                        LoopedAtStart_Internal();
                    }
                    
                    _localTime = duration;
                    
                    if (execute)
                    {
                        ProcessTime(motor, -delta);
                    }
                    
                    return;
                
                case ClockworkFlowAction.RestartMotor:
                    motor.Reset();
                    motor.StartMotor();
                    return;
            }
        }
        
        // ----------------------------------------------------------------------------------------------- Internals //

        // --- Enters --- //

        private void EnteredForwards_Internal(T1 motor) 
        {
            Log("EnteredForwards_Internal");

            // If this event isn't null, invoke it.
            // This should prevent it from being new()'d unless something has
            // subscribed already thanks to the getter.
            _onEnterForwards?.Invoke(this as T0);
                    
            EnteredForwards(motor);
        }
        
        private void EnteredBackwards_Internal(T1 motor) 
        {
            Log("EnteredBackwards_Internal");

            // If this event isn't null, invoke it.
            // This should prevent it from being new()'d unless something has
            // subscribed already thanks to the getter.
            _onEnterBackwards?.Invoke(this as T0);
                    
            EnteredBackwards(motor);
        }

        // --- Exits --- //
       
        private void ExitedForwards_Internal(T1 motor) 
        {
            Log("ExitForwards_Internal");

            _localTime = duration;
                    
            if (execute)
            {
                Run(motor, true);
            }

            onExitForwards.Invoke(this as T0);
            
            Handle_Deactivation(motor, true);
            
            ExitedForwards(motor);
        }
        
        private void ExitedBackwards_Internal(T1 motor) 
        {
            Log("ExitBackwards_Internal");

            _localTime = 0;

            if (execute)
            {
                Run(motor, false);
            }

            onExitBackwards.Invoke(this as T0);
            
            Handle_Deactivation(motor, false);
            
            ExitedBackwards(motor);
        }
        
        // --- Active --- //

        private void BecameActive_Internal(T1 motor, bool timeDirection)
        {
            BecameActive(
                motor: motor,
                timeDirection: timeDirection);
        }
        
        private void BecameInactive_Internal(T1 motor, bool timeDirection) 
        {
            BecameInactive(
                motor: motor,
                timeDirection: timeDirection);
        }

        // --- Loop --- //

        private void LoopedAtEnd_Internal() 
        {
            LoopedAtEnd();
        }
        
        private void LoopedAtStart_Internal() 
        {
            LoopedAtStart();
        }
        
        
        // --- Bounce --- //
        
        private void BounceOffEnd_Internal(T1 motor, float delta)
        {
            BounceOffEnd(motor, delta);
        }
        
        private void BounceOffStart_Internal(T1 motor, float delta) 
        {
            BounceOffStart(motor, delta);
        }
        
        
        // -------------------------------------------------------------------------------------------- Inheritables //

        // --- Enters --- //

        protected abstract void EnteredForwards(T1 motor);
        protected abstract void EnteredBackwards(T1 motor);

        // --- Exits --- //

        protected abstract void ExitedForwards(T1 motor);
        protected abstract void ExitedBackwards(T1 motor);
        
        // --- Active --- //

        protected virtual void BecameActive(T1 motor, bool timeDirection) {}
        protected virtual void BecameInactive(T1 motor, bool timeDirection) {}

        // --- Loop --- //

        protected virtual void LoopedAtEnd() {}
        protected virtual void LoopedAtStart() {}
        
        // --- Bounce --- //
        
        protected virtual void BounceOffEnd(T1 motor, float delta) {}
        protected virtual void BounceOffStart(T1 motor, float delta) {}
        
        // ------------------------------------------------------------------------------------------------- Helpers //

        protected virtual void PassControlBackwards(T1 motor, float delta)
        {
            if (backwardsGears.Count < 0) return;

            Log("Passing control backwards!");

            foreach (var g in backwardsGears)
            {
                g.Handle_Enter_Backwards(motor, delta, this as T0);
            }
        }
        
        protected virtual void PassControlForwards(T1 motor, float delta)
        {
            if (forwardsGears.Count < 0) return;
            
            Log("Passing control forwards!");

            foreach (var g in forwardsGears)
            {
                g.Handle_Enter_Forwards(motor, delta, this as T0);
            }
        }

        private void Handle_Activation(T1 motor, bool timeDirection)
        {
            if (active) return;
            
            active = true;

            motor.GearActivated(this as T0);

            #if UNITY_EDITOR
            if (activateGameObjectWhenActive)
                gameObject.SetActive(true);

            if (selectGameObjectWhenActive)
                Selection.activeGameObject = gameObject;
            #endif

//            BecameActive_Internal(motor, timeDirection);
        }

        public void Handle_Deactivation(T1 motor, bool timeDirection)
        {
            if (!active) return;
            
            active = false;

            motor.GearDectivated(this as T0);
            
            #if UNITY_EDITOR
            if (activateGameObjectWhenActive)
                gameObject.SetActive(false);
            #endif

//            BecameInactive_Internal(motor, timeDirection);
        }

        private void ToggleTimeFlip()
        {
            localTimeFlip = !localTimeFlip;
        }
    }
}