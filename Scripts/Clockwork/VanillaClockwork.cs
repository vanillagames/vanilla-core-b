﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanilla.Sequencing;

namespace Vanilla.Clockwork
{
    public enum ClockworkFlowAction
    {
        Continue,
        StopLocal,
        StopGlobal,
        Skip,
        BounceLocal,
        BounceGlobal,
        Loop,
        RestartMotor
    }

    public class ClockworkEpoch : BasicEpoch
    {
            
    }
    
    public static class VanillaClockwork
    {

    }
}