﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanilla.Voxels;

namespace Vanilla.Voxels
{
    [Serializable]
    public class BinaryMatrix : VanillaMatrix<bool> 
    {
        protected override void InitializeNewMatrix()
        {
            
        }
    }
    
    [Serializable]
    public class BinaryTransformMatrix : TransformMatrix<bool, BinaryMatrix>
    {
        protected override Color GetGizmoColour(int x, int y, int z)
        {
            return matrix.matrix[x, y, z] ? Color.green : Color.red;
        }
    }
}