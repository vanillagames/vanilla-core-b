﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//
//using UnityEngine;
//
//namespace Vanilla.Voxels
//{
//    [Serializable]
//    public abstract class TransformMatrix<T> : ActiveBehaviour
//    {
//        [SerializeField] 
//        public ProtectedEventMatrix<T> matrix;
//
//        public float blockSize = 1.0f;
//
//        public override void Awake()
//        {
//            base.Awake();
//
//            Log(matrix.matrix.Length.ToString());
//        }
//
//        public void PaintByWorldPosition(Vector3 position, T value)
//        {
//            var localPosition = t.InverseTransformPoint(position) * blockSize;
//
//            var x = Mathf.RoundToInt(localPosition.x);
//            var y = Mathf.RoundToInt(localPosition.y);
//            var z = Mathf.RoundToInt(localPosition.z);
//
//            matrix.SafeSet(x, y, z, value);
//        }
//
//        private void OnDrawGizmos()
//        {
//            if (!Application.isPlaying || !debugFlags.HasFlag(DebugFlags.Gizmos)) return;
//
//            for (var z = 0; z < matrix.zSize; z++)
//            {
//                for (var y = 0; y < matrix.ySize; y++)
//                {
//                    for (var x = 0; x < matrix.xSize; x++)
//                    {
//                        Gizmos.color = GetGizmoColour(x, y, z);
//
//                        Gizmos.DrawCube(t.InverseTransformPoint(x, y, z), Vector3.one * blockSize);
//                    }
//                }
//            }
//        }
//
//        protected abstract Color GetGizmoColour(int x, int y, int z);
//    }
//}