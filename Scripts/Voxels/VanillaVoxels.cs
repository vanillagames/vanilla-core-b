﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
//using Vanilla.Core;
using Vanilla.Math;

namespace Vanilla.Voxels
{
    public interface IVoxel<T0,T1>
        where T0 : IVoxel<T0,T1>
        where T1: InterfaceMatrix<T0,T1>
    { 
        T1 matrix { get; set; }

        void Edit(T0 change);
    }
    
    [Serializable]
    public abstract class VanillaVoxel : VanillaClass
    {
        public override void Reset() {}
    }

    // ------------------------------------------------------------------------------------------------- Base Matrix //
    
    [Serializable]
    public abstract class VanillaMatrix<T> : VanillaClass
    {
        [SerializeField] 
        protected T[,,] _matrix;
        public T[,,] matrix
        {
            get
            {
                if (_matrix != null) return _matrix;

                _matrix = new T[_xSize,_ySize,_zSize];
                
//                for (var z = 0; z < zSize; z++)
//                {
//                    for (var y = 0; y < ySize; y++)
//                    {
//                        for (var x = 0; x < xSize; x++)
//                        {
//                            _matrix[x, y, z] = default;
//                        }
//                    }
//                }

                InitializeNewMatrix();
                
                return _matrix;
            }
            set => _matrix = value;
        }

        /// <summary>
        ///     This function is called immediately after a new matrix array has been generated.
        /// </summary>
        protected abstract void InitializeNewMatrix();

        [SerializeField]
        private int _xSize = 16;
        public virtual int xSize
        {
            get => _xSize;
            set => _xSize = value.GetClamp(1, 128);
        }

        [SerializeField]
        private int _ySize = 16;
        public virtual int ySize
        {
            get => _ySize;
            set => _ySize = value.GetClamp(1, 128);
        }
        
        [SerializeField]
        private int _zSize = 16;
        public virtual int zSize
        {
            get => _zSize;
            set => _zSize = value.GetClamp(1, 128);
        }

        public int? _volume;
        public int? volume => _volume ?? (_volume = _xSize * _ySize * _zSize);
        
        /// <summary>
        ///     SafeGet will check if the given position is within the matrix bounds before accessing it.
        ///
        ///     If the given position is invalid, it will return the default value for T.
        /// </summary>
        public virtual T SafeGet(int x, int y, int z)
        {
            return InvalidPosition(x, y, z) ? default : _matrix[x, y, z];
        }
        
        /// <summary>
        ///     SafeGet will check if the given position is within the matrix bounds before accessing it.
        ///
        ///     If the given position is invalid, it will return the default value for T.
        /// </summary>
        public virtual T SafeGet(Int3 p)
        {
            return InvalidPosition(p) ? default : _matrix[p.x, p.y, p.z];
        }

        /// <summary>
        ///     SafeSet will check if the given position is within the matrix bounds before accessing it.
        ///
        ///     If the given position is invalid, it won't do anything.
        /// </summary>
        public virtual void SafeSet(int x, int y, int z, T edit)
        {
            if (InvalidPosition(x, y, z)) return;

            Set(x, y, z, edit);
        }
        
        /// <summary>
        ///     SafeSet will check if the given position is within the matrix bounds before accessing it.
        ///
        ///     If the given position is invalid, it won't do anything.
        /// </summary>
        public virtual void SafeSet(Int3 p, T edit)
        {
            if (InvalidPosition(p)) return;

            Set(p, edit);
        }

        /// <summary>
        ///     Get will bypass the internal null-check on the given grid position, speeding things up a little bit.
        ///     As a result, be certain when using this that the position you're checking is guaranteed to not be null.
        /// </summary>
        public virtual T Get(int x, int y, int z)
        {
            return _matrix[x, y, z];
        }

        /// <summary>
        ///     Set will bypass the internal null-check on the given grid position, speeding things up a little bit.
        ///     As a result, be certain when using this that the position you're checking is guaranteed to not be null.
        /// </summary>
        protected virtual void Set(int x, int y, int z, T edit)
        {
            _matrix[x, y, z] = edit;
        }
        
        /// <summary>
        ///     Set will bypass the internal null-check on the given grid position, speeding things up a little bit.
        ///     As a result, be certain when using this that the position you're checking is guaranteed to not be null.
        /// </summary>
        protected virtual void Set(Int3 p, T edit)
        {
            _matrix[p.x, p.y, p.z] = edit;
        }

        public virtual bool InvalidPosition(int x, int y, int z)
        {
            if (x.IsWithinInclusiveRange(0, xSize - 1) &&
                y.IsWithinInclusiveRange(0, ySize - 1) &&
                z.IsWithinInclusiveRange(0, zSize - 1))
            { return false; }

            Warning($"Attempted access on this matrix at position [X {x}, Y {y}, Z {z}] was invalid.");

            return true;
        }
        
        public virtual bool InvalidPosition(Int3 p)
        {
            if (p.x.IsWithinInclusiveRange(0, xSize - 1) &&
                p.y.IsWithinInclusiveRange(0, ySize - 1) &&
                p.z.IsWithinInclusiveRange(0, zSize - 1))
            { return false; }

            Warning($"Attempted access on this matrix at position [X {p.x}, Y {p.y}, Z {p.z}] was invalid.");

            return true;
        }

        public override void Reset() {}
    }

    // --------------------------------------------------------------------------------------------------- Mutations //
    
    [Serializable]
    public abstract class EventMatrix<T> : VanillaMatrix<T>
    {
        public Action<int, int, int, T, T> onEdit;
        
        protected abstract override void InitializeNewMatrix();

        protected override void Set(int x, int y, int z, T to)
        {
            base.Set(x,y,z,to);
            
            onEdit.Invoke(x, y, z, _matrix[x,y,z], to);
        }
    }

    [Serializable]
    public abstract class ProtectedMatrix<T> : VanillaMatrix<T>
    {
        protected abstract override void InitializeNewMatrix();

        protected override void Set(int x, int y, int z, T edit)
        {
            if (Equals(_matrix[x, y, z], edit)) return;

            base.Set(x, y, z, edit);
        }
    }

    [Serializable]
    public abstract class ProtectedEventMatrix<T> : EventMatrix<T>
    {
        protected abstract override void InitializeNewMatrix();

        protected override void Set(int x, int y, int z, T edit)
        {
            if (Equals(_matrix[x, y, z], edit)) return;
        
            base.Set(x,y,z,edit);
        }
    }

    [Serializable]
    public abstract class InterfaceMatrix<T0,T1> : VanillaMatrix<T0>
        where T0 : IVoxel<T0,T1>
        where T1: InterfaceMatrix<T0,T1>
    {
        protected abstract override void InitializeNewMatrix();

        protected override void Set(int x, int y, int z, T0 edit)
        {
            edit.Edit(edit);
        }
    }
    
    // -------------------------------------------------------------------------------------------------- Base Brush //

    public abstract class MatrixBrush<T> : ActiveBehaviour
    {
        public T applicationValue;

        public abstract void BrushMoved();

        public virtual void Apply(int x, int y, int z, VanillaMatrix<T> matrix)
        {
            matrix.SafeSet(x, y, z, applicationValue);
        }

        public override void Reset() {}
    }
    
    // --------------------------------------------------------------------------------------------- Transform Brush //

    [Serializable]
    public abstract class TransformBrush<T> : ChangeOnlyTransform
    {
        public T value;

        protected abstract override void TransformMoved(Vector3 newPosition);

        protected virtual void Apply(int x, int y, int z, VanillaMatrix<T> matrix)
        {
            matrix.SafeSet(x,y,z,value);
        }
    }
    
    
    
    // --------------------------------------------------------------------------------------------------- Mutations //

    [Serializable]
    public class VoxelMatrix : ProtectedMatrix<VanillaVoxel>
    {
        public float blockSize = 0.5f;
        
        protected override void InitializeNewMatrix()
        {
            
        }
    }
    
    // -------------------------------------------------------------------------------------------- Transform Matrix //

    [Serializable]
    public abstract class TransformMatrix<T0, T1> : ActiveBehaviour
        where T1 : VanillaMatrix<T0>
    {
        [SerializeField] 
        public T1 matrix;

        public float blockSize = 1.0f;
        public float gizmoSize = 0.1f;

        public void Awake()
        {
            Log(matrix.matrix.Length.ToString());
        }

        public Vector3 WorldToFloatVoxelPosition(Vector3 position)
        {
            return (t.InverseTransformPoint(position) / blockSize).GetRoundToInt();
        }
        
        public Int3 WorldToIntVoxelPosition(Vector3 position)
        {
            return (t.InverseTransformPoint(position) / blockSize).ToInt3();
        }

        public Vector3 VoxelToWorldPosition(int x, int y, int z)
        {
            return t.TransformPoint(x * blockSize, y * blockSize, z * blockSize);
        }

        public virtual void PaintByWorldPosition(Vector3 position, T0 value)
        {
            var localPosition = t.InverseTransformPoint(position) / blockSize;

            var x = Mathf.RoundToInt(localPosition.x);
            var y = Mathf.RoundToInt(localPosition.y);
            var z = Mathf.RoundToInt(localPosition.z);
            
            matrix.SafeSet(x, y, z, value);
        }
        
        private void OnDrawGizmos()
        {
            if (!Application.isPlaying || !debugFlags.HasFlag(DebugFlags.Gizmos)) return;

            for (var z = 0; z < matrix.zSize; z++)
            {
                for (var y = 0; y < matrix.ySize; y++)
                {
                    for (var x = 0; x < matrix.xSize; x++)
                    {
                        Gizmos.color = GetGizmoColour(x, y, z);
                        
                        Gizmos.DrawCube(VoxelToWorldPosition(x,y,z), Vector3.one * gizmoSize);
                    }
                }
            }
        }

        protected abstract Color GetGizmoColour(int x, int y, int z);
    }
}
