﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vanilla.Voxels
{
    [Serializable]
    public class BinaryBrush : TransformBrush<bool>
    {
        [SerializeField]
        public BinaryTransformMatrix currentTransformMatrix;

        protected override void TransformMoved(Vector3 newPosition)
        {
            Log("I moved!");
            
            currentTransformMatrix.PaintByWorldPosition(newPosition, value);
        }
    }
}