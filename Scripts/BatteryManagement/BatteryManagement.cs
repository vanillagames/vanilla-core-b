// Creation Date                                                                              [ 16/03/2020 6:21:31 PM ]
// Project                                                                                                   [ Rewild ]
// Developer                                                                                                 [ PHORIA ]

using UnityEngine;
using UnityEngine.Events;

namespace Vanilla.BatteryManagement
{

	public class BatteryManagement : VanillaSingleton<BatteryManagement>
	{

		// -------------------------------------------------------------------------------------------- Update Loops //

		#region Update Loops



		private void CheckBattery()
		{
			lowPower = SystemInfo.batteryStatus == BatteryStatus.Discharging &&
			           SystemInfo.batteryLevel  < c_LowThreshold;
		}



		#endregion

		// ----------------------------------------------------------------------------------------------- Variables //

		#region Variables



		[SerializeField] [ReadOnly]
		private bool _lowPower;
		public bool lowPower
		{
			get => _lowPower;
			private set
			{
				if (_lowPower == value) return;

				_lowPower = value;

				if (_lowPower)
					onBatteryLevelRed?.Invoke();
				else
					onBatteryLevelGreen?.Invoke();
			}
		}

		public UnityAction onBatteryLevelRed;
		public UnityAction onBatteryLevelGreen;

		private const float c_CheckTimer = 10.0f;

		private const float c_LowThreshold = 0.25f;



		#endregion

		// ------------------------------------------------------------------------------------------ Initialization //

		#region Initialization



		private void OnEnable()
		{
			if (SystemInfo.batteryLevel < -0.5f || SystemInfo.batteryStatus == BatteryStatus.Unknown)

				// Apparently Unity returns -1.0f for level or BatteryStatus.Unknown if a platform doesn't
				// offer battery support, upon which we should nuke this script.

				Destroy(this);

			InvokeRepeating(methodName: nameof(CheckBattery),
			                time: c_CheckTimer,
			                repeatRate: c_CheckTimer);
		}


		private void OnDisable()
		{
			CancelInvoke(methodName: nameof(CheckBattery));
		}



		#endregion

	}

}