﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Vanilla.Math;

namespace Vanilla.UI
{

    /// <summary>
    ///     This struct is used to easily manipulate the alpha value of a CanvasRenderer UI component.
    ///
    ///     By passing in normals, the CanvasRendere will automatically activate itself as needed and
    ///     clamp its alpha value.
    /// </summary>
    [Serializable]
    public struct CanvasRendererFadeSlot
    {

        public CanvasRenderer renderer;

        public float from;
        public float to;


        public float GetFade
        (
            float n)
        {
            return Mathf.Lerp(a: from,
                              b: to,
                              t: n);
        }


        public void ApplyFade
        (
            float n)
        {
            renderer.SetAlpha(GetFade(n));
        }


        public void ToggleActiveState
        (
            float n)
        {
            renderer.gameObject.SetActive(!GetFade(n).IsRoughly(0.0f));
        }


        public CanvasRendererFadeSlot
        (
            CanvasRenderer renderer,
            float          from = 0.0f,
            float          to   = 1.0f)
        {
            this.renderer = renderer;
            this.from     = from;
            this.to       = to;
        }

    }
    
    [Serializable]
    public class VanillaScrollPanel : VanillaPool<VanillaScrollEntry, VanillaScrollPanel>
    {

//        public RectTransform header;
        public RectTransform scrollPanel;

//        public RectTransform resizer;

        [SerializeField]
        private float perElementHeight = -1.0f;

        public KeyCode testGetKey = KeyCode.A;


        [ContextMenu("Get")]
        public override VanillaScrollEntry Get()
        {
            cache = base.Get();

            if (HardNullCheck(cache)) return cache;

            if (perElementHeight.IsNegative())
            {
                perElementHeight = cache.GetComponent<RectTransform>().sizeDelta.y;
            }

            scrollPanel.sizeDelta += new Vector2(x: 0,
                                                 y: perElementHeight);

            scrollPanel.ForceUpdateRectTransforms();

            return cache;
        }


        [ContextMenu("Fill")]
        public void TestFill()
        {
            base.Fill();
        }


        [ContextMenu("Empty")]
        public void TestEmpty()
        {
            base.Empty();
        }


        [ContextMenu("Trim")]
        public void TestTrim()
        {
            base.Trim();
        }


        [ContextMenu("Get")]
        protected VanillaScrollEntry TestGet()
        {
            return Get();
        }


        [ContextMenu("Return")]
        protected void TestReturn()
        {
            if (used.Count > 0)
                Return(used[0]);
        }


        void Update()
        {
            if (Input.GetKeyDown(testGetKey))
            {
                Get();
            }
        }


        void Awake()
        {
            VanillaScrollEntry.currentEntry.onSlotChange.AddListener(HandleEntrySwap);
        }


        public void HandleEntrySwap
        (
            VanillaScrollEntry @old,
            VanillaScrollEntry @new)
        {
            if (!SilentNullCheck(@old))
            {
                @old.Collapse();
            }

            if (!SilentNullCheck(@new))
            {
                @new.Expand();
            }

            Canvas.ForceUpdateCanvases();
        }


//        void Update()
//        {
//            contentRect.ForceUpdateRectTransforms();
//        }

//        public void OnDrag(PointerEventData eventData)
//        {
//            Log("OnDrag");
//        }
//
//        public void OnBeginDrag(PointerEventData eventData)
//        {
//            Log("BeginDrag");
//        }
//
//        public void OnEndDrag(PointerEventData eventData)
//        {
//            Log("EndDrag");
//        }

        public override void Reset() { }

    }

}