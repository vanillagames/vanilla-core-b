﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.UI;

using Vanilla.Core.Inputs;

namespace Vanilla.UI
{
    [Serializable]
    public abstract class VanillaDialogue<T0, T1> : VanillaBehaviour
        where T0 : DialogueRoutine<T0,T1>
        where T1 : VanillaDialogue<T0,T1>
    {
        public BoolInputAction continueAction;
        
        [Header("Type Routine")]
        public T0 routine = default;

        protected virtual void Awake()
        {
            Initialize();
        }

        public virtual void OnEnable()
        {
            continueAction.slot.onSlotChange.AddListener(HandleContinuePressed);
        }

        public virtual void OnDisable()
        {
            continueAction.slot.onSlotChange.RemoveListener(HandleContinuePressed);
        }

        void Initialize()
        {
            routine.type = this as T1;

            routine.Initialize();
        }


        public virtual void HandleContinuePressed
        (
            bool @old,
            bool @new)
        {
            if (!@new) return;

            routine.Continue();
        }


        public override void Reset()
        {
            routine.Reset();
        }
    }
    
    [Serializable]
    public class DialogueRoutine<T0,T1> : VanillaManualRoutine
        where T0 : DialogueRoutine<T0,T1>
        where T1 : VanillaDialogue<T0,T1>
    {
        [Header("String Builder")] 
        public StringBuilder sb;
        
        [SerializeField] public int stringBuilderDefaultCapacity = 256;

        [Header("Control Flags")]
        public bool canSkip = true;

        [Header("Text Bank")]
        [ReadOnly]
        public List<string> textBank = new List<string>();
        
        [Header("Text Style")]
        [SerializeField]
        private Color _textColor = Color.white;
        public Color textColor
        {
            get => _textColor;
            set
            {
                UpdateTextColorTag(); 
            }
        }

        [Header("Constants")]
        [HideInInspector, ReadOnly] private string startColorTag;
        private const string endColorTag = "</color>";
        
        [Header("Target UI Elements")]
        public Text[] textElements = new Text[0];

        [Header("Debug")]
        [HideInInspector, ReadOnly] public T1 type;

        [HideInInspector, ReadOnly] protected string targetString;
        [HideInInspector, ReadOnly] protected string processedTargetString;
        
        [HideInInspector, ReadOnly] protected string _output;
        protected string output
        {
            get => _output;
            set
            {
                _output = value;
                
                foreach (var t in textElements)
                {
                    t.text = _output;
                }
            }
        }

        [HideInInspector, ReadOnly] protected int currentFrame;
        [HideInInspector, ReadOnly] protected int frameCount; // How many frames should we run?
        [HideInInspector, ReadOnly] protected int currentChar;
        
        // --- Per Character Wait --- //

        private WaitForSeconds _normalWait;

        [SerializeField]
        private float _perCharWaitLength = 0.025f;
        public float perCharWaitLength
        {
            get => _perCharWaitLength;
            set => UpdateTypeSpeed(value);
        }
        
        // --- End-of-sentence Wait --- //

        private WaitForSeconds _longWait;

        [Tooltip("When running a Pause, how long should it take?")]
        [SerializeField]
        private float _pauseSeconds = 0.25f;
        public float pauseSeconds
        {
            get => _pauseSeconds;
            set => UpdateLongPauseLength(value);
        }

        public char[] eventChars = new char[0];
        
        [HideInInspector] protected byte[] characterEvents = new byte[256];

        private const byte standardCharacterByte = 255;
        
        // ---------------------------------------------------------------------------------------------------- Init //

        public void Initialize()
        {
            sb = new StringBuilder(stringBuilderDefaultCapacity);

            characterEvents = new byte[stringBuilderDefaultCapacity];

            UpdateTextColorTag();

            UpdateTypeSpeed(_perCharWaitLength);
            UpdateLongPauseLength(_pauseSeconds);
        }
        
        // ------------------------------------------------------------------------------------------- Value Updates //
        
        public void UpdateTextColorTag()
        {
//            #if UNITY_EDITOR
            startColorTag = $"<color=#{ColorUtility.ToHtmlStringRGB(_textColor)}00>";
//            startColor = $"<color=#{ColorUtility.ToHtmlStringRGB(_textColor)}FF>";
//            #endif
        }

        private void UpdateTypeSpeed(float length)
        {
            _perCharWaitLength = length;

            _normalWait = new WaitForSeconds(length);
        }

        private void UpdateLongPauseLength(float length)
        {
            _pauseSeconds = length;
            
            _longWait = new WaitForSeconds(_pauseSeconds);
        }
        
        // ----------------------------------------------------------------------------------------------------- Go! //

        public void Begin()
        {
            if (textBank.Count < 1) return;
            
            sb.Clear(); // Clear the builder
            
            sb.Append(textBank[0]); // Chuck the input in so we can shuffle it around internally without any new string assignments

            textBank.RemoveAt(0);
            
            ProcessTargetString(); // Figure out how many frames to run, where the pauses are, etc

            sb.Append(endColorTag); // Stick the endColor invisible constant in at the end, since this bit never moves.

            processedTargetString = sb.ToString(); // Now that we have a sanitized string, we need to keep it 
                                                   // somewhere so it can be copied across each frame.
            
            currentFrame = 0;
            currentChar = 0;

            Start(type);
        }
        
        protected virtual void ProcessTargetString()
        {
            // Reset the event array
            characterEvents.SetAll(standardCharacterByte);
            
            int c; // We use c to iterate through the string, removing event characters and recording their positions.
            
            var eventCount = 0; // How many non-standard characters are present?
            
            for (c = 0; c < sb.Length; c++) // For each character in the target string (now found in the builder)
            {
                for (byte eventChar = 0; eventChar < eventChars.Length; eventChar++) // Does this character match this event character?
                {
                    if (sb[c] != eventChars[eventChar]) continue; // If not, shneh.

                    characterEvents[c+eventCount] = eventChar; // If so, record the type.
                    
                    // The reason we associate via byte instead of a character is that the characters can be
                    // switched out without affecting functionality if needed.

                    sb.Remove(startIndex: c--, 
                              length: 1); // Once processed, remove the event character so it isn't typed.

                    eventCount++;
                }
            }

            frameCount = c + eventCount;
        }

        [ContextMenu("Continue")]
        public void Continue()
        {
            if (isRunning)
            {
                if (canSkip)
                {
                    JumpToEnd();
                }
            }
            else if (textBank.Count > 0)
            {
                Begin();
            }
            else
            {
                TextBankEmpty();
            }
        }

        // --------------------------------------------------------------------------------------- Routine Callbacks //

        protected sealed override void Run()
        {
            if (!keepRunning) return;
            
            if (characterEvents[currentFrame] == standardCharacterByte)
            {
                HandleStandardCharacterFrame();
            }
            else
            {
                HandleEventCharacterFrame(characterEvents[currentFrame]);
            }

            currentFrame++;

            keepRunning = currentFrame < frameCount;
        }

        protected override void Ended()
        {
            
        }
        
        // ------------------------------------------------------------------------------------------------ Controls //

        public void JumpToEnd()
        {
            if (!isRunning) return;

            currentChar = frameCount - endColorTag.Length - 1; // This must be the last valid string position..?
            
            UpdateTextOutput();

            keepRunning = false; // What if this happens before Run()?
        }

        // ----------------------------------------------------------------------------------------------- Overrides //

        protected virtual void HandleStandardCharacterFrame()
        {
            #if UNITY_EDITOR
                Log($"Handling frame [{currentFrame}] - Standard character [{currentChar}] - [{sb[currentChar]}]");
            #endif
            
            currentChar++;
            
            UpdateTextOutput();

            endOfFrameAction = _normalWait;
        }

        protected virtual void HandleEventCharacterFrame(byte characterEvent)
        {
            #if UNITY_EDITOR
                Log($"Handling frame [{currentFrame}] - Event type [{characterEvent}]");
            #endif
            
            switch (characterEvent)
            {
                case 0:
                    HandlePauseFrame();
                    break;
            }
        }

        private void HandlePauseFrame()
        {
            endOfFrameAction = _longWait;
        }

        protected virtual void TextBankEmpty()
        {
            sb.Clear();

            ClearTextElements();
        }

        public void ClearTextElements()
        {
            foreach (var t in textElements)
            {
                t.text = string.Empty;
            }
        }
        
        // ------------------------------------------------------------------------------------------------- Helpers //

        public void UpdateTextOutput()
        {
            sb.Clear();

            sb.Append(processedTargetString);

            sb.Insert(index: currentChar,
                      value: startColorTag);
            
            output = sb.ToString();
        }

        public override void Reset()
        {
            Stop(type);

            sb.Clear();
            
            textBank.Clear();

            ClearTextElements();
        }
    }
}