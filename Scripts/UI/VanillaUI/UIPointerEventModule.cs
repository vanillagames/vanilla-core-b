﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Vanilla.UI
{
    [Serializable]
    public class VanillaPointerEvent : UnityEvent<PointerEventData, UIPointerEventModule> {}

    [Serializable]
    public abstract class UIPointerEventModule :
        VanillaUIBehaviour,
        IPointerDownHandler,
        IPointerUpHandler,
        IPointerEnterHandler,
        IPointerExitHandler,
        IPointerClickHandler
    {
        [ReadOnly]
        public bool pointerOverObject;
        
        public bool handlePointerDown = true;
        public bool handlePointerUp = true;
        public bool handlePointerEnter = true;
        public bool handlePointerExit = true;
        public bool handlePointerClick = true;

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!handlePointerDown) return;

            HandlePointerDown(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!handlePointerUp) return;
            
            HandlePointerUp(eventData);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!handlePointerEnter) return;
            
            pointerOverObject = true;

            HandlePointerEnter(eventData);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!handlePointerExit) return;
            
            pointerOverObject = false;

            HandlePointerExit(eventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!handlePointerClick) return;

            HandlePointerClick(eventData);
        }

        public abstract void HandlePointerDown(PointerEventData eventData);
        public abstract void HandlePointerUp(PointerEventData eventData);
        public abstract void HandlePointerEnter(PointerEventData eventData);
        public abstract void HandlePointerExit(PointerEventData eventData);
        public abstract void HandlePointerClick(PointerEventData eventData);
    }
}