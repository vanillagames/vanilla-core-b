﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using Vanilla.UI;

public class UICopyDimensionsTo : UIBehaviour
{
	[SerializeField]
	private RectTransform rect;
	public RectTransform target;

	protected void OnValidate()
	{
		rect = (RectTransform) transform;
	}


	protected override void OnRectTransformDimensionsChange()
	{
		base.OnRectTransformDimensionsChange();
		
		target.SetSizeWithCurrentAnchors(axis: RectTransform.Axis.Horizontal, 
		                                 size: rect.sizeDelta.x);
		
		target.SetSizeWithCurrentAnchors(axis: RectTransform.Axis.Vertical, 
		                                 size: rect.sizeDelta.y);

		target.position = rect.position;
	}

}