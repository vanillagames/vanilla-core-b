﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vanilla.UI
{
    [Serializable]
    public class VanillaConsole : VanillaUIWindow
    {
        public VanillaScrollPanel scrollPanel = new VanillaScrollPanel();

        [ContextMenu("Populate")]
        public void Populate()
        {
            Populate();
            
        }
    }
}