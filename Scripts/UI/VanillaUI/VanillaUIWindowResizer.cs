﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Vanilla.UI
{
    public class VanillaUIWindowResizer : VanillaUIWindowComponent, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public void OnBeginDrag(PointerEventData eventData)
        {
            window.BeginResizerDrag(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            window.ResizerDrag(eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            window.EndResizerDrag(eventData);
        }
    }
}