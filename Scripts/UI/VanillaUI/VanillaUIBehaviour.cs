﻿using System;

using UnityEngine;


namespace Vanilla.UI
{

    [Serializable]
    public abstract class VanillaUIBehaviour : VanillaBehaviour
    {

        private RectTransform _rect;
        public RectTransform rect =>
            _rect ?
                _rect :
                _rect = (RectTransform) transform;

    }

}