﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

using Vanilla;

namespace Vanilla.UI
{
    public class VanillaUIWindowHeader : VanillaUIWindowComponent, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
    {
        private float lastClick;

        public float doubleClickInterval = 0.5f;
        
        public void OnBeginDrag(PointerEventData eventData)
        {
            window.BeginHeaderDrag(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            window.HeaderDrag(eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            window.EndHeaderDrag(eventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (Time.time - lastClick < doubleClickInterval)
            {
                window.ToggleHeaderCollapse();

                lastClick = Time.time;

                // Add a second on so we can't accidentally activate a third toggle by mis-clicking!
                lastClick -= 1.0f;

                return;
            }

            lastClick = Time.time;
        }
    }
}