﻿using UnityEngine;
using Vanilla.Math;

namespace Vanilla.UI
{

    public class VanillaUIWindowShortcut : VanillaBehaviour
    {

        public GameObject windowGameObject;

        public KeyCode[] shortCutKeys;

        private float width1;
        private float width2;

        private float height1;
        private float height2;


        void OnEnable()
        {
            width1 = Screen.width * 0.333f;
            width2 = Screen.width * 0.666f;

            height1 = Screen.height * 0.333f;
            height2 = Screen.height * 0.666f;
        }


        void Update()
        {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            #if UNITY_STANDALONE
                    for (var i = 0; i < shortCutKeys.Length - 1; i++)
                    {
                        if (!Input.GetKey(shortCutKeys[i])) return;
                    }

                    if (!Input.GetKeyDown(shortCutKeys[shortCutKeys.Length - 1])) return;
            #elif UNITY_ANDROID || UNITY_IOS
            if (Input.touchCount < 3) return;

            var tPos = Input.GetTouch(0).position;

            if (tPos.x > width1 || tPos.y < height2) return;

            tPos = Input.GetTouch(1).position;

            if (!tPos.x.IsWithinInclusiveRange(min: width1,
                                               max: width2) || tPos.y > height1) return;

            tPos = Input.GetTouch(2).position;

            if (tPos.x > width2 || tPos.y < height2) return;
            #endif

            windowGameObject.SetActive(!windowGameObject.activeSelf);
            #endif
        }

    }

}