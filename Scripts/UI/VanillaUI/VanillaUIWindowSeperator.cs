﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Vanilla.UI
{

    public class VanillaUIWindowSeperator : VanillaUIWindowComponent,
                                            IBeginDragHandler,
                                            IDragHandler,
                                            IEndDragHandler
    {

        public void OnBeginDrag(PointerEventData eventData)
        {
            window.BeginSeperatorDrag(eventData);
        }


        public void OnDrag(PointerEventData eventData)
        {
            window.SeperatorDrag(eventData);
        }


        public void OnEndDrag(PointerEventData eventData)
        {
            window.EndSeperatorDrag(eventData);
        }

    }

}