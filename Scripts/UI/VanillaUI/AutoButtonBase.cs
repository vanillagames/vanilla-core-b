// Creation Date                                                                              [ 16/03/2020 3:21:30 PM ]
// Project                                                                                                   [ Rewild ]
// Developer                                                                                                 [ PHORIA ]


using UnityEngine.UI;

namespace Vanilla.UI
{

	public abstract class AutoButtonBase : VanillaBehaviour
	{

		// ---------------------------------------------------------------------------------------------- Validation //

		#region Validation
		
		public bool makeNonInteractiveOnClick = true;
		
		#endregion
		
		// ---------------------------------------------------------------------------------------------- Validation //

		#region Validation



		protected virtual void Awake()
		{
			GetComponent<Button>()?.onClick.AddListener(ButtonClicked_Internal);
		}
		
		
		
		#endregion

		// ----------------------------------------------------------------------------------------------- Protected //

		#region Protected



		private void ButtonClicked_Internal()
		{
			var button = GetComponent<Button>();

			if (makeNonInteractiveOnClick)
			{
				button.interactable = false;
			}
			
			ButtonClicked(button);
		}

		protected abstract void ButtonClicked(Button button);



		#endregion

	}

}