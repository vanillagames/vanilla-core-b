﻿using UnityEngine;
using UnityEngine.EventSystems;

using Vanilla.Math;

namespace Vanilla.UI
{

	public class VanillaUIWindow : VanillaUIBehaviour
	{

		[Header("Debug Stuff")]
		public const float minWidth = 200.0f;
		public const float minHeight = 160.0f;

		[SerializeField] [ReadOnly]
		private bool _collapsed;

		[SerializeField]
		private VanillaUIWindowContent _content;

		[SerializeField]
		private RectTransform _contentRect;

		[SerializeField]
		private VanillaUIWindowHeader _header;

		[SerializeField]
		private VanillaUIWindowResizer _resizer;

		[SerializeField]
		private VanillaUIWindowSeperator _seperator;

		public float extraContentHeight;

		[SerializeField] [ReadOnly]
		public float height;

		[SerializeField] [ReadOnly]
		public float width;

		public VanillaUIWindowHeader header =>
			_header != null ?
				_header :
				_header = GetComponentInChildren<VanillaUIWindowHeader>();

		public VanillaUIWindowResizer resizer =>
			_resizer != null ?
				_resizer :
				_resizer = GetComponentInChildren<VanillaUIWindowResizer>();

		public VanillaUIWindowSeperator seperator =>
			_seperator != null ?
				_seperator :
				_seperator = GetComponentInChildren<VanillaUIWindowSeperator>();

		public VanillaUIWindowContent content =>
			_content != null ?
				_content :
				_content = GetComponentInChildren<VanillaUIWindowContent>();

		public RectTransform contentRect =>
			_contentRect != null ?
				_contentRect :
				_contentRect = (RectTransform) _content.transform;

		public bool collapsed
		{
			get => _collapsed;
			set
			{
				if (_collapsed == value) return;

				_collapsed = value;

				var s = rect.sizeDelta;

				s.y = collapsed ?
					      32 :
					      height;

				rect.sizeDelta = s;

				ClampWindowPosition();
			}
		}


		private void Awake()
		{
			width  = rect.sizeDelta.x;
			height = rect.sizeDelta.y;
		}


		// Header collapse


		public void ToggleHeaderCollapse()
		{
			collapsed = !collapsed;
		}


		// Header drag


		public void BeginHeaderDrag(PointerEventData eventData)
		{
			ClampWindowPosition();
		}


		public void HeaderDrag(PointerEventData eventData)
		{
			rect.anchoredPosition += eventData.delta;

			ClampWindowPosition();
		}


		public void EndHeaderDrag(PointerEventData eventData)
		{
			ClampWindowPosition();
		}


		public void ClampWindowPosition()
		{
			var p = rect.anchoredPosition;

			rect.anchoredPosition = new Vector2(x: p.x.GetClamp(min: 0,
			                                                    max: Screen.width - rect.sizeDelta.x),
			                                    y: p.y.GetClamp(min: -( Screen.height - rect.sizeDelta.y ),
			                                                    max: 0));
		}


		// Resizer drag


		public void BeginResizerDrag(PointerEventData eventData)
		{
			ResizeByDelta(eventData.delta);

			ClampWindowPosition();
		}


		public void ResizerDrag(PointerEventData eventData)
		{
			ResizeByDelta(eventData.delta);

			ClampWindowPosition();
		}


		public void EndResizerDrag(PointerEventData eventData)
		{
			ResizeByDelta(eventData.delta);

			ClampWindowPosition();
		}


		public void ResizeByDelta(Vector2 delta)
		{
			var s = rect.sizeDelta;

			s.x = ( s.x + delta.x ).GetClamp(min: minWidth,
			                                 max: Screen.width);

			s.y = ( s.y - delta.y ).GetClamp(min: minHeight,
			                                 max: Screen.height);

			width  = s.x;
			height = s.y;

			rect.sizeDelta = s;
		}


		// Seperator drag

		public void BeginSeperatorDrag(PointerEventData eventData) { }

		public void SeperatorDrag(PointerEventData eventData) { }

		public void EndSeperatorDrag(PointerEventData eventData) { }

		// Content scroll


		public void ContentPointerEnter()
		{
			content.scroll.enabled = true;
		}


		public void ContentPointerExit()
		{
			content.scroll.enabled = false;
		}

	}

}