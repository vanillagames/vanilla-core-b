﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Vanilla.UI
{
    public class VanillaUIWindowContent : VanillaUIWindowComponent, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private VanillaUIWindowContentScroll _scroll;
        public VanillaUIWindowContentScroll scroll => _scroll != null ? _scroll : (_scroll = GetComponentInChildren<VanillaUIWindowContentScroll>());

        void Awake()
        {
            enabled = true;
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            scroll.enabled = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            scroll.enabled = false;
        }
    }
}