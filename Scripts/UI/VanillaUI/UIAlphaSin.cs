﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vanilla;

public class UIAlphaSin : VanillaBehaviour
{

	[SerializeField, ReadOnly]
	private CanvasRenderer _renderer;
	
	public VanillaSinModule sin;

	protected override void InSceneValidation() => _renderer = GetComponent<CanvasRenderer>();

	void Update() => _renderer.SetAlpha(alpha: sin.Update());

}