﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vanilla;
using Vanilla.Math;
using Vanilla.UI;

public class VanillaUIWindowContentScroll : VanillaUIWindowComponent
{
    public float scrollStrength = 1.0f;

    void Update()
    {
        UpdateContentHeight();
            
        UpdateContentPosition();        
    }

    public void UpdateContentHeight()
    {
        Vector2 s = rect.sizeDelta;

        s.y = 0;
        
        foreach (Transform t in rect.GetImmediateChildren())
        {
            s.y += ((RectTransform) t).sizeDelta.y;
        }

        rect.sizeDelta = s;
    }

    public void UpdateContentPosition()
    {
        Vector2 p = rect.anchoredPosition;

        // This is the height of the entire window, minus the header and footer.
        // Or in other words, the current visible height of the content rect.
        float contentView = window.height - 32 - 32;

        // If the contents height (determined just before this) is less than what can actually be seen...
        // Don't allow scroll, set position manually to 0 and back out.
        if (rect.sizeDelta.y < contentView)
        {
            p.y = 0;

            rect.anchoredPosition = p;

            return;
        }
        
        
        
        p.y -= Input.GetAxis("Mouse ScrollWheel") * scrollStrength;

        float offScreenAmount = rect.sizeDelta.y - contentView;
        
        p.y = p.y.GetClamp(-offScreenAmount, 0);

        rect.anchoredPosition = p;
    }
    
    public void ClampContentHeight()
    {
        
    }
}