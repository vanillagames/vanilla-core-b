﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Vanilla.Math;
using Random = UnityEngine.Random;

namespace Vanilla.UI
{
    public class VanillaScrollEntry : VanillaUIBehaviour, IPoolable<VanillaScrollEntry, VanillaScrollPanel>, IPointerClickHandler
    {
        public static VanillaScrollPanel _pool;
        public VanillaScrollPanel pool
        {
            get => _pool;
            set => _pool = value;
        }

        private static float _fontPixelHeight = -1.0f;
        private float fontPixelHeight => _fontPixelHeight.IsPositive() ? _fontPixelHeight : _fontPixelHeight = text.font.lineHeight;

        [SerializeField]
        private float _padding = -1.0f;
        public float padding
        {
            get => _padding.IsPositive() ? _padding : _padding = Mathf.Abs(text.rectTransform.sizeDelta.y);
        }
        
        public Text text;

        public static ProtectedSlot<VanillaScrollEntry> currentEntry = new ProtectedSlot<VanillaScrollEntry>();

        public bool collapsed;

        public Image collapsedIcon;
        
        public void OnGet()
        {
            text.text = string.Empty;
            
            while (Random.value < 0.95f)
            {
                text.text += "Blah ";
            }
            
            Collapse();
        }

        [ContextMenu("Collapse")]
        public void Collapse()
        {
            (transform as RectTransform).SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, text.font.lineHeight + padding);

//            Canvas.ForceUpdateCanvases();

            collapsed = true;
        }

        [ContextMenu("Expand")]
        public void Expand()
        {
            Canvas.ForceUpdateCanvases();

//            int length = 0;
            
//            for (int i = 0; i < text.cachedTextGenerator.lines.Count; i++)
//            {
//                int startIndex = text.cachedTextGenerator.lines[i].startCharIdx;
//                int endIndex = (i == text.cachedTextGenerator.lines.Count - 1)
//                    ? text.text.Length
//                    : text.cachedTextGenerator.lines[i + 1].startCharIdx;
//                length = endIndex - startIndex;
//            }

            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, text.preferredHeight + padding);

            collapsed = false;
        }

        public void OnReturn()
        {
            
        }

        public void OnPointerClick(PointerEventData eventData)
        {
//            currentEntry.current = this;
            
            if (collapsed)
            {
                Expand();
            }
            else
            {
                Collapse();
            }
        }
    }
}