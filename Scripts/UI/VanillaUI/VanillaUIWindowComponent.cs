﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vanilla.UI
{
    public class VanillaUIWindowComponent : VanillaBehaviour
    {
        private RectTransform _rect;
        public RectTransform rect => _rect != null ? _rect : (_rect = (RectTransform) transform);
        
        
        private VanillaUIWindow _window;
        public VanillaUIWindow window => _window != null ? _window : (_window = GetComponentInParent<VanillaUIWindow>());
    }
}