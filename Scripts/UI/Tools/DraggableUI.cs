﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

using Vanilla.Math;
using Vanilla.UI.Tools;

namespace Vanilla.UI
{

    public class DraggableUI : VanillaUIBehaviour
    {

        [SerializeField, ReadOnly]
        public int touchIndex = -1;

        [SerializeField]
        private ImagePointerEventModule _imagePointerEventModule;

        public ImagePointerEventModule imagePointerEventModule =>
            _imagePointerEventModule != null ?
                _imagePointerEventModule :
                _imagePointerEventModule = GetComponentInChildren<ImagePointerEventModule>();

        public UnityEvent onDragBegun = new UnityEvent();
        public UnityEvent onDragEnd   = new UnityEvent();


        public void Awake()
        {
            if (!imagePointerEventModule)
            {
                Error(message: "I don't have an ImagePointerEvents script assigned. " +
                               "This is used to drive the dragging events; please attach a VanillaImagePointerEvents component to the image GameObject you want to control this script with.");

                return;
            }

            imagePointerEventModule.handlePointerEnter = false;
            imagePointerEventModule.handlePointerExit  = false;
            imagePointerEventModule.handlePointerClick = false;

            imagePointerEventModule.onPointerDown.AddListener(HandlePointerDown);
            imagePointerEventModule.onPointerUp.AddListener(HandlePointerUp);

            enabled = false;
        }


        public void HandlePointerDown
        (
            PointerEventData     eventData,
            UIPointerEventModule pointerBehaviour)
        {
            #if UNITY_EDITOR

            // These are -1, -2 and -3 respectively for the mouse buttons? Why negative? Switch handling..?
            eventData.pointerId = eventData.pointerId.GetPositive();
            #endif

            // This ensures another pointer can't steal it while its being dragged.
            if (touchIndex != -1) return;

            // Cache the pointer index.
            touchIndex = eventData.pointerId;

            // Turn on so we can run Update()
            Enable();
        }


        public void HandlePointerUp
        (
            PointerEventData     eventData,
            UIPointerEventModule pointerBehaviour)
        {
            // If this pointer isn't OUR pointer, ignore it.
            if (eventData.pointerId != touchIndex) return;

            // Reset our pointer index
            touchIndex = -1;

            // Turn off so we no longer run Update()
            Disable();
        }


        void Update()
        {
            #if UNITY_EDITOR
            if (Input.GetMouseButton(0))
            {
                rect.position = Input.mousePosition;
            }
            else if (Input.GetMouseButton(1))
            {
                rect.position = Input.mousePosition;
            }
            else if (Input.GetMouseButton(2))
            {
                rect.position = Input.mousePosition;
            }
            #else
                rect.position += Input.GetTouch(touchIndex).deltaPosition.XYToXY(0);
            #endif
        }

    }

}