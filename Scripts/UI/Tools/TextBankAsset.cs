﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Vanilla.Core;

namespace Vanilla.UI
{
	#if UNITY_EDITOR
	[CreateAssetMenu(menuName = Constants.VanillaSubPath +
	                            Constants.CoreSubPath    +
	                            Constants.UISubPath 	 +
	                            "Text Bank Asset",
		fileName = "New Text Bank Asset")]
	#endif
	public class TextBankAsset : VanillaScriptable
	{

		public string[] entries;

		protected override void OnValidate()
		{
			base.OnValidate();

			if (entries.Length > 0)
			{
				NameSelfByType(entries[0]);
			}
		}

	}

}