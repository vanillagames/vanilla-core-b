﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using UnityEngine.UI;

namespace Vanilla.UI.Tools
{
    public class FollowMouse : VanillaUIBehaviour
    {
        void Update()
        {
            rect.position = Input.mousePosition;
        }
    }
}