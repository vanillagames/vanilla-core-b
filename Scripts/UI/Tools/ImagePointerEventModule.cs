﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using UnityEngine.EventSystems;

namespace Vanilla.UI.Tools
{
    [Serializable]
    public class ImagePointerEventModule : UIPointerEventModule
    {
        [SerializeField] public VanillaPointerEvent _onPointerDown;
        public VanillaPointerEvent onPointerDown => _onPointerDown ?? (_onPointerDown = new VanillaPointerEvent());

        [SerializeField] public VanillaPointerEvent _onPointerUp;
        public VanillaPointerEvent onPointerUp => _onPointerUp ?? (_onPointerUp = new VanillaPointerEvent());

        [SerializeField] public VanillaPointerEvent _onPointerEnter;
        public VanillaPointerEvent onPointerEnter => _onPointerEnter ?? (_onPointerEnter = new VanillaPointerEvent());

        [SerializeField] public VanillaPointerEvent _onPointerExit;
        public VanillaPointerEvent onPointerExit => _onPointerExit ?? (_onPointerExit = new VanillaPointerEvent());

        [SerializeField] public VanillaPointerEvent _onPointerClick;
        public VanillaPointerEvent onPointerClick => _onPointerClick ?? (_onPointerClick = new VanillaPointerEvent());

        public override void HandlePointerDown(PointerEventData eventData)
        {
            #if UNITY_EDITOR
                Log($"Pointer Event [{eventData.pointerId}] [Down]");
            #endif
            
            onPointerDown.Invoke(
                arg0: eventData,
                arg1: this);
        }

        public override void HandlePointerUp(PointerEventData eventData)
        {
            #if UNITY_EDITOR
                Log($"Pointer Event [{eventData.pointerId}] [Up]");
            #endif
            
            onPointerUp.Invoke(
                arg0: eventData,
                arg1: this);
        }

        public override void HandlePointerEnter(PointerEventData eventData)
        {
            #if UNITY_EDITOR
                Log($"Pointer Event [{eventData.pointerId}] [Enter]");
            #endif
            
            onPointerEnter.Invoke(
                arg0: eventData,
                arg1: this);
        }

        public override void HandlePointerExit(PointerEventData eventData)
        {
            #if UNITY_EDITOR
                Log($"Pointer Event [{eventData.pointerId}] [Exit]");
            #endif
            
            onPointerExit.Invoke(
                arg0: eventData,
                arg1: this);
        }

        public override void HandlePointerClick(PointerEventData eventData)
        {
            #if UNITY_EDITOR
                Log($"Pointer Event [{eventData.pointerId}] [Click]");
            #endif
            
            onPointerClick.Invoke(
                arg0: eventData,
                arg1: this);
        }
    }
}