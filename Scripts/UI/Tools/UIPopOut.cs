﻿using UnityEngine;

namespace Vanilla.UI
{

	public class UIPopOut : VanillaUIBehaviour
	{

		[SerializeField] [ReadOnly]
		private Vector2 hidden;

		[SerializeField] [ReadOnly]
		private Vector2 origin;

		public Axis2D   popoutAxis      = Axis2D.Y;
		public Polarity popoutDirection = Polarity.Positive;


		protected virtual void Awake()
		{
			origin = rect.anchoredPosition;

			switch (popoutAxis)
			{
				case Axis2D.X:

					switch (popoutDirection)
					{
						case Polarity.Positive:

							hidden = new Vector2(x: origin.x + rect.sizeDelta.x,
							                     y: origin.y);

							break;

						case Polarity.Negative:

							hidden = new Vector2(x: origin.x - rect.sizeDelta.x,
							                     y: origin.y);

							break;
					}

					break;

				case Axis2D.Y:

					switch (popoutDirection)
					{
						case Polarity.Positive:

							hidden = new Vector2(x: origin.x,
							                     y: origin.y + rect.sizeDelta.y);

							break;

						case Polarity.Negative:

							hidden = new Vector2(x: origin.x,
							                     y: origin.y - rect.sizeDelta.y);

							break;
					}

					break;
			}
		}


		public void PopOutFrame
		(
			float n)
		{
			rect.anchoredPosition = Vector2.Lerp(a: origin,
			                                     b: hidden,
			                                     t: n);
		}

	}

}